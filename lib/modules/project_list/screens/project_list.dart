import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/project_card.dart';
import 'package:simpk/components/search_bar.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/project_dashboard_page.dart';
import 'package:simpk/modules/project_list/models/project_data.dart';
import 'package:simpk/services/api_provider.dart';

class ProjectList extends StatefulWidget {
  const ProjectList({
    this.saveOffline = false,
    this.partnerId,
    Key? key,
    this.targetPage,
  }) : super(key: key);

  final int? partnerId;
  final bool saveOffline;
  final Widget Function(Project project)? targetPage;

  @override
  _ProjectListState createState() => _ProjectListState();
}

class _ProjectListState extends State<ProjectList> {
  APIProvider api = GetIt.I();
  List<Project> projectList = [];
  bool _isLoading = false;
  final TextEditingController _searchController = new TextEditingController();
  String _searchValue = "";
  Map<String, dynamic> _filterParam = {};
  final PagingController<int, Project> _pagingController = PagingController(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );
  int _maxPage = 5;

  @override
  void initState() {
    super.initState();
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    fetchSearchResult();
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String title;
    if (_searchValue.isNotEmpty || _filterParam.isNotEmpty) {
      title = "Hasil Pencarian";
    } else if (widget.partnerId != null) {
      title = "Daftar Proyek yang Ditugaskan";
    } else {
      title = "Daftar Proyek";
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SearchBar(
            label: "Cari Proyek",
            controller: _searchController,
            onSubmit: (value) {
              setState(() {
                _searchValue = value;
                _pagingController.refresh();
              });
            },
          ),
          SizedBox(height: 40),
          Text(title, style: Theme.of(context).textTheme.headline3),
          SizedBox(height: 16),
          _buildPaginatorListView(),
        ],
      ),
    );
  }

  void setProjects(List<Project> newProjectList) {
    setState(() {
      projectList = newProjectList;
    });
  }

  Widget getTargetPage(Project project) {
    if (widget.targetPage != null) {
      return widget.targetPage!(project);
    }

    return ProjectDashboardPage(project: project);
  }

  Future<void> fetchProjects() async {
    _toggleLoading();

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("token")!;

    var projects = await api.getAllProjects(token);
    setProjects(projects);

    _toggleLoading();
  }

  Future<void> fetchSearchResult() async {
    if (_filterParam.isEmpty &&
        _searchValue.isEmpty &&
        widget.partnerId == null) {
      return fetchProjects();
    }

    _toggleLoading();

    SharedPreferences pref = await SharedPreferences.getInstance();

    String token = pref.getString("token")!;

    final filter = widget.partnerId != null
        ? {..._filterParam, 'mitra': widget.partnerId.toString()}
        : _filterParam;
    var projects = await api.searchProjects(token, _searchValue, filter);
    setProjects(projects);

    _toggleLoading();
  }

  void _toggleLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  List<Project> projectsMapToList(ProjectData projectData) {
    List<Project> projects = [];
    projectData.reports!.forEach((reportJson) {
      Project project = Project.fromJson(reportJson as Map<String, dynamic>);
      projects.add(project);
    });
    return projects;
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      ProjectData projectData;
      if (_filterParam.isEmpty &&
          _searchValue.isEmpty &&
          widget.partnerId == null) {
        projectData = await api.getProjects(pageKey, _searchValue);
      } else {
        final filter = widget.partnerId != null
            ? {..._filterParam, 'mitra': widget.partnerId.toString()}
            : _filterParam;
        projectData = await api.getProjects(pageKey, _searchValue, filter);
      }

      List<Project> newItems = projectsMapToList(projectData);
      final isLastPage = pageKey == _maxPage;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + 1;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Widget _buildPaginatorListView() {
    return RefreshIndicator(
      onRefresh: () async {
        await Future.sync(() => _pagingController.refresh());
      },
      child: PagedListView<int, Project>(
        pagingController: _pagingController,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        builderDelegate: PagedChildBuilderDelegate<Project>(
          animateTransitions: true,
          transitionDuration: Duration(milliseconds: 500),
          itemBuilder: (context, project, index) => ProjectCard(
            projectTitle: project.name,
            description: project.description,
            location: project.region,
            progressValue: project.progress,
            targetPage: getTargetPage(project),
          ),
          noItemsFoundIndicatorBuilder: (_) => Center(
            child: Text(
              "Hasil pencarian tidak ditemukan",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ),
      ),
    );
  }
}
