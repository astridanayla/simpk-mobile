import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simpk/components/bottom_navbar.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/blocs/auth_state.dart';
import 'package:simpk/modules/offline_report/screens/manage_offline_report.dart';
import 'package:simpk/modules/project_list/screens/project_list.dart';
import 'package:simpk/modules/user_profile/screens/user_profile.dart';

class ProjectPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) => Scaffold(
        appBar: MainAppbar(
          preferredSize: Size.fromHeight(112),
          profileColor: Theme.of(context).disabledColor,
          wifiColor: Theme.of(context).disabledColor,
          wifiOnPressed: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ManageOfflineReport(),
              ),
            );
          },
          profileOnPressed: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    UserProfile(),
              ),
            );
          },
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(16, 20, 16, 40),
          child: ProjectList(saveOffline: false),
        ),
        bottomNavigationBar: BottomNavbar(
          currentPageIndex: state.isRoleInternal() ? 3 : 2,
        ),
      ),
    );
  }
}
