import 'package:flutter/material.dart';
import 'package:simpk/components/basic_appbar.dart';
import 'package:simpk/components/custom_dropdown.dart';
import 'package:simpk/components/labeled_checkbox.dart';
import 'package:simpk/modules/monitoring/models/select_choice.dart';

class FilterProjectPage extends StatefulWidget {
  FilterProjectPage({this.filterParam = const <String, dynamic>{}});

  final Map<String, dynamic> filterParam;

  @override
  _FilterProjectPageState createState() => _FilterProjectPageState();
}

class _FilterProjectPageState extends State<FilterProjectPage> {
  late List<String> activityList;
  List<String> regionList = <String>["Indonesia", "DKI Jakarta"];
  Map<String, bool> statusList = {
    "Selesai": false,
    "Berjalan": false,
    "Belum Mulai": false,
  };
  String _selectedActivityType = '---';
  RangeValues _percentageRange = const RangeValues(0, 100);
  late TextEditingController _locationController;

  static const _keyStatus = 'status';
  static const _keyLocation = 'daerah';
  static const _keyActivityType = 'jenis_kegiatan';
  static const _keyPercentage = 'persentase';
  static const _statusCodes = {
    "Selesai": 'selesai',
    "Berjalan": 'berjalan',
    "Belum Mulai": 'belum_mulai',
  };

  @override
  void initState() {
    super.initState();

    activityList = ['---'];
    activityList.addAll(SelectChoice.getTexts(activityTypeList));

    if (widget.filterParam.containsKey(_keyStatus)) {
      final tempList = widget.filterParam[_keyStatus] as List<String>;
      _statusCodes.forEach((key, value) {
        statusList[key] = tempList.contains(value);
      });
    }

    _locationController = TextEditingController(
        text: widget.filterParam[_keyLocation] as String?);

    if (widget.filterParam.containsKey(_keyActivityType)) {
      final activityTypeCode = widget.filterParam[_keyActivityType] as String;
      _selectedActivityType =
          SelectChoice.getText(activityTypeList, activityTypeCode) ?? '---';
    }

    if (widget.filterParam.containsKey(_keyPercentage)) {
      final tempRange = widget.filterParam[_keyPercentage] as String;
      final rangeSplit = tempRange.split('-').map((e) => int.parse(e)).toList();
      _percentageRange = RangeValues(
        rangeSplit[0].toDouble(),
        rangeSplit[1].toDouble(),
      );
    }
  }

  Wrap _checkboxLayout(
      Map<String, bool> dataMap, double spacing, double height) {
    List<Widget> rowChildren = [];
    for (var idx = 0; idx < dataMap.length; idx++) {
      Widget labeledCheckbox = LabeledCheckbox(
          label: dataMap.keys.elementAt(idx),
          value: dataMap[dataMap.keys.elementAt(idx)]!,
          onChanged: (bool newValue) {
            setState(() {
              dataMap[dataMap.keys.elementAt(idx)] = newValue;
            });
          });
      rowChildren.add(labeledCheckbox);
    }
    return Wrap(
        alignment: WrapAlignment.start,
        spacing: spacing,
        runSpacing: height,
        direction: Axis.horizontal,
        children: rowChildren);
  }

  void _applyFilter(BuildContext context) {
    final filterParam = <String, dynamic>{};

    // status
    final countStatus = statusList.values.any((element) => element);
    if (countStatus) {
      final statuses = <String>[];
      statusList.forEach((key, value) {
        if (value) statuses.add(_statusCodes[key] as String);
      });
      filterParam[_keyStatus] = statuses;
    }

    // daerah
    if (_locationController.text.isNotEmpty) {
      filterParam[_keyLocation] = _locationController.text;
    }

    // jenis_kegiatan
    final activityTypeCode =
        SelectChoice.getCode(activityTypeList, _selectedActivityType);
    if (activityTypeCode != null) {
      filterParam[_keyActivityType] = activityTypeCode;
    }

    // percentage
    final percentMin = _percentageRange.start.round();
    final percentMax = _percentageRange.end.round();
    if (percentMin != 0 || percentMax != 100) {
      filterParam[_keyPercentage] = '$percentMin-$percentMax';
    }

    Navigator.pop(context, filterParam);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BasicAppbar("Filter"),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(16, 32, 16, 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Status Proyek", style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 20),
            _checkboxLayout(statusList, 24, 16),
            SizedBox(height: 36),
            Text("Lokasi Proyek", style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 18),
            TextFormField(
              controller: _locationController,
              decoration: InputDecoration(
                labelText: 'Lokasi',
              ),
              style: Theme.of(context).textTheme.bodyText1,
            ),
            SizedBox(height: 24),
            Text("Jenis Kegiatan",
                style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 18),
            CustomDropdown(
                label: "Kegiatan",
                items: activityList,
                value: _selectedActivityType,
                onChanged: (value) {
                  setState(() {
                    _selectedActivityType = value!;
                  });
                }),
            SizedBox(height: 32),
            Text("Persentase Proyek",
                style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 20),
            _buildRangeSlider(context),
            SizedBox(height: 56),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.symmetric(vertical: 40, horizontal: 16),
        child: ElevatedButton(
            child: Text("Terapkan Filter"),
            onPressed: () {
              _applyFilter(context);
            }),
      ),
    );
  }

  Widget _buildRangeSlider(BuildContext context) {
    return RangeSlider(
      values: _percentageRange,
      min: 0,
      max: 100,
      divisions: 10,
      labels: RangeLabels(
        _percentageRange.start.round().toString(),
        _percentageRange.end.round().toString(),
      ),
      onChanged: (values) {
        setState(() {
          _percentageRange = values;
        });
      },
    );
  }
}
