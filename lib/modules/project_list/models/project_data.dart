import 'dart:convert' as convert;
import 'package:http/http.dart';

class ProjectData {
  ProjectData({
    this.reports,
    this.statusCode,
    this.total,
    this.nItems,
    this.errorMessage,
  });

  factory ProjectData.fromResponse(Response response) {
    List jsonData = convert.jsonDecode(response.body) as List;
    return ProjectData(
      reports: jsonData,
      statusCode: response.statusCode,
      nItems: jsonData.length,
    );
  }

  factory ProjectData.withError(String errorMessage) {
    return ProjectData(errorMessage: errorMessage);
  }

  List<dynamic>? reports;
  int? statusCode;
  String? errorMessage;
  int? total;
  int? nItems;
}
