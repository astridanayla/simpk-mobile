import 'dart:convert' as convert;
import 'dart:io';

import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
    defaultValue: 'https://simpk-staging.herokuapp.com');

class HomeAPIProvider {
  HomeAPIProvider({required this.client});
  final Client client;

  Future<Map<String, String>> _getAuthHeader() async {
    final pref = await SharedPreferences.getInstance();
    final token = await pref.getString('token');
    if (token == null) {
      throw ErrorNoLoginToken();
    }
    final header = {HttpHeaders.authorizationHeader: 'Bearer ' + token};

    return header;
  }

  Uri _getUri(String path, [Map<String, dynamic>? queryParameters]) {
    final urlSplit = _baseUrl.split('://');
    if (urlSplit[0] == 'https') {
      return Uri.https(urlSplit.last, path, queryParameters);
    } else {
      return Uri.http(urlSplit.last, path, queryParameters);
    }
  }

  Future<Map<String, dynamic>> getProjectStatusCounts() async {
    Uri getProjectStatusCountsURI = _getUri(
      'monitoring/proyek',
      {"status_count": "true"},
    );

    Response response = await client.get(
      getProjectStatusCountsURI,
      headers: await _getAuthHeader(),
    );

    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;

    return jsonResponse;
  }

  Future<Map<String, dynamic>> getUniqueRegions() async {
    Uri getProjectStatusCountsURI = _getUri(
      'monitoring/proyek',
      {"unique_regions": "true"},
    );

    Response response = await client.get(
      getProjectStatusCountsURI,
      headers: await _getAuthHeader(),
    );

    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;

    return jsonResponse;
  }

  Future<Map<String, dynamic>> getProjectStatusCountsByRegion(
      String region) async {
    Uri getProjectStatusCountsURI = _getUri(
      'monitoring/proyek',
      {
        "status_count": "true",
        "region": region,
      },
    );

    Response response = await client.get(
      getProjectStatusCountsURI,
      headers: await _getAuthHeader(),
    );

    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;

    return jsonResponse;
  }
}

class ErrorNoLoginToken extends Error {}
