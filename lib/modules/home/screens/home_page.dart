import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/bottom_navbar.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/components/project_card.dart';
import 'package:simpk/components/search_bar.dart';
import 'package:simpk/modules/home/screens/monev_summary.dart';
import 'package:simpk/modules/offline_report/screens/manage_offline_report.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/project_dashboard_page.dart';
import 'package:simpk/modules/user_profile/screens/user_profile.dart';
import 'package:simpk/services/api_provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final APIProvider api = GetIt.I();
  final TextEditingController _searchController = new TextEditingController();
  late SharedPreferences sharedPreferences;

  bool _isSearchLoading = false;
  List<Project> projectResultList = [];
  String? _searchValue = "";

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: MainAppbar(
          preferredSize: Size.fromHeight(112),
          profileColor: Theme.of(context).disabledColor,
          wifiColor: Theme.of(context).disabledColor,
          wifiOnPressed: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ManageOfflineReport(),
              ),
            );
          },
          profileOnPressed: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    UserProfile(),
              ),
            );
          },
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(16, 20, 16, 40),
          child: _buildHomepage(context),
        ),
        bottomNavigationBar: BottomNavbar(currentPageIndex: 0),
      ),
    );
  }

  Widget _buildHomepage(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildSearchBar(),
        SizedBox(height: 40),
        _buildContent(_searchValue)
      ],
    );
  }

  Widget _buildProjectCards(BuildContext context) {
    if (_isSearchLoading) {
      return Padding(
          padding: EdgeInsets.all(8),
          child: Center(child: CircularProgressIndicator()));
    } else if (projectResultList.length > 0 && _searchValue != "") {
      List<Widget> colChildren = [];
      for (Project result in projectResultList) {
        colChildren.add(ProjectCard(
            projectTitle: result.name,
            location: result.region,
            description: result.description,
            progressValue: result.progress,
            targetPage: ProjectDashboardPage(project: result)));
      }
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start, children: colChildren);
    } else {
      return Text(
        "Hasil pencarian tidak ditemukan",
        style: Theme.of(context).textTheme.bodyText1,
      );
    }
  }

  Widget _buildCards(BuildContext context) {
    return _buildProjectCards(context);
  }

  Widget _buildSearchBar() {
    return SearchBar(
      label: "Cari Proyek",
      controller: _searchController,
      onSubmit: (value) => {
        setState(() {
          _searchValue = value;
          if (value != "") {
            fetchSearchProjectsResult();
          }
        })
      },
    );
  }

  Widget _buildContent(String? searchValue) {
    if (searchValue!.isNotEmpty) {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text("Hasil Pencarian", style: Theme.of(context).textTheme.headline3),
        SizedBox(height: 16),
        _buildCards(context)
      ]);
    } else {
      return MoNevSummary();
    }
  }

  void setProjectsResult(List<Project> newProjectResultList) {
    setState(() {
      projectResultList = newProjectResultList;
    });
  }

  void _toggleLoading() {
    setState(() {
      _isSearchLoading = !_isSearchLoading;
    });
  }

  Future<SharedPreferences> _getSharedPreferences() async {
    return SharedPreferences.getInstance();
  }

  void fetchSearchProjectsResult() async {
    _toggleLoading();

    SharedPreferences pref = await _getSharedPreferences();
    String token = pref.getString("token")!;

    var projects = await api.searchProjects(token, _searchValue!);
    setProjectsResult(projects);

    _toggleLoading();
  }
}
