import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:simpk/components/data_card.dart';
import 'package:simpk/components/pie_graph.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';

class MoNevSummary extends StatefulWidget {
  const MoNevSummary({this.showPieGraph = true, Key? key}) : super(key: key);

  final bool showPieGraph;

  @override
  _MoNevSummaryState createState() => _MoNevSummaryState();
}

class _MoNevSummaryState extends State<MoNevSummary> {
  HomeAPIProvider homeAPI = GetIt.I();

  int _finishedProjectsCount = 0;
  int _runningProjectsCount = 0;
  bool _isLoadingStatusCounts = false;
  Map<String, double> _dataMap = {};
  Map<String, HexColor> colorMap = {
    "Berjalan": HexColor("#F2994A"),
    "Selesai": HexColor("#27AE60")
  };

  @override
  void initState() {
    super.initState();
    _getProjectStatusCounts();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Ringkasan Monitoring & Evaluasi",
            style: Theme.of(context).textTheme.headline3),
        SizedBox(height: 20),
        _isLoadingStatusCounts
            ? Center(child: CircularProgressIndicator())
            : Column(
                children: [
                  if (widget.showPieGraph)
                    Column(
                      children: [
                        PieGraph(dataMap: _dataMap),
                        SizedBox(height: 8),
                        _buildLabel(colorMap),
                        SizedBox(height: 24),
                      ],
                    ),
                  _buildDataCard(),
                ],
              ),
      ],
    );
  }

  Widget _buildLabel(Map<String, HexColor> colorMap) {
    List<Widget> rowChildren = [];
    for (int idx = 0; idx < colorMap.length; idx++) {
      rowChildren.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  color: colorMap.values.elementAt(idx),
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
              SizedBox(width: 8),
              Text(
                colorMap.keys.elementAt(idx),
                style: Theme.of(context).textTheme.bodyText2,
              )
            ],
          ),
        ),
      );
    }
    return Row(
      children: rowChildren,
      mainAxisAlignment: MainAxisAlignment.center,
    );
  }

  Widget _buildDataCard() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: DataCard(
            data: _runningProjectsCount.toString(),
            label: "Proyek Berjalan",
            colorHex: "#F2994A",
          ),
        ),
        SizedBox(width: 8),
        Expanded(
          flex: 1,
          child: DataCard(
            data: _finishedProjectsCount.toString(),
            label: "Proyek Selesai",
            colorHex: "#27AE60",
          ),
        )
      ],
    );
  }

  void _getProjectStatusCounts() async {
    _toggleStatusCountsLoading();

    Map<String, dynamic> response = await homeAPI.getProjectStatusCounts();

    if (response["status_code"] == 200) {
      Map data = response["data"] as Map;
      _setNewPieGraphData(data);
    }

    _toggleStatusCountsLoading();
  }

  void _setNewPieGraphData(Map data) {
    _setFinishedProjectsCount(data["selesai"] as int);
    _setRunningProjectsCount(data["berjalan"] as int);

    Map<String, double> newDataMap = {};
    final int totalCount = _runningProjectsCount + _finishedProjectsCount;
    newDataMap["Berjalan"] = _runningProjectsCount / totalCount;
    newDataMap["Selesai"] = _finishedProjectsCount / totalCount;

    _setDataMap(newDataMap);
  }

  void _setFinishedProjectsCount(int newFinishedProjectsCount) {
    setState(() {
      _finishedProjectsCount = newFinishedProjectsCount;
    });
  }

  void _setRunningProjectsCount(int newRunningProjectsCount) {
    setState(() {
      _runningProjectsCount = newRunningProjectsCount;
    });
  }

  void _toggleStatusCountsLoading() {
    setState(() {
      _isLoadingStatusCounts = !_isLoadingStatusCounts;
    });
  }

  void _setDataMap(Map<String, double> dataMap) {
    setState(() {
      _dataMap = dataMap;
    });
  }
}
