import 'package:flutter/material.dart';
import 'package:simpk/modules/project_list/screens/project_list.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/offline_report/screens/save_offline_report_2.dart';

class SaveOfflineReport1 extends StatefulWidget {
  const SaveOfflineReport1({
    required this.offlineReportId,
    Key? key,
  }) : super(key: key);

  final int offlineReportId;

  @override
  _SaveOfflineReport1State createState() => _SaveOfflineReport1State();
}

class _SaveOfflineReport1State extends State<SaveOfflineReport1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(elevation: 0),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(16, 8, 16, 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Simpan Proyek Offline (1/2)",
                style: Theme.of(context).textTheme.headline1),
            SizedBox(height: 12),
            Text("Pilih proyek untuk laporan yang akan disimpan",
                style: Theme.of(context).textTheme.bodyText1),
            SizedBox(height: 36),
            ProjectList(
              saveOffline: true,
              targetPage: (Project project) {
                return SaveOfflineReport2(
                  projectId: project.id,
                  offlineReportId: widget.offlineReportId,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
