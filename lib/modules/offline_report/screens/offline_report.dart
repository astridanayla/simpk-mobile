import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/card.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_1.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';

class OfflineReport extends StatefulWidget {
  @override
  _OfflineReportState createState() => _OfflineReportState();
}

class _OfflineReportState extends State<OfflineReport> {
  final ReportService _reportRepository = GetIt.I();
  List<Report> reports = [];

  @override
  void initState() {
    super.initState();
    _getOfflineReports();
  }

  void _setReports(List<Report> newReports) {
    setState(() {
      reports = newReports;
    });
  }

  Future _getOfflineReports() async {
    List<Report> _reports = await _reportRepository.queryAllOffline();
    _setReports(_reports);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildBanner(context),
            _buildCreateReport(context),
            _buildReportList(context),
            SizedBox(height: 36),
          ],
        ),
      ),
    );
  }

  Widget _buildCreateReport(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 32, 16, 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Buat Laporan Offline",
              style: Theme.of(context).textTheme.headline3),
          Container(
            margin: EdgeInsets.fromLTRB(0, 16, 0, 20),
            child: Text(
              "Buat laporan offline baru. Spesifikasikan proyek ketika " +
                  "anda sudah kembali online.",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          ElevatedButton.icon(
            key: Key('buat_laporan_button'),
            label: Text('Buat Laporan'),
            icon: Icon(Icons.add_circle_outline_rounded, size: 20),
            onPressed: () async {
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CreateReport1Screen(
                    projectId: 0,
                    periodNumber: 0,
                  ),
                ),
              );
              await _getOfflineReports();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildReportList(BuildContext context) {
    List<Widget> reportsWidgetCardsList = [];
    reportsWidgetCardsList.addAll(
      [
        Text(
          "Daftar Laporan Offline",
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 16),
      ],
    );

    if (reports.length == 0) {
      reportsWidgetCardsList.add(Center(
        child: Text(
          "Tidak ada laporan offline yang sudah dibuat!",
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ));
    }

    for (var _report in reports) {
      reportsWidgetCardsList.add(
        CustomCard(
          lastEdited: _report.modifiedDate!,
          onClick: () {},
          title: _report.activityName!,
          isOfflineReport: true,
          onOfflineReportDelete: () async {
            _deleteConfirmAlert(context, _report.id!);
          },
        ),
      );
    }

    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 24),
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: reportsWidgetCardsList,
      ),
    );
  }

  Widget _buildBanner(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(16, 48, 16, 16),
      width: double.infinity,
      color: Color(0xFFF7DFDF),
      alignment: Alignment.center,
      child: Text(
        "Anda Sedang Offline",
        style: Theme.of(context)
            .textTheme
            .headline3!
            .copyWith(color: Theme.of(context).errorColor),
      ),
    );
  }

  Future _deleteConfirmAlert(BuildContext context, int reportId) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Hapus Laporan?",
            style: Theme.of(context).textTheme.headline2,
          ),
          content: Text(
            "Anda yakin ingin menghapus laporan ini?",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                  (states) => Color(0xFF4F4F4F).withOpacity(0.1),
                ),
              ),
              child: Text(
                "Tidak",
                style: TextStyle(
                  color: Color(0xFF4F4F4F),
                ),
              ),
            ),
            TextButton(
              onPressed: () async {
                await _reportRepository.deleteByIdOffline(reportId);
                Navigator.of(context).pop(false);
                await _getOfflineReports();
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                  (states) => Theme.of(context).errorColor.withOpacity(0.1),
                ),
              ),
              child: Text(
                "Ya",
                style: TextStyle(
                  color: Theme.of(context).errorColor,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
