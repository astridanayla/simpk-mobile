import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';

class SaveOfflineReport2 extends StatefulWidget {
  const SaveOfflineReport2({
    required this.projectId,
    required this.offlineReportId,
    Key? key,
    this.periodNumber = 1,
  }) : super(key: key);

  final int periodNumber;
  final int projectId;
  final int offlineReportId;

  @override
  _SaveOfflineReport2State createState() =>
      _SaveOfflineReport2State(this.periodNumber);
}

class _SaveOfflineReport2State extends State<SaveOfflineReport2> {
  _SaveOfflineReport2State(this.periodNumber);

  final int periodNumber;

  final ReportService _reportRepository = GetIt.I();
  List<String> _periods = [];
  int _highestPeriod = 1;
  String? _selectedMonitoringPeriod;
  bool _newPeriodAdded = false;
  bool _isLoadingSaveData = false;

  @override
  void initState() {
    super.initState();
    _reportRepository
        .queryMonitoringPeriodByProjectId(widget.projectId)
        .then((value) {
      if (mounted)
        setState(() {
          _highestPeriod = _getHighestPeriod(value);
          for (int idx = 1; idx <= _highestPeriod; idx++) {
            _periods.add("Periode $idx");
          }
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(elevation: 0),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(16, 8, 16, 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Simpan Proyek Offline (2/2)",
                style: Theme.of(context).textTheme.headline1,
              ),
              SizedBox(height: 12),
              Text(
                "Pilih periode untuk laporan yang akan disimpan",
                style: Theme.of(context).textTheme.bodyText1,
              ),
              SizedBox(height: 36),
              _buildDropdownField(
                label: "Pilih Periode Monitoring",
                items: _periods,
                value: _selectedMonitoringPeriod,
                onChanged: (value) {
                  setState(
                    () {
                      _selectedMonitoringPeriod = value;
                    },
                  );
                },
              ),
              SizedBox(height: 4),
              Container(
                alignment: Alignment.topLeft,
                child: TextButton(
                  key: Key('btn_add_period'),
                  onPressed: _newPeriodAdded ? null : _incrementPeriod,
                  child: Text("+ Tambah Periode Baru"),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 40),
          child: _buildBottomNav(),
        ));
  }

  Widget _buildDropdownField(
      {required String label,
      required List<String> items,
      required String? value,
      required void Function(String?) onChanged}) {
    return DropdownButtonFormField<String>(
      value: value,
      validator: (val) {
        val != null;
      },
      autovalidateMode: AutovalidateMode.always,
      isExpanded: true,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: label,
      ),
      items: items
          .map<DropdownMenuItem<String>>(
            (String value) => DropdownMenuItem(
              value: value,
              child: Text(
                value,
                softWrap: true,
              ),
            ),
          )
          .toList(),
      onChanged: onChanged,
    );
  }

  Widget _buildBottomNav() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: () async {
              await _onSave();
            },
            child: _isLoadingSaveData
                ? SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white,
                      ),
                    ),
                  )
                : Text("Simpan"),
          ),
        ),
      ],
    );
  }

  void _toggleIsLoadingSaveData() {
    setState(() {
      _isLoadingSaveData = !_isLoadingSaveData;
    });
  }

  Future _onSave() async {
    Report offlineReport = await _getOfflineReportById(widget.offlineReportId);
    offlineReport
      ..id = null
      ..projectId = widget.projectId
      ..monitoringPeriod = _selectedMonitoringPeriod;

    if (_selectedMonitoringPeriod != null) {
      _toggleIsLoadingSaveData();
      await _reportRepository.insert(offlineReport);
      for (ReportImage image in offlineReport.reportImages!) {
        image.image.delete();
      }
      _toggleIsLoadingSaveData();
      _buildsnackbar(
        context,
        Text("Laporan monitoring telah tersimpan. Mohon dicek kembali " +
            "di halaman proyek."),
      );
      await Future.delayed(const Duration(seconds: 2), () {});
      await _reportRepository.deleteByIdOffline(widget.offlineReportId);
      // Pop back to List Laporan Offline
      int count = 0;
      Navigator.of(context).popUntil((_) => count++ >= 2);
    } else {
      _buildsnackbar(
          context,
          Text("Mohon pilih periode monitoring terlebih dahulu " +
              "sebelum menyimpan laporan."));
    }
  }

  Future<Report> _getOfflineReportById(int id) async {
    Report offlineReport = await _reportRepository.queryByIdOffline(id);
    return offlineReport;
  }

  void _incrementPeriod() {
    final newMonitoringPeriod = "Periode ${_highestPeriod + 1}";
    setState(() {
      _newPeriodAdded = true;
      _selectedMonitoringPeriod = newMonitoringPeriod;
      _periods.add(newMonitoringPeriod);
    });
  }

  int _getHighestPeriod(List<String> periods) {
    if (periods.length == 0) {
      return 0;
    }

    final periodNumbers = periods
        .map((period) => int.tryParse(period.split(' ').last) ?? 1)
        .toList();
    periodNumbers.sort();

    return periodNumbers.last;
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildsnackbar(
      BuildContext context, Widget content) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }
}
