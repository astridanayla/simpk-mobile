import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/card.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/offline_report/screens/save_offline_report_1.dart';
import 'package:simpk/modules/user_profile/screens/user_profile.dart';

class ManageOfflineReport extends StatefulWidget {
  @override
  _ManageOfflineReportState createState() => _ManageOfflineReportState();
}

class _ManageOfflineReportState extends State<ManageOfflineReport> {
  final ReportService _reportRepository = GetIt.I();
  List<Report> reports = [];

  @override
  void initState() {
    super.initState();
    _getOfflineReports();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppbar(
        preferredSize: Size.fromHeight(112),
        profileColor: Theme.of(context).disabledColor,
        wifiColor: Theme.of(context).primaryColor,
        wifiOnPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  HomePage(),
            ),
          );
        },
        profileOnPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  UserProfile(),
            ),
          );
        },
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(16, 24, 16, 40),
        child: Container(
          alignment: Alignment.topLeft,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 8),
              Text(
                "Daftar Laporan Offline",
                style: Theme.of(context).textTheme.headline3,
              ),
              SizedBox(height: 16),
              reports.length > 0
                  ? _buildReportList(context)
                  : Center(
                      child: Text(
                        "Tidak ada laporan offline yang sudah dibuat!",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildReportList(BuildContext context) {
    List<Widget> reportsWidgetCardsList = [];

    for (var _report in reports) {
      reportsWidgetCardsList.add(
        CustomCard(
          lastEdited: _report.modifiedDate!,
          onClick: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SaveOfflineReport1(
                  offlineReportId: _report.id!,
                ),
              ),
            ).then((_) => _getOfflineReports());
          },
          title: _report.activityName!,
          isOfflineReport: true,
          onOfflineReportDelete: () async {
            _deleteConfirmAlert(context, _report.id!);
          },
        ),
      );
    }

    return Container(
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: reportsWidgetCardsList,
      ),
    );
  }

  void _setReports(List<Report> newReports) {
    setState(() {
      reports = newReports;
    });
  }

  Future _getOfflineReports() async {
    List<Report> _reports = await _reportRepository.queryAllOffline();
    _setReports(_reports);
  }

  Future _deleteConfirmAlert(BuildContext context, int reportId) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Hapus Laporan?",
            style: Theme.of(context).textTheme.headline2,
          ),
          content: Text(
            "Anda yakin ingin menghapus laporan ini?",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                  (states) => Color(0xFF4F4F4F).withOpacity(0.1),
                ),
              ),
              child: Text(
                "Tidak",
                style: TextStyle(
                  color: Color(0xFF4F4F4F),
                ),
              ),
            ),
            TextButton(
              onPressed: () async {
                await _reportRepository.deleteByIdOffline(reportId);
                Navigator.of(context).pop(false);
                await _getOfflineReports();
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                  (states) => Theme.of(context).errorColor.withOpacity(0.1),
                ),
              ),
              child: Text(
                "Ya",
                style: TextStyle(
                  color: Theme.of(context).errorColor,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
