class User {
  User(
      {required String this.name,
      required String this.email,
      required String this.handphone_number,
      required String this.role});

  factory User.empty() {
    return User(name: "", email: "", handphone_number: "", role: "");
  }

  factory User.fromJson(Map<String, dynamic> json) {
    var json_data = json["data"][0];

    return User(
      name: json_data['nama_lengkap'].toString(),
      email: json_data['email'].toString(),
      handphone_number: json_data['no_telp'].toString(),
      role: json_data['role'].toString(),
    );
  }

  String name;
  String email;
  String handphone_number;
  String role;
}
