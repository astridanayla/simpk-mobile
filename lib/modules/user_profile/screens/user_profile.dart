import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/input_form_field.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/blocs/auth_event.dart';
import 'package:simpk/modules/authentication/screens/login.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/modules/home/screens/monev_summary.dart';
import 'package:simpk/modules/offline_report/screens/manage_offline_report.dart';
import 'package:simpk/modules/user_profile/models/user.dart';
import 'package:simpk/services/api_provider.dart';

class UserProfile extends StatefulWidget {
  UserProfile();

  final _UserProfileState state = new _UserProfileState();

  @override
  _UserProfileState createState() => state;
}

class _UserProfileState extends State<UserProfile> {
  APIProvider api = APIProvider();
  TextEditingController _newPasswordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _editUser = 'edit_user';
  final _changePass = 'change_pass';

  late User user;
  String? _editing;
  bool _isLoadingUser = false;
  bool _isLoadingPutUserDetails = false;
  String? _oldPassword;
  String? _newPassword;
  String? _confirmPassword;

  @override
  void initState() {
    super.initState();
    fetchUserDetails();
  }

  @override
  void dispose() {
    _newPasswordController.dispose();
    super.dispose();
  }

  void setUser(User newUser) {
    setState(() {
      user = newUser;
    });
  }

  void _setScreen(String? state) {
    setState(() {
      _editing = state;
    });
  }

  void _toggleLoadingUser() {
    setState(() {
      _isLoadingUser = !_isLoadingUser;
    });
  }

  void _toggleLoadingPutUser() {
    setState(() {
      _isLoadingPutUserDetails = !_isLoadingPutUserDetails;
    });
  }

  Future<SharedPreferences> _getSharedPreferences() async {
    return SharedPreferences.getInstance();
  }

  Future<String> _getToken() async {
    SharedPreferences pref = await _getSharedPreferences();
    return pref.getString("token") as String;
  }

  void fetchUserDetails() async {
    _toggleLoadingUser();

    SharedPreferences pref = await _getSharedPreferences();
    String token = pref.getString("token")!;

    var user = await api.getUserDetails(token);
    setUser(user);

    _toggleLoadingUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppbar(
        preferredSize: Size.fromHeight(112),
        profileColor: Theme.of(context).primaryColor,
        wifiColor: Theme.of(context).disabledColor,
        wifiOnPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  ManageOfflineReport(),
            ),
          );
        },
        profileOnPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  HomePage(),
            ),
          );
        },
      ),
      body: SingleChildScrollView(
        child: _userProfileScreenBody(context),
      ),
    );
  }

  Widget _userProfileScreenBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          _buildUserImage(context),
          SizedBox(height: 40),
          _editing == _editUser
              ? _buildEditUser(context)
              : _editing == _changePass
                  ? _buildChangePassword(context)
                  : _buildUserInfo(context),
        ],
      ),
    );
  }

  Widget _buildUserInfo(BuildContext context) {
    return Column(children: [
      _isLoadingUser
          ? Center(child: CircularProgressIndicator())
          : _buildUserDetails(context),
      SizedBox(height: 56),
      MoNevSummary(showPieGraph: false),
      SizedBox(height: 40),
      _buildEditButton(context, 'Edit Profil', _editUser),
      SizedBox(height: 16),
      _buildEditButton(context, 'Ubah Password', _changePass),
      SizedBox(height: 16),
      _buildLogoutButton(context),
      SizedBox(height: 48),
    ]);
  }

  Widget _buildUserImage(BuildContext context) {
    return Center(
      child: Container(
        child: Icon(
          Icons.account_circle,
          size: 120,
          color: Theme.of(context).disabledColor,
        ),
      ),
    );
  }

  Widget _buildUserDetails(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _userInfoField(context, 'Nama', user.name),
          SizedBox(height: 20),
          _userInfoField(context, 'Email', user.email),
          SizedBox(height: 20),
          _userInfoField(
              context, 'No Handphone', user.handphone_number.toString()),
          SizedBox(height: 20),
          _userInfoField(context, 'Peran', user.role),
        ],
      ),
    );
  }

  Widget _userInfoField(
      BuildContext context, String fieldName, String fieldValue) {
    return Row(
      children: [
        Expanded(
          flex: 5,
          child: Text(
            fieldName,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: HexColor("#333333"),
            ),
          ),
        ),
        Expanded(
          flex: 5,
          child: Text(
            fieldValue,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ],
    );
  }

  Widget _buildEditButton(BuildContext context, String label, String screen) {
    return Material(
      child: OutlinedButton(
        onPressed: () {
          _newPasswordController.clear();
          _setScreen(screen);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(label),
          ],
        ),
      ),
    );
  }

  Widget _buildLogoutButton(BuildContext context) {
    return Center(
      child: GestureDetector(
        child: Text(
          'Keluar',
          style: TextStyle(
            color: Theme.of(context).errorColor,
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        onTap: () {
          _logoutConfirmAlert(context);
        },
      ),
    );
  }

  Future _logoutConfirmAlert(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Keluar",
            style: Theme.of(context).textTheme.headline2,
          ),
          content: Text(
            "Anda yakin ingin keluar dari akun ini?",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                    (states) => Color(0xFF4F4F4F).withOpacity(0.1)),
              ),
              child:
                  Text("Kembali", style: TextStyle(color: Color(0xFF4F4F4F))),
            ),
            TextButton(
              onPressed: () async {
                SharedPreferences pref = await _getSharedPreferences();
                pref.clear();
                context.read<AuthBloc>().add(AuthLogoutEvent());
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginScreen()),
                  (route) => false,
                );
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                    (states) => Theme.of(context).errorColor.withOpacity(0.1)),
              ),
              child: Text("Keluar",
                  style: TextStyle(color: Theme.of(context).errorColor)),
            ),
          ],
        );
      },
    );
  }

  Widget _buildEditUser(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          InputFormField(
            label: 'Email',
            initialValue: user.email,
            enabled: false,
          ),
          InputFormField(
            label: 'Nama',
            autovalidateMode: AutovalidateMode.always,
            onChanged: (value) {
              user.name = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Nama tidak boleh kosong.';
              }
              return null;
            },
            initialValue: user.name,
          ),
          InputFormField(
            label: 'No Handphone',
            autovalidateMode: AutovalidateMode.always,
            onChanged: (value) {
              user.handphone_number = value;
            },
            validator: (value) {
              String pattern = r'(^(?:[+0]8)?[0-9]{10,14}$)';
              RegExp regExp = new RegExp(pattern);
              if (value == null || value.isEmpty) {
                return 'No Handphone tidak boleh kosong.';
              }
              if (value.length < 10) {
                return 'Minimal no handphone 10 digit.';
              }
              if (value.length > 14) {
                return 'Maksimal no handphone 14 digit.';
              }
              if (!regExp.hasMatch(value)) {
                return 'Harap memasukkan no handphone yang valid.';
              }
              return null;
            },
            initialValue: user.handphone_number,
          ),
          SizedBox(height: 40),
          _buildBatalSimpanButton(context)
        ],
      ),
    );
  }

  Widget _buildChangePassword(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          InputFormField(
            label: 'Password Lama',
            obscureText: true,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            onSaved: (value) {
              _oldPassword = value;
            },
            validator: (value) {
              if (value?.isNotEmpty != true) {
                return "Password Lama tidak boleh kosong";
              }
            },
          ),
          InputFormField(
            label: 'Password Baru',
            obscureText: true,
            controller: _newPasswordController,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            onSaved: (value) {
              _newPassword = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Password Baru tidak boleh kosong";
              } else if (value.length < 8) {
                return "Password terlalu pendek, minimal 8 karakter";
              }
            },
          ),
          InputFormField(
            label: 'Konfirmasi Password Baru',
            obscureText: true,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            onSaved: (value) {
              _confirmPassword = value;
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Konfirmasi Password Baru tidak boleh kosong";
              } else if (value != _newPasswordController.text) {
                return "Konfirmasi Password tidak sama dengan Password Baru";
              }
            },
          ),
          SizedBox(height: 40),
          _buildBatalSimpanButton(context, changePass: true)
        ],
      ),
    );
  }

  Widget _buildBatalSimpanButton(BuildContext context,
      {bool changePass = false}) {
    return Row(
      children: <Widget>[
        Expanded(
          child: OutlinedButton(
            onPressed: () {
              _setScreen(null);

              fetchUserDetails();
            },
            child: Text(
              'Batal',
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            style: OutlinedButton.styleFrom(
              side: BorderSide(
                width: 2,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ),
        SizedBox(width: 8),
        Expanded(
          child: ElevatedButton(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                _buildSnackBar(
                    context, Text('Harap mengisi dengan data yang sesuai!'));
              } else {
                _toggleLoadingPutUser();

                String token = await _getToken();
                bool success;
                String message;

                if (!changePass) {
                  success = await api.putUserDetails(
                    token,
                    user.email,
                    user.name,
                    user.handphone_number,
                  );

                  if (success) {
                    message = 'Data berhasil tersimpan!';
                  } else {
                    message = 'Data gagal tersimpan! Mohon coba lagi!';
                  }
                } else {
                  _formKey.currentState!.save();
                  final responseMap = await api.changePassword(
                      token, _oldPassword!, _newPassword!, _confirmPassword!);
                  success = responseMap['password_changed'] as bool;
                  message = responseMap['message'] as String;
                }

                _toggleLoadingPutUser();

                _buildSnackBar(context, Text(message));
                if (success) {
                  _setScreen(null);

                  fetchUserDetails();
                }
              }
            },
            child: _isLoadingPutUserDetails
                ? SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor),
                    ),
                  )
                : Text("Simpan"),
            style: OutlinedButton.styleFrom(
                side: BorderSide(
                  width: 2,
                  color: Theme.of(context).primaryColor,
                ),
                backgroundColor: _isLoadingPutUserDetails
                    ? Colors.white
                    : Theme.of(context).primaryColor),
          ),
        ),
      ],
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildSnackBar(
      BuildContext context, Widget content) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }
}
