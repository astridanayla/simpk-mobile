import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/bottom_navbar.dart';
import 'package:simpk/components/input_form_field.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/modules/offline_report/screens/manage_offline_report.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/user_profile/screens/user_profile.dart';
import 'package:simpk/services/api_provider.dart';

class MapsPage extends StatefulWidget {
  final _MapsPageState state = _MapsPageState();
  @override
  _MapsPageState createState() => state;
}

class _MapsPageState extends State<MapsPage> {
  APIProvider api = APIProvider();
  Completer<GoogleMapController> _mapController = Completer();
  final TextEditingController _searchController = new TextEditingController();
  Map<String, Marker> _markers = {};
  LatLng _userLocation = LatLng(-6.200000, 106.816666);
  bool _isLoadingMaps = false;
  List<Project> _searchResults = [];
  bool _isSearching = false;

  @override
  void initState() {
    super.initState();
    _getInitialLocation();
    _getProjectMarkers();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {FocusScope.of(context).unfocus()},
      child: Scaffold(
        appBar: MainAppbar(
          preferredSize: Size.fromHeight(112),
          profileColor: Theme.of(context).disabledColor,
          wifiColor: Theme.of(context).disabledColor,
          wifiOnPressed: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    ManageOfflineReport(),
              ),
            );
          },
          profileOnPressed: () {
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation, secondaryAnimation) =>
                    UserProfile(),
              ),
            );
          },
        ),
        resizeToAvoidBottomInset: false,
        body: _isLoadingMaps
            ? Center(
                child: CircularProgressIndicator(),
              )
            : _buildMapsContent(),
        bottomNavigationBar: BottomNavbar(currentPageIndex: 1),
      ),
    );
  }

  Widget _buildMapsContent() {
    return Column(
      children: [
        _buildSearchField(),
        _buildMap(),
      ],
    );
  }

  Widget _buildSearchField() {
    return Expanded(
      flex: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: InputFormField(
          label: "Cari Proyek",
          controller: _searchController,
          onChanged: (val) async {
            if (val != "") {
              _setSearch(true);
              var searchResult = await fetchSearchResult(val);

              _setSearchResults(searchResult);
            } else {
              _setSearch(false);
              _clearSearchResults();
            }
            ;
          },
          prefixIcon: Icon(Icons.search),
          suffixIcon: _searchController.text.length > 0
              ? IconButton(
                  color: Color(0xFF828282),
                  icon: Icon(Icons.clear),
                  onPressed: () {
                    setState(() {
                      _searchController.clear();
                      _isSearching = false;
                    });
                  },
                )
              : null,
        ),
      ),
    );
  }

  Widget _buildMap() {
    return Expanded(
      flex: 13,
      child: Stack(
        children: [
          GoogleMap(
            onMapCreated: (GoogleMapController controller) {
              _mapController.complete(controller);
            },
            initialCameraPosition: CameraPosition(
              target: _userLocation,
              zoom: 14,
            ),
            myLocationEnabled: true,
            markers: _markers.values.toSet(),
          ),
          Visibility(
            visible: _isSearching,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(.6),
                backgroundBlendMode: BlendMode.darken,
              ),
            ),
          ),
          Visibility(
            visible: _isSearching,
            child: Container(
              child: _searchResults.length == 0
                  ? Container(
                      color: Theme.of(context).backgroundColor,
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
                      width: double.infinity,
                      child: Text(
                        "Hasil pencarian tidak ditemukan",
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).errorColor),
                      ),
                    )
                  : _buildSearchResultList(),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSearchResultList() {
    return ListView.builder(
      itemCount: _searchResults.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            List<String> coordinates =
                _searchResults[index].coordinate.split(", ");

            var newPosition = CameraPosition(
              target: LatLng(
                double.parse(coordinates[0]),
                double.parse(coordinates[1]),
              ),
              zoom: 14,
            );

            CameraUpdate update = CameraUpdate.newCameraPosition(newPosition);

            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }

            _setSearchText(_searchResults[index].name);
            _clearSearchResults();
            _setSearch(false);

            _goToPlace(update);
          },
          child: Container(
            color: Theme.of(context).backgroundColor,
            child: ListTile(
              title: Text(_searchResults[index].name,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontWeight: FontWeight.bold)),
            ),
          ),
        );
      },
    );
  }

  void _toggleLoadingMaps() {
    setState(() {
      _isLoadingMaps = !_isLoadingMaps;
    });
  }

  void _setSearch(bool boolean) {
    setState(() {
      _isSearching = boolean;
    });
  }

  void _setSearchResults(List<Project> searchResults) {
    setState(() {
      _searchResults = searchResults;
    });
  }

  void _clearSearchResults() {
    setState(() {
      _searchResults.clear();
    });
  }

  void _setSearchText(String text) {
    setState(() {
      _searchController.text = text;
    });
  }

  void _getInitialLocation() async {
    _toggleLoadingMaps();
    Position pos = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    setState(() {
      _userLocation = LatLng(pos.latitude, pos.longitude);
    });
  }

  void _getProjectMarkers() async {
    _markers.clear();

    SharedPreferences pref = await _getSharedPreferences();
    String token = pref.getString("token")!;

    var projects = await api.getAllProjects(token);

    projects.forEach((project) {
      List<String> coordinates = project.coordinate.split(", ");
      if (coordinates.length == 2) {
        final marker = Marker(
          markerId: MarkerId(project.name),
          position: LatLng(
            double.parse(coordinates[0]),
            double.parse(coordinates[1]),
          ),
          infoWindow: InfoWindow(
            title: project.name,
            snippet: project.partner,
          ),
        );
        _markers[project.name] = marker;
      }
    });

    _toggleLoadingMaps();
  }

  Future<List<Project>> fetchSearchResult(String? searchQuery) async {
    SharedPreferences pref = await _getSharedPreferences();
    String token = pref.getString("token")!;

    var projects = await api.searchProjects(token, searchQuery!);

    return projects;
  }

  Future<void> _goToPlace(CameraUpdate cameraUpdate) async {
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(cameraUpdate);
  }

  Future<SharedPreferences> _getSharedPreferences() async {
    return SharedPreferences.getInstance();
  }
}
