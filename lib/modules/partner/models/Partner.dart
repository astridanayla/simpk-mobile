class Partner {
  Partner(
      {required int this.id,
      required String this.name,
      required String this.email,
      required String this.address});

  factory Partner.fromJson(dynamic json_data) {
    return Partner(
      id: int.parse(json_data['partnerId'].toString()),
      name: json_data['name'].toString(),
      email: json_data['email'].toString(),
      address: json_data['address'].toString(),
    );
  }

  int id;
  String name;
  String email;
  String address;

  Map<String, String> asMap() {
    return {"Nama": this.name, "Email": this.email, "Alamat": address};
  }
}
