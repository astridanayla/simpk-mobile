import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:simpk/components/dashboard_appbar.dart';
import 'package:simpk/modules/partner/models/Partner.dart';
import 'package:simpk/modules/project_list/screens/project_list.dart';

class PartnerDashboardPage extends StatefulWidget {
  PartnerDashboardPage({required this.partner, Key? key}) : super(key: key);

  final Partner partner;

  @override
  _PartnerDashboardPageState createState() => _PartnerDashboardPageState();
}

class _PartnerDashboardPageState extends State<PartnerDashboardPage> {
  TextEditingController controller = TextEditingController();

  Widget _buildCopyButton(BuildContext context, String value) {
    return Container(
      width: 32,
      height: 32,
      child: IconButton(
        icon: Icon(Icons.copy, size: 20),
        color: Theme.of(context).primaryColor,
        onPressed: () {
          final snackBar = SnackBar(
            content: Text("Teks berhasil disalin!"),
            width: 360.0,
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
            ),
          );
          Clipboard.setData(ClipboardData(text: value));
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
      ),
    );
  }

  Widget _buildBiodata(BuildContext context) {
    List<Widget> columnChildren = [];

    for (var idx = 1; idx < 3; idx++) {
      List<Widget> rowChildren = [];
      rowChildren.add(Expanded(
        flex: 1,
        child: Text(widget.partner.asMap().keys.elementAt(idx),
            style: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(color: HexColor("#828282"))),
      ));
      rowChildren.add(SizedBox(width: 16));
      rowChildren.add(Expanded(
        flex: 3,
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Text(widget.partner.asMap().values.elementAt(idx),
              style: Theme.of(context).textTheme.bodyText1),
          SizedBox(width: 8),
          _buildCopyButton(
              context, widget.partner.asMap().values.elementAt(idx))
        ]),
      ));
      columnChildren.add(Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 4),
        child: Row(children: rowChildren),
      ));
    }
    return Column(children: columnChildren);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DashboardAppBar("MITRA", widget.partner.name),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(16, 32, 16, 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Biodata Mitra", style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 12),
            _buildBiodata(context),
            SizedBox(height: 32),
            ProjectList(partnerId: widget.partner.id, saveOffline: false),
          ],
        ),
      ),
    );
  }
}
