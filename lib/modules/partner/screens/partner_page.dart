import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/bottom_navbar.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/components/partner_card.dart';
import 'package:simpk/modules/offline_report/screens/manage_offline_report.dart';
import 'package:simpk/modules/partner/models/Partner.dart';
import 'package:simpk/modules/partner/screens/partner_dashboard_page.dart';
import 'package:simpk/modules/user_profile/screens/user_profile.dart';
import 'package:simpk/services/api_provider.dart';

class PartnerPage extends StatefulWidget {
  final _PartnerPageState state = new _PartnerPageState();

  @override
  _PartnerPageState createState() => state;
}

class _PartnerPageState extends State<PartnerPage> {
  APIProvider api = APIProvider();
  List<Partner> partnerList = [];
  List<Partner> resultList = [];
  bool _isLoading = false;

  @override
  void initState() {
    fetchPartners();
    super.initState();
  }

  void setPartners(List<Partner> newPartnerList) {
    setState(() {
      partnerList = newPartnerList;
    });
  }

  void setResult(List<Partner> newResultList) {
    setState(() {
      resultList = newResultList;
    });
  }

  void _toggleLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  Future<SharedPreferences> _getSharedPreferences() async {
    return SharedPreferences.getInstance();
  }

  void fetchPartners() async {
    _toggleLoading();

    SharedPreferences pref = await _getSharedPreferences();
    String token = pref.getString("token")!;

    var partners = await api.getAllPartners(token);
    setPartners(partners);

    _toggleLoading();
  }

  Widget _buildPartnerCards(BuildContext context) {
    if (_isLoading) {
      return Padding(
          padding: EdgeInsets.all(8),
          child: Center(child: CircularProgressIndicator()));
    } else if (resultList.length > 0) {
      List<Widget> colChildren = [];
      for (Partner result in resultList) {
        colChildren.add(
          PartnerCard(
            partnerName: result.name,
            targetPage: PartnerDashboardPage(partner: result),
          ),
        );
      }
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start, children: colChildren);
    } else if (partnerList.length > 0) {
      List<Widget> colChildren = [];
      for (Partner partner in partnerList) {
        colChildren.add(
          PartnerCard(
            partnerName: partner.name,
            targetPage: PartnerDashboardPage(partner: partner),
          ),
        );
      }
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start, children: colChildren);
    } else {
      return Text(
        "Hasil pencarian tidak ditemukan",
        style: Theme.of(context).textTheme.bodyText1,
      );
    }
  }

  Widget _buildContent() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text("Daftar Mitra", style: Theme.of(context).textTheme.headline3),
      SizedBox(height: 16),
      _buildPartnerCards(context)
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MainAppbar(
        preferredSize: Size.fromHeight(112),
        profileColor: Theme.of(context).disabledColor,
        wifiColor: Theme.of(context).disabledColor,
        wifiOnPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  ManageOfflineReport(),
            ),
          );
        },
        profileOnPressed: () {
          Navigator.push(
            context,
            PageRouteBuilder(
              pageBuilder: (context, animation, secondaryAnimation) =>
                  UserProfile(),
            ),
          );
        },
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(16, 20, 16, 40),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [_buildContent()]),
      ),
      bottomNavigationBar: BottomNavbar(currentPageIndex: 2),
    );
  }
}
