import 'package:simpk/modules/monitoring/models/Report.dart';

abstract class ReportService {
  Future<Report> insert(Report report);

  Future<bool> delete(int id);

  Future<List<Report>> queryByProjectId(int projectId);

  Future<Report> queryById(int id);

  Future<List<Report>> queryByProjectIdAndMonitoringPeriod(
      int projectId, String period);

  Future<List<String>> queryMonitoringPeriodByProjectId(int projectId);

  Future<Report> insertOffline(Report report);

  Future<List<Report>> queryAllOffline();

  Future<Report> queryByIdOffline(int id);

  Future<int> deleteByIdOffline(int id);

  Future<bool> deleteById(int id);
}
