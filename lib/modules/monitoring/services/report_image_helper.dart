import 'dart:io';

import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class ReportImageHelper {
  ReportImageHelper({required this.dbProvider});

  final DatabaseProvider dbProvider;

  static const tableName = 'report_image';

  static const columnId = DatabaseProvider.columnId;
  static const columnReportId = 'report_id';
  static const columnImagePath = 'image_path';

  static const onCreateSQL = '''
    CREATE TABLE $tableName (
    $columnId                     INTEGER PRIMARY KEY,
    $columnReportId               INTEGER NOT NULL,
    $columnImagePath              TEXT    NOT NULL,
    FOREIGN KEY ($columnReportId)
      REFERENCES ${ReportHelper.tableName} (${ReportHelper.columnId}));
    ''';

  Future<int> insert(ReportImage reportImage) async {
    Database db = await dbProvider.database;
    return db.insert(
      tableName,
      _toMap(reportImage),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<ReportImage>> queryAll() async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(tableName);
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<List<ReportImage>> queryByReportId(int id) async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(
      tableName,
      where: '$columnReportId = ?',
      whereArgs: [id],
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Map<String, dynamic> _toMap(ReportImage reportImage) {
    return {
      columnId: reportImage.id,
      columnReportId: reportImage.reportId,
      columnImagePath: reportImage.image.path,
    };
  }

  ReportImage _fromMap(Map<String, dynamic> map) {
    return ReportImage(
      id: map[columnId] as int,
      reportId: map[columnReportId] as int,
      image: File(map[columnImagePath] as String),
    );
  }
}
