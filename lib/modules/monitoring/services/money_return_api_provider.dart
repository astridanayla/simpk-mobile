import 'dart:io';

import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';

class MoneyReturnAPIProvider {
  MoneyReturnAPIProvider({required this.dio});
  final Dio dio;

  Future<String> _getToken() async {
    final pref = await SharedPreferences.getInstance();
    final token = await pref.getString('token');
    if (token == null) {
      throw ErrorNoLoginToken();
    }
    return "Bearer " + token;
  }

  Future<bool> postMoneyReturnReport(
      MoneyReturnReport report, String projectId) async {
    String path = "/laporan_pengembalian_uang/";
    String token = await _getToken();
    dio.options.headers = {
      HttpHeaders.authorizationHeader: token,
    };

    Map<String, dynamic> body = {
      "judul": report.title,
      "deskripsi": report.description,
      "proyek": projectId,
      "bukti_foto": await MultipartFile.fromFile(report.imageFile!.path),
    };

    FormData formData = FormData.fromMap(body);

    Response response = await dio.post(path, data: formData);

    if (response.statusCode == 201) {
      return true;
    }
    return false;
  }

  Future<List<MoneyReturnReport>> getMoneyReturnReportsByProjectId(
      int projectId) async {
    String path = "/laporan_pengembalian_uang/";
    String token = await _getToken();
    dio.options.headers = {
      HttpHeaders.authorizationHeader: token,
    };
    Response response = await dio.get(
      path,
      queryParameters: {"project_id": projectId},
    );
    List responseData = response.data["data"] as List;

    List<MoneyReturnReport> moneyReturnReports = [];
    for (var report in responseData) {
      moneyReturnReports
          .add(MoneyReturnReport.fromJson(report as Map<String, dynamic>));
    }

    return moneyReturnReports;
  }

  Future<Map<String, dynamic>> putMoneyReturnReport(
      MoneyReturnReport report) async {
    String path = "/laporan_pengembalian_uang/" + report.id;
    String token = await _getToken();
    dio.options.headers = {
      HttpHeaders.authorizationHeader: token,
    };

    Map<String, dynamic> body = {
      "judul": report.title,
      "deskripsi": report.description,
    };

    if (report.imageFile != null) {
      body["bukti_foto"] = await MultipartFile.fromFile(report.imageFile!.path);
    }

    FormData formData = FormData.fromMap(body);

    Response response = await dio.put(path, data: formData);

    Map<String, dynamic> responseMap = {
      "status_code": response.statusCode,
    };

    if (response.statusCode == 202) {
      MoneyReturnReport reportResponse =
          MoneyReturnReport.fromJson(response.data as Map<String, dynamic>);
      responseMap["report"] = reportResponse;
    }

    return responseMap;
  }

  Future<bool> deleteMoneyReturnReport(String reportId) async {
    String path = "/laporan_pengembalian_uang/" + reportId;
    String token = await _getToken();
    dio.options.headers = {
      HttpHeaders.authorizationHeader: token,
    };

    try {
      Response response = await dio.delete(path);
      return response.statusCode == 204 ? true : false;
    } catch (e) {
      return false;
    }
  }
}

class ErrorNoLoginToken extends Error {}
