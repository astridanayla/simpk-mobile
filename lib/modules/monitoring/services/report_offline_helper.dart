import 'dart:convert';

import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/services/response_mapper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class ReportOfflineHelper {
  ReportOfflineHelper({required this.dbProvider});

  final DatabaseProvider dbProvider;

  static const tableName = 'report_offline';
  static const columnId = DatabaseProvider.columnId;
  static const columnJson = 'json';

  static const onCreateSQL = '''
    CREATE TABLE $tableName (
    $columnId               INTEGER PRIMARY KEY AUTOINCREMENT,
    $columnJson             TEXT    NOT NULL)
    ''';

  Future<Report> insert(Report report) async {
    Database db = await dbProvider.database;
    int reportId = await db.insert(
      tableName,
      _toMap(report),
    );
    report.id = reportId;

    return report;
  }

  Future<int> deleteById(int id) async {
    Database db = await dbProvider.database;
    int count = await db.delete(
      tableName,
      where: "$columnId = ?",
      whereArgs: [id],
    );

    return count;
  }

  Future<List<Report>> queryAll() async {
    Database db = await dbProvider.database;
    final mapList = await db.query(
      tableName,
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<Report> queryById(int id) async {
    Database db = await dbProvider.database;
    final mapList = await db.query(
      tableName,
      where: '$columnId = ?',
      whereArgs: [id],
    );

    return _fromMap(mapList[0]);
  }

  Map<String, dynamic> _toMap(Report report) {
    final reportJson = ResponseMapper.reportToJson(report);
    reportJson['activity_items'] = report.activityItems!
        .map((e) => ResponseMapper.activityItemToJson(e))
        .toList();
    reportJson['realization_items'] = report.realizationItems!
        .map((e) => ResponseMapper.realizationItemToJson(e))
        .toList();
    reportJson['activity_analysis'] = report.activityAnalysis!
        .map((e) => ResponseMapper.activityAnalysisToJson(e))
        .toList();
    reportJson['report_images'] = report.reportImages!
        .map((e) => ResponseMapper.reportImageToJson(e))
        .toList();

    return {
      columnId: report.id,
      columnJson: jsonEncode(reportJson),
    };
  }

  Report _fromMap(Map<String, dynamic> map) {
    final jsonMap =
        jsonDecode(map[columnJson] as String) as Map<String, dynamic>;
    final report = ResponseMapper.reportFromJson(jsonMap);
    report.id = map[columnId] as int?;
    report.activityItems = (jsonMap['activity_items'] as List<dynamic>)
        .map((e) =>
            ResponseMapper.activityItemFromJson(e as Map<String, dynamic>))
        .toList();
    report.activityItems = (jsonMap['realization_items'] as List<dynamic>)
        .map((e) =>
            ResponseMapper.activityItemFromJson(e as Map<String, dynamic>))
        .toList();
    report.activityAnalysis = (jsonMap['activity_analysis'] as List<dynamic>)
        .map((e) =>
            ResponseMapper.activityAnalysisFromJson(e as Map<String, dynamic>))
        .toList();
    report.reportImages = (jsonMap['report_images'] as List<dynamic>)
        .map((e) =>
            ResponseMapper.reportImageFromJson(e as Map<String, dynamic>))
        .toList();

    return report;
  }
}
