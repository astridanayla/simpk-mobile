import 'dart:convert' as convert;
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http/http.dart' show Client;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/response_mapper.dart';

const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
    defaultValue: 'https://simpk-staging.herokuapp.com');

class MonitoringAPIProvider {
  MonitoringAPIProvider({
    required this.client,
    required this.dio,
  });

  final Client client;
  final Dio dio;

  Future<Map<String, String>> _getAuthHeader({bool contentJson = false}) async {
    final pref = await SharedPreferences.getInstance();
    final token = await pref.getString('token');
    if (token == null) {
      throw ErrorNoLoginToken();
    }
    final header = {HttpHeaders.authorizationHeader: 'Bearer ' + token};

    if (contentJson) {
      header['Content-Type'] = 'application/json; charset=UTF-8';
    }

    return header;
  }

  Uri _getUri(String path, [Map<String, dynamic> params = const {}]) {
    final urlSplit = _baseUrl.split('://');
    if (urlSplit[0] == 'https') {
      return Uri.https(urlSplit.last, path, params);
    } else {
      return Uri.http(urlSplit.last, path, params);
    }
  }

  Future<List<Report>> getReportsByProjectId(int projectId) async {
    final response = await client.get(
      _getUri('monitoring/laporan', {'proyek': projectId.toString()}),
      headers: await _getAuthHeader(),
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    return jsonResponse
        .map((e) => ResponseMapper.reportFromJson(e as Map<String, dynamic>))
        .toList();
  }

  Future<Report> insertReport(Report report) async {
    final response = await client.post(
      _getUri('monitoring/laporan'),
      headers: await _getAuthHeader(contentJson: true),
      body: convert.jsonEncode(ResponseMapper.reportToJson(report)),
    );
    final jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;
    final reportId = ResponseMapper.reportFromJson(jsonResponse).id;
    if (reportId == null) {
      throw 'Failed upload report';
    }

    report.id = reportId;

    try {
      // activity item
      final activityItems = report.activityItems;
      if (activityItems != null) {
        for (var x = 0; x < activityItems.length; x++) {
          final response = await client.post(
            _getUri('monitoring/laporan/${report.id}/item'),
            headers: await _getAuthHeader(contentJson: true),
            body: convert.jsonEncode(
                ResponseMapper.activityItemToJson(activityItems[x])),
          );
          final jsonResponse = convert.jsonDecode(response.body);
          if (!(jsonResponse is Map<String, dynamic>)) {
            throw 'Fail upload activity item: $jsonResponse';
          }

          final item = ResponseMapper.activityItemFromJson(jsonResponse);
          activityItems[x] = item;
        }
        report.activityItems = activityItems;
      }

      // realization item
      final realizationItems = report.realizationItems;
      if (realizationItems != null) {
        for (var x = 0; x < realizationItems.length; x++) {
          final response = await client.post(
            _getUri('monitoring/laporan/${report.id}/realisasi-item'),
            headers: await _getAuthHeader(contentJson: true),
            body: convert.jsonEncode(
                ResponseMapper.realizationItemToJson(realizationItems[x])),
          );
          final jsonResponse = convert.jsonDecode(response.body);
          if (!(jsonResponse is Map<String, dynamic>)) {
            throw 'Fail upload realization item: $jsonResponse';
          }

          final item = ResponseMapper.realizationItemFromJson(jsonResponse);
          realizationItems[x] = item;
        }
        report.realizationItems = realizationItems;
      }

      // activity analysis
      final activityAnalysis = report.activityAnalysis;
      if (activityAnalysis != null) {
        for (var x = 0; x < activityAnalysis.length; x++) {
          final response = await client.post(
            _getUri('monitoring/laporan/${report.id}/poin-analisa'),
            headers: await _getAuthHeader(contentJson: true),
            body: convert.jsonEncode(
                ResponseMapper.activityAnalysisToJson(activityAnalysis[x])),
          );
          final jsonResponse = convert.jsonDecode(response.body);
          if (!(jsonResponse is Map<String, dynamic>)) {
            throw 'Fail upload activity analysis: $jsonResponse';
          }

          final item = ResponseMapper.activityAnalysisFromJson(jsonResponse);
          activityAnalysis[x] = item;
        }
        report.activityAnalysis = activityAnalysis;
      }

      final reportImages = report.reportImages;
      if (reportImages != null) {
        for (var x = 0; x < reportImages.length; x++) {
          final imageFile = reportImages[x];
          final formData = FormData.fromMap({
            'file': await MultipartFile.fromFile(imageFile.image.path),
          });

          final response = await dio.post(
            '/monitoring/laporan/${report.id}/bukti-laporan',
            data: formData,
            options: Options(
              headers: await _getAuthHeader(),
              contentType: 'multipart/form-data',
            ),
          );

          final jsonResponse = response.data;
          if (!(jsonResponse is Map<String, dynamic>) ||
              !jsonResponse.containsKey('file')) {
            throw 'Fail upload report image: $jsonResponse';
          }
        }
      }
    } catch (e) {
      final successDelete = await deleteReport(reportId);
      if (!successDelete) {
        throw 'Delete fail & $e';
      }
      throw e;
    }

    return report;
  }

  Future<List<ActivityItem>> getActivityItems(int reportId) async {
    final response = await client.get(
      _getUri('monitoring/laporan/$reportId/item'),
      headers: await _getAuthHeader(),
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    return jsonResponse
        .map((e) =>
            ResponseMapper.activityItemFromJson(e as Map<String, dynamic>))
        .toList();
  }

  Future<List<RealizationItem>> getRealizationItems(int reportId) async {
    final response = await client.get(
      _getUri('monitoring/laporan/$reportId/realisasi-item'),
      headers: await _getAuthHeader(),
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    return jsonResponse
        .map((e) =>
            ResponseMapper.realizationItemFromJson(e as Map<String, dynamic>))
        .toList();
  }

  Future<List<ActivityAnalisis>> getActivityAnalysis(int reportId) async {
    final response = await client.get(
      _getUri('monitoring/laporan/$reportId/poin-analisa'),
      headers: await _getAuthHeader(),
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    return jsonResponse
        .map((e) =>
            ResponseMapper.activityAnalysisFromJson(e as Map<String, dynamic>))
        .toList();
  }

  Future<List<String>> getReportImageUrlsByReportId(int reportId) async {
    final response = await client.get(
      _getUri('monitoring/laporan/$reportId/bukti-laporan'),
      headers: await _getAuthHeader(),
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;

    List<String> imageUrls = [];

    for (var object in jsonResponse) {
      Map<String, dynamic> imageObj = object as Map<String, dynamic>;
      String imageUrl = imageObj["file"].toString();
      imageUrls.add(imageUrl);
    }

    return imageUrls;
  }

  Future<Report> getReportById(int reportId) async {
    final response = await client.get(
      _getUri('monitoring/laporan/$reportId'),
      headers: await _getAuthHeader(),
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    final report =
        ResponseMapper.reportFromJson(jsonResponse[0] as Map<String, dynamic>);

    report.activityItems = await getActivityItems(reportId);
    report.realizationItems = await getRealizationItems(reportId);
    report.activityAnalysis = await getActivityAnalysis(reportId);

    return report;
  }

  Future<bool> deleteReport(
    int reportId,
  ) async {
    final response = await client
        .delete(
          _getUri('monitoring/laporan/$reportId'),
          headers: await _getAuthHeader(contentJson: true),
        )
        .timeout(Duration(seconds: 10));

    try {
      convert.jsonDecode(response.body) as List<dynamic>;
      return true;
    } catch (e) {
      return false;
    }
  }
}

class ErrorNoLoginToken extends Error {}
