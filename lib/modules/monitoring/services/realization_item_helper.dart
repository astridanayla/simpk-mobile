import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class RealizationItemHelper {
  RealizationItemHelper({required this.dbProvider});

  static const tableName = 'realization_item';

  static const columnId = DatabaseProvider.columnId;
  static const columnReportId = 'report_id';
  static const columnName = 'name';
  static const columnApproved = 'approved';
  static const columnRealization = 'realization';
  static const columnDifference = 'difference';
  static const columnDifferenceDescription = 'difference_desc';

  static const onCreateSQL = '''
    CREATE TABLE $tableName (
    $columnId                     INTEGER PRIMARY KEY,
    $columnReportId               INTEGER NOT NULL,
    $columnName                   TEXT    NOT NULL,
    $columnApproved               INTEGER NOT NULL,
    $columnRealization            INTEGER NOT NULL,
    $columnDifference             INTEGER NOT NULL,
    $columnDifferenceDescription  TEXT,
    FOREIGN KEY ($columnReportId)
      REFERENCES ${ReportHelper.tableName} (${ReportHelper.columnId}));
    ''';

  final DatabaseProvider dbProvider;

  Future<int> insert(RealizationItem realizationItem) async {
    Database db = await dbProvider.database;
    return db.insert(
      tableName,
      _toMap(realizationItem),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<RealizationItem>> queryAll() async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(tableName);
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<List<RealizationItem>> queryByReportId(int id) async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(
      tableName,
      where: '$columnReportId = ?',
      whereArgs: [id],
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Map<String, dynamic> _toMap(RealizationItem realizationItem) {
    return {
      columnId: realizationItem.id,
      columnReportId: realizationItem.reportId,
      columnName: realizationItem.name,
      columnApproved: realizationItem.approved,
      columnRealization: realizationItem.realization,
      columnDifference: realizationItem.difference ? 1 : 0,
      columnDifferenceDescription: realizationItem.differenceDescription,
    };
  }

  RealizationItem _fromMap(Map<String, dynamic> map) {
    return RealizationItem(
      id: map[columnId] as int,
      reportId: map[columnReportId] as int,
      name: map[columnName] as String,
      approved: map[columnApproved] as int,
      realization: map[columnRealization] as int,
      difference: map[columnDifference] as int == 1,
      differenceDescription: map[columnDifferenceDescription] as String?,
    );
  }
}
