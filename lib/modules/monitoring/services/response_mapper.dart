import 'dart:io';

import 'package:intl/intl.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/models/select_choice.dart';

class ResponseMapper {
  static Report reportFromJson(Map<String, dynamic> json) {
    return Report()
      ..id = json['id'] as int?
      ..activityType = SelectChoice.getText(
          activityTypeList, json['jenis_kegiatan'] as String)
      ..activityName = json['nama_kegiatan'] as String
      ..executionMethod = SelectChoice.getText(
          executionMethodList, json['metode_pelaksanaan'] as String)
      ..monitoringDate =
          DateFormat('yyyy-MM-dd').parse(json['tgl_monitoring'] as String)
      ..monitoringPeriod = 'Periode ' + (json['periode'] as int).toString()
      ..activityStage = SelectChoice.getText(
          activityStageList, json['tahapan_dicapai'] as String)
      ..conclusion =
          SelectChoice.getText(conclusionList, json['kesimpulan'] as String)
      ..modifiedDate =
          DateFormat('yyyy-MM-dd').parse(json['tgl_monitoring'] as String)
      ..userId = json['user'] as int?
      ..projectId = json['proyek'] as int?
      ..authorName = json['user_name'] as String?;
  }

  static Map<String, dynamic> reportToJson(Report input) {
    return {
      'id': input.id,
      'proyek': input.projectId,
      'jenis_kegiatan':
          SelectChoice.getCode(activityTypeList, input.activityType!),
      'nama_kegiatan': input.activityName,
      'metode_pelaksanaan':
          SelectChoice.getCode(executionMethodList, input.executionMethod!),
      'tgl_monitoring': DateFormat('yyyy-MM-dd').format(input.monitoringDate!),
      'periode': int.tryParse(input.monitoringPeriod!.split(' ')[1]),
      'tahapan_dicapai':
          SelectChoice.getCode(activityStageList, input.activityStage!),
      'kesimpulan': SelectChoice.getCode(conclusionList, input.conclusion!),
      'user_name': input.authorName,
    };
  }

  static ActivityItem activityItemFromJson(Map<String, dynamic> json) {
    return ActivityItem(
      id: json['id'] as int?,
      name: json['nama_item'] as String,
      approved: json['persetujuan'].toString(),
      realization: json['realisasi'].toString(),
      difference: (json['perbedaan'] as String).fromYesNo(),
      differenceDescription: json['deskripsi_perbedaan'] as String,
    );
  }

  static Map<String, dynamic> activityItemToJson(ActivityItem input) {
    return {
      'id': input.id,
      'nama_item': input.name,
      'persetujuan': input.approved,
      'realisasi': input.realization,
      'perbedaan': input.difference.toYesNo(),
      'deskripsi_perbedaan': input.differenceDescription ?? "kosong",
    };
  }

  static RealizationItem realizationItemFromJson(Map<String, dynamic> json) {
    return RealizationItem(
      id: json['id'] as int?,
      name: json['nama_item'] as String,
      approved: json['persetujuan'] as int,
      realization: json['realisasi'] as int,
      difference: (json['perbedaan'] as String).fromYesNo(),
      differenceDescription: json['deskripsi_perbedaan'] as String,
    );
  }

  static Map<String, dynamic> realizationItemToJson(RealizationItem input) {
    return {
      'id': input.id,
      'nama_item': input.name,
      'persetujuan': input.approved,
      'realisasi': input.realization,
      'perbedaan': input.difference.toYesNo(),
      'deskripsi_perbedaan': input.differenceDescription ?? "kosong",
    };
  }

  static ActivityAnalisis activityAnalysisFromJson(Map<String, dynamic> json) {
    return ActivityAnalisis(
      id: json['id'] as int?,
      problem: json['masalah'] as String,
      recommendation: json['rekomendasi'] as String,
    );
  }

  static Map<String, dynamic> activityAnalysisToJson(ActivityAnalisis input) {
    return {
      'id': input.id,
      'masalah': input.problem,
      'rekomendasi': input.recommendation,
    };
  }

  static ReportImage reportImageFromJson(Map<String, dynamic> json) {
    return ReportImage(
      id: json['id'] as int?,
      image: File(json['file_path'] as String),
    );
  }

  static Map<String, dynamic> reportImageToJson(ReportImage input) {
    return {
      'id': input.id,
      'file_path': input.image.path,
    };
  }
}

extension _BooleanParsing on bool {
  String toYesNo() {
    return this ? 'ya' : 'tidak';
  }
}

extension _StringParsing on String {
  bool fromYesNo() {
    return this == 'ya';
  }
}
