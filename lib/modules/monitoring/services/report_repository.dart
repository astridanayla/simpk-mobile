import 'dart:async';

import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/modules/monitoring/services/report_offline_helper.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/services/network_info.dart';

class ReportRepository extends ReportService {
  ReportRepository({
    required this.localDataSource,
    required this.remoteDataSource,
    required this.networkInfo,
    required this.reportOfflineHelper,
  });

  final ReportHelper localDataSource;
  final MonitoringAPIProvider remoteDataSource;
  final NetworkInfo networkInfo;
  final ReportOfflineHelper reportOfflineHelper;

  @override
  Future<Report> insert(Report report) async {
    if (await networkInfo.isConnected()) {
      report = await remoteDataSource.insertReport(report);
      await localDataSource.insert(report);
    } else {
      await insertOffline(report);
    }
    return report;
  }

  @override
  Future<bool> delete(int id) async {
    if (await networkInfo.isConnected()) {
      final success = await remoteDataSource.deleteReport(id);
      if (success) {
        await localDataSource.deleteById(id);
      }
      return success;
    }
    return false;
  }

  @override
  Future<List<Report>> queryByProjectId(int projectId) async {
    if (await networkInfo.isConnected()) {
      final reports = await remoteDataSource.getReportsByProjectId(projectId);
      await localDataSource
          .deleteByNotInListOfId(reports.map((e) => e.id!).toList());
      for (final element in reports) {
        await localDataSource.insert(element);
      }
    }
    return localDataSource.queryByProjectId(projectId);
  }

  @override
  Future<Report> queryById(int id) async {
    if (await networkInfo.isConnected()) {
      final report = await remoteDataSource.getReportById(id);
      await localDataSource.insert(report);
    }
    return localDataSource.queryById(id);
  }

  @override
  Future<List<Report>> queryByProjectIdAndMonitoringPeriod(
      int projectId, String period) async {
    if (await networkInfo.isConnected()) {
      await queryByProjectId(projectId);
    }
    return localDataSource.queryByProjectIdAndMonitoringPeriod(
        projectId, period);
  }

  @override
  Future<List<String>> queryMonitoringPeriodByProjectId(int projectId) async {
    if (await networkInfo.isConnected()) {
      await queryByProjectId(projectId);
    }
    return localDataSource.queryMonitoringPeriodByProjectId(projectId);
  }

  @override
  Future<Report> insertOffline(Report report) async {
    return reportOfflineHelper.insert(report);
  }

  @override
  Future<List<Report>> queryAllOffline() async {
    return reportOfflineHelper.queryAll();
  }

  @override
  Future<Report> queryByIdOffline(int id) async {
    return reportOfflineHelper.queryById(id);
  }

  @override
  Future<int> deleteByIdOffline(int id) async {
    return reportOfflineHelper.deleteById(id);
  }

  @override
  Future<bool> deleteById(int id) async {
    if (await networkInfo.isConnected()) {
      return remoteDataSource.deleteReport(id);
    }
    return false;
  }
}
