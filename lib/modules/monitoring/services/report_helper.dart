import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/services/activity_analysis_helper.dart';
import 'package:simpk/modules/monitoring/services/activity_item_helper.dart';
import 'package:simpk/modules/monitoring/services/realization_item_helper.dart';
import 'package:simpk/modules/monitoring/services/report_image_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class ReportHelper {
  ReportHelper({
    required this.dbProvider,
    required this.activityItemHelper,
    required this.realizationItemHelper,
    required this.activityAnalysisHelper,
    required this.reportImageHelper,
  });

  final DatabaseProvider dbProvider;
  final ActivityItemHelper activityItemHelper;
  final RealizationItemHelper realizationItemHelper;
  final ActivityAnalysisHelper activityAnalysisHelper;
  final ReportImageHelper reportImageHelper;

  static const tableName = 'report';
  static const columnId = DatabaseProvider.columnId;
  static const columnProjectId = 'project_id';
  static const columnActivityType = 'activity_type';
  static const columnActivityName = 'activity_name';
  static const columnExecutionMethod = 'execution_method';
  static const columnMonitoringDate = 'monitoring_date';
  static const columnMonitoringPeriod = 'monitoring_period';
  static const columnActivityStage = 'activity_stage';
  static const columnConclusion = 'conclusion';
  static const columnModifiedDate = 'modified_date';
  static const columnAuthor = 'author_name';

  static const onCreateSQL = '''
    CREATE TABLE $tableName (
    $columnId               INTEGER PRIMARY KEY,
    $columnProjectId        INTEGER NOT NULL,
    $columnActivityType     TEXT    NOT NULL,
    $columnActivityName     TEXT    NOT NULL,
    $columnExecutionMethod  TEXT    NOT NULL,
    $columnMonitoringDate   INTEGER NOT NULL,
    $columnMonitoringPeriod TEXT    NOT NULL,
    $columnActivityStage    TEXT    NOT NULL,
    $columnConclusion       TEXT    NOT NULL,
    $columnModifiedDate     INTEGER NOT NULL,
    $columnAuthor           TEXT)
    ''';

  Future<Report> insert(Report report) async {
    Database db = await dbProvider.database;
    int reportId = await db.insert(
      tableName,
      _toMap(report),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    report.id = reportId;

    report.activityItems?.forEach((activityItem) async {
      activityItem.reportId = report.id;
      await activityItemHelper.insert(activityItem);
    });
    report.realizationItems?.forEach((realizationItem) async {
      realizationItem.reportId = report.id;
      await realizationItemHelper.insert(realizationItem);
    });
    report.activityAnalysis?.forEach((activityAnalysis) async {
      activityAnalysis.reportId = report.id;
      await activityAnalysisHelper.insert(activityAnalysis);
    });
    report.reportImages?.forEach((reportImage) async {
      reportImage.reportId = reportId;
      await reportImageHelper.insert(reportImage);
    });

    return report;
  }

  Future<int> deleteById(int id) async {
    Database db = await dbProvider.database;

    final relationTables = [
      [ActivityItemHelper.tableName, ActivityItemHelper.columnReportId],
      [RealizationItemHelper.tableName, RealizationItemHelper.columnReportId],
      [ActivityAnalysisHelper.tableName, ActivityAnalysisHelper.columnReportId],
      [ReportImageHelper.tableName, ReportImageHelper.columnReportId],
    ];
    for (List<String> table in relationTables) {
      await db.delete(
        table[0],
        where: "${table[1]} = ?",
        whereArgs: [id],
      );
    }

    return db.delete(
      tableName,
      where: "$columnId = ?",
      whereArgs: [id],
    );
  }

  Future<int> deleteByNotInListOfId(List<int> ids) async {
    Database db = await dbProvider.database;

    final relationTables = [
      [ActivityItemHelper.tableName, ActivityItemHelper.columnReportId],
      [RealizationItemHelper.tableName, RealizationItemHelper.columnReportId],
      [ActivityAnalysisHelper.tableName, ActivityAnalysisHelper.columnReportId],
      [ReportImageHelper.tableName, ReportImageHelper.columnReportId],
    ];
    for (List<String> table in relationTables) {
      await db.delete(
        table[0],
        where: "${table[1]} NOT IN (${ids.map((_) => '?').join(', ')})",
        whereArgs: ids,
      );
    }

    return db.delete(
      tableName,
      where: "$columnId NOT IN (${ids.map((_) => '?').join(', ')})",
      whereArgs: ids,
    );
  }

  Future<List<Report>> queryByProjectId(int projectId) async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(
      tableName,
      where: '$columnProjectId = ?',
      whereArgs: [projectId],
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<Report> queryById(int id) async {
    Database db = await dbProvider.database;
    final mapList = await db.query(
      tableName,
      where: '$columnId = ?',
      whereArgs: [id],
    );
    final report = _fromMap(mapList[0]);

    report.activityItems = await activityItemHelper.queryByReportId(id);
    report.realizationItems = await realizationItemHelper.queryByReportId(id);
    report.activityAnalysis = await activityAnalysisHelper.queryByReportId(id);
    report.reportImages = await reportImageHelper.queryByReportId(id);

    return report;
  }

  Future<List<Report>> queryByProjectIdAndMonitoringPeriod(
      int projectId, String period) async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(
      tableName,
      where: '$columnProjectId = ? AND $columnMonitoringPeriod = ?',
      whereArgs: [projectId, period],
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<List<String>> queryMonitoringPeriodByProjectId(int projectId) async {
    Database db = await dbProvider.database;
    final mapList = await db.query(
      tableName,
      columns: [columnMonitoringPeriod],
      where: '$columnProjectId = ?',
      whereArgs: [projectId],
      distinct: true,
      orderBy: columnMonitoringPeriod,
    );
    return mapList.map((e) => e[columnMonitoringPeriod] as String).toList();
  }

  Map<String, dynamic> _toMap(Report report) {
    return {
      columnId: report.id,
      columnProjectId: report.projectId,
      columnActivityType: report.activityType,
      columnActivityName: report.activityName,
      columnExecutionMethod: report.executionMethod,
      columnMonitoringDate: report.monitoringDate?.millisecondsSinceEpoch,
      columnMonitoringPeriod: report.monitoringPeriod,
      columnActivityStage: report.activityStage,
      columnConclusion: report.conclusion,
      columnModifiedDate: report.modifiedDate?.millisecondsSinceEpoch,
      columnAuthor: report.authorName,
    };
  }

  Report _fromMap(Map<String, dynamic> map) {
    return Report()
      ..id = map[columnId] as int
      ..projectId = map[columnProjectId] as int
      ..activityType = map[columnActivityType] as String
      ..activityName = map[columnActivityName] as String
      ..executionMethod = map[columnExecutionMethod] as String
      ..monitoringDate =
          DateTime.fromMillisecondsSinceEpoch(map[columnMonitoringDate] as int)
      ..monitoringPeriod = map[columnMonitoringPeriod] as String
      ..activityStage = map[columnActivityStage] as String
      ..conclusion = map[columnConclusion] as String
      ..modifiedDate =
          DateTime.fromMillisecondsSinceEpoch(map[columnModifiedDate] as int)
      ..authorName = map[columnAuthor] as String;
  }
}
