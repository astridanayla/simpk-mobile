import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class ActivityAnalysisHelper {
  ActivityAnalysisHelper({required this.dbProvider});

  final DatabaseProvider dbProvider;

  static const tableName = 'activity_analysis';

  static const columnId = DatabaseProvider.columnId;
  static const columnReportId = 'report_id';
  static const columnProblem = 'problem';
  static const columnRecommendation = 'recommendation';

  static const onCreateSQL = '''
    CREATE TABLE $tableName (
    $columnId                     INTEGER PRIMARY KEY,
    $columnReportId               INTEGER NOT NULL,
    $columnProblem                TEXT    NOT NULL,
    $columnRecommendation         TEXT    NOT NULL,
    FOREIGN KEY ($columnReportId)
      REFERENCES ${ReportHelper.tableName} (${ReportHelper.columnId}));
    ''';

  Future<int> insert(ActivityAnalisis activityAnalysis) async {
    Database db = await dbProvider.database;
    return db.insert(
      tableName,
      _toMap(activityAnalysis),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<ActivityAnalisis>> queryAll() async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(tableName);
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<List<ActivityAnalisis>> queryByReportId(int id) async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(
      tableName,
      where: '$columnReportId = ?',
      whereArgs: [id],
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Map<String, dynamic> _toMap(ActivityAnalisis activityAnalysis) {
    return {
      columnId: activityAnalysis.id,
      columnReportId: activityAnalysis.reportId,
      columnProblem: activityAnalysis.problem,
      columnRecommendation: activityAnalysis.recommendation,
    };
  }

  ActivityAnalisis _fromMap(Map<String, dynamic> map) {
    return ActivityAnalisis()
      ..id = map[columnId] as int
      ..reportId = map[columnReportId] as int
      ..problem = map[columnProblem] as String
      ..recommendation = map[columnRecommendation] as String;
  }
}
