import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class ActivityItemHelper {
  ActivityItemHelper({required this.dbProvider});

  static const tableName = 'activity_item';

  static const columnId = DatabaseProvider.columnId;
  static const columnReportId = 'report_id';
  static const columnName = 'name';
  static const columnApproved = 'approved';
  static const columnRealization = 'realization';
  static const columnDifference = 'difference';
  static const columnDifferenceDescription = 'difference_desc';

  static const onCreateSQL = '''
    CREATE TABLE $tableName (
    $columnId                     INTEGER PRIMARY KEY,
    $columnReportId               INTEGER NOT NULL,
    $columnName                   TEXT    NOT NULL,
    $columnApproved               TEXT    NOT NULL,
    $columnRealization            TEXT    NOT NULL,
    $columnDifference             INTEGER NOT NULL,
    $columnDifferenceDescription  TEXT,
    FOREIGN KEY ($columnReportId)
      REFERENCES ${ReportHelper.tableName} (${ReportHelper.columnId}));
    ''';

  final DatabaseProvider dbProvider;

  Future<int> insert(ActivityItem activityItem) async {
    Database db = await dbProvider.database;
    return db.insert(
      tableName,
      _toMap(activityItem),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<ActivityItem>> queryAll() async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(tableName);
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Future<List<ActivityItem>> queryByReportId(int id) async {
    Database db = await dbProvider.database;
    List<Map<String, dynamic>> mapList = await db.query(
      tableName,
      where: '$columnReportId = ?',
      whereArgs: [id],
    );
    return mapList.map((e) => _fromMap(e)).toList();
  }

  Map<String, dynamic> _toMap(ActivityItem activityItem) {
    return {
      columnId: activityItem.id,
      columnReportId: activityItem.reportId,
      columnName: activityItem.name,
      columnApproved: activityItem.approved,
      columnRealization: activityItem.realization,
      columnDifference: activityItem.difference ? 1 : 0,
      columnDifferenceDescription: activityItem.differenceDescription,
    };
  }

  ActivityItem _fromMap(Map<String, dynamic> map) {
    return ActivityItem(
      id: map[columnId] as int,
      reportId: map[columnReportId] as int,
      name: map[columnName] as String,
      approved: map[columnApproved] as String,
      realization: map[columnRealization] as String,
      difference: map[columnDifference] as int == 1,
      differenceDescription: map[columnDifferenceDescription] as String?,
    );
  }
}
