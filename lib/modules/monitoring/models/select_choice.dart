class SelectChoice {
  const SelectChoice(this.code, this.text);

  final String code;
  final String text;

  static String? getCode(List<SelectChoice> choices, String text) {
    final filtered = choices.where((element) => element.text == text);
    if (filtered.isEmpty)
      return null;
    else
      return filtered.first.code;
  }

  static String? getText(List<SelectChoice> choices, String code) {
    final filtered = choices.where((element) => element.code == code);
    if (filtered.isEmpty)
      return null;
    else
      return filtered.first.text;
  }

  static List<String> getTexts(List<SelectChoice> choices) {
    return choices.map((e) => e.text).toList();
  }
}

final activityTypeList = [
  SelectChoice("kendaraan", "Kendaraan"),
  SelectChoice("sarana_prasarana", "Sarana dan Prasarana"),
  SelectChoice("kegiatan", "Kegiatan"),
  SelectChoice("lainnya", "Lainnya")
];

final executionMethodList = [
  SelectChoice('pemeriksaan',
      'Pemeriksaan Laporan dari Mitra Kemaslahatan / Penerima Manfaat'),
  SelectChoice('cek_fisik', 'Cek Fisik oleh Bidang Kemaslahatan'),
  SelectChoice('laporan_pihak_ketiga', 'Laporan Konsultan/pihak ketiga'),
  SelectChoice('lainnya', 'Lainnya'),
];

final activityStageList = [
  SelectChoice(
      'pembayaran', 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'),
  SelectChoice(
      'penerimaan',
      'Penerimaan Kendaraan / Mesin / Peralatan oleh Mitra Kemaslahatan / ' +
          'Penerima manfaat'),
];

final conclusionList = [
  SelectChoice(
      'sesuai',
      """Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH dan
          dapat dilanjutkan ke tahap selanjutnya"""
          .replaceAll(RegExp(r"\s+"), " ")),
  SelectChoice(
      'belum_sesuai',
      """Progres Kegiatan Kemaslahatan belum sesuai dengan persetujuan dari
          BPKH dan tidak dapat dilanjutkan ke tahap selanjutnya"""
          .replaceAll(RegExp(r"\s+"), " ")),
  SelectChoice(
      'belum_sesuai_lanjut',
      """Progres Kegiatan Kemaslahatan belum sesuai dengan persetujuan dari BPKH
          dan masih dapat dilanjutkan ke tahap selanjutnya dengan melakukan
          perbaikan sesuai rekomendasi yang di jelaskan pada Tabel Analisa
          Pelaksanaan Kegiatan di atas untuk ditindak lanjuti oleh Mitra
          Kemaslahatan/Penerima Manfaat"""
          .replaceAll(RegExp(r"\s+"), " ")),
];
