class RealizationItem {
  RealizationItem({
    this.name = '',
    this.approved = 0,
    this.realization = 0,
    this.difference = false,
    this.id,
    this.reportId,
    this.differenceDescription,
  })  : assert(approved == realization || difference),
        assert(!difference || differenceDescription != null);

  int? id;
  int? reportId;
  String name;
  int approved;
  int realization;
  bool difference;
  String? differenceDescription;
}
