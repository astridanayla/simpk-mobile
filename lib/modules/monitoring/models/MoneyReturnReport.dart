import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MoneyReturnReport {
  MoneyReturnReport({
    required this.id,
    required this.title,
    required this.description,
    required this.userId,
    required this.userName,
    this.projectId,
    this.image,
    this.imageFile,
    this.lastUpdated,
  });

  factory MoneyReturnReport.fromJson(Map<String, dynamic> json) {
    const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
        defaultValue: 'https://simpk-staging.herokuapp.com');

    return MoneyReturnReport(
      id: json["id"].toString(),
      title: json["judul"].toString(),
      description: json["deskripsi"].toString(),
      userId: json["user"].toString(),
      userName: json["user_name"].toString(),
      projectId: json["proyek"].toString(),
      image: CachedNetworkImage(
        imageUrl: _baseUrl + json["bukti_foto"].toString(),
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            CircularProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) => Center(
          child: Text(
            "Gagal mendownload gambar!",
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ),
      lastUpdated: DateTime.parse(json["terakhir_diupdate"].toString()),
    );
  }

  factory MoneyReturnReport.copy(MoneyReturnReport oldReport) {
    return MoneyReturnReport(
      id: oldReport.id,
      title: oldReport.title,
      description: oldReport.description,
      userId: oldReport.userId,
      userName: oldReport.userName,
      projectId: oldReport.projectId,
      image: oldReport.image,
      imageFile: oldReport.imageFile,
      lastUpdated: oldReport.lastUpdated,
    );
  }

  String id;
  String title;
  String description;
  String userId;
  String userName;
  String? projectId;
  CachedNetworkImage? image;
  File? imageFile;
  DateTime? lastUpdated;
}
