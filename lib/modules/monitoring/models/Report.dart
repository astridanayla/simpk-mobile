import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';

class Report {
  int? id;
  int? projectId;
  int? userId;
  String? activityType;
  String? activityName;
  String? executionMethod;
  DateTime? monitoringDate;
  String? monitoringPeriod;
  String? activityStage;
  List<ActivityItem>? activityItems;
  List<RealizationItem>? realizationItems;
  List<ActivityAnalisis>? activityAnalysis;
  List<ReportImage>? reportImages;
  String? conclusion;
  DateTime? modifiedDate;
  String? authorName;
}
