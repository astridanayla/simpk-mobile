class ActivityAnalisis {
  ActivityAnalisis({
    this.id,
    this.reportId,
    this.problem = "",
    this.recommendation = "",
  });

  int? id;
  int? reportId;
  String problem = "";
  String recommendation = "";
}
