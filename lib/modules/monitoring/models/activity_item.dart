class ActivityItem {
  ActivityItem({
    this.name = '',
    this.approved = '',
    this.realization = '',
    this.difference = false,
    this.id,
    this.reportId,
    this.differenceDescription,
  })  : assert(approved == realization || difference),
        assert(!difference || differenceDescription != null);

  int? id;
  int? reportId;
  String name;
  String approved;
  String realization;
  bool difference;
  String? differenceDescription;
}
