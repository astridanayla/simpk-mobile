import 'dart:io';

class ReportImage {
  ReportImage({required this.image, this.id, this.reportId});

  int? id;
  int? reportId;
  File image;
}
