import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';

typedef void _OnDelete(int index);

enum ActivityItemFormType { text, number }

class ActivityItemForm extends StatefulWidget {
  ActivityItemForm({
    required this.activityItem,
    required this.onDelete,
    this.type = ActivityItemFormType.text,
    Key? key,
  }) : super(key: key);

  final ActivityItem activityItem;
  final _OnDelete onDelete;
  final ActivityItemFormType type;
  final state = _ActivityItemFormState();

  @override
  _ActivityItemFormState createState() => state;

  bool isValid() => state.validate();

  void updateState({int? itemIndex, bool? showDelete}) {
    state.updateState(itemIndex: itemIndex, showDelete: showDelete);
  }
}

class _ActivityItemFormState extends State<ActivityItemForm> {
  late int itemIndex;
  late bool showDelete;
  final _formKey = GlobalKey<FormState>();
  final _approvedController = TextEditingController();
  final _realizationController = TextEditingController();

  bool? _selectedDiff;

  @override
  void initState() {
    super.initState();

    _approvedController.text = widget.activityItem.approved;
    _realizationController.text = widget.activityItem.realization;
    if (widget.activityItem.approved.isNotEmpty)
      _selectedDiff = widget.activityItem.difference;
  }

  @override
  void dispose() {
    _approvedController.dispose();
    _realizationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (itemIndex > 0) Divider(),
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Item ${itemIndex + 1}',
                style: Theme.of(context).textTheme.headline4,
              ),
              if (showDelete)
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(
                    maxHeight: 24,
                    maxWidth: 24,
                  ),
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    widget.onDelete(itemIndex);
                  },
                  color: Theme.of(context).errorColor,
                ),
            ],
          ),
          SizedBox(height: 16),
          _buildTextField(
            label: 'Nama Item',
            initialValue: widget.activityItem.name,
            onSaved: (value) {
              widget.activityItem.name = value!;
            },
          ),
          SizedBox(height: 16),
          _buildTextField(
            label: 'Persetujuan BPKH',
            controller: _approvedController,
            onSaved: (value) {
              widget.activityItem.approved = value!;
            },
            keyboardType: widget.type == ActivityItemFormType.number
                ? TextInputType.number
                : TextInputType.text,
          ),
          SizedBox(height: 16),
          _buildTextField(
            label: 'Realisasi',
            controller: _realizationController,
            onSaved: (value) {
              widget.activityItem.realization = value!;
            },
            otherValidator: (value) {
              String approved = _approvedController.text;
              if (_selectedDiff != null) {
                if (value == approved && _selectedDiff!) {
                  return 'Persetujuan dan realisasi bernilai sama';
                } else if (value != approved && !_selectedDiff!) {
                  return 'Persetujuan dan realisasi bernilai beda';
                }
              }
            },
            keyboardType: widget.type == ActivityItemFormType.number
                ? TextInputType.number
                : TextInputType.text,
          ),
          SizedBox(height: 16),
          Text(
            'Ada Perbedaan di Persetujuan dan Realisasi?',
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Radio<bool>(
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          _selectedDiff = value;
                        });
                      }
                    },
                    groupValue: _selectedDiff,
                    value: true,
                    visualDensity: VisualDensity.compact,
                  ),
                  Text(
                    'Ya',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
              Row(
                children: [
                  Radio<bool>(
                    onChanged: (value) {
                      if (value != null) {
                        setState(() {
                          _selectedDiff = value;
                        });
                      }
                    },
                    groupValue: _selectedDiff,
                    value: false,
                    visualDensity: VisualDensity.compact,
                  ),
                  Text(
                    'Tidak',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  SizedBox(width: 48),
                ],
              ),
            ],
          ),
          SizedBox(height: 12),
          if (_selectedDiff != null && _selectedDiff!)
            _buildTextField(
              label: 'Deskripsi Perbedaan',
              initialValue: widget.activityItem.differenceDescription,
              maxLines: 5,
              onSaved: (value) {
                widget.activityItem.differenceDescription = value!;
              },
            ),
          if (_selectedDiff != null && _selectedDiff!) SizedBox(height: 16),
        ],
      ),
    );
  }

  bool validate() {
    final isValid = _formKey.currentState?.validate();
    final isOtherValid = _selectedDiff != null;
    if (isValid != null && isValid && isOtherValid) {
      _formKey.currentState?.save();
      widget.activityItem.difference = _selectedDiff!;
      return true;
    }
    return false;
  }

  void updateState({int? itemIndex, bool? showDelete}) {
    if (mounted) {
      setState(() {
        if (itemIndex != null) this.itemIndex = itemIndex;
        if (showDelete != null) this.showDelete = showDelete;
      });
    } else {
      if (itemIndex != null) this.itemIndex = itemIndex;
      if (showDelete != null) this.showDelete = showDelete;
    }
  }

  Widget _buildTextField({
    required String label,
    TextEditingController? controller,
    String? initialValue,
    int? maxLines,
    void Function(String? value)? onSaved,
    String? Function(String? value)? otherValidator,
    TextInputType keyboardType = TextInputType.text,
    int maxLength = 30,
  }) {
    String _formatNumber(String s) {
      final num =
          NumberFormat.decimalPattern('id_ID').format(int.tryParse(s) ?? -1);
      if (num == '-1') {
        return s;
      } else {
        return num;
      }
    }

    return TextFormField(
      maxLines: maxLines,
      controller: controller,
      initialValue: initialValue,
      keyboardType: keyboardType,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '$label tidak boleh kosong';
        } else if (keyboardType == TextInputType.number) {
          String numStr = value.replaceAll('.', '');
          if (!RegExp(r'^\d+$').hasMatch(numStr)) {
            return '$label harus angka';
          } else if (int.tryParse(numStr) == null) {
            return 'Angka $label terlalu besar';
          }
        } else if (keyboardType == TextInputType.text &&
            maxLines == null &&
            value.length > maxLength) {
          return '$label maksimal $maxLength karakter';
        } else if (otherValidator != null) {
          return otherValidator(value);
        }
        return null;
      },
      onChanged: keyboardType == TextInputType.number
          ? (value) {
              value = '${_formatNumber(value.replaceAll('.', ''))}';
              controller?.value = TextEditingValue(
                text: value,
                selection: TextSelection.collapsed(offset: value.length),
              );
            }
          : null,
      onSaved: onSaved,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        prefixText: keyboardType == TextInputType.number ? 'Rp ' : null,
        labelText: label,
        alignLabelWithHint: true,
      ),
    );
  }
}
