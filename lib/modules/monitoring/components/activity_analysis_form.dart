import 'package:flutter/material.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';

typedef void _OnDelete(int index);

class ActivityAnalysisForm extends StatefulWidget {
  ActivityAnalysisForm({
    required Key key,
    required this.activityAnalysis,
    required this.onDelete,
  }) : super(key: key);

  final _OnDelete onDelete;
  final ActivityAnalisis activityAnalysis;
  final state = _ActivityAnalysisFormState();

  @override
  _ActivityAnalysisFormState createState() => state;

  set index(int index) => state.setIndex(index);
  int get index => state.index;

  bool isValid() => state.isValid();
}

class _ActivityAnalysisFormState extends State<ActivityAnalysisForm> {
  final _formKey = GlobalKey<FormState>();
  late int index;

  @override
  void initState() {
    super.initState();
  }

  void setIndex(int newIndex) {
    if (mounted) {
      setState(() {
        this.index = newIndex;
      });
    } else {
      this.index = newIndex;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    "Poin " + (this.index + 1).toString(),
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(
                    maxHeight: 24,
                    maxWidth: 24,
                  ),
                  icon: Icon(Icons.delete),
                  color: Theme.of(context).errorColor,
                  onPressed: () => widget.onDelete(index),
                ),
              ],
            ),
            // Problems Text Field
            _buildTextField(
              label: 'Masalah / Kendala / Hambatan yang Dihadapi',
              initialValue: widget.activityAnalysis.problem,
              onSaved: (val) {
                widget.activityAnalysis.problem = val!;
              },
            ),
            // Recommendation TextArea
            _buildTextField(
              label: 'Rekomendasi',
              initialValue: widget.activityAnalysis.recommendation,
              maxLines: 6,
              onSaved: (val) {
                widget.activityAnalysis.recommendation = val!;
              },
            ),
            Container(
              margin: EdgeInsets.only(top: 16),
              child: Divider(),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTextField({
    required String label,
    required void Function(String?) onSaved,
    String? initialValue,
    int maxLines = 1,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: TextFormField(
        initialValue: initialValue,
        maxLines: maxLines,
        onSaved: onSaved,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return '$label tidak boleh kosong';
          }
          return null;
        },
        autovalidateMode: AutovalidateMode.onUserInteraction,
        textInputAction: TextInputAction.done,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: label,
          alignLabelWithHint: true,
        ),
      ),
    );
  }

  bool isValid() {
    final isValid = _formKey.currentState?.validate();
    if (isValid == true) {
      _formKey.currentState?.save();
      return true;
    }
    return false;
  }
}
