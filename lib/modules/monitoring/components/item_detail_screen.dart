import 'package:flutter/material.dart';
import 'package:simpk/components/scroll_app_bar.dart';

abstract class ItemDetailScreen<T> extends StatefulWidget {
  ItemDetailScreen(T this.item);

  final T item;

  @override
  ItemDetailScreenState createState();
}

abstract class ItemDetailScreenState<T> extends State<ItemDetailScreen<T>> {
  final _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ScrollAppBar(scrollController: _scrollController),
      body: body(context),
    );
  }

  Widget body(BuildContext context);

  Widget buildInfo(BuildContext context, String label, String text) {
    return Padding(
      padding: EdgeInsets.only(top: 4, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          const SizedBox(height: 8),
          Text(text, style: Theme.of(context).textTheme.bodyText1),
        ],
      ),
    );
  }
}
