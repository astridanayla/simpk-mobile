import 'package:flutter/material.dart';
import 'package:simpk/modules/monitoring/components/activity_item_form.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/screens/create_report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_4.dart';

class CreateReport3Screen extends CreateReportScreen {
  CreateReport3Screen({required Report report, Key? key})
      : super(key: key, report: report);

  @override
  _CreateReport3ScreenState createState() => _CreateReport3ScreenState();

  @override
  int get pageNumber => 3;
}

class _CreateReport3ScreenState
    extends CreateReportScreenState<CreateReport3Screen> {
  List<ActivityItemForm> _activityItemForms = [];

  @override
  void initState() {
    super.initState();

    report.realizationItems?.forEach((element) {
      _onAddItem(_toActivityItem(element));
    });
    if (_activityItemForms.isEmpty) {
      _onAddItem();
    }
    print(_activityItemForms.length);
  }

  @override
  List<Widget> buildChildren(BuildContext context) {
    return [
      _buildActivityItemForms(),
    ];
  }

  @override
  void goNext(BuildContext context) async {
    List<RealizationItem> realizationItems = _activityItemForms.map((form) {
      return _toRealizationItem(form.activityItem);
    }).toList();

    report..realizationItems = realizationItems;

    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CreateReport4Screen(report: report)));
    if (result == true) {
      Navigator.of(context).pop(true);
    }
  }

  @override
  void goBack(BuildContext context) {
    List<RealizationItem> elements = [];
    _activityItemForms.forEach((element) {
      if (element.isValid()) {
        elements.add(_toRealizationItem(element.activityItem));
      }
    });
    report..realizationItems = elements;
  }

  @override
  bool valuesAreValid() {
    bool isItemsValid = true;
    _activityItemForms.forEach((element) {
      isItemsValid &= element.isValid();
    });

    return isItemsValid;
  }

  void _onAddItem([ActivityItem? item]) {
    if (item == null) item = ActivityItem();

    setState(() {
      _activityItemForms.add(ActivityItemForm(
        key: UniqueKey(),
        activityItem: item!,
        onDelete: _onDelete,
        type: ActivityItemFormType.number,
      ));
    });
    _refreshItemState();
  }

  void _onDelete(int index) {
    setState(() {
      _activityItemForms.removeAt(index);
    });
    _refreshItemState();
  }

  Widget _buildActivityItemForms() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Realisasi Penggunaan Dana',
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 4),
        ..._activityItemForms,
        SizedBox(height: 8),
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(bottom: 48),
          child: buildAddItemButton(
            'Tambah Item',
            _onAddItem,
            key: Key('btn_add_item'),
          ),
        ),
      ],
    );
  }

  void _refreshItemState() {
    bool showDelete = _activityItemForms.length > 1;
    for (int index = 0; index < _activityItemForms.length; index++) {
      _activityItemForms[index].updateState(
        itemIndex: index,
        showDelete: showDelete,
      );
    }
  }

  ActivityItem _toActivityItem(RealizationItem item) {
    return ActivityItem(
      name: item.name,
      approved: item.approved.toString(),
      realization: item.realization.toString(),
      difference: item.difference,
      differenceDescription: item.differenceDescription,
    );
  }

  RealizationItem _toRealizationItem(ActivityItem item) {
    return RealizationItem(
      name: item.name,
      approved: int.parse(item.approved.replaceAll('.', '')),
      realization: int.parse(item.realization.replaceAll('.', '')),
      difference: item.difference,
      differenceDescription: item.differenceDescription,
    );
  }
}
