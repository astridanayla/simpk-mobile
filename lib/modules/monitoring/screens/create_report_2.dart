import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simpk/modules/monitoring/components/activity_item_form.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/select_choice.dart';
import 'package:simpk/modules/monitoring/screens/create_report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_3.dart';

class CreateReport2Screen extends CreateReportScreen {
  CreateReport2Screen({required Report report, Key? key})
      : super(key: key, report: report);

  @override
  _CreateReport2ScreenState createState() => _CreateReport2ScreenState();

  @override
  int get pageNumber => 2;
}

class _CreateReport2ScreenState
    extends CreateReportScreenState<CreateReport2Screen> {
  String? _selectedActivityStage;
  List<ActivityItemForm> _activityItemForms = [];

  @override
  void initState() {
    super.initState();

    _selectedActivityStage = report.activityStage;
    report.activityItems?.forEach((element) {
      _onAddItem(element);
    });
    if (_activityItemForms.isEmpty) {
      _onAddItem();
    }
  }

  @override
  List<Widget> buildChildren(BuildContext context) {
    return [
      _buildActivityStageSection(),
      SizedBox(height: 40),
      _buildActivityItemForms(),
    ];
  }

  @override
  void goNext(BuildContext context) async {
    report
      ..activityStage = _selectedActivityStage!
      ..activityItems = _activityItemForms.map((e) => e.activityItem).toList();

    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CreateReport3Screen(report: report)));
    if (result == true) {
      Navigator.of(context).pop(true);
    }
  }

  @override
  void goBack(BuildContext context) {
    List<ActivityItem> elements = [];
    _activityItemForms.forEach((element) {
      if (element.isValid()) {
        elements.add(element.activityItem);
      }
    });
    report
      ..activityStage = _selectedActivityStage
      ..activityItems = elements;
  }

  @override
  bool valuesAreValid() {
    final isActivityStageValid = _selectedActivityStage != null;
    bool isItemsValid = true;
    _activityItemForms.forEach((element) {
      isItemsValid &= element.isValid();
    });

    return isActivityStageValid && isItemsValid;
  }

  Widget _buildActivityStageSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Tahapan Kegiatan yang Akan Dicapai',
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 12),
        ...SelectChoice.getTexts(activityStageList)
            .map((label) => RadioListTile(
                  contentPadding: EdgeInsets.all(0),
                  value: label,
                  groupValue: _selectedActivityStage,
                  onChanged: _setActivityStage,
                  title: Text(
                    label,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                )),
      ],
    );
  }

  void _onAddItem([ActivityItem? item]) {
    if (item == null) item = ActivityItem();

    setState(() {
      _activityItemForms.add(ActivityItemForm(
        key: UniqueKey(),
        activityItem: item!,
        onDelete: _onDelete,
      ));
    });
    _refreshItemState();
  }

  void _setActivityStage(String? value) {
    if (value != null) {
      setState(() {
        _selectedActivityStage = value;
      });
    }
  }

  void _onDelete(int index) {
    setState(() {
      _activityItemForms.removeAt(index);
    });
    _refreshItemState();
  }

  Widget _buildActivityItemForms() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Rincian Kegiatan yang Dilakukan',
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 4),
        ..._activityItemForms,
        SizedBox(height: 8),
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(bottom: 48),
          child: buildAddItemButton(
            'Tambah Item',
            _onAddItem,
            key: Key('btn_add_item'),
          ),
        ),
      ],
    );
  }

  void _refreshItemState() {
    bool showDelete = _activityItemForms.length > 1;
    for (int index = 0; index < _activityItemForms.length; index++) {
      _activityItemForms[index].updateState(
        itemIndex: index,
        showDelete: showDelete,
      );
    }
  }
}
