import 'package:flutter/material.dart';
import 'package:simpk/modules/monitoring/components/item_detail_screen.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';

class ActivityAnalysisDetailScreen extends ItemDetailScreen<ActivityAnalisis> {
  ActivityAnalysisDetailScreen(ActivityAnalisis item) : super(item);

  @override
  _ActivityAnalysisDetailScreenState createState() =>
      _ActivityAnalysisDetailScreenState();
}

class _ActivityAnalysisDetailScreenState
    extends ItemDetailScreenState<ActivityAnalisis> {
  @override
  Widget body(BuildContext context) {
    return ListView(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 24),
      children: [
        Text(
          'Analisa Pelaksanaan Kegiatan',
          style: Theme.of(context).textTheme.headline1,
        ),
        const SizedBox(height: 28),
        buildInfo(context, 'Masalah / Kendala / Hambatan yang Dihadapi',
            widget.item.problem),
        buildInfo(context, 'Rekomendasi', widget.item.recommendation),
      ],
    );
  }
}
