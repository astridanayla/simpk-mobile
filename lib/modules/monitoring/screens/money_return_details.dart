import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:simpk/components/input_form_field.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/services/image_compressor.dart';

class MoneyReturnDetails extends StatefulWidget {
  MoneyReturnDetails({
    required this.initMoneyReturnReport,
    Key? key,
  }) : super(key: key);

  final MoneyReturnReport initMoneyReturnReport;
  final _MoneyReturnReportState state = _MoneyReturnReportState();

  @override
  _MoneyReturnReportState createState() => state;
}

class _MoneyReturnReportState extends State<MoneyReturnDetails> {
  final MoneyReturnAPIProvider _moneyReturnAPI = GetIt.I();
  bool _isEdit = false;
  late MoneyReturnReport moneyReturnReport;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  final ImagePicker _picker = ImagePicker();
  bool _isLoadingPutMoneyReturnReport = false;

  @override
  void initState() {
    moneyReturnReport = MoneyReturnReport.copy(widget.initMoneyReturnReport);
    _titleController.text = moneyReturnReport.title;
    _descriptionController.text = moneyReturnReport.description;
    super.initState();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _isEdit ? null : AppBar(elevation: 0),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(0, 8, 0, 16),
        child: _isEdit
            ? _buildEditMoneyReturnDetails(context)
            : _buildMoneyReturnDetails(context),
      ),
      bottomNavigationBar: _isEdit ? _buildEditBottomNavbar() : null,
    );
  }

  void _changeScreen() {
    setState(() {
      _isEdit = !_isEdit;
    });
  }

  Future<void> _pickImage(ImageSource source) async {
    var selected = await _picker.getImage(source: source);

    if (selected != null) {
      File compressedImage = (await compressImage(File(selected.path)))!;
      setImage(compressedImage);
    }
  }

  void setImage(File file) {
    setState(() {
      try {
        moneyReturnReport.imageFile!.delete();
      } catch (e) {
        debugPrint(
            "MoneyReturnReportEdit: " + "Image file to be deleted not found!");
      }
      moneyReturnReport.imageFile = file;
    });
  }

  void _toggleIsLoadingMoneyReturnReports() {
    setState(() {
      _isLoadingPutMoneyReturnReport = !_isLoadingPutMoneyReturnReport;
    });
  }

  void _setMoneyReturnReport(MoneyReturnReport newMoneyReturnReport) {
    setState(() {
      moneyReturnReport = newMoneyReturnReport;
    });
  }

  Future<Map<String, dynamic>> putMoneyReturnReport() async {
    moneyReturnReport.title = _titleController.text;
    moneyReturnReport.description = _descriptionController.text;
    Map<String, dynamic> response =
        await _moneyReturnAPI.putMoneyReturnReport(moneyReturnReport);
    return response;
  }

  Future<bool> deleteMoneyReturnReport() async {
    bool success =
        await _moneyReturnAPI.deleteMoneyReturnReport(moneyReturnReport.id);
    return success;
  }

  String _getFormatedLastEdited() {
    String formattedTime =
        DateFormat('dd/MM/yyyy').format(moneyReturnReport.lastUpdated!);
    return "Terakhir diedit: " + formattedTime;
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildsnackbar(
      BuildContext context, Widget content) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }

  Widget _buildEditMoneyReturnDetails(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 56, 16, 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Edit Pengembalian Uang",
            style: Theme.of(context).textTheme.headline1,
          ),
          _buildFormFields(context),
          _buildImageAndImageButton(context),
        ],
      ),
    );
  }

  Widget _buildFormFields(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            InputFormField(
              label: 'Judul',
              autovalidateMode: AutovalidateMode.always,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Judul tidak boleh kosong.';
                }
                return null;
              },
              controller: _titleController,
            ),
            SizedBox(height: 8),
            InputFormField(
              label: 'Deskripsi',
              autovalidateMode: AutovalidateMode.always,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Deskripsi tidak boleh kosong.';
                }
                return null;
              },
              controller: _descriptionController,
              maxLines: 6,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildImageAndImageButton(BuildContext context) {
    return Column(
      children: [
        moneyReturnReport.imageFile != null
            ? Image.file(moneyReturnReport.imageFile!)
            : moneyReturnReport.image != null
                ? moneyReturnReport.image!
                : Container(),
        SizedBox(height: 16),
        Container(
          child: OutlinedButton(
            onPressed: () {
              _pickImage(ImageSource.camera);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.edit),
                SizedBox(width: 8),
                Text("Ganti foto bukti")
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildMoneyReturnDetails(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildHeader(context),
              SizedBox(height: 12),
              Text(
                _getFormatedLastEdited(),
                style: Theme.of(context).textTheme.bodyText1?.copyWith(
                      color: Color(0xFF828282),
                    ),
              ),
              SizedBox(height: 24),
              _buildReportMadeBy(context),
              SizedBox(height: 32),
              _buildDescription(context),
              SizedBox(height: 32),
            ],
          ),
        ),
        _buildMoneyReturnImage(),
      ],
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            moneyReturnReport.title,
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        IconButton(
          key: Key('edit_button'),
          padding: EdgeInsets.zero,
          constraints: BoxConstraints(
            maxHeight: 24,
            maxWidth: 24,
          ),
          icon: Icon(Icons.edit, size: 24),
          onPressed: () {
            _changeScreen();
          },
          color: Theme.of(context).primaryColor,
        ),
        SizedBox(width: 16),
        IconButton(
          key: Key('btn_delete'),
          padding: EdgeInsets.zero,
          constraints: BoxConstraints(
            maxHeight: 24,
            maxWidth: 24,
          ),
          icon: Icon(Icons.delete, size: 24),
          onPressed: () {
            _deleteConfirmAlert(context);
          },
          color: Theme.of(context).errorColor,
        ),
      ],
    );
  }

  Widget _buildReportMadeBy(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          Icons.account_circle,
          color: HexColor("#BDBDBD"),
          size: 32,
        ),
        SizedBox(width: 12),
        Text(
          moneyReturnReport.userName,
          style: Theme.of(context).textTheme.headline4,
        ),
      ],
    );
  }

  Widget _buildDescription(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Deskripsi',
            style:
                Theme.of(context).textTheme.headline4?.copyWith(fontSize: 16)),
        SizedBox(height: 8),
        Text(
          moneyReturnReport.description,
          style: TextStyle(fontSize: 14),
        ),
      ],
    );
  }

  Widget _buildMoneyReturnImage() {
    return moneyReturnReport.image!;
  }

  Future _deleteConfirmAlert(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Hapus Laporan Pengembalian Uang",
            style: Theme.of(context).textTheme.headline2,
          ),
          content: Text(
            "Apakah anda yakin ingin menghapus laporan pengembalian uang ini?",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                  (states) => Color(0xFF4F4F4F).withOpacity(0.1),
                ),
              ),
              child: Text(
                "Kembali",
                style: TextStyle(
                  color: Color(0xFF4F4F4F),
                ),
              ),
            ),
            TextButton(
              onPressed: () async {
                bool success = await deleteMoneyReturnReport();
                if (success) {
                  Navigator.of(context).pop(false);
                  _buildsnackbar(context, Text('Data berhasil terhapus!'));
                  Navigator.pop(context);
                } else {
                  _buildsnackbar(
                      context, Text('Data gagal dihapus! Mohon coba lagi!'));
                  Navigator.of(context).pop(false);
                }
              },
              style: ButtonStyle(
                overlayColor: MaterialStateColor.resolveWith(
                  (states) => Theme.of(context).errorColor.withOpacity(0.1),
                ),
              ),
              child: Text(
                "Hapus",
                style: TextStyle(color: Theme.of(context).errorColor),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildEditBottomNavbar() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: OutlinedButton(
              onPressed: () {
                _changeScreen();
              },
              child: Text("Batal"),
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            flex: 1,
            child: ElevatedButton(
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  _buildsnackbar(context, Text('Isi data dengan penuh!'));
                } else {
                  _toggleIsLoadingMoneyReturnReports();
                  Map<String, dynamic> response = await putMoneyReturnReport();

                  if (response["status_code"] == 202) {
                    _toggleIsLoadingMoneyReturnReports();
                    _buildsnackbar(context, Text('Data berhasil tersimpan!'));
                    try {
                      moneyReturnReport.imageFile!.delete();
                    } catch (e) {
                      debugPrint("Edit Profile PUT on Success: " +
                          "File to be deleted not found!");
                    }
                    _setMoneyReturnReport(
                        response["report"] as MoneyReturnReport);
                    await Future.delayed(const Duration(seconds: 1), () {});
                    _changeScreen();
                  } else {
                    _toggleIsLoadingMoneyReturnReports();
                    _buildsnackbar(context,
                        Text('Data gagal tersimpan! Mohon coba lagi!'));
                  }
                }
              },
              child: _isLoadingPutMoneyReturnReport
                  ? SizedBox(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        strokeWidth: 3,
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.white,
                        ),
                      ),
                    )
                  : Text("Simpan"),
            ),
          ),
        ],
      ),
    );
  }
}
