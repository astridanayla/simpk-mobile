import 'package:flutter/material.dart';
import 'package:simpk/modules/monitoring/components/item_detail_screen.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';

class ActivityItemDetailScreen extends ItemDetailScreen<ActivityItem> {
  ActivityItemDetailScreen(ActivityItem activityItem) : super(activityItem);

  @override
  _ActivityItemDetailScreenState createState() =>
      _ActivityItemDetailScreenState();
}

class _ActivityItemDetailScreenState
    extends ItemDetailScreenState<ActivityItem> {
  @override
  Widget body(context) {
    return ListView(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 24),
      children: [
        Text(
          widget.item.name,
          style: Theme.of(context).textTheme.headline1,
        ),
        const SizedBox(height: 28),
        buildInfo(context, 'Persetujuan BPKH', widget.item.approved),
        buildInfo(context, 'Realisasi', widget.item.realization),
        buildInfo(
            context, 'Ada Perbedaan?', widget.item.difference ? 'Ya' : 'Tidak'),
        if (widget.item.difference)
          buildInfo(
              context, 'Alasan Perbedaan', widget.item.differenceDescription!),
      ],
    );
  }
}
