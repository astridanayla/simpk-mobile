import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simpk/components/secondary_button.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/models/select_choice.dart';
import 'package:simpk/modules/monitoring/screens/create_report.dart';
import 'package:simpk/modules/monitoring/screens/image_full_screen.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/services/image_compressor.dart';
import 'package:simpk/services/network_info.dart';

class CreateReport5Screen extends CreateReportScreen {
  CreateReport5Screen({required Report report, Key? key})
      : super(key: key, report: report);

  final _CreateReport5ScreenState state = _CreateReport5ScreenState();

  @override
  _CreateReport5ScreenState createState() => state;

  @override
  int get pageNumber => 5;
}

class _CreateReport5ScreenState
    extends CreateReportScreenState<CreateReport5Screen> {
  late List<File> _imageList;
  final ImagePicker _picker = ImagePicker();
  String? _conclusion;
  ReportService _reportRepository = GetIt.I();

  @override
  void initState() {
    super.initState();

    _conclusion = report.conclusion;
    _imageList = report.reportImages?.map((e) => e.image).toList() ?? [];
  }

  Future<void> _pickImage(ImageSource source) async {
    PickedFile? selected = await _picker.getImage(source: source);

    if (selected != null) {
      File compressedImage = (await compressImage(File(selected.path)))!;
      addImage(compressedImage);
    }
  }

  void addImage(File image) {
    setState(() {
      _imageList.add(image);
    });
  }

  void _clearImage(int index) {
    setState(() {
      _imageList.elementAt(index).delete();
      _imageList.removeAt(index);
    });
  }

  Future<bool> isConnected() async {
    NetworkInfo networkInfo = GetIt.I();
    bool isConnected = await networkInfo.isConnected();
    return isConnected;
  }

  @override
  void goNext(BuildContext context) async {
    setLoading(true);
    try {
      if (_conclusion != null) {
        report.reportImages =
            _imageList.map((e) => ReportImage(image: e)).toList();
        report.conclusion = _conclusion!;
        report.modifiedDate = DateTime.now();

        await _reportRepository.insert(report);
        if (isConnected() == true) {
          for (File image in _imageList) {
            image.delete();
          }
        }
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Laporan tersimpan!'),
        ));

        Navigator.of(context).pop(true);
      }
    } catch (e, s) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Error: $e'),
      ));
      print('Ex: $e');
      print('St: $s');
    } finally {
      setLoading(false);
    }
  }

  @override
  void goBack(BuildContext context) {
    report
      ..conclusion = _conclusion
      ..reportImages = _imageList.map((e) => ReportImage(image: e)).toList();
  }

  @override
  bool valuesAreValid() {
    return _conclusion != null && _imageList.length > 0;
  }

  @override
  List<Widget> buildChildren(BuildContext context) {
    return [
      _buildMonitoringPhotoField(context),
      _buildMonitoringConclusion(context),
      _buildConclusionRadioForm(),
    ];
  }

  Widget _buildMonitoringPhotoField(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
          child: Text("Bukti Foto Monitoring",
              style: Theme.of(context).textTheme.headline3),
        ),
        _imageList.length > 0 ? _buildImageList() : Container(),
        _customButton(
          context,
          _imageList.length > 0 ? "Ambil Foto Lain" : "Ambil Foto",
          Icons.photo_camera,
          () {
            _pickImage(ImageSource.camera);
          },
        ),
        SizedBox(height: 40)
      ],
    );
  }

  Widget _buildMonitoringConclusion(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
      child: Text("Kesimpulan Hasil Monitoring",
          style: Theme.of(context).textTheme.headline3),
    );
  }

  Widget _buildImageCard(
    BuildContext context,
    Widget child,
    VoidCallback onTap,
  ) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(.15),
              blurRadius: 5,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(4),
            onTap: onTap,
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.all(16),
              child: child,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildImageList() {
    return ListView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      physics: NeverScrollableScrollPhysics(),
      itemCount: _imageList.length,
      itemBuilder: (_, index) => _buildImageCard(
        context,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 8),
                    child: Icon(
                      Icons.photo_size_select_actual,
                      color: HexColor("#01788E"),
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: "Gambar ${index + 1}",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        WidgetSpan(child: SizedBox(width: 4)),
                        WidgetSpan(child: Icon(Icons.open_in_new))
                      ],
                    ),
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () => {_clearImage(index)},
              color: Colors.red,
            ),
          ],
        ),
        () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) {
                return ImageFullScreen(
                  fromFile: true,
                  imageFile: _imageList[index],
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget _buildConclusionRadioForm() {
    return Container(
      child: Column(
        children:
            conclusionList.map((e) => _customListTile(e.text, e.text)).toList(),
      ),
    );
  }

  Widget _customListTile(
    String title,
    String value,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4),
      child: RadioListTile(
        title: Text(
          title,
          style: TextStyle(
              fontSize: 14,
              color: HexColor("#333333"),
              fontWeight: FontWeight.normal,
              height: 1.6),
        ),
        contentPadding: EdgeInsets.all(0),
        value: value,
        groupValue: _conclusion,
        onChanged: (String? value) {
          setState(() {
            _conclusion = value;
          });
        },
      ),
    );
  }

  Widget _customButton(
    BuildContext context,
    String label,
    IconData icon,
    VoidCallback onPressed,
  ) {
    return SecondaryButton(text: label, iconData: icon, onPressed: onPressed);
  }
}
