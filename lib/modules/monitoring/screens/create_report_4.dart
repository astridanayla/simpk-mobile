import 'package:flutter/material.dart';
import 'package:simpk/components/check_display_size.dart';
import 'package:simpk/modules/monitoring/components/activity_analysis_form.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/screens/create_report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_5.dart';

class CreateReport4Screen extends CreateReportScreen {
  CreateReport4Screen({required Report report, Key? key})
      : super(key: key, report: report);

  @override
  _CreateReport4ScreenState createState() => _CreateReport4ScreenState();

  @override
  int get pageNumber => 4;
}

class _CreateReport4ScreenState
    extends CreateReportScreenState<CreateReport4Screen> {
  List<ActivityAnalysisForm> _activityAnalysisFormList = [];

  @override
  void initState() {
    super.initState();

    report.activityAnalysis?.forEach((element) {
      _onAddForm(element);
    });
  }

  @override
  void goNext(BuildContext context) async {
    report.activityAnalysis =
        _activityAnalysisFormList.map((e) => e.activityAnalysis).toList();

    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CreateReport5Screen(report: report)));
    if (result == true) {
      Navigator.of(context).pop(true);
    }
  }

  @override
  void goBack(BuildContext context) {
    List<ActivityAnalisis> elements = [];
    _activityAnalysisFormList.forEach((element) {
      if (element.isValid()) {
        elements.add(element.activityAnalysis);
      }
    });
    report..activityAnalysis = elements;
  }

  @override
  bool valuesAreValid() {
    bool isItemsValid = true;
    _activityAnalysisFormList.forEach((element) {
      isItemsValid &= element.isValid();
    });
    return isItemsValid;
  }

  @override
  List<Widget> buildChildren(BuildContext context) {
    return [
      Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
        child: Text("Analisa Pelaksanaan Kegiatan",
            style: Theme.of(context).textTheme.headline3),
      ),
      SizedBox(height: 12),
      _buildList(),
      _buildAddButton(),
    ];
  }

  Widget _buildList() {
    return Column(
      children: _activityAnalysisFormList,
    );
  }

  Widget _buildAddButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 24),
      alignment: Alignment.center,
      child: Container(
        width: displayWidth(context) - 32,
        child: buildAddItemButton('Tambah Poin', _onAddForm),
      ),
    );
  }

  /// on add form
  void _onAddForm([ActivityAnalisis? item]) {
    if (item == null) item = ActivityAnalisis();
    setState(() {
      _activityAnalysisFormList.add(
        ActivityAnalysisForm(
          key: UniqueKey(),
          activityAnalysis: item!,
          onDelete: onDelete,
        ),
      );
      refreshPointNumber();
    });
  }

  void onDelete(int index) {
    setState(() {
      _activityAnalysisFormList.removeAt(index);
      refreshPointNumber();
    });
  }

  void refreshPointNumber() {
    for (int index = 0; index < _activityAnalysisFormList.length; index++) {
      _activityAnalysisFormList[index].index = index;
    }
  }
}
