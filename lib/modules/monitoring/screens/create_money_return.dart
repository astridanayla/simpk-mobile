import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simpk/components/check_display_size.dart';
import 'package:simpk/components/input_form_field.dart';
import 'package:simpk/components/secondary_button.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/services/image_compressor.dart';

class CreateMoneyReturn extends StatefulWidget {
  CreateMoneyReturn(this.project);
  final Project project;
  final _CreateMoneyReturnState state = _CreateMoneyReturnState();

  @override
  _CreateMoneyReturnState createState() => state;
}

class _CreateMoneyReturnState extends State<CreateMoneyReturn> {
  final MoneyReturnAPIProvider _moneyReturnAPI = GetIt.I();
  final ImagePicker _picker = ImagePicker();
  late MoneyReturnReport moneyReturnReport;
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  bool _isLoadingPostMoneyReturnReport = false;
  final _formKey = GlobalKey<FormState>();

  void _toggleIsLoadingPostMoneyReturnReports() {
    setState(() {
      _isLoadingPostMoneyReturnReport = !_isLoadingPostMoneyReturnReport;
    });
  }

  @override
  void initState() {
    super.initState();
    moneyReturnReport = MoneyReturnReport(
      title: '',
      description: '',
      id: '',
      userId: '',
      userName: '',
    );
    _titleController.text = moneyReturnReport.title;
    _descriptionController.text = moneyReturnReport.description;
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  Future<void> _pickImage(ImageSource source) async {
    var selected = await _picker.getImage(source: source);

    if (selected != null) {
      File compressedImage = (await compressImage(File(selected.path)))!;
      setImage(compressedImage);
    }
  }

  void setImage(File file) {
    setState(() {
      moneyReturnReport.imageFile = file;
    });
  }

  void clearImage() {
    setState(() {
      moneyReturnReport.imageFile!.delete();
      moneyReturnReport.imageFile = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 56, horizontal: 16),
        children: [
          Text(
            "Buat Pengembalian Uang",
            style: Theme.of(context).textTheme.headline1,
          ),
          _buildFormFields(context),
          _buildImageAndImageButton(context),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(16),
        child: _buildBottomNav(context),
      ),
    );
  }

  Widget _buildFormFields(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 48, 0, 4),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            InputFormField(
              label: 'Judul',
              autovalidateMode: AutovalidateMode.always,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Judul tidak boleh kosong.';
                }
                return null;
              },
              controller: _titleController,
            ),
            SizedBox(height: 8),
            InputFormField(
              label: 'Deskripsi',
              autovalidateMode: AutovalidateMode.always,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Deskripsi tidak boleh kosong.';
                }
                return null;
              },
              controller: _descriptionController,
              maxLines: 6,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildImageAndImageButton(BuildContext context) {
    return Column(
      children: [
        moneyReturnReport.imageFile != null
            ? Image.file(moneyReturnReport.imageFile!)
            : Container(),
        SizedBox(height: 16),
        moneyReturnReport.imageFile == null
            ? Container(
                width: displayWidth(context),
                child: SecondaryButton(
                  text: "Upload Foto Bukti Kwitansi / Invoice",
                  iconData: Icons.photo_camera,
                  onPressed: () {
                    _pickImage(ImageSource.camera);
                  },
                ),
              )
            : Container(
                child: OutlinedButton(
                  onPressed: () {
                    clearImage();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.delete),
                      SizedBox(width: 8),
                      Text("Hapus gambar")
                    ],
                  ),
                ),
              )
      ],
    );
  }

  Widget _buildBottomNav(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: OutlinedButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            child: Text("Batal"),
          ),
        ),
        SizedBox(width: 8),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: () async {
              if (!_formKey.currentState!.validate()) {
                _buildsnackbar(context, Text('Mohon isi data dengan lengkap!'));
              } else if (!imageExists()) {
                _buildsnackbar(context, Text('Mohon upload foto bukti!'));
              } else {
                _toggleIsLoadingPostMoneyReturnReports();
                bool success = await postMoneyReturnReport();

                if (success) {
                  _toggleIsLoadingPostMoneyReturnReports();
                  _buildsnackbar(context, Text('Data berhasil tersimpan!'));
                  moneyReturnReport.imageFile!.delete();
                  await Future.delayed(const Duration(seconds: 1), () {});
                  Navigator.pop(context);
                } else {
                  _buildsnackbar(
                      context, Text('Data gagal tersimpan! Mohon coba lagi!'));
                }

                _toggleIsLoadingPostMoneyReturnReports();
              }
            },
            child: _isLoadingPostMoneyReturnReport
                ? SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white,
                      ),
                    ),
                  )
                : Text("Simpan"),
          ),
        ),
      ],
    );
  }

  bool imageExists() {
    return moneyReturnReport.imageFile != null;
  }

  Future<bool> postMoneyReturnReport() async {
    moneyReturnReport.title = _titleController.text;
    moneyReturnReport.description = _descriptionController.text;
    bool success = await _moneyReturnAPI.postMoneyReturnReport(
        moneyReturnReport, widget.project.id.toString());
    return success;
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildsnackbar(
      BuildContext context, Widget content) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }
}
