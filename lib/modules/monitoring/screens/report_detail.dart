import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:simpk/components/scroll_app_bar.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/screens/activity_analysis_detail.dart';
import 'package:simpk/modules/monitoring/screens/activity_item_detail.dart';
import 'package:simpk/modules/monitoring/screens/image_full_screen.dart';
import 'package:simpk/modules/monitoring/screens/realization_item_detail.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';

class ReportDetailScreen extends StatefulWidget {
  ReportDetailScreen(this.report, {Key? key}) : super(key: key);

  final Report report;

  @override
  _ReportDetailScreenState createState() =>
      _ReportDetailScreenState(this.report);
}

class _ReportDetailScreenState extends State<ReportDetailScreen> {
  _ReportDetailScreenState(this.report);

  final ScrollController _scrollController = ScrollController();
  final ReportService _reportRepository = GetIt.I();
  final MonitoringAPIProvider _apiProvider = GetIt.I();
  Report report;
  late List<String> imageUrls;

  @override
  void initState() {
    super.initState();
    if (report.activityItems == null) {
      report.activityItems = [];
      report.realizationItems = [];
      report.activityAnalysis = [];
      report.reportImages = [];
    }

    _reportRepository.queryById(report.id!).then((value) {
      if (mounted) {
        setState(() {
          report = value;
          print(value.authorName);
        });
      }
    });

    _fetchReportImageUrl();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ScrollAppBar(
        scrollController: _scrollController,
      ),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 8, 16, 48),
          child: Column(
            children: [
              _buildReportInfo(context),
              SizedBox(height: 16),
              Divider(),
              SizedBox(height: 16),
              _buildActivityExecutions(context),
              SizedBox(height: 16),
              Divider(),
              SizedBox(height: 16),
              _buildActivityAnalysis(context),
              SizedBox(height: 16),
              Divider(),
              SizedBox(height: 16),
              _buildConclusionInfo(context),
              SizedBox(height: 16),
              Divider(),
              SizedBox(height: 16),
              _buildImageInfo(context),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildImageInfo(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Lampiran Monitoring",
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(fontWeight: FontWeight.w900),
          ),
          SizedBox(height: 20),
          _buildImageList(context)
        ],
      ),
    );
  }

  Widget _buildImageList(BuildContext context) {
    List<Widget> imageList = [];

    for (int i = 0; i < imageUrls.length; i++) {
      imageList.add(
        Container(
          child: _buildElevatedCard(
            context,
            () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) {
                    return ImageFullScreen(
                      imageUrl: imageUrls[i],
                    );
                  },
                ),
              );
            },
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "Lampiran ${i + 1}",
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                    ),
                  ),
                  WidgetSpan(child: SizedBox(width: 4)),
                  WidgetSpan(child: Icon(Icons.open_in_new))
                ],
              ),
            ),
          ),
        ),
      );
    }

    if (imageList.length == 0) {
      return Text(
        'Tidak ada lampiran laporan',
        style: Theme.of(context).textTheme.bodyText1,
      );
    } else {
      return Column(children: imageList);
    }
  }

  void _goBack(BuildContext context) {
    int count = 0;
    Navigator.popUntil(context, (route) {
      return count++ == 2;
    });
  }

  void _onDelete() async {
    await _reportRepository.delete(report.id!);
    _goBack(context);
  }

  void showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
            (states) => Color(0xFF4F4F4F).withOpacity(0.1)),
      ),
      child: Text("Kembali", style: TextStyle(color: Color(0xFF4F4F4F))),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
            (states) => Theme.of(context).errorColor.withOpacity(0.1)),
      ),
      child:
          Text("Hapus", style: TextStyle(color: Theme.of(context).errorColor)),
      onPressed: () {
        _onDelete();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text(
        "Perhatian!",
        style: Theme.of(context).textTheme.headline2,
      ),
      content: Text(
        "Anda yakin ingin menghapus laporan ini?",
        style: Theme.of(context).textTheme.bodyText1,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _buildReportInfo(BuildContext context) {
    final modifiedDate = DateFormat('dd/MM/yyyy').format(report.modifiedDate!);
    final monitoringDate =
        DateFormat('dd/MM/yyyy').format(report.monitoringDate!);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                'Laporan Monev',
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
            IconButton(
              key: Key('btn_delete'),
              padding: EdgeInsets.zero,
              constraints: BoxConstraints(
                maxHeight: 24,
                maxWidth: 24,
              ),
              icon: Icon(Icons.delete, size: 24),
              onPressed: () {
                showAlertDialog(context);
              },
              color: Theme.of(context).errorColor,
            ),
          ],
        ),
        const SizedBox(height: 12),
        Text(
          'Dibuat pada: $modifiedDate',
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: Color(0xFF828282)),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 24, bottom: 36),
          child: Row(
            children: [
              Icon(
                Icons.account_circle,
                color: Color(0xFFBDBDBD),
                size: 32,
              ),
              SizedBox(width: 12),
              Text(report.authorName!,
                  style: Theme.of(context).textTheme.headline4),
            ],
          ),
        ),
        _buildInfo(context, 'Jenis Kegiatan', report.activityType!),
        _buildInfo(context, 'Nama Kegiatan', report.activityName!),
        _buildInfo(context, 'Metode Pelaksanaan', report.executionMethod!),
        _buildInfo(context, 'Tanggal Monitoring', monitoringDate),
        _buildInfo(context, 'Periode Monitoring', report.monitoringPeriod!),
      ],
    );
  }

  Widget _buildActivityExecutions(BuildContext context) {
    bool activityItemsConformity = true;
    report.activityItems?.forEach((element) {
      activityItemsConformity &= !element.difference;
    });
    bool realizationItemsConformity = true;
    report.realizationItems?.forEach((element) {
      realizationItemsConformity &= !element.difference;
    });

    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 16),
            child: Text(
              'Pelaksanaan Kegiatan',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontWeight: FontWeight.w900),
            ),
          ),
          _buildInfo(context, 'Tahapan Kegiatan yang Akan Dicapai',
              report.activityStage!),
          _buildConformity(
            context,
            'Rincian Kegiatan yang Dilakukan',
            activityItemsConformity,
          ),
          Column(
            children: report.activityItems!
                .map((activityItem) => _buildItemCard(
                      context,
                      () => _navigateTo(context,
                          builder: (_) =>
                              ActivityItemDetailScreen(activityItem)),
                      activityItem.name,
                      activityItem.approved,
                      activityItem.realization,
                    ))
                .toList(),
          ),
          const SizedBox(height: 12),
          _buildConformity(
            context,
            'Realisasi Penggunaan Dana',
            realizationItemsConformity,
          ),
          Column(
            children: report.realizationItems!.map((realizationItem) {
              final numberFormat =
                  NumberFormat.decimalPattern(Intl.defaultLocale);

              return _buildItemCard(
                context,
                () => _navigateTo(
                  context,
                  builder: (_) => RealizationItemDetailScreen(realizationItem),
                ),
                realizationItem.name,
                'Rp ${numberFormat.format(realizationItem.approved)}',
                'Rp ${numberFormat.format(realizationItem.realization)}',
              );
            }).toList(),
          ),
          const SizedBox(height: 4),
        ],
      ),
    );
  }

  Widget _buildActivityAnalysis(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 16),
            child: Text(
              'Analisa Pelaksanaan Kegiatan',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontWeight: FontWeight.w900),
            ),
          ),
          Column(
            children: report.activityAnalysis!
                .map((activityAnalysis) => _buildActivityAnalysisItem(
                      context,
                      () => _navigateTo(
                        context,
                        builder: (_) =>
                            ActivityAnalysisDetailScreen(activityAnalysis),
                      ),
                      activityAnalysis.problem,
                    ))
                .toList(),
          ),
          const SizedBox(height: 4),
        ],
      ),
    );
  }

  Widget _buildConclusionInfo(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 16),
            child: Text(
              'Kesimpulan Hasil Monitoring',
              style: Theme.of(context)
                  .textTheme
                  .headline3!
                  .copyWith(fontWeight: FontWeight.w900),
            ),
          ),
          Text(
            report.conclusion!,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(height: 4),
        ],
      ),
    );
  }

  Widget _buildInfo(BuildContext context, String label, String text) {
    return Padding(
      padding: EdgeInsets.only(top: 4, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 8),
          Text(text, style: Theme.of(context).textTheme.bodyText1),
        ],
      ),
    );
  }

  Widget _buildConformity(BuildContext context, String label, bool matching) {
    return Padding(
      padding: EdgeInsets.only(top: 4, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 8),
          RichText(
            text: TextSpan(
              style: Theme.of(context).textTheme.bodyText1,
              children: [
                TextSpan(text: 'Tingkat Kesesuaian : '),
                matching
                    ? TextSpan(
                        text: 'Sesuai',
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      )
                    : TextSpan(
                        text: 'Tidak Sesuai',
                        style: TextStyle(color: Theme.of(context).errorColor),
                      ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildItemCard(
    BuildContext context,
    VoidCallback onTap,
    String name,
    String approved,
    String realization,
  ) {
    return _buildElevatedCard(
      context,
      onTap,
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: Theme.of(context).textTheme.headline3,
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Persetujuan BPKH',
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            color: const Color(0xFF828282),
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    const SizedBox(height: 2),
                    Text(
                      approved,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Realisasi',
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            color: const Color(0xFF828282),
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                    Text(
                      realization,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildActivityAnalysisItem(
    BuildContext context,
    VoidCallback onTap,
    String problem,
  ) {
    return _buildElevatedCard(
      context,
      onTap,
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Masalah / Kendala / Hambatan yang Dihadapi',
            style: TextStyle(color: Color(0xFF828282)),
          ),
          const SizedBox(height: 4),
          Text(
            problem,
            style: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Widget _buildElevatedCard(
    BuildContext context,
    VoidCallback onTap,
    Widget child,
  ) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(.15),
              blurRadius: 5,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(4),
            onTap: onTap,
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.all(16),
              child: child,
            ),
          ),
        ),
      ),
    );
  }

  void _navigateTo(
    BuildContext context, {
    required Widget Function(BuildContext context) builder,
  }) {
    Navigator.push(context, MaterialPageRoute(builder: builder));
  }

  Future<void> _fetchReportImageUrl() async {
    imageUrls = [];
    imageUrls = await _apiProvider.getReportImageUrlsByReportId(report.id!);
  }
}
