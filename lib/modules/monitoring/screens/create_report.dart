import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/secondary_button.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/services/network_info.dart';

abstract class CreateReportScreen extends StatefulWidget {
  CreateReportScreen({required this.report, Key? key}) : super(key: key);

  final Report report;

  /// Page number on the header: 'Buat Laporan ([pageNumber]/5)'.
  int get pageNumber;
}

abstract class CreateReportScreenState<Page extends CreateReportScreen>
    extends State<Page> {
  /// Report in CreateReportScreenState
  late Report report;
  NetworkInfo _networkInfo = GetIt.I();
  bool _isLoading = false;

  bool valuesAreValid();

  @override
  void initState() {
    super.initState();
    report = widget.report;
  }

  List<Widget> buildChildren(BuildContext context);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        goBack(context);
        Navigator.of(context).pop(false);
        return true;
      },
      child: Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildHeader(),
              ...buildChildren(context),
            ],
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: buildBottomNav(),
        ),
      ),
    );
  }

  Future<bool> isOffline() async {
    bool _isConnected = await _networkInfo.isConnected();
    return !_isConnected;
  }

  /// Action when 'Selanjutnya' or 'Simpan' is pressed.
  void goNext(BuildContext context);

  /// Action when 'Kembali' or 'Batal' is pressed.
  void goBack(BuildContext context);

  /// Build a widget containing 'Buat Laporan([widget.pageNumber]/5)'.
  Widget buildHeader() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 56, 0, 48),
      child: Text(
        "Buat Laporan (${widget.pageNumber}/5)",
        style: Theme.of(context).textTheme.headline1,
      ),
    );
  }

  /// Build a bottom navigation.
  Widget buildBottomNav() {
    String backText;
    if (widget.pageNumber == 1) {
      backText = 'Batal';
    } else {
      backText = 'Kembali';
    }

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 40),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: OutlinedButton(
              onPressed: () {
                goBack(context);
                Navigator.of(context).pop(false);
              },
              child: Text(backText),
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            flex: 1,
            child: ElevatedButton(
              onPressed: () {
                if (!_isLoading) {
                  _onPressedNext();
                }
              },
              child: _isLoading
                  ? SizedBox(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        strokeWidth: 3,
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.white,
                        ),
                      ),
                    )
                  : Text("Selanjutnya"),
            ),
          ),
        ],
      ),
    );
  }

  void setLoading(bool state) {
    setState(() {
      _isLoading = state;
    });
  }

  Widget buildAddItemButton(String label, void Function() onPressed,
      {Key? key}) {
    return SecondaryButton(
      key: key,
      text: label,
      iconData: Icons.add_circle_outline,
      onPressed: onPressed,
    );
  }

  void _onPressedNext() {
    if (valuesAreValid()) {
      goNext(context);
    }
  }
}
