import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simpk/components/date_picker.dart';
import 'package:simpk/components/input_form_field.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/select_choice.dart';
import 'package:simpk/modules/monitoring/screens/create_report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_2.dart';

class CreateReport1Screen extends CreateReportScreen {
  CreateReport1Screen({required int projectId, this.periodNumber = 1})
      : super(report: Report()..projectId = projectId);

  final int periodNumber;

  @override
  _CreateReport1ScreenState createState() =>
      _CreateReport1ScreenState(this.periodNumber);

  @override
  int get pageNumber => 1;
}

class _CreateReport1ScreenState
    extends CreateReportScreenState<CreateReport1Screen> {
  _CreateReport1ScreenState(this.periodNumber);

  final _formKey = GlobalKey<FormState>();
  final int periodNumber;

  String? _activityType;
  String? _executionMethod;
  String? _monitoringPeriod;
  List<String> _periodList = [];
  bool _newPeriodAdded = false;

  late TextEditingController _activityNameController;
  late TextEditingController _monitoringDateController;
  late FocusNode _activityNameFocusNode;

  @override
  void initState() {
    super.initState();
    _activityNameController = TextEditingController();
    _monitoringDateController = TextEditingController();
    _activityNameFocusNode = FocusNode();

    for (int idx = 1; idx <= periodNumber; idx++) {
      _periodList.add("Periode $idx");
    }
  }

  @override
  void dispose() {
    _activityNameController.dispose();
    _monitoringDateController.dispose();
    _activityNameFocusNode.dispose();
    super.dispose();
  }

  bool valuesAreValid() {
    _formKey.currentState?.validate();
    bool _isValid = _activityType != null &&
        _activityNameController.text != "" &&
        _executionMethod != null &&
        _monitoringDateController.text != "";
    if (isOffline == false) {
      _isValid = _isValid && _monitoringPeriod != null;
    }
    return _isValid;
  }

  @override
  void goNext(BuildContext context) async {
    report
      ..activityType = _activityType
      ..activityName = _activityNameController.text
      ..executionMethod = _executionMethod
      ..monitoringDate = DateFormat.yMd().parse(_monitoringDateController.text);

    bool _isOffline = await isOffline();
    if (_isOffline == true) {
      report..monitoringPeriod = "Periode 0";
    } else {
      report..monitoringPeriod = _monitoringPeriod;
    }

    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CreateReport2Screen(report: report)));
    if (result == true) {
      Navigator.of(context).pop(report);
    }
  }

  @override
  void goBack(BuildContext context) {}

  @override
  List<Widget> buildChildren(BuildContext context) {
    return [
      _buildDropdownField(
        label: "Jenis Kegiatan",
        items: SelectChoice.getTexts(activityTypeList),
        value: _activityType,
        onChanged: (value) {
          setState(() {
            _activityType = value;
          });
        },
      ),
      _buildActivityTypeSelected(context),
    ];
  }

  Widget _buildActivityTypeSelected(BuildContext context) {
    return Visibility(
      visible: _activityType != null,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height: 8),
            _buildTextField(
              label: "Nama Kegiatan",
              controller: _activityNameController,
              focusNode: _activityNameFocusNode,
              maxLength: 50,
            ),
            SizedBox(height: 8),
            _buildDropdownField(
              label: "Pilih Metode Pelaksanaan",
              items: SelectChoice.getTexts(executionMethodList),
              value: _executionMethod,
              onChanged: (value) {
                setState(() {
                  _executionMethod = value;
                });
              },
            ),
            SizedBox(height: 16),
            DatePicker(
              label: "Tanggal Monitoring",
              controller: _monitoringDateController,
              onTap: _clearFocus,
            ),
            SizedBox(height: 16),
            if (report.projectId != 0) // projectID=0 == Offline
              Column(
                children: [
                  _buildDropdownField(
                    label: "Pilih Periode Monitoring",
                    items: _periodList, // [Periode 1, Period2 , ]
                    value: _monitoringPeriod, //Perio 1
                    onChanged: (value) {
                      setState(() {
                        _monitoringPeriod = value;
                      });
                    },
                  ),
                  SizedBox(height: 4),
                  Container(
                    alignment: Alignment.topLeft,
                    child: TextButton(
                      key: Key('btn_add_period'),
                      onPressed: _newPeriodAdded ? null : _incrementPeriod,
                      child: Text("+ Tambah Periode Baru"),
                    ),
                  ),
                ],
              )
          ],
        ),
      ),
    );
  }

  Widget _buildDropdownField(
      {required String label,
      required List<String> items,
      required String? value,
      required void Function(String?) onChanged}) {
    return DropdownButtonFormField<String>(
      onTap: _clearFocus,
      value: value,
      isExpanded: true,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: label,
      ),
      items: items
          .map((String value) => DropdownMenuItem(
                value: value,
                child: Text(
                  value,
                  softWrap: true,
                ),
              ))
          .toList(),
      onChanged: onChanged,
    );
  }

  Widget _buildTextField({
    required String label,
    required TextEditingController controller,
    FocusNode? focusNode,
    int? maxLength,
  }) {
    return InputFormField(
      controller: controller,
      focusNode: focusNode,
      label: label,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '$label tidak boleh kosong';
        }
        if (maxLength != null && value.length > maxLength) {
          return '$label maksimal $maxLength karakter';
        }
        return null;
      },
    );
  }

  void _incrementPeriod() {
    final newMonitoringPeriod = "Periode ${periodNumber + 1}";
    setState(() {
      _newPeriodAdded = true;
      _monitoringPeriod = newMonitoringPeriod;
      _periodList.add(newMonitoringPeriod);
    });
  }

  void _clearFocus() {
    _activityNameFocusNode.unfocus();
  }
}
