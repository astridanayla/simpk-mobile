import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/custom_card.dart';
import 'package:simpk/components/secondary_button.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/blocs/auth_state.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/screens/create_money_return.dart';
import 'package:simpk/modules/monitoring/screens/create_report_1.dart';
import 'package:simpk/modules/monitoring/screens/money_return_details.dart';
import 'package:simpk/modules/monitoring/screens/report_detail.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project/blocs/project_bloc.dart';
import 'package:simpk/modules/project/blocs/project_event.dart';
import 'package:simpk/modules/project/blocs/project_state.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/edit_detail_page.dart';

class MonitoringTab extends StatefulWidget {
  MonitoringTab({required this.projectId});

  final int projectId;

  @override
  _MonitoringTabState createState() => _MonitoringTabState();
}

class _MonitoringTabState extends State<MonitoringTab> {
  List<Report> _reports = [];
  List<String> _periods = [];
  List<MoneyReturnReport> _moneyReturnReports = [];
  bool _loading = true;
  bool _isLoadingMoneyReturnReports = false;
  int _highestPeriod = 1;
  String? _selectedPeriod;
  final ReportService _reportRepository = GetIt.I();
  final MoneyReturnAPIProvider moneyReturnAPI = GetIt.I();

  @override
  void initState() {
    super.initState();
    _reportRepository
        .queryMonitoringPeriodByProjectId(widget.projectId)
        .then((value) {
      if (mounted)
        setState(() {
          _periods = value;
          _highestPeriod = _getHighestPeriod(_periods);
          _loading = false;
        });
    });
    _fetchMoneyReturnReports(widget.projectId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // ----- BUAT LAPORAN -----
            BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                if (!state.isRoleProfessional())
                  return _buildCreateReport(context);
                return SizedBox(height: 32);
              },
            ),
            // ---- LIST LAPORAN -----
            _buildReportList(context),
            // ---- LAPORAN PENGEMBALIAN UANG ----
            _buildMoneyReturnReport(context),
            // ---- Keismpulan ----
            _buildConclusion(context),
          ],
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> generatePeriodItems(List<String> periods) {
    List<DropdownMenuItem<String>> items = [];

    for (var period in periods) {
      items.add(
        DropdownMenuItem(
          key: Key(period),
          child: Text(period),
          value: period,
        ),
      );
    }

    return items;
  }

  Widget generateReports() {
    return Column(
      children: _reports
          .map((report) => new CustomCard(
                title: report.activityName!,
                userName: report.authorName!,
                lastEdited: report.modifiedDate!,
                onClick: ReportDetailScreen(report),
                userIcon: Icon(
                  Icons.person_rounded,
                ),
                displayIcon: Icons.notes,
                afterOnClick: _fetchAfterDelete,
              ))
          .toList(),
    );
  }

  Widget generateMoneyReturnReports(
      List<MoneyReturnReport> moneyReturnReports) {
    if (moneyReturnReports.length == 0) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Center(
          child: Text(
            "Belum ada laporan pengembalian uang",
            style: Theme.of(context).textTheme.headline4,
          ),
        ),
      );
    }

    List<Widget> reports = [];

    for (var report in moneyReturnReports) {
      reports.add(
        CustomCard(
          title: report.title,
          userName: report.userName,
          lastEdited: report.lastUpdated!,
          onClick: MoneyReturnDetails(
            initMoneyReturnReport: report,
          ),
          userIcon: Icon(
            Icons.person_rounded,
          ),
          displayIcon: Icons.notes,
          customOnPressed: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => MoneyReturnDetails(
                initMoneyReturnReport: report,
              ),
            ),
          ).then((value) => _fetchMoneyReturnReports(widget.projectId)),
        ),
      );
    }

    return new Column(children: reports);
  }

  Widget _buildCreateReport(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 32, 16, 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Buat Laporan Monitoring",
              style: Theme.of(context).textTheme.headline3),
          Container(
            margin: EdgeInsets.fromLTRB(0, 16, 0, 20),
            child: Text(
              "Buat laporan baru atau lihat yang telah anda buat.",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          ElevatedButton.icon(
            key: Key('buat_laporan_button'),
            label: Text('Buat Laporan'),
            icon: Icon(Icons.add_circle_outline_rounded, size: 20),
            onPressed: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CreateReport1Screen(
                      projectId: widget.projectId,
                      periodNumber: _highestPeriod),
                ),
              );
              if (result is Report) {
                _fetchNewReport(result);
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildReportList(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 24),
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Lihat Laporan", style: Theme.of(context).textTheme.headline3),
          Container(
            margin: EdgeInsets.fromLTRB(0, 16, 0, 24),
            child: Text(
              "Pilih periode terlebih dahulu untuk melihat laporan.",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          DropdownButtonFormField<String>(
            key: Key('period_dropdown'),
            items: generatePeriodItems(_periods),
            value: _selectedPeriod,
            onChanged: (period) {
              setState(() {
                _selectedPeriod = period;
              });
              if (period != null) {
                _reportRepository
                    .queryByProjectIdAndMonitoringPeriod(
                        widget.projectId, period)
                    .then((value) {
                  setState(() {
                    _reports = value;
                  });
                });
              }
            },
            decoration: InputDecoration(
                labelText: "Pilih Periode",
                labelStyle: TextStyle(
                    fontWeight: FontWeight.normal, fontSize: 14, height: 0)),
            style: Theme.of(context).textTheme.bodyText1,
          ),
          if (!_loading && _periods.isEmpty)
            Container(
              padding: EdgeInsets.only(top: 16),
              alignment: Alignment.center,
              child: Text(
                "Belum ada laporan",
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: _selectedPeriod != null
                ? Container(
                    margin: EdgeInsets.only(bottom: 16),
                    child: generateReports())
                : Container(),
          ),
        ],
      ),
    );
  }

  Widget _buildMoneyReturnReport(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 40),
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Laporan Pengembalian Uang",
              style: Theme.of(context).textTheme.headline3),
          SizedBox(height: 16),
          _isLoadingMoneyReturnReports
              ? Center(child: CircularProgressIndicator())
              : generateMoneyReturnReports(_moneyReturnReports),
          SizedBox(height: 12),
          BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state) {
              if (!state.isRoleProfessional()) {
                return BlocBuilder<ProjectBloc, ProjectState>(
                  builder: (context, state) => SecondaryButton(
                    text: "Tambah Laporan Pengembalian Uang",
                    iconData: Icons.add_circle_outline,
                    onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => CreateMoneyReturn(state.project),
                      ),
                    ).then(
                        (value) => _fetchMoneyReturnReports(widget.projectId)),
                  ),
                );
              }
              return SizedBox();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildConclusion(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 48),
      alignment: Alignment.topLeft,
      child: BlocBuilder<ProjectBloc, ProjectState>(
        builder: (context, state) {
          final String summary = state.project.monitoringSummary;
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Kesimpulan", style: Theme.of(context).textTheme.headline3),
              Container(
                margin: EdgeInsets.fromLTRB(0, 16, 0, 20),
                child: Text(
                  summary.isNotEmpty
                      ? summary
                      : "Tambah kesimpulan setelah monitoring selesai.",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              SecondaryButton(
                text: summary.isNotEmpty
                    ? "Edit Kesimpulan"
                    : "Tambah Kesimpulan",
                iconData: summary.isNotEmpty
                    ? Icons.edit
                    : Icons.add_circle_outline_rounded,
                onPressed: () async {
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => EditDetailPage(
                        "Tambah Kesimpulan",
                        "Kesimpulan Monitoring",
                        summary,
                      ),
                    ),
                  );
                  if (result is String) {
                    final Project updated = state.project.copy()
                      ..monitoringSummary = result;
                    context.read<ProjectBloc>().add(
                        ProjectUpdateEvent(updated, 'Error updating summary.'));
                  }
                },
              ),
            ],
          );
        },
      ),
    );
  }

  void _fetchAfterDelete() {
    setState(() {
      _reports = [];
      _periods = [];
    });
    _reportRepository
        .queryMonitoringPeriodByProjectId(widget.projectId)
        .then((value) {
      setState(() {
        _periods = value;
        _highestPeriod = _getHighestPeriod(_periods);
      });
      if (!_periods.contains(_selectedPeriod)) {
        setState(() {
          _selectedPeriod = _periods.last;
        });
      }
      if (_selectedPeriod != null) {
        _reportRepository
            .queryByProjectIdAndMonitoringPeriod(
                widget.projectId, _selectedPeriod!)
            .then((value) {
          setState(() {
            _reports = value;
          });
        });
      }
    });
  }

  void _fetchNewReport(Report report) {
    _reportRepository
        .queryMonitoringPeriodByProjectId(widget.projectId)
        .then((value) {
      setState(() {
        _periods = value;
        _highestPeriod = _getHighestPeriod(_periods);
      });
      if (report.monitoringPeriod != null) {
        _reportRepository
            .queryByProjectIdAndMonitoringPeriod(
                widget.projectId, report.monitoringPeriod!)
            .then((value) {
          setState(() {
            _selectedPeriod = report.monitoringPeriod!;
            _reports = value;
          });
        });
      }
    });
  }

  int _getHighestPeriod(List<String> periods) {
    if (periods.length == 0) {
      return 0;
    }

    final periodNumbers = periods
        .map((period) => int.tryParse(period.split(' ').last) ?? 1)
        .toList();
    periodNumbers.sort();

    return periodNumbers.last;
  }

  void _toggleIsLoadingMoneyReturnReports() {
    if (mounted) {
      setState(() {
        _isLoadingMoneyReturnReports = !_isLoadingMoneyReturnReports;
      });
    }
  }

  void _setMoneyReturnReport(List<MoneyReturnReport> newMoneyReturnReports) {
    if (mounted) {
      setState(() {
        _moneyReturnReports = newMoneyReturnReports;
      });
    }
  }

  void _fetchMoneyReturnReports(int projectId) async {
    _toggleIsLoadingMoneyReturnReports();
    try {
      final moneyReturnReportsResponse =
          await moneyReturnAPI.getMoneyReturnReportsByProjectId(projectId);
      _setMoneyReturnReport(moneyReturnReportsResponse);
    } catch (e) {}
    _toggleIsLoadingMoneyReturnReports();
  }
}
