import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simpk/modules/monitoring/components/item_detail_screen.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';

class RealizationItemDetailScreen extends ItemDetailScreen<RealizationItem> {
  RealizationItemDetailScreen(RealizationItem item) : super(item);

  @override
  _RealizationItemDetailScreenState createState() =>
      _RealizationItemDetailScreenState();
}

class _RealizationItemDetailScreenState
    extends ItemDetailScreenState<RealizationItem> {
  @override
  Widget body(BuildContext context) {
    final numberFormat = NumberFormat.decimalPattern(Intl.defaultLocale);

    return ListView(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 24),
      children: [
        Text(
          widget.item.name,
          style: Theme.of(context).textTheme.headline1,
        ),
        const SizedBox(height: 28),
        buildInfo(context, 'Persetujuan BPKH',
            'Rp ${numberFormat.format(widget.item.approved)}'),
        buildInfo(context, 'Realisasi',
            'Rp ${numberFormat.format(widget.item.realization)}'),
        buildInfo(
            context, 'Ada Perbedaan?', widget.item.difference ? 'Ya' : 'Tidak'),
        if (widget.item.difference)
          buildInfo(
              context, 'Alasan Perbedaan', widget.item.differenceDescription!),
      ],
    );
  }
}
