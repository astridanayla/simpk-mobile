import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ImageFullScreen extends StatefulWidget {
  const ImageFullScreen({
    this.imageUrl = "",
    this.fromFile = false,
    this.imageFile = null,
    Key? key,
  }) : super(key: key);

  final bool fromFile;
  final String imageUrl;
  final File? imageFile;

  @override
  _ImageFullScreenState createState() => _ImageFullScreenState();
}

class _ImageFullScreenState extends State<ImageFullScreen> {
  static const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
      defaultValue: 'https://simpk-staging.herokuapp.com');

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Center(
          child: widget.fromFile
              ? Image.file(widget.imageFile!)
              : Hero(
                  tag: "imageHero",
                  child: CachedNetworkImage(
                    imageUrl: _baseUrl + widget.imageUrl,
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) =>
                            CircularProgressIndicator(
                                value: downloadProgress.progress),
                    errorWidget: (context, url, error) => Center(
                      child: Text(
                        "Gagal mendownload gambar!",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ),
                ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
