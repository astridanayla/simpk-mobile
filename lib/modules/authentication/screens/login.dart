import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/input_form_field.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/blocs/auth_event.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/services/api_provider.dart';

class LoginScreen extends StatefulWidget {
  final _LoginScreenState state = _LoginScreenState();

  @override
  _LoginScreenState createState() => state;
}

class _LoginScreenState extends State<LoginScreen> {
  APIProvider api = APIProvider();
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isLoadingLogin = false;
  bool _wrongCredentials = false;
  bool _forgotPassword = false;
  bool _obscurePasswordText = true;

  void _toggleLoadingLogin() {
    setState(() {
      _isLoadingLogin = !_isLoadingLogin;
    });
  }

  void _toggleWrongCredentials() {
    setState(() {
      _wrongCredentials = !_wrongCredentials;
    });
  }

  void _toggleForgotPassword() {
    setState(() {
      _forgotPassword = !_forgotPassword;
    });
  }

  void _toggleObscurePasswordText() {
    setState(() {
      _obscurePasswordText = !_obscurePasswordText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: _forgotPassword
              ? _buildForgotPassword(context)
              : _buildLoginForm(context)),
    );
  }

  ListView _buildLoginForm(BuildContext context) {
    return ListView(
      children: [
        SizedBox(height: 100),
        _header(context),
        SizedBox(height: 64),
        _form(context),
        SizedBox(height: 24),
        _wrongCredentialsText(context),
        SizedBox(height: 40),
        _buttons(context),
      ],
    );
  }

  Container _header(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      height: MediaQuery.of(context).size.height / 7,
      child: Image.asset(
        "lib/assets/images/logo_bpkh_2.png",
        key: Key("logo_bpkh"),
        fit: BoxFit.fitHeight,
      ),
    );
  }

  Container _form(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            _emailField(context),
            _passwordField(context),
          ],
        ),
      ),
    );
  }

  Container _emailField(BuildContext context) {
    return Container(
      child: InputFormField(
        label: "Email",
        controller: _emailController,
        onChanged: (value) {},
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Email tidak boleh kosong.';
          }
          if (!EmailValidator.validate(value)) {
            return "Masukkan email dengan format yang benar.";
          }
          return null;
        },
      ),
    );
  }

  Container _passwordField(BuildContext context) {
    return Container(
      child: InputFormField(
        label: "Password",
        controller: _passwordController,
        onChanged: (value) {},
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Password tidak boleh kosong.';
          }
          return null;
        },
        obscureText: _obscurePasswordText,
        autocorrect: false,
        enableSuggestions: false,
        enableInteractiveSelections: false,
        suffixIcon: GestureDetector(
          onTap: () {
            _toggleObscurePasswordText();
          },
          child: Icon(
            _obscurePasswordText ? Icons.visibility : Icons.visibility_off,
          ),
        ),
      ),
    );
  }

  Container _buttons(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: ElevatedButton(
              child: _isLoadingLogin
                  ? SizedBox(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        strokeWidth: 3,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Theme.of(context).primaryColor),
                      ),
                    )
                  : Text("Masuk"),
              onPressed: () async {
                if (_wrongCredentials) _toggleWrongCredentials();

                if (_formKey.currentState!.validate()) {
                  _toggleLoadingLogin();

                  Map response = await api.login(
                      _emailController.text, _passwordController.text);

                  _toggleLoadingLogin();

                  if (response["success"] != null) {
                    SharedPreferences sharedPreferences =
                        await SharedPreferences.getInstance();

                    sharedPreferences.setString(
                        'token', response['token'] as String);
                    sharedPreferences.setString(
                        'login_time', DateTime.now().toString());
                    context
                        .read<AuthBloc>()
                        .add(AuthLoginEvent(response['role'] as String));

                    Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => HomePage()),
                      (route) => false,
                    );
                  } else {
                    _toggleWrongCredentials();
                  }
                }
              },
              style: OutlinedButton.styleFrom(
                  side: BorderSide(
                    width: 2,
                    color: Theme.of(context).primaryColor,
                  ),
                  backgroundColor: _isLoadingLogin
                      ? Colors.white
                      : Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(height: 16),
          InkWell(
            child: Text(
              "Lupa Password?",
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            onTap: () {
              _toggleForgotPassword();
            },
          )
        ],
      ),
    );
  }

  Widget _wrongCredentialsText(BuildContext context) {
    return Visibility(
      visible: _wrongCredentials,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: Text(
          "Log In gagal, alamat email tidak terdaftar atau password " +
              "yang dimasukkan tidak sesuai",
          style: TextStyle(
            color: Theme.of(context).errorColor,
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  ListView _buildForgotPassword(BuildContext context) {
    return ListView(
      children: [
        SizedBox(height: 100),
        _header(context),
        SizedBox(height: 64),
        _buildForgotPasswordText(context),
        SizedBox(height: 138),
        _buildBackButton(context),
      ],
    );
  }

  Container _buildForgotPasswordText(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Lupa Password?",
            style: TextStyle(
              color: HexColor("#333333"),
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 12),
          Text(
            "Segera hubungi admin untuk memperbarui password anda" +
                " dengan mengirim email ke zona.ariemenda@bpkh.go.id",
            style: TextStyle(
              color: HexColor("#333333"),
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }

  Container _buildBackButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        child: Text("Kembali ke Halaman Utama"),
        onPressed: () {
          _toggleForgotPassword();
        },
      ),
    );
  }
}
