class AuthState {
  AuthState(this.role);

  String? role;

  bool isRoleInternal() => role == 'Internal' || role == 'Admin';

  bool isRoleProfessional() => role == 'Professional';
}
