abstract class AuthEvent {}

class AuthGetRoleEvent extends AuthEvent {}

class AuthLoginEvent extends AuthEvent {
  AuthLoginEvent(this.role);

  String role;
}

class AuthLogoutEvent extends AuthEvent {}
