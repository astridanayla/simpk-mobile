import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/authentication/blocs/auth_event.dart';
import 'package:simpk/modules/authentication/blocs/auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthState(null)) {
    add(AuthGetRoleEvent());
  }

  AuthBloc.withRole(String role) : super(AuthState(role));

  static const prefRole = 'role';

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (event is AuthGetRoleEvent) {
      String? role = await sharedPreferences.getString(prefRole);
      yield AuthState(role);
    } else if (event is AuthLoginEvent) {
      await sharedPreferences.setString(prefRole, event.role);
      yield AuthState(event.role);
    } else if (event is AuthLogoutEvent) {
      if (await sharedPreferences.remove(prefRole)) {
        yield AuthState(null);
      }
    }
  }
}
