import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' show Dio, MultipartFile, FormData, Options;
import 'package:http/http.dart' show Client, Response;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/project/models/Attachment.dart';
import 'package:simpk/modules/project/models/Project.dart';

const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
    defaultValue: 'https://simpk-staging.herokuapp.com');

class ProjectAPIProvider {
  ProjectAPIProvider({required this.client, required this.dio});

  final Dio dio;
  final Client client;

  Future<Map<String, String>> _getAuthHeader({bool contentJson = false}) async {
    final pref = await SharedPreferences.getInstance();
    final token = await pref.getString('token');
    if (token == null) {
      throw ErrorNoLoginToken();
    }
    final header = {HttpHeaders.authorizationHeader: 'Bearer ' + token};

    if (contentJson) {
      header['Content-Type'] = 'application/json; charset=UTF-8';
    }

    return header;
  }

  Uri _getUri(String path) {
    final urlSplit = _baseUrl.split('://');
    if (urlSplit[0] == 'https') {
      return Uri.https(urlSplit.last, path);
    } else {
      return Uri.http(urlSplit.last, path);
    }
  }

  Future<int> updateProject(Project project) async {
    final response = await client
        .put(
          _getUri('monitoring/proyek/${project.id}'),
          headers: await _getAuthHeader(contentJson: true),
          body: jsonEncode(project.toJson()),
        )
        .timeout(Duration(seconds: 10))
        .onError((error, stackTrace) async => Response('onError', 400));

    return int.parse(response.statusCode.toString());
  }

  Future<Project> getProject(int projectId) async {
    final response = await client
        .get(
          _getUri('monitoring/proyek/$projectId'),
          headers: await _getAuthHeader(contentJson: true),
        )
        .timeout(Duration(seconds: 10))
        .onError((error, stackTrace) async => Response('onError', 400));

    return Project.fromJson(jsonDecode(response.body) as Map<String, dynamic>);
  }

  Future<bool> createAttachment({
    required String attachmentName,
    required File attachmentFile,
    required String attachmentType,
    required int projectId,
  }) async {
    String path = "/monitoring/proyek/$projectId/lampiran";
    Map<String, dynamic> body = {
      "lampiran_file": await MultipartFile.fromFile(attachmentFile.path),
      "nama_lampiran": attachmentName,
      "jenis_lampiran": attachmentType,
      "proyek": projectId,
    };

    FormData formData = FormData.fromMap(body);

    final response = await dio.post(
      path,
      data: formData,
      options: Options(
        headers: await _getAuthHeader(),
        contentType: 'multipart/form-data',
      ),
    );

    if (response.statusCode == 201) {
      return true;
    }
    return false;
  }

  Future<List<Attachment>> getAllAttachment(int projectId) async {
    final response = await client
        .get(
          _getUri('monitoring/proyek/$projectId/lampiran'),
          headers: await _getAuthHeader(contentJson: true),
        )
        .timeout(Duration(seconds: 10))
        .onError((error, stackTrace) async => Response('onError', 400));

    var jsonResponse = [];
    if (response.body.isNotEmpty) {
      jsonResponse = jsonDecode(response.body) as List<dynamic>;
    }

    List<Attachment> attachmentList = [];
    if (jsonResponse.length > 0) {
      for (int idx = 0; idx < jsonResponse.length; idx++) {
        Attachment attachment = Attachment.fromJson(jsonResponse[idx]);
        attachmentList.add(attachment);
      }
    }

    return attachmentList;
  }

  Future<bool> deleteAttachment(int projectId, int attachmentId) async {
    await client
        .delete(
          _getUri('monitoring/proyek/$projectId/lampiran/$attachmentId'),
          headers: await _getAuthHeader(contentJson: true),
        )
        .timeout(Duration(seconds: 10))
        .onError((error, stackTrace) async => Response('onError', 400));

    return true;
  }
}
