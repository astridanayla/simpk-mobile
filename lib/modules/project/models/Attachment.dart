import 'dart:io';

import 'package:flutter/material.dart';

class Attachment {
  Attachment(
      {required this.id,
      required this.attachmentName,
      required this.attachmentType,
      required this.attachmentPath,
      required this.dateCreated,
      required this.projectId,
      required this.userName,
      this.image,
      this.imageFile});

  factory Attachment.empty() {
    return Attachment(
        id: 0,
        attachmentName: "",
        attachmentType: "",
        dateCreated: DateTime.utc(2021, 1, 1),
        attachmentPath: "",
        projectId: 0,
        userName: "");
  }

  factory Attachment.fromJson(dynamic json_data) {
    const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
        defaultValue: 'https://simpk-staging.herokuapp.com');

    return Attachment(
      id: int.parse(json_data['id'].toString()),
      attachmentName: json_data["nama_lampiran"].toString(),
      attachmentType: json_data["jenis_lampiran"].toString(),
      attachmentPath: _baseUrl + json_data["lampiran_file"].toString(),
      dateCreated: DateTime.parse(json_data["created_at"].toString()),
      projectId: int.parse(json_data['proyek'].toString()),
      userName: json_data["user_name"].toString(),
      image: Image.network(_baseUrl + json_data["lampiran_file"].toString()),
    );
  }

  int id;
  String attachmentName;
  String attachmentType;
  String attachmentPath;
  int projectId;
  DateTime dateCreated;
  String userName;
  Image? image;
  File? imageFile;
}
