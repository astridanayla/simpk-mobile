class Project {
  Project({
    required this.id,
    required this.name,
    required this.status,
    required this.partner,
    required this.activityType,
    required this.region,
    required this.coordinate,
    required this.description,
    required this.progress,
    required this.allocation,
    this.monitoringSummary = "",
    this.partnerName,
    this.realization,
    this.reportCount,
  });

  factory Project.fromJson(Map<String, dynamic> json_data) {
    var project_name = json_data['nama_proyek'].toString();
    project_name = project_name != 'null' ? project_name : 'Proyek Tanpa Nama';
    return Project(
      id: int.parse(json_data['project_id'].toString()),
      name: project_name,
      status: json_data['status'].toString(),
      partner: json_data['mitra'].toString(),
      activityType: json_data['jenis_kegiatan'].toString(),
      region: json_data['daerah'].toString(),
      coordinate: json_data['koordinat_lokasi'].toString(),
      description: json_data['deskripsi'].toString(),
      progress: double.parse(json_data['progres'].toString()) / 100,
      allocation: double.parse(json_data['alokasi_dana'].toString()).toInt(),
      monitoringSummary: json_data['kesimpulan_monitoring'].toString(),
      partnerName: json_data['mitra_nama'] as String?,
      realization: json_data['total_realisasi'] as int?,
      reportCount: json_data['jumlah_laporan'] as int?,
    );
  }

  int id;
  String name;
  String status;
  String partner;
  String activityType;
  String region;
  String coordinate;
  String description;
  double progress;
  int allocation;
  String monitoringSummary;
  final String? partnerName;
  final int? realization;
  final int? reportCount;

  Map<String, dynamic> toJson() {
    return {
      'project_id': id,
      'nama_proyek': name,
      'status': status,
      'mitra': partner,
      'jenis_kegiatan': activityType,
      'daerah': region,
      'koordinat_lokasi': coordinate,
      'deskripsi': description,
      'progres': (progress * 100).round(),
      'alokasi_dana': allocation,
      'kesimpulan_monitoring': monitoringSummary,
    };
  }

  Project copy() {
    return Project(
      id: this.id,
      name: this.name,
      status: this.status,
      partner: this.partner,
      activityType: this.activityType,
      region: this.region,
      coordinate: this.coordinate,
      description: this.description,
      progress: this.progress,
      allocation: this.allocation,
      partnerName: this.partnerName,
      realization: this.realization,
      reportCount: this.reportCount,
      monitoringSummary: this.monitoringSummary,
    );
  }
}
