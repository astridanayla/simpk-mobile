import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simpk/components/custom_dropdown.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';
import 'package:simpk/services/image_compressor.dart';

class AddAttachment extends StatefulWidget {
  AddAttachment({required this.projectId, Key? key}) : super(key: key);

  final int projectId;

  final _AddAttachmentState state = _AddAttachmentState();

  @override
  _AddAttachmentState createState() => state;
}

class _AddAttachmentState extends State<AddAttachment> {
  String? _attachmentType;
  String _attachmentName = 'File Name';
  File? _attachmentItem;

  TextEditingController _attachmentNameController = TextEditingController();
  final imagePicker = ImagePicker();
  final ProjectAPIProvider api = GetIt.I();
  bool initialState = true;
  bool _isLoadingSaveData = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 56, horizontal: 16),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text("Tambah Lampiran",
                style: Theme.of(context).textTheme.headline1),
            SizedBox(height: 48),
            CustomDropdown(
                label: 'Pilih Jenis Lampiran',
                items: ['Foto', 'Berkas'],
                value: _attachmentType,
                onChanged: (value) {
                  setState(() {
                    _attachmentType = value;
                    _attachmentItem = null;
                    _attachmentName = '';
                    initialState = true;
                    _attachmentNameController.text = _attachmentName;
                  });
                }),
            _buildAttachmentTypeImageSelected(),
            _buildAttachmentTypeFileSelected()
          ]),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 40),
          child: _buildBottomNav(),
        ));
  }

  Widget _buildBottomNav() {
    String text = 'Batal';
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                  side: BorderSide(
                      width: 2, color: Theme.of(context).primaryColor),
                  primary: Theme.of(context).primaryColor),
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text(text)),
        ),
        SizedBox(width: 8),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: _valuesAreNotEmpty() ? () => {_storeData()} : null,
            child: _isLoadingSaveData
                ? SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white,
                      ),
                    ),
                  )
                : Text("Simpan"),
          ),
        ),
      ],
    );
  }

  Widget _buildAttachmentTypeImageSelected() {
    return Visibility(
      visible: _attachmentType == 'Foto',
      child: Column(
        children: [
          SizedBox(height: 16),
          _buildTextField(
              label: 'Nama Lampiran', controller: _attachmentNameController),
          SizedBox(height: 24),
          Visibility(
              visible: initialState,
              child: OutlinedButton(
                onPressed: getImage,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.photo_camera),
                    SizedBox(width: 8),
                    Text("Ambil Foto")
                  ],
                ),
              )),
          Visibility(
              visible: !initialState,
              child: Container(
                  width: double.maxFinite,
                  height: MediaQuery.of(context).size.height / 15,
                  child: Row(children: [
                    SizedBox(width: 5),
                    Icon(
                      Icons.insert_photo,
                      color: Color(0xff01788E),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: Text(
                        _attachmentName + ".jpg",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                    ),
                    InkWell(
                      child: Text("Edit",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 14)),
                      onTap: getImage,
                    ),
                    SizedBox(width: 10),
                  ]))),
        ],
      ),
    );
  }

  Widget _buildAttachmentTypeFileSelected() {
    return Visibility(
      visible: _attachmentType == 'Berkas',
      child: Column(
        children: [
          SizedBox(height: 16),
          _buildTextField(
              label: 'Nama Lampiran', controller: _attachmentNameController),
          SizedBox(height: 24),
          Visibility(
              visible: initialState,
              child: OutlinedButton(
                onPressed: getFile,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.file_upload),
                    SizedBox(width: 8),
                    Text("Upload Berkas")
                  ],
                ),
              )),
          Visibility(
              visible: !initialState,
              child: Container(
                  width: double.maxFinite,
                  height: MediaQuery.of(context).size.height / 15,
                  child: Row(children: [
                    SizedBox(width: 5),
                    Icon(Icons.insert_drive_file, color: Color(0xff01788E)),
                    SizedBox(width: 16),
                    Expanded(
                        child: Text(
                      _attachmentName + " file",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                    )),
                    InkWell(
                      child: Text("Edit",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 14)),
                      onTap: getFile,
                    ),
                    SizedBox(width: 10)
                  ]))),
        ],
      ),
    );
  }

  Widget _buildTextField(
      {required String label, required TextEditingController controller}) {
    return TextFormField(
      controller: controller,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        labelText: label,
      ),
      onChanged: (value) {
        setState(() {
          _attachmentName = value;
        });
      },
    );
  }

  bool _valuesAreNotEmpty() {
    return _attachmentType != null &&
        _attachmentName != "File Name" &&
        _attachmentItem != null &&
        !_isLoadingSaveData;
  }

  Future getImage() async {
    final pickedFile = await imagePicker.getImage(source: ImageSource.gallery);

    File? compressedImage = await compressImage(File(pickedFile!.path));

    setState(() {
      if (compressedImage != null) {
        _attachmentItem = compressedImage;

        initialState = false;
      }
    });
  }

  Future getFile() async {
    FilePickerResult result = (await FilePicker.platform.pickFiles())!;

    setState(() {
      _attachmentItem = File(result.files.single.path!);
      debugPrint(_attachmentItem.toString());
      initialState = false;
      debugPrint(initialState.toString());
    });
  }

  void setFileName(String name) async {
    _attachmentName = name;
  }

  void _storeData() async {
    bool success = false;
    if(!_isLoadingSaveData){
      _toggleIsLoadingSaveData();
      success = await api.createAttachment(
      attachmentName: _attachmentName,
      attachmentType: _attachmentType!,
      attachmentFile: _attachmentItem!,
      projectId: widget.projectId,
      );
    }

    if (!success) {
      _buildsnackbar(context, Text("Lampiran gagal tersimpan"));
      _toggleIsLoadingSaveData();
    } else {
      _buildsnackbar(context, Text("Lampiran berhasil tersimpan"));
      _toggleIsLoadingSaveData();
      Navigator.pop(context, false);
    }
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildsnackbar(
      BuildContext context, Widget content) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }

  void _toggleIsLoadingSaveData() {
    setState(() {
      _isLoadingSaveData = !_isLoadingSaveData;
    });
  }
}
