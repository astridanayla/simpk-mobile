import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:simpk/modules/booking/screens/booking_tab.dart';
import 'package:simpk/modules/monitoring/screens/monitoring_tab.dart';
import 'package:simpk/modules/project/blocs/lampiran_bloc.dart';
import 'package:simpk/modules/project/blocs/project_bloc.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/project_detail_tab.dart';

class ProjectDashboardPage extends StatefulWidget {
  ProjectDashboardPage({required this.project, Key? key}) : super(key: key);

  final Project project;

  @override
  _ProjectDashboardPageState createState() => _ProjectDashboardPageState();
}

class _ProjectDashboardPageState extends State<ProjectDashboardPage> {
  late TabBarView myTabBarView;

  void initState() {
    myTabBarView = TabBarView(
      children: [
        ProjectDetailPage(),
        MonitoringTab(projectId: widget.project.id),
        BookingTab(project: widget.project),
      ],
    );
    super.initState();
  }

  TabBar tabBar = TabBar(tabs: [
    Tab(
      text: "RINGKASAN",
    ),
    Tab(
      text: "MONITORING",
    ),
    Tab(
      text: "BOOKING",
    )
  ]);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProjectBloc>(
          create: (context) => ProjectBloc(widget.project),
        ),
        BlocProvider<AttachmentBloc>(
            create: (context) => AttachmentBloc(widget.project.id)),
      ],
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
              title: Column(
                children: [
                  Text(
                    "PROYEK",
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        ?.copyWith(color: HexColor("#0A3254")),
                  ),
                  SizedBox(height: 8),
                  Text(
                    widget.project.name,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ],
              ),
              centerTitle: true,
              bottom: tabBar),
          body: myTabBarView,
        ),
      ),
    );
  }
}
