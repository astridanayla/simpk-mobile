import 'package:flutter/material.dart';

class EditDetailPage extends StatefulWidget {
  EditDetailPage(
    this.title,
    this.label,
    this.placeholder, {
    this.inputInstruction,
  });

  final String title;
  final String label;
  final String placeholder;
  final String? inputInstruction;

  @override
  _EditDetailPageState createState() => _EditDetailPageState();
}

class _EditDetailPageState extends State<EditDetailPage> {
  late TextEditingController _controller;
  final _formKey = GlobalKey<FormState>();
  bool _btnEnabled = true;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: widget.placeholder);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  int _getMinLine() {
    if ((widget.title != "Edit Deskripsi Proyek") &&
        (widget.title != "Tambah Kesimpulan")) {
      return 1;
    } else {
      return 10;
    }
  }

  Widget _buildBottomNav() {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
                side:
                    BorderSide(width: 2, color: Theme.of(context).primaryColor),
                primary: Theme.of(context).primaryColor),
            onPressed: () {
              Navigator.pop(context, null);
            },
            child: Text('Batal'),
          ),
        ),
        SizedBox(width: 8),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: _btnEnabled
                ? () {
                    Navigator.pop(context, _controller.text);
                  }
                : null,
            child: Text("Simpan"),
          ),
        ),
      ],
    );
  }

  Widget _buildTextField({
    required String label,
  }) {
    return Form(
      key: _formKey,
      onChanged: () =>
          setState(() => _btnEnabled = _formKey.currentState!.validate()),
      child: TextFormField(
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(
            labelText: widget.label,
            alignLabelWithHint: true,
          ),
          maxLines: 20,
          controller: _controller,
          keyboardType: widget.title == "Edit Progress"
              ? TextInputType.number
              : TextInputType.text,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: (value) {
            if (widget.title == "Edit Progress") {
              if (value == null ||
                  int.tryParse(value) == null ||
                  int.parse(value) < 0 ||
                  int.parse(value) > 100) {
                return "Input harus desimal 0-100";
              }
            } else if (widget.title == "Edit Lokasi") {
              if (!value!.contains(", ") ||
                  double.tryParse(value.split(", ")[0]) == null ||
                  double.tryParse(value.split(", ")[1]) == null) {
                return "Input harus koordinat yang dipisahkan oleh ', '";
              }
            }
          },
          minLines: _getMinLine()),
    );
  }

  Widget _buildDescription() {
    final instruction = widget.inputInstruction;
    if (instruction == null) {
      return Container();
    } else {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(height: 16),
        Text(
          "Catatan:",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Text(
          instruction,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 56, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.title,
                style: Theme.of(context).textTheme.headline1,
              ),
              SizedBox(height: 48),
              _buildTextField(label: widget.label),
              _buildDescription()
            ],
          )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.fromLTRB(16, 0, 16, 40),
        child: _buildBottomNav(),
      ),
    );
  }
}
