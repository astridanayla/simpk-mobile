import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:simpk/components/custom_card.dart';
import 'package:simpk/components/ghost_button.dart';
import 'package:simpk/components/progress_bar.dart';
import 'package:simpk/components/secondary_button.dart';
import 'package:simpk/modules/project/blocs/lampiran_bloc.dart';
import 'package:simpk/modules/project/blocs/lampiran_event.dart';
import 'package:simpk/modules/project/blocs/lampiran_state.dart';
import 'package:simpk/modules/project/blocs/project_bloc.dart';
import 'package:simpk/modules/project/blocs/project_event.dart';
import 'package:simpk/modules/project/blocs/project_state.dart';
import 'package:simpk/modules/project/models/Attachment.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/add_attachment.dart';
import 'package:simpk/modules/project/screens/attachment_file_detail_page.dart';
import 'package:simpk/modules/project/screens/attachment_image_detail_page.dart';
import 'package:simpk/modules/project/screens/edit_detail_page.dart';
import 'package:url_launcher/url_launcher.dart';

class ProjectDetailPage extends StatelessWidget {
  Widget _buildFinanceReport(BuildContext context, Project project) {
    final idrFormat =
        NumberFormat.currency(locale: 'id_ID', symbol: 'Rp ', decimalDigits: 0);

    final Map<String, String> financeMap = {
      "Dana yang Disetujui": idrFormat.format(project.allocation),
    };

    final realization = project.realization;
    if (realization != null) {
      final remaining = project.allocation - realization;
      financeMap.addAll({
        "Total Realisasi": idrFormat.format(realization),
        "Sisa Dana": idrFormat.format(remaining),
      });
    } else {
      financeMap.addAll({
        "Total Realisasi": 'Rp -',
        "Sisa Dana": 'Rp -',
      });
    }

    List<Widget> columnChildren = [];
    for (var idx = 0; idx < financeMap.length; idx++) {
      Widget dataRow = Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(financeMap.keys.elementAt(idx),
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Color(0xFF828282))),
              Text(financeMap.values.elementAt(idx),
                  style: Theme.of(context).textTheme.bodyText1),
            ],
          ));
      columnChildren.add(dataRow);
    }
    return Container(
        margin: EdgeInsets.fromLTRB(0, 12, 0, 40),
        child: Column(children: columnChildren));
  }

  String titleCase(String text) {
    if (text.length <= 1) return text.toUpperCase();
    var words = text.split(' ');
    var capitalized = words.map((word) {
      var first = word.substring(0, 1).toUpperCase();
      var rest = word.substring(1);
      return '$first$rest';
    });
    return capitalized.join(' ');
  }

  String _textIfNotEmpty(String text, String label) {
    if (text.isNotEmpty) {
      return text;
    } else {
      return "$label kosong";
    }
  }

  Widget _buildBiodata(BuildContext context, Project project) {
    Map<String, String> biodataMap = {
      "Status": titleCase(project.status),
      "Mitra": project.partnerName ?? project.partner,
      "Jenis Kegiatan": project.activityType,
      "Daerah": project.region,
      "Koordinat Lokasi": _textIfNotEmpty(project.coordinate, "Koordinat"),
    };

    List<Widget> columnChildren = [];
    for (var idx = 0; idx < biodataMap.length; idx++) {
      List<Widget> rowChildren = [];
      rowChildren.add(Expanded(
        flex: 3,
        child: Text(biodataMap.keys.elementAt(idx),
            style: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(color: Color(0xFF828282))),
      ));
      rowChildren.add(SizedBox(width: 16));
      rowChildren.add(Expanded(
        flex: 5,
        child: Text(biodataMap.values.elementAt(idx),
            style: Theme.of(context).textTheme.bodyText1),
      ));
      if (idx == biodataMap.length - 1) {
        rowChildren.add(Expanded(
            flex: 1,
            child: Container(
              width: 32,
              height: 32,
              margin: EdgeInsets.zero,
              child: IconButton(
                padding: EdgeInsets.zero,
                icon: Icon(Icons.copy, size: 20),
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  final snackBar = SnackBar(
                    content: Text("Teks berhasil disalin!"),
                    width: 360.0,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                    behavior: SnackBarBehavior.floating,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                  );
                  Clipboard.setData(
                      ClipboardData(text: biodataMap.values.elementAt(idx)));
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
              ),
            )));
      } else {
        rowChildren.add(Expanded(flex: 1, child: Container()));
      }
      columnChildren.add(Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 12),
        child: Row(
            children: rowChildren,
            crossAxisAlignment: CrossAxisAlignment.start),
      ));
    }
    return Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
        child: Column(children: columnChildren));
  }

  Future _openMap(String latLong) async {
    String mapsUrl = 'https://www.google.com/maps/search/?api=1&query=$latLong';
    await launch(mapsUrl);
  }

  Widget generateAttachmentList(
      List<Attachment> attachments, BuildContext context) {
    if (attachments.length < 1) {
      return Text(
        "Belum ada lampiran yang telah dibuat",
        style: Theme.of(context).textTheme.bodyText1,
      );
    } else {
      List<Widget> attachmentList = [];
      for (Attachment attachment in attachments) {
        if (attachment.attachmentType == "Foto") {
          attachmentList.add(
            CustomCard(
                title: attachment.attachmentName,
                userName: attachment.userName,
                createdOn: attachment.dateCreated,
                onClick: AttachmentImageDetail(
                    projectId: attachment.projectId, attachment: attachment),
                userIcon: Icon(Icons.account_circle),
                displayIcon: Icons.photo_size_select_actual,
                afterOnClick: () =>
                    context.read<AttachmentBloc>().add(AttachmentFetchEvent())),
          );
        } else {
          attachmentList.add(
            CustomCard(
              title: attachment.attachmentName,
              userName: attachment.userName,
              createdOn: attachment.dateCreated,
              onClick: AttachmentFileDetail(
                  projectId: attachment.projectId, attachment: attachment),
              userIcon: Icon(Icons.account_circle),
              displayIcon: Icons.file_present,
              afterOnClick: () =>
                  context.read<AttachmentBloc>().add(AttachmentFetchEvent()),
            ),
          );
        }
      }
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: attachmentList);
    }
  }

  Widget _buildAttachmentList(BuildContext context, int projectId) {
    return BlocBuilder<AttachmentBloc, AttachmentState>(
        builder: (context, state) {
      final attachmentState = state.attachments;
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Lampiran Proyek", style: Theme.of(context).textTheme.headline3),
          SizedBox(height: 16),
          generateAttachmentList(attachmentState, context),
          SizedBox(height: 16),
          SecondaryButton(
            text: "Tambah Lampiran",
            iconData: Icons.add_circle_outline,
            onPressed: () async {
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          AddAttachment(projectId: projectId)));
              context.read<AttachmentBloc>().add(AttachmentFetchEvent());
            },
          ),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProjectBloc, ProjectState>(
      listener: (context, state) {
        if (state.failedMessage != null) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.failedMessage!),
          ));
        }
      },
      child: Material(
        child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(16, 32, 16, 48),
          child: BlocBuilder<ProjectBloc, ProjectState>(
            builder: (context, state) {
              final projectState = state.project;
              final count = projectState.reportCount ?? '-';
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Progress Penyelesaian",
                      style: Theme.of(context).textTheme.headline3),
                  SizedBox(height: 16),
                  Text(
                    "Total Monitoring : $count kali",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  SizedBox(height: 16),
                  ProgressBar(progressValue: projectState.progress),
                  GhostButton(
                    text: "Edit Progress",
                    iconData: Icons.edit,
                    targetPage: EditDetailPage(
                      "Edit Progress",
                      "Persentase Progress",
                      (projectState.progress * 100).round().toString(),
                      inputInstruction: "Masukkan bilangan desimal 0-100",
                    ),
                    startOffset: Offset(0.0, 1.0),
                    onClick: (result) async {
                      if (result == null) {
                        return;
                      }
                      final updated = projectState.copy()
                        ..progress = double.parse(result as String) / 100;

                      context.read<ProjectBloc>().add(ProjectUpdateEvent(
                            updated,
                            'Error updating progress.',
                          ));
                    },
                  ),
                  _buildFinanceReport(context, projectState),
                  Text("Biodata Proyek",
                      style: Theme.of(context).textTheme.headline3),
                  SizedBox(height: 16),
                  _buildBiodata(context, projectState),
                  Row(
                    children: [
                      ElevatedButton(
                        child: Text("Rute ke Lokasi"),
                        onPressed: () => _openMap(projectState.coordinate),
                      ),
                      SizedBox(width: 16),
                      GhostButton(
                        text: "Edit Lokasi",
                        iconData: Icons.edit,
                        targetPage: EditDetailPage(
                          "Edit Lokasi",
                          "Koordinat Lokasi",
                          projectState.coordinate,
                        ),
                        startOffset: Offset(0.0, 1.0),
                        onClick: (result) async {
                          if (result == null) {
                            return;
                          }

                          final updated = projectState.copy()
                            ..coordinate = result as String;

                          context.read<ProjectBloc>().add(ProjectUpdateEvent(
                                updated,
                                'Error updating coordinate.',
                              ));
                        },
                      )
                    ],
                  ),
                  SizedBox(height: 48),
                  Text("Deskripsi Proyek",
                      style: Theme.of(context).textTheme.headline3),
                  SizedBox(height: 16),
                  Text(
                    _textIfNotEmpty(projectState.description, "Deskripsi"),
                    style: Theme.of(context).textTheme.bodyText1,
                    textAlign: TextAlign.justify,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 4, 0, 32),
                    child: GhostButton(
                      text: "Edit Deskripsi",
                      iconData: Icons.edit,
                      targetPage: EditDetailPage(
                        "Edit Deskripsi Proyek",
                        "Deskripsi Proyek",
                        projectState.description,
                      ),
                      startOffset: Offset(0.0, 1.0),
                      onClick: (result) async {
                        if (result == null) {
                          return;
                        }

                        final updated = projectState.copy()
                          ..description = result as String;

                        context.read<ProjectBloc>().add(ProjectUpdateEvent(
                              updated,
                              'Error updating description.',
                            ));
                      },
                    ),
                  ),
                  _buildAttachmentList(context, projectState.id),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
