import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:simpk/modules/project/models/Attachment.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

class AttachmentImageDetail extends StatefulWidget {
  AttachmentImageDetail(
      {required this.projectId, required this.attachment, Key? key})
      : super(key: key);

  final Attachment attachment;
  final int projectId;

  @override
  _AttachmentImageDetailState createState() => _AttachmentImageDetailState();
}

class _AttachmentImageDetailState extends State<AttachmentImageDetail> {
  final ProjectAPIProvider _apiProvider = GetIt.I();

  String _getFormatedLastEdited() {
    String formattedTime =
        DateFormat('dd/MM/yyyy').format(widget.attachment.dateCreated);
    return "Dibuat pada: " + formattedTime;
  }

  Widget _buildImage() {
    return widget.attachment.image!;
  }

  void _goBack(BuildContext context) {
    int count = 0;
    Navigator.popUntil(context, (route) {
      return count++ == 2;
    });
  }

  void _onDelete() async {
    await _apiProvider.deleteAttachment(widget.projectId, widget.attachment.id);
    _goBack(context);
  }

  void showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
            (states) => Color(0xFF4F4F4F).withOpacity(0.1)),
      ),
      child: Text("Kembali", style: TextStyle(color: Color(0xFF4F4F4F))),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
            (states) => Theme.of(context).errorColor.withOpacity(0.1)),
      ),
      child:
          Text("Hapus", style: TextStyle(color: Theme.of(context).errorColor)),
      onPressed: () {
        _onDelete();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text(
        "Perhatian!",
        style: Theme.of(context).textTheme.headline2,
      ),
      content: Text(
        "Anda yakin ingin menghapus lampiran ini?",
        style: Theme.of(context).textTheme.bodyText1,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(elevation: 0),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(0, 8, 0, 40),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            widget.attachment.attachmentName,
                            style: Theme.of(context).textTheme.headline1,
                          ),
                        ),
                        IconButton(
                          key: Key('btn_delete'),
                          padding: EdgeInsets.zero,
                          constraints: BoxConstraints(
                            maxHeight: 24,
                            maxWidth: 24,
                          ),
                          icon: Icon(Icons.delete, size: 24),
                          onPressed: () {
                            showAlertDialog(context);
                          },
                          color: Theme.of(context).errorColor,
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Row(children: [
                      Text(
                        _getFormatedLastEdited(),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            ?.copyWith(color: Color(0xFF828282)),
                        textAlign: TextAlign.left,
                      ),
                    ]),
                    SizedBox(height: 24),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.account_circle,
                          color: HexColor("#BDBDBD"),
                          size: 32,
                        ),
                        SizedBox(width: 12),
                        Text(widget.attachment.userName,
                            style: Theme.of(context).textTheme.headline4),
                      ],
                    ),
                    SizedBox(height: 36),
                  ],
                ),
              ),
              _buildImage()
            ],
          ),
        ));
  }
}
