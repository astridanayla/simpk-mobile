import 'package:simpk/modules/project/models/Project.dart';

abstract class ProjectEvent {}

class ProjectFetchEvent extends ProjectEvent {}

class ProjectUpdateEvent extends ProjectEvent {
  ProjectUpdateEvent(this.newProject, this.failedMessage);

  Project newProject;
  String failedMessage;
}
