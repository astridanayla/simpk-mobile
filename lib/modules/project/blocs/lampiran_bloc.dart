import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/modules/project/blocs/lampiran_event.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import 'package:simpk/modules/project/blocs/lampiran_state.dart';

class AttachmentBloc extends Bloc<AttachmentEvent, AttachmentState> {
  AttachmentBloc(this.projectId, {bool skip = false})
      : super(AttachmentState([])) {
    if (!skip) add(AttachmentFetchEvent());
  }
  final int projectId;
  final ProjectAPIProvider _apiProvider = GetIt.I();

  @override
  Stream<AttachmentState> mapEventToState(AttachmentEvent event) async* {
    if (event is AttachmentFetchEvent) {
      final attachments = await _apiProvider.getAllAttachment(projectId);
      yield AttachmentState(attachments);
    }
  }
}
