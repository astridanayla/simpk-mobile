import 'package:simpk/modules/project/models/Attachment.dart';

class AttachmentState {
  AttachmentState(this.attachments, [this.failedMessage]);

  List<Attachment> attachments;
  String? failedMessage;
}
