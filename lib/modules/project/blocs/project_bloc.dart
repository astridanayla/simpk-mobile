import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/modules/project/blocs/project_event.dart';
import 'package:simpk/modules/project/blocs/project_state.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

class ProjectBloc extends Bloc<ProjectEvent, ProjectState> {
  ProjectBloc(Project project, {bool skip = false})
      : super(ProjectState(project)) {
    if (!skip) add(ProjectFetchEvent());
  }

  final ProjectAPIProvider _apiProvider = GetIt.I();

  @override
  Stream<ProjectState> mapEventToState(ProjectEvent event) async* {
    if (event is ProjectFetchEvent) {
      try {
        final project = await _apiProvider.getProject(state.project.id);
        yield ProjectState(project);
      } catch (e) {
        yield ProjectState(state.project, 'get project error');
      }
    } else if (event is ProjectUpdateEvent) {
      final resultStatusCode =
          await _apiProvider.updateProject(event.newProject);
      if (resultStatusCode == 202) {
        yield ProjectState(event.newProject);
      } else {
        yield ProjectState(state.project, event.failedMessage);
      }
    }
  }
}
