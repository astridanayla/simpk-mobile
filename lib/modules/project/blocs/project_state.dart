import 'package:simpk/modules/project/models/Project.dart';

class ProjectState {
  ProjectState(this.project, [this.failedMessage]);

  Project project;
  String? failedMessage;
}
