class Booking {
  Booking(
      {required int this.id,
      required String this.activityName,
      required String this.date,
      required String this.time,
      required String this.creatorName,
      String? this.description});

  factory Booking.empty() {
    return Booking(
        id: 0,
        activityName: "",
        date: "",
        time: "",
        description: "",
        creatorName: "");
  }

  factory Booking.fromJson(dynamic json_data) {
    return Booking(
        id: int.parse(json_data['id'].toString()),
        activityName: json_data['nama_kegiatan'].toString(),
        date: json_data['tanggal_booking'].toString(),
        time: json_data['waktu_booking'].toString(),
        description: json_data['deskripsi'].toString(),
        creatorName: json_data['user_name'].toString());
  }

  int id;
  String activityName;
  String date;
  String time;
  String? description = "";
  String creatorName;
}
