import 'dart:convert' as convert;
import 'dart:io';

import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';

const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
    defaultValue: 'https://simpk-staging.herokuapp.com');

class BookingAPIProvider {
  BookingAPIProvider({
    required this.client,
  });

  final Client client;

  Future<Map<String, String>> _getAuthHeader({bool contentJson = false}) async {
    final pref = await SharedPreferences.getInstance();
    final token = await pref.getString('token');
    if (token == null) {
      throw ErrorNoLoginToken();
    }
    final header = {HttpHeaders.authorizationHeader: 'Bearer ' + token};

    if (contentJson) {
      header['Content-Type'] = 'application/json; charset=UTF-8';
    }

    return header;
  }

  Uri _getUri(String path) {
    final urlSplit = _baseUrl.split('://');
    if (urlSplit[0] == 'https') {
      return Uri.https(urlSplit.last, path);
    } else {
      return Uri.http(urlSplit.last, path);
    }
  }

  Future<List<Booking>> getAllBooking(int pk) async {
    Response response = await client
        .get(
          _getUri('booking-app/by-proyek/$pk'),
          headers: await _getAuthHeader(contentJson: true),
        )
        .timeout(Duration(seconds: 10))
        .onError((error, stackTrace) async => Response('onError', 400));

    var jsonResponse = [];
    if (response.body.isNotEmpty) {
      jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    }

    List<Booking> bookingList = [];
    if (jsonResponse.length > 0) {
      for (int idx = 0; idx < jsonResponse.length; idx++) {
        Booking booking = Booking.fromJson(jsonResponse[idx]);
        bookingList.add(booking);
      }
    }

    return bookingList;
  }

  Future<bool> deleteBooking(
    int projectId,
    int bookingId,
  ) async {
    await client
        .delete(
          _getUri('booking-app/by-proyek/$projectId/booking/$bookingId'),
          headers: await _getAuthHeader(contentJson: true),
        )
        .timeout(Duration(seconds: 10))
        .onError((error, stackTrace) async => Response('onError', 400));

    return true;
  }
}
