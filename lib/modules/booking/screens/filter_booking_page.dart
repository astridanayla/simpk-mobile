import 'package:flutter/material.dart';
import 'package:simpk/components/basic_appbar.dart';
import 'package:simpk/components/labeled_checkbox.dart';

class FilterBookingPage extends StatefulWidget {
  @override
  _FilterBookingPageState createState() => _FilterBookingPageState();
}

enum PersonList { saya, semua }

class _FilterBookingPageState extends State<FilterBookingPage> {
  PersonList? _person;

  Map<String, bool> timeRangeList = {
    "Hari ini": false,
    "Minggu ini": false,
    "Dalam 3 Hari": false,
    "Bulan ini": false
  };

  Wrap _checkboxLayout(
      Map<String, bool> dataMap, double spacing, double height) {
    List<Widget> rowChildren = [];
    for (var idx = 0; idx < dataMap.length; idx++) {
      Widget labeledCheckbox = LabeledCheckbox(
          label: dataMap.keys.elementAt(idx),
          value: dataMap[dataMap.keys.elementAt(idx)]!,
          onChanged: (bool newValue) {
            setState(() {
              dataMap[dataMap.keys.elementAt(idx)] = newValue;
            });
          });
      rowChildren.add(labeledCheckbox);
    }
    return Wrap(
        alignment: WrapAlignment.start,
        spacing: spacing,
        runSpacing: height,
        direction: Axis.horizontal,
        children: rowChildren);
  }

  void _goBack(BuildContext context) {
    Navigator.pop(context, false);
  }

  @override
  Widget build(BuildContext) {
    return Scaffold(
      appBar: BasicAppbar("Filter"),
      body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(16, 32, 16, 48),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text("Staff Ditugaskan",
                style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 4),
            Wrap(
                alignment: WrapAlignment.start,
                direction: Axis.horizontal,
                children: [
                  _customListTile("Saya", PersonList.saya, context),
                  _customListTile("Semua", PersonList.semua, context)
                ]),
            SizedBox(height: 24),
            Text("Tanggal Booking",
                style: Theme.of(context).textTheme.headline3),
            SizedBox(height: 20),
            _checkboxLayout(timeRangeList, 32, 16),
            SizedBox(height: 48),
          ])),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.symmetric(vertical: 40, horizontal: 16),
        child: ElevatedButton(
            child: Text("Terapkan Filter"),
            onPressed: () {
              _goBack(context);
            }),
      ),
    );
  }

  Widget _customListTile(String title, PersonList value, BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      width: 160,
      child: RadioListTile<PersonList>(
        title: Text(title,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(height: 1)),
        contentPadding: EdgeInsets.zero,
        value: value,
        groupValue: _person,
        onChanged: (PersonList? value) => {
          setState(() {
            _person = value;
          })
        },
      ),
    );
  }
}
