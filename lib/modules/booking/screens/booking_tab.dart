import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/booking_card.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/booking/screens/booking_detail.dart';
import 'package:simpk/modules/booking/screens/create_booking.dart';
import 'package:simpk/modules/booking/services/booking_api_provider.dart';

import 'package:simpk/modules/project/models/Project.dart';

class BookingTab extends StatefulWidget {
  BookingTab({required this.project, Key? key}) : super(key: key);

  final Project project;

  @override
  _BookingTabState createState() => new _BookingTabState();
}

class _BookingTabState extends State<BookingTab> {
  List<Booking> bookingList = [];
  final BookingAPIProvider _apiProvider = GetIt.I();

  @override
  void initState() {
    super.initState();
    fetchBooking();
  }

  void setBookings(List<Booking> newBookingList) {
    setState(() {
      bookingList = newBookingList;
    });
  }

  void fetchBooking() async {
    _apiProvider.getAllBooking(widget.project.id).then((value) {
      if (mounted) {
        setBookings(value);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(16, 32, 16, 40),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildCreateBoooking(context),
                  _buildBookingList(context)
                ])));
  }

  Widget _buildCreateBoooking(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
            child: Text("Buat Booking Kunjungan",
                style: Theme.of(context).textTheme.headline3),
          ),
          Text(
              "Buat booking kunjungan baru atau " +
                  "lihat booking kunjungan yang telah anda buat.",
              style: Theme.of(context).textTheme.bodyText1),
          Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 32),
            child: ElevatedButton.icon(
              key: Key('buat_booking_button'),
              label: Text('Buat Booking'),
              icon: Icon(Icons.add_circle_outline_rounded),
              onPressed: () {
                Navigator.push(
                    context,
                    PageRouteBuilder(
                      pageBuilder: (context, animation, secondaryAnimation) =>
                          CreateBooking(projectId: widget.project.id),
                      transitionsBuilder:
                          (context, animation, secondaryAnimation, child) {
                        var begin = Offset(0.0, 1.0);
                        var end = Offset.zero;
                        var curve = Curves.ease;

                        var tween = Tween(begin: begin, end: end)
                            .chain(CurveTween(curve: curve));

                        return SlideTransition(
                          position: animation.drive(tween),
                          child: child,
                        );
                      },
                    )).then((_) => fetchBooking());
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget generateBookingList(List<Booking> bookings) {
    if (bookings.length < 1) {
      return Text(
        "Belum ada booking yang telah dibuat",
        style: Theme.of(context).textTheme.bodyText1,
      );
    } else {
      List<Widget> bookingList = [];
      for (Booking booking in bookings) {
        bookingList.add(
          BookingCard(
            projectName: widget.project.name,
            bookingDate: DateTime.parse(booking.date + " " + booking.time),
            bookingTitle: booking.activityName,
            bookingTime: DateTime.parse(booking.date + " " + booking.time),
            assignedUser: booking.creatorName,
            onClick: BookingDetail(project: widget.project, booking: booking),
            afterOnClick: () => fetchBooking(),
          ),
        );
      }
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start, children: bookingList);
    }
  }

  Widget _buildBookingList(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 16),
          child: Text("Daftar Booking",
              style: Theme.of(context).textTheme.headline3),
        ),
        Container(
          child: generateBookingList(bookingList),
        ),
      ],
    );
  }
}
