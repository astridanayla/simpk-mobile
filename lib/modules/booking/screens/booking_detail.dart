import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/booking/services/booking_api_provider.dart';
import 'package:simpk/modules/project/models/Project.dart';

class BookingDetail extends StatefulWidget {
  BookingDetail({required this.project, required this.booking, Key? key})
      : super(key: key);

  final Project project;
  final Booking booking;

  @override
  _BookingDetailState createState() => new _BookingDetailState();
}

class _BookingDetailState extends State<BookingDetail> {
  final BookingAPIProvider _apiProvider = GetIt.I();

  void _goBack(BuildContext context) {
    int count = 0;
    Navigator.popUntil(context, (route) {
      return count++ == 2;
    });
  }

  void _onDelete() async {
    await _apiProvider.deleteBooking(widget.project.id, widget.booking.id);
    _goBack(context);
  }

  void showAlertDialog(BuildContext context) {
    Widget cancelButton = TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
            (states) => Color(0xFF4F4F4F).withOpacity(0.1)),
      ),
      child: Text("Kembali", style: TextStyle(color: Color(0xFF4F4F4F))),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
            (states) => Theme.of(context).errorColor.withOpacity(0.1)),
      ),
      child:
          Text("Hapus", style: TextStyle(color: Theme.of(context).errorColor)),
      onPressed: () {
        _onDelete();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text(
        "Perhatian!",
        style: Theme.of(context).textTheme.headline2,
      ),
      content: Text(
        "Anda yakin ingin menghapus booking ini?",
        style: Theme.of(context).textTheme.bodyText1,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _buildDeleteButton(BuildContext context) {
    return IconButton(
      key: Key('btn_delete'),
      padding: EdgeInsets.zero,
      constraints: BoxConstraints(
        maxHeight: 24,
        maxWidth: 24,
      ),
      icon: Icon(Icons.delete, size: 24),
      onPressed: () {
        showAlertDialog(context);
      },
      color: Theme.of(context).errorColor,
    );
  }

  String timeFormatter(String time) {
    List<String> splitedTime = time.split(":");
    return splitedTime[0] + "." + splitedTime[1];
  }

  String _getMonthFormat(DateTime date) {
    return DateFormat.MMMM().format(date);
  }

  String dateFormatter(String date) {
    List<String> splitedDate = date.split("-");
    String month = _getMonthFormat(DateTime.parse(date));
    return splitedDate[2] + " " + month + " " + splitedDate[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(elevation: 0),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(16, 8, 16, 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      widget.booking.activityName,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  _buildDeleteButton(context)
                ],
              ),
              SizedBox(height: 32),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.account_circle,
                    color: HexColor("#BDBDBD"),
                    size: 32,
                  ),
                  SizedBox(width: 12),
                  Text(widget.booking.creatorName,
                      style: Theme.of(context).textTheme.headline4),
                ],
              ),
              SizedBox(height: 48),
              Text("Data Kunjungan",
                  style: Theme.of(context).textTheme.headline3),
              SizedBox(height: 16),
              Row(
                children: [
                  Icon(
                    Icons.date_range,
                    color: HexColor("#333333"),
                    size: 20,
                  ),
                  SizedBox(width: 16),
                  Text(dateFormatter(widget.booking.date),
                      style: Theme.of(context).textTheme.bodyText1),
                ],
              ),
              SizedBox(height: 12),
              Row(
                children: [
                  Icon(
                    Icons.access_time,
                    color: HexColor("#333333"),
                    size: 20,
                  ),
                  SizedBox(width: 16),
                  Text(timeFormatter(widget.booking.time),
                      style: Theme.of(context).textTheme.bodyText1),
                ],
              ),
              SizedBox(height: 32),
              Text("Keterangan", style: Theme.of(context).textTheme.headline3),
              SizedBox(height: 12),
              Text(widget.booking.description!,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(height: 2)),
            ],
          ),
        ));
  }
}
