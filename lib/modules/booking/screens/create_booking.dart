import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:date_format/date_format.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/services/api_provider.dart';

class CreateBooking extends StatefulWidget {
  CreateBooking({required this.projectId, Key? key}) : super(key: key);

  final int projectId;

  final _CreateBookingState state = new _CreateBookingState();

  @override
  _CreateBookingState createState() => state;
}

class _CreateBookingState extends State<CreateBooking> {
  DateTime _selectedTime = DateTime.now();
  DateTime? _selectedDate;

  TextEditingController _activityNameController = TextEditingController();
  TextEditingController _bookingDateController = TextEditingController();
  TextEditingController _bookingTimeController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  APIProvider api = APIProvider();
  late Booking booking;
  bool _isLoading = false;

  void _toggleLoading() {
    setState(() {
      _isLoading = !_isLoading;
    });
  }

  void _goBack(BuildContext context) {
    Navigator.pop(context, false);
  }

  Future<SharedPreferences> _getSharedPreferences() async {
    return SharedPreferences.getInstance();
  }

  void _storeData() async {
    _toggleLoading();

    String activityName = _activityNameController.text;
    DateTime bookingDate = DateFormat.yMd().parse(_bookingDateController.text);
    DateTime bookingTime = _selectedTime;
    String description;
    if (_descriptionController.text.length > 0) {
      description = _descriptionController.text;
    } else {
      description = "Tidak ada keterangan";
    }

    SharedPreferences pref = await _getSharedPreferences();
    String token = pref.getString("token")!;

    Booking createdBooking = await api.createBooking(token, activityName,
        bookingDate, bookingTime, description, widget.projectId);

    _toggleLoading();

    if (createdBooking != Booking.empty()) {
      _goBack(context);
    }
  }

  Widget _buildTextField(
      {required String label, required TextEditingController controller}) {
    return TextFormField(
      controller: controller,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: label,
      ),
    );
  }

  void _selectTime(BuildContext context) async {
    TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 11, minute: 0),
    );
    if (picked != null)
      setState(() {
        _selectedTime = DateTime(2019, 08, 1, picked.hour, picked.minute, 0);
        _bookingTimeController.text =
            formatDate(_selectedTime, [hh, ':', nn, " ", am]).toString();
      });
  }

  Widget _buildTimePicker(BuildContext context) {
    return TextFormField(
      controller: _bookingTimeController,
      onTap: () => _selectTime(context),
      keyboardType: TextInputType.text,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        suffixIcon: Icon(Icons.access_time),
        border: OutlineInputBorder(),
        labelText: "Waktu",
      ),
    );
  }

  void _selectDate(BuildContext context) async {
    final now = DateTime.now();
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: now.subtract(Duration(days: 365)),
        lastDate: now.add(Duration(days: 365)));
    if (picked != null)
      setState(() {
        _selectedDate = picked;
        _bookingDateController.text = DateFormat.yMd().format(_selectedDate!);
      });
  }

  Widget _buildDatePicker(BuildContext context) {
    return TextFormField(
      controller: _bookingDateController,
      onTap: () => _selectDate(context),
      keyboardType: TextInputType.text,
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        suffixIcon: Icon(Icons.calendar_today),
        border: OutlineInputBorder(),
        labelText: "Tanggal",
      ),
    );
  }

  bool _valuesAreNotEmpty() {
    return _activityNameController.text != "" &&
        _bookingDateController.text != "" &&
        _bookingTimeController.text != "" &&
        !_isLoading;
  }

  Widget _buildBottomNav() {
    String text = 'Batal';
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                  side: BorderSide(
                      width: 2, color: Theme.of(context).primaryColor),
                  primary: Theme.of(context).primaryColor),
              onPressed: () {
                _goBack(context);
              },
              child: Text(text)),
        ),
        SizedBox(width: 8),
        Expanded(
          flex: 1,
          child: ElevatedButton(
            onPressed: _valuesAreNotEmpty() ? _storeData : null,
            child: _isLoading
                ? SizedBox(
                    height: 15,
                    width: 15,
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white,
                      ),
                    ),
                  )
                : Text("Simpan"),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 56, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Buat Booking",
                style: Theme.of(context).textTheme.headline1,
              ),
              SizedBox(height: 48),
              _buildTextField(
                  label: "Nama Kegiatan", controller: _activityNameController),
              SizedBox(height: 16),
              _buildDatePicker(context),
              SizedBox(height: 16),
              _buildTimePicker(context),
              SizedBox(height: 16),
              TextFormField(
                  style: Theme.of(context).textTheme.bodyText1,
                  decoration: InputDecoration(
                    labelText: "Keterangan",
                    alignLabelWithHint: true,
                  ),
                  maxLines: 10,
                  controller: _descriptionController,
                  textInputAction: TextInputAction.done,
                  minLines: 10)
            ],
          ),
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 40),
          child: _buildBottomNav(),
        ));
  }
}
