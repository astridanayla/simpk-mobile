import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/services/injection_container.dart';
import 'package:simpk/modules/authentication/screens/login.dart';
import 'package:simpk/modules/offline_report/screens/offline_report.dart';
import 'package:simpk/services/network_info.dart';

void main() {
  Intl.defaultLocale = 'id_ID';
  initializeDateFormatting();
  initServiceLocator();
  runApp(MyApp());
}

Future<bool> isConnected() async {
  NetworkInfo networkInfo = GetIt.I();
  bool isConnected = await networkInfo.isConnected();
  return isConnected;
}

Future<bool> isLoggedIn() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  if (pref.getString("token") != null) {
    bool isTimedOut = await isSessionTimedOut();
    if (!isTimedOut) {
      return true;
    }
    pref.clear();
  }
  return false;
}

Future<bool> isSessionTimedOut() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  String prefLoginTime = pref.getString("login_time")!;
  DateTime loginTime = DateTime.parse(prefLoginTime);
  DateTime now = DateTime.now();
  bool isTimedOut = now.difference(loginTime).inDays > 0;
  return isTimedOut;
}

class MyApp extends StatelessWidget {
  // Root widget
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: appTheme(),
        home: FutureBuilder(
          future: Future.wait([isConnected(), isLoggedIn()]),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List futureData = snapshot.data as List;
              final _isConnected = futureData[0];
              if (_isConnected == true) {
                final _isLoggedIn = futureData[1];
                return _isLoggedIn == true ? HomePage() : LoginScreen();
              } else {
                return OfflineReport();
              }
            }
            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
