import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';

class NetworkInfo {
  NetworkInfo(this.connectivity);

  final Connectivity connectivity;

  Future<bool> isConnected() async {
    return await connectivity.checkConnectivity() != ConnectivityResult.none;
  }
}
