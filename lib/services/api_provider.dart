import 'dart:convert' as convert;
import 'dart:io';

import 'package:http/http.dart' show Client, Response;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/partner/models/Partner.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project_list/models/project_data.dart';
import 'package:simpk/modules/user_profile/models/user.dart';

const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
    defaultValue: 'https://simpk-staging.herokuapp.com');

class APIProvider {
  Client client = Client();

  Uri _getUri(String path, [Map<String, dynamic>? queryParameters]) {
    final urlSplit = _baseUrl.split('://');
    if (urlSplit[0] == 'https') {
      return Uri.https(urlSplit.last, path, queryParameters);
    } else {
      return Uri.http(urlSplit.last, path, queryParameters);
    }
  }

  Future<String> _getToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString("token")!;
    return token;
  }

  Future<Map<String, dynamic>> login(String email, String password) async {
    var uri = _getUri("account/login");
    Map data = {'email': email, 'password': password};

    var response = await client.post(uri,
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: convert.jsonEncode(data));

    return convert.jsonDecode(response.body) as Map<String, dynamic>;
  }

  Future<User> getUserDetails(String jwtToken) async {
    var uri = _getUri("account/user");
    String auth_header = "Bearer " + jwtToken;

    var response = await client.get(
      uri,
      headers: {HttpHeaders.authorizationHeader: auth_header},
    );

    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;

    User user;

    if (jsonResponse.containsKey("detail")) {
      user = User.empty();
    } else {
      user = User.fromJson(jsonResponse);
    }

    return user;
  }

  Future<Map<String, dynamic>> changePassword(
    String jwtToken,
    String oldPass,
    String newPass,
    String confirmPass,
  ) async {
    var uri = _getUri("account/change-password");
    String auth_header = "Bearer " + jwtToken;

    var response = await client.post(
      uri,
      headers: {
        HttpHeaders.authorizationHeader: auth_header,
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: convert.jsonEncode({
        "prev_password": oldPass,
        "new_password": newPass,
        "confirm_password": confirmPass,
      }),
    );

    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;

    return jsonResponse;
  }

  Future<List<Project>> getAllProjects(String jwtToken) async {
    var uri = _getUri("monitoring/proyek");
    String auth_header = "Bearer " + jwtToken;

    var response = await client.get(
      uri,
      headers: {HttpHeaders.authorizationHeader: auth_header},
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;

    List<Project> projectList = [];
    for (int idx = 0; idx < jsonResponse.length; idx++) {
      Project project =
          Project.fromJson(jsonResponse[idx] as Map<String, dynamic>);
      projectList.add(project);
    }

    return projectList;
  }

  Future<ProjectData> getProjects(int page, String searchValue,
      [Map<String, dynamic>? queryParameters]) async {
    try {
      // print(page);
      if (queryParameters == null) queryParameters = {};
      if (searchValue != '' && searchValue.isNotEmpty)
        queryParameters['namaProyek'] = searchValue;
      queryParameters['page'] = page.toString();

      Uri uri = _getUri("search/proyek", queryParameters);
      // print(uri.toString());

      String token = "Bearer " + await _getToken();
      Response response = await client.get(
        uri,
        headers: {HttpHeaders.authorizationHeader: token},
      );
      return ProjectData.fromResponse(response);
    } catch (e) {
      if (e is IOException) {
        return ProjectData.withError("Tidak ada koneksi internet");
      } else {
        print(e.toString());
        return ProjectData.withError("Terjadi kesalahan");
      }
    }
  }

  Future<List<Project>> searchProjects(String jwtToken, String searchValue,
      [Map<String, dynamic>? otherParams]) async {
    if (otherParams == null) otherParams = {};
    if (searchValue != '') otherParams['namaProyek'] = searchValue;

    var uri = _getUri("search/proyek", otherParams);
    String auth_header = "Bearer " + jwtToken;

    var response = await client.get(
      uri,
      headers: {HttpHeaders.authorizationHeader: auth_header},
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;
    List<Project> projectList = [];
    if (jsonResponse.length > 0) {
      for (int idx = 0; idx < jsonResponse.length; idx++) {
        Project project =
            Project.fromJson(jsonResponse[idx] as Map<String, dynamic>);
        projectList.add(project);
      }
    }
    return projectList;
  }

  Future<List<Partner>> getAllPartners(String jwtToken) async {
    var uri = _getUri("monitoring/mitra");
    String auth_header = "Bearer " + jwtToken;

    var response = await client.get(
      uri,
      headers: {HttpHeaders.authorizationHeader: auth_header},
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;

    List<Partner> partnerList = [];
    for (int idx = 0; idx < jsonResponse.length; idx++) {
      Partner partner = Partner.fromJson(jsonResponse[idx]);
      partnerList.add(partner);
    }

    return partnerList;
  }

  Future<List<Partner>> searchPartners(
      String jwtToken, String searchValue) async {
    var queryParameters = {'namaMitra': searchValue};
    var uri = _getUri("search/mitra", queryParameters);
    String auth_header = "Bearer " + jwtToken;

    var response = await client.get(
      uri,
      headers: {HttpHeaders.authorizationHeader: auth_header},
    );

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;

    List<Partner> partnerList = [];
    if (jsonResponse.length > 0) {
      for (int idx = 0; idx < jsonResponse.length; idx++) {
        Partner partner = Partner.fromJson(jsonResponse[idx]);
        partnerList.add(partner);
      }
    }
    return partnerList;
  }

  Future<bool> putUserDetails(
    String jwtToken,
    String userEmail,
    String userName,
    String userHpNum,
  ) async {
    var uri = _getUri("account/user");
    var auth_header = "Bearer " + jwtToken;

    var response = await client.put(
      uri,
      headers: {
        HttpHeaders.authorizationHeader: auth_header,
        HttpHeaders.contentTypeHeader: "application/json; charset=utf-8",
      },
      body: convert.jsonEncode({
        'email': userEmail,
        'nama_lengkap': userName,
        'no_telp': userHpNum,
      }),
    );

    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;

    return jsonResponse['success'] as bool;
  }

  Future<Booking> createBooking(
    String jwtToken,
    String activityName,
    DateTime bookingDate,
    DateTime bookingTime,
    String description,
    int projectId,
  ) async {
    var uri = _getUri("booking-app/by-proyek/$projectId");
    String auth_header = "Bearer " + jwtToken;

    var response = await client.post(
      uri,
      headers: {
        HttpHeaders.authorizationHeader: auth_header,
        'Content-Type': 'application/json; charset=UTF-8'
      },
      body: convert.jsonEncode({
        "nama_kegiatan": activityName,
        "tanggal_booking": DateFormat('yyyy-MM-dd').format(bookingDate),
        "waktu_booking": DateFormat('HH:mm:ss').format(bookingTime),
        "deskripsi": description,
        "proyek": projectId,
      }),
    );
    var jsonResponse =
        convert.jsonDecode(response.body) as Map<String, dynamic>;
    Booking booking;

    if (jsonResponse.length == 7) {
      booking = Booking.fromJson(jsonResponse);
    } else {
      booking = Booking.empty();
    }

    return booking;
  }
}
