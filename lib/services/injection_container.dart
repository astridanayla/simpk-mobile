import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:simpk/modules/booking/services/booking_api_provider.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/services/activity_analysis_helper.dart';
import 'package:simpk/modules/monitoring/services/activity_item_helper.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/realization_item_helper.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/modules/monitoring/services/report_image_helper.dart';
import 'package:simpk/modules/monitoring/services/report_offline_helper.dart';
import 'package:simpk/modules/monitoring/services/report_repository.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';
import 'package:simpk/services/api_provider.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:simpk/services/network_info.dart';

const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
    defaultValue: 'https://simpk-staging.herokuapp.com');

Future<void> initServiceLocator() async {
  // features
  _initReportFeatures();
  _initHomePageFeature();
  _initMoneyReturnFeatures();

  // repository
  GetIt.I.registerLazySingleton<ReportService>(
    () => ReportRepository(
      localDataSource: GetIt.I<ReportHelper>(),
      remoteDataSource: GetIt.I(),
      networkInfo: GetIt.I(),
      reportOfflineHelper: GetIt.I(),
    ),
  );
  GetIt.I.registerLazySingleton(() => APIProvider());

  // core
  GetIt.I.registerSingleton(DatabaseProvider(onCreateSQLs: [
    ReportHelper.onCreateSQL,
    ActivityItemHelper.onCreateSQL,
    RealizationItemHelper.onCreateSQL,
    ActivityAnalysisHelper.onCreateSQL,
    ReportImageHelper.onCreateSQL,
    ReportOfflineHelper.onCreateSQL,
  ]));
  GetIt.I.registerSingleton(NetworkInfo(Connectivity()));
}

void _initReportFeatures() {
  final client = Client();
  final dio = Dio(BaseOptions(baseUrl: _baseUrl));

  GetIt.I.registerLazySingleton<ReportHelper>(
    () => ReportHelper(
      dbProvider: GetIt.I(),
      activityItemHelper: ActivityItemHelper(dbProvider: GetIt.I()),
      realizationItemHelper: RealizationItemHelper(dbProvider: GetIt.I()),
      activityAnalysisHelper: ActivityAnalysisHelper(dbProvider: GetIt.I()),
      reportImageHelper: ReportImageHelper(dbProvider: GetIt.I()),
    ),
  );

  GetIt.I.registerLazySingleton(
    () => ReportOfflineHelper(dbProvider: GetIt.I()),
  );

  GetIt.I.registerLazySingleton(
    () => ProjectAPIProvider(
      client: client,
      dio: dio,
    ),
  );

  GetIt.I.registerLazySingleton(
    () => MonitoringAPIProvider(
      client: client,
      dio: dio,
    ),
  );

  GetIt.I.registerLazySingleton(() => BookingAPIProvider(client: client));
}

void _initHomePageFeature() {
  Client client = Client();
  GetIt.I.registerLazySingleton(() => HomeAPIProvider(client: client));
}

void _initMoneyReturnFeatures() {
  const _baseUrl = String.fromEnvironment('SIMPK_BASE_URL',
      defaultValue: 'https://simpk-staging.herokuapp.com');

  Dio dio = Dio(
    BaseOptions(
      baseUrl: _baseUrl,
    ),
  );

  GetIt.I.registerSingleton<MoneyReturnAPIProvider>(
      MoneyReturnAPIProvider(dio: dio));
}
