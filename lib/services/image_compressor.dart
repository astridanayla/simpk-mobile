import 'dart:io';

import 'package:flutter_image_compress/flutter_image_compress.dart';

Future<File?> compressImage(File file) async {
  String filePath = file.absolute.path;

  int fileSize = file.lengthSync();

  if (fileSize < 250000) return file;

  int quality;

  if (fileSize < 1000000) {
    quality = 75;
  } else if (fileSize < 5000000) {
    quality = 50;
  } else {
    quality = 25;
  }
  ;

  int lastIndex = filePath.lastIndexOf(RegExp(r'.jp|.pn'));
  String fileName = filePath.substring(0, lastIndex);
  String fileExtension = filePath.substring(lastIndex);
  String outPath = "${fileName}_compressed$fileExtension";

  File? compressedFile = await FlutterImageCompress.compressAndGetFile(
    file.absolute.path,
    outPath,
    quality: quality,
  );

  file.delete();

  return compressedFile;
}
