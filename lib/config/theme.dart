import 'package:flutter/material.dart';

final primary1 = Color(0xFF0A9EA2);
final primary2 = Color(0xFF01788E);
final primary3 = Color(0xFF0A4B73);
final primary4 = Color(0xFF0A3254);
final backgroundColor = Color(0xFFF8F8F8);
final gray1 = Color(0xFF333333);
final gray2 = Color(0xFF4F4F4F);
final gray3 = Color(0xFF828282);
final gray4 = Color(0xFFBDBDBD);
final white = Color(0xFFFFFFFF);

ThemeData appTheme() {
  return ThemeData(
    fontFamily: "Lato",
    textTheme: TextTheme(
      headline1: TextStyle(
        fontWeight: FontWeight.w900,
        fontSize: 24,
        height: 1,
        color: primary4,
      ),
      headline2: TextStyle(
        fontWeight: FontWeight.w900,
        fontSize: 18,
        height: 1,
        color: primary4,
      ),
      headline3: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16,
        height: 1,
        color: primary3,
      ),
      headline4: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 14,
        height: 1,
        color: gray1,
      ),
      headline5: TextStyle(
        fontSize: 32,
        fontWeight: FontWeight.w900,
        color: primary3,
      ),
      subtitle1: TextStyle(
        fontWeight: FontWeight.w900,
        fontSize: 20,
        height: 1,
        color: primary3,
      ),
      bodyText1: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 14,
        height: 1.6,
        color: gray1,
      ),
      bodyText2: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 12,
        height: 1.6,
        color: gray2,
      ),
    ),
    backgroundColor: backgroundColor,
    disabledColor: gray4,
    errorColor: Color(0xFFEB5757),
    indicatorColor: primary1,
    primarySwatch: _createMaterialColor(primary1),
    primaryColor: primary1,
    primaryColorLight: primary2,
    shadowColor: Colors.grey.withOpacity(0.4),
    scaffoldBackgroundColor: backgroundColor,
    appBarTheme: AppBarTheme(
      backgroundColor: backgroundColor,
      iconTheme: IconThemeData(
        color: gray2,
      ),
      shadowColor: Colors.grey.withOpacity(0.4),
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
        type: BottomNavigationBarType.fixed,
        backgroundColor: backgroundColor,
        selectedItemColor: primary1,
        unselectedItemColor: gray4),
    cardTheme: CardTheme(
      color: white,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
    ),
    iconTheme: IconThemeData(size: 20, color: gray2),
    inputDecorationTheme: InputDecorationTheme(
      alignLabelWithHint: true,
      border: OutlineInputBorder(),
      labelStyle:
          TextStyle(fontWeight: FontWeight.normal, fontSize: 14, height: 0.8),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: primary1,
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 20),
        textStyle: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 14,
          height: 1,
          color: white,
        ),
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: primary1,
        textStyle: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 14,
          height: 1,
          color: primary1,
        ),
      ),
    ),
    tabBarTheme: TabBarTheme(
      labelColor: primary1,
      unselectedLabelColor: gray3,
      labelStyle: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 14,
        height: 1,
      ),
      unselectedLabelStyle: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: 14,
        height: 1,
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        side: BorderSide(width: 2, color: primary1),
        primary: primary1,
        padding: EdgeInsets.all(16),
        textStyle: TextStyle(
            fontWeight: FontWeight.w700, fontSize: 14, color: primary1),
      ),
    ),
    radioTheme: RadioThemeData(
      fillColor: MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.selected)) {
          return primary1;
        } else {
          return null;
        }
      }),
    ),
    dividerTheme: DividerThemeData(
      color: gray4,
      thickness: 1,
      space: 16,
    ),
    snackBarTheme: SnackBarThemeData(
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
    ),
  );
}

MaterialColor _createMaterialColor(Color color) {
  List<double> strengths = [.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}
