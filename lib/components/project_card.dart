import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:simpk/components/progress_bar.dart';

class ProjectCard extends StatefulWidget {
  ProjectCard(
      {required this.projectTitle,
      required this.location,
      required this.description,
      required this.progressValue,
      required this.targetPage,
      Key? key})
      : super(key: key);

  final String projectTitle;
  final String location;
  final String description;
  final double progressValue;
  final Widget targetPage;

  @override
  _ProjectCardState createState() => _ProjectCardState();
}

class _ProjectCardState extends State<ProjectCard> {
  void _onPressed() {
    Widget child = widget.targetPage;
    setState(() {
      Navigator.push(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation, secondaryAnimation) => child,
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              var begin = Offset(1.0, 0.0);
              var end = Offset.zero;
              var curve = Curves.ease;

              var tween =
                  Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

              return SlideTransition(
                position: animation.drive(tween),
                child: child,
              );
            },
          ));
    });
  }

  String _getDescription() {
    if (widget.description.length >= 100) {
      return widget.description.substring(0, 100) + "...";
    } else {
      return widget.description;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2,
        child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: _onPressed,
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.projectTitle,
                      style: Theme.of(context).textTheme.headline4),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Icon(
                        Icons.place,
                        size: 14,
                        color: HexColor("#EB5757"),
                      ),
                      SizedBox(width: 8),
                      Text(widget.location,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              ?.copyWith(color: HexColor("#EB5757"))),
                    ],
                  ),
                  if (widget.description.isNotEmpty) ...[
                    SizedBox(height: 12),
                    Text(_getDescription(),
                        style: Theme.of(context).textTheme.bodyText2),
                  ],
                  SizedBox(height: 20),
                  ProgressBar(progressValue: widget.progressValue)
                ],
              ),
            )));
  }
}
