import 'package:flutter/material.dart';

class InputFormField extends StatelessWidget {
  InputFormField({
    required this.label,
    this.onChanged,
    this.onSaved,
    this.validator,
    this.initialValue,
    this.enabled = true,
    this.maxLines = 1,
    this.numberInput = false,
    this.controller,
    this.autovalidateMode = AutovalidateMode.disabled,
    this.obscureText = false,
    this.enableSuggestions = true,
    this.autocorrect = true,
    this.suffixIcon,
    this.prefixIcon,
    this.enableInteractiveSelections = true,
    this.focusNode,
  });

  final String label;
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final Function(String?)? onSaved;
  final String? Function(String?)? validator;
  final AutovalidateMode autovalidateMode;
  final bool obscureText;
  final bool enableSuggestions;
  final bool autocorrect;
  final bool enableInteractiveSelections;
  final String? initialValue;
  final bool enabled;
  final int maxLines;
  final bool numberInput;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: TextFormField(
        controller: controller,
        autovalidateMode: autovalidateMode,
        keyboardType: numberInput ? TextInputType.number : TextInputType.text,
        initialValue: initialValue,
        validator: validator,
        obscureText: obscureText,
        autocorrect: autocorrect,
        enableSuggestions: enableSuggestions,
        enableInteractiveSelection: enableInteractiveSelections,
        maxLines: maxLines,
        enabled: enabled,
        onChanged: onChanged,
        onSaved: onSaved,
        focusNode: focusNode,
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: label,
            alignLabelWithHint: true,
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon),
      ),
    );
  }
}
