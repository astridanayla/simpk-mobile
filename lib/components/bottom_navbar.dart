import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/blocs/auth_state.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/modules/maps/screen/maps.dart';
import 'package:simpk/modules/partner/screens/partner_page.dart';
import 'package:simpk/modules/project_list/screens/project_page.dart';

class BottomNavbar extends StatefulWidget {
  const BottomNavbar({
    required this.currentPageIndex,
    Key? key,
  }) : super(key: key);

  final int currentPageIndex;

  @override
  _BottomNavbarState createState() => _BottomNavbarState();
}

class _BottomNavbarState extends State<BottomNavbar> {
  int currentIndex = 0;

  List<Widget> _widgetOptionsRoleInternal = [
    HomePage(),
    MapsPage(),
    PartnerPage(),
    ProjectPage(),
  ];

  List<Widget> _widgetOptions = [
    HomePage(),
    MapsPage(),
    ProjectPage(),
  ];

  @override
  void initState() {
    super.initState();
    currentIndex = widget.currentPageIndex;
  }

  void _onItemTapped(int index, bool internal) {
    setState(() {
      currentIndex = index;
    });
    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (_, __, ___) => internal
            ? _widgetOptionsRoleInternal[index]
            : _widgetOptions[index],
        transitionDuration: Duration(seconds: 0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      buildWhen: (previousState, state) {
        return previousState.role != state.role;
      },
      builder: (context, state) => Container(
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Theme.of(context).shadowColor,
                spreadRadius: 0,
                blurRadius: 4,
                offset: Offset(0, 0))
          ],
        ),
        child: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Beranda",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.near_me),
              label: "Lokasi",
            ),
            if (state.isRoleInternal())
              BottomNavigationBarItem(
                icon: Icon(Icons.people),
                label: "Mitra",
              ),
            BottomNavigationBarItem(
              icon: Icon(Icons.content_paste),
              label: "Proyek",
            )
          ],
          currentIndex: widget.currentPageIndex,
          onTap: (index) => _onItemTapped(index, state.isRoleInternal()),
        ),
      ),
    );
  }
}
