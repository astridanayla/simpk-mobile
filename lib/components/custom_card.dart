import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class CustomCard extends StatefulWidget {
  CustomCard(
      {required this.title,
      required this.userName,
      required this.onClick,
      required this.userIcon,
      required this.displayIcon,
      this.customOnPressed,
      this.lastEdited,
      this.createdOn,
      this.afterOnClick,
      Key? key})
      : super(key: key);

  final String title;
  final String userName;
  DateTime? lastEdited;
  DateTime? createdOn;
  final Widget onClick;
  final Icon userIcon;
  final IconData displayIcon;
  VoidCallback? customOnPressed;
  final VoidCallback? afterOnClick;

  @override
  _CustomCardState createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {
  String _getFormatedLastEdited() {
    if (widget.lastEdited != null) {
      String formattedTime =
          DateFormat('dd/MM/yyyy').format(widget.lastEdited!);
      return "Terakhir diedit: " + formattedTime;
    } else if (widget.createdOn != null) {
      String formattedTime = DateFormat('dd/MM/yyyy').format(widget.createdOn!);
      return "Dibuat pada: " + formattedTime;
    } else {
      return "";
    }
  }

  void _onPressed() async {
    Widget child = widget.onClick;

    await Navigator.push(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => child,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            var begin = Offset(1.0, 0.0);
            var end = Offset.zero;
            var curve = Curves.ease;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            return SlideTransition(
              position: animation.drive(tween),
              child: child,
            );
          },
        ));

    if(widget.afterOnClick != null){
      widget.afterOnClick!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
          onTap: widget.customOnPressed == null
              ? _onPressed
              : widget.customOnPressed,
          splashColor: Colors.blue.withAlpha(30),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  color: HexColor("#DEECED"),
                  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 32.0),
                  child: Icon(widget.displayIcon,
                      color: Theme.of(context).primaryColor, size: 48),
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.title,
                          style: Theme.of(context).textTheme.headline4),
                      SizedBox(height: 8),
                      Text(_getFormatedLastEdited(),
                          style: Theme.of(context).textTheme.bodyText2),
                      SizedBox(height: 16),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.account_circle),
                          SizedBox(width: 8),
                          Expanded(
                            flex: 1,
                            child: Text(widget.userName,
                                style: Theme.of(context).textTheme.bodyText2),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
