import 'package:flutter/material.dart';

class CustomDropdown extends StatefulWidget {
  CustomDropdown(
      {required this.label,
      required this.items,
      required this.value,
      required this.onChanged,
      Key? key})
      : super(key: key);

  final String label;
  final List<String> items;
  final String? value;
  final void Function(String?) onChanged;

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      value: widget.value,
      isExpanded: true,
      decoration: InputDecoration(labelText: widget.label),
      style: Theme.of(context).textTheme.bodyText1,
      items: widget.items
          .map<DropdownMenuItem<String>>((String value) => DropdownMenuItem(
                value: value,
                child: Text(
                  value,
                  softWrap: true,
                ),
              ))
          .toList(),
      onChanged: widget.onChanged,
    );
  }
}
