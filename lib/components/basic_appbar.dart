import 'package:flutter/material.dart';

class BasicAppbar extends StatelessWidget implements PreferredSizeWidget {
  BasicAppbar(this.title) : preferredSize = Size.fromHeight(60.0);

  final String title;
  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(title, style: Theme.of(context).textTheme.headline2),
        centerTitle: false,
      ),
    );
  }
}
