import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BookingCard extends StatefulWidget {
  const BookingCard({
    required this.bookingDate,
    required this.bookingTitle,
    required this.bookingTime,
    required this.projectName,
    required this.assignedUser,
    required this.onClick,
    this.afterOnClick,
    Key? key,
  }) : super(key: key);

  final DateTime bookingDate;
  final String bookingTitle;
  final DateTime bookingTime;
  final String projectName;
  final String assignedUser;
  final Widget onClick;
  final VoidCallback? afterOnClick;

  @override
  _BookingCardState createState() => _BookingCardState();
}

class _BookingCardState extends State<BookingCard> {
  String _getDateFormat() {
    return widget.bookingDate.day.toString();
  }

  String _getMonthFormat() {
    String monthFormat = DateFormat.MMMM().format(widget.bookingDate);
    return monthFormat.substring(0, 3);
  }

  String _getTimeFormat() {
    String timeFormat = DateFormat.jm().format(widget.bookingTime);
    return timeFormat;
  }

  void _onPressed() async {
    Widget child = widget.onClick;

    await Navigator.push(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => child,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            var begin = Offset(1.0, 0.0);
            var end = Offset.zero;
            var curve = Curves.ease;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            return SlideTransition(
              position: animation.drive(tween),
              child: child,
            );
          },
        ));

    if (widget.afterOnClick != null) {
      widget.afterOnClick!();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color(0xFFFFFFFF),
      elevation: 2,
      child: InkWell(
        onTap: _onPressed,
        splashColor: Colors.blue.withAlpha(30),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                color: Color(0xFFDEECED),
                padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 32.0),
                child: Column(
                  children: [
                    Text(_getDateFormat(),
                        style: Theme.of(context).textTheme.headline5),
                    SizedBox(height: 8),
                    Text(_getMonthFormat(),
                        style: Theme.of(context).textTheme.subtitle1),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.bookingTitle,
                        style: Theme.of(context).textTheme.headline4),
                    SizedBox(height: 20),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.access_time),
                        SizedBox(width: 8),
                        Text(_getTimeFormat(),
                            style: Theme.of(context).textTheme.bodyText2),
                      ],
                    ),
                    SizedBox(height: 12),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(Icons.account_circle),
                        SizedBox(width: 8),
                        Expanded(
                          flex: 1,
                          child: Text(widget.assignedUser,
                              style: Theme.of(context).textTheme.bodyText2),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
