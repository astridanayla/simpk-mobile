import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class DataCard extends StatelessWidget {
  const DataCard(
      {required this.data,
      required this.label,
      required this.colorHex,
      Key? key})
      : super(key: key);

  final String data;
  final String label;
  final String colorHex;

  @override
  Widget build(BuildContext context) {
    return Card(
        color: HexColor(colorHex).withOpacity(0.16),
        elevation: 0,
        child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 12.0, vertical: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(data,
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        ?.copyWith(color: HexColor(colorHex))),
                SizedBox(height: 4),
                Text(label,
                    style: Theme.of(context)
                        .textTheme
                        .headline3
                        ?.copyWith(color: HexColor(colorHex))),
              ],
            )));
  }
}
