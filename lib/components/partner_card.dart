import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class PartnerCard extends StatelessWidget {
  PartnerCard({required this.partnerName, required this.targetPage, Key? key})
      : super(key: key);

  final String partnerName;
  final Widget targetPage;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 2,
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          child: Padding(
            padding: EdgeInsets.all(16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.account_circle,
                  color: HexColor("#BDBDBD"),
                  size: 40,
                ),
                SizedBox(width: 12),
                Text(partnerName, style: Theme.of(context).textTheme.headline4),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
