import 'package:flutter/material.dart';

class SecondaryButton extends StatefulWidget {
  SecondaryButton(
      {required this.text,
      required this.iconData,
      required this.onPressed,
      Key? key})
      : super(key: key);

  final String text;
  final IconData iconData;
  final VoidCallback onPressed;

  @override
  _SecondaryButtonState createState() => _SecondaryButtonState();
}

class _SecondaryButtonState extends State<SecondaryButton> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: OutlinedButton(
        onPressed: widget.onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(widget.iconData),
            SizedBox(width: 8),
            Text(widget.text)
          ],
        ),
      ),
    );
  }
}
