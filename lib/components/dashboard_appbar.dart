import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class DashboardAppBar extends StatelessWidget implements PreferredSizeWidget {
  DashboardAppBar(this.label, this.title)
      : preferredSize = Size.fromHeight(60.0);

  final String label;
  final String title;
  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: AppBar(
          title: Column(
            children: [
              Text(
                label,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    ?.copyWith(color: HexColor("#0A3254")),
              ),
              SizedBox(height: 8),
              Text(
                title,
                style: Theme.of(context).textTheme.headline2,
              ),
            ],
          ),
          centerTitle: true),
    );
  }
}
