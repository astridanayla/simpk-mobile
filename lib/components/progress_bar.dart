import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class ProgressBar extends StatelessWidget {
  ProgressBar({required this.progressValue, Key? key}) : super(key: key);

  final double progressValue;

  String _getPercentage() {
    return (progressValue * 100).toString() + "%";
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 11,
          child: LinearPercentIndicator(
            lineHeight: 14.0,
            percent: progressValue,
            backgroundColor: Theme.of(context).indicatorColor.withOpacity(0.2),
            progressColor: Theme.of(context).indicatorColor,
          ),
        ),
        SizedBox(width: 16),
        Expanded(
          flex: 2,
          child: Text(_getPercentage(),
              style: Theme.of(context).textTheme.headline4),
        ),
      ],
    );
  }
}
