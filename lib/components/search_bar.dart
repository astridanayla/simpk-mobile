import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget {
  SearchBar(
      {required this.label,
      required this.onSubmit,
      required this.controller,
      Key? key})
      : super(key: key);

  final String label;
  final void Function(String) onSubmit;
  final TextEditingController controller;

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(children: [
        TextFormField(
            textInputAction: TextInputAction.search,
            style: Theme.of(context).textTheme.bodyText1,
            onFieldSubmitted: widget.onSubmit,
            controller: widget.controller,
            decoration: InputDecoration(
              labelText: widget.label,
              prefixIcon: Icon(
                Icons.search,
              ),
            )),
      ]),
    );
  }
}
