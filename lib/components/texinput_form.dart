import 'package:flutter/material.dart';

class TextInput extends StatelessWidget {
  TextInput(this.label);

  final String label;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: TextFormField(
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(labelText: label)),
    );
  }
}
