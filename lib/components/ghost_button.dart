import 'package:flutter/material.dart';

class GhostButton extends StatefulWidget {
  GhostButton(
      {required this.text,
      required this.iconData,
      required this.targetPage,
      required this.startOffset,
      this.onClick,
      Key? key})
      : super(key: key);

  final String text;
  final IconData iconData;
  final Widget targetPage;
  final Offset startOffset;
  final void Function(dynamic value)? onClick;

  @override
  _GhostButtonState createState() => _GhostButtonState();
}

class _GhostButtonState extends State<GhostButton> {
  void _onPressed() async {
    Widget child = widget.targetPage;
    final result = await Navigator.push(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) => child,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            var begin = widget.startOffset;
            var end = Offset.zero;
            var curve = Curves.ease;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            return SlideTransition(
              position: animation.drive(tween),
              child: child,
            );
          },
        ));

    if (widget.onClick != null) {
      widget.onClick!(result);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: TextButton(
          onPressed: _onPressed,
          child: Row(
            children: [
              Icon(
                widget.iconData,
              ),
              SizedBox(width: 8),
              Text(widget.text)
            ],
          )),
    );
  }
}
