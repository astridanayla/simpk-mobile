import 'package:flutter/material.dart';

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    required this.label,
    required this.value,
    required this.onChanged,
    Key? key,
  }) : super(key: key);

  final String label;
  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          onChanged(!value);
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 24.0,
              width: 24.0,
              child: Checkbox(
                value: value,
                activeColor: Theme.of(context).primaryColor,
                onChanged: (bool? newValue) {
                  onChanged(newValue);
                },
              ),
            ),
            SizedBox(width: 8),
            Text(label, style: Theme.of(context).textTheme.bodyText1),
          ],
        ),
      ),
    );
  }
}
