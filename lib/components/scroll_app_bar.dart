import 'package:flutter/material.dart';

class ScrollAppBar extends StatefulWidget implements PreferredSizeWidget {
  ScrollAppBar({
    required this.scrollController,
    this.targetElevation,
    Key? key,
  }) : super(key: key);

  final ScrollController scrollController;
  final double? targetElevation;

  @override
  _ScrollAppBarState createState() => _ScrollAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _ScrollAppBarState extends State<ScrollAppBar> {
  double? _elevation = 0;

  @override
  void initState() {
    super.initState();
    widget.scrollController.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: _elevation,
    );
  }

  @override
  void dispose() {
    super.dispose();
    widget.scrollController.removeListener(_scrollListener);
  }

  void _scrollListener() {
    double? newElevation =
        widget.scrollController.offset > 1 ? widget.targetElevation : 0;
    if (_elevation != newElevation) {
      setState(() {
        _elevation = newElevation;
      });
    }
  }
}
