import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// https://medium.com/flutterdevs/date-and-time-picker-in-flutter-72141e7531c

class DatePicker extends StatefulWidget {
  const DatePicker({
    required this.label,
    this.controller,
    this.onTap,
    Key? key,
  }) : super(key: key);

  final String label;
  final void Function()? onTap;
  final TextEditingController? controller;

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  late TextEditingController _dateController;
  DateTime? selectedDate;

  @override
  void initState() {
    super.initState();
    if (widget.controller != null) {
      _dateController = widget.controller!;
    } else {
      _dateController = TextEditingController();
    }
  }

  void _selectDate(BuildContext context) async {
    final now = DateTime.now();
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate ?? now,
        firstDate: now.subtract(Duration(days: 365)),
        lastDate: now);
    if (picked != null)
      setState(() {
        selectedDate = picked;
        _dateController.text = DateFormat.yMd().format(selectedDate!);
      });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _dateController,
      enableInteractiveSelection: false,
      focusNode: _AlwaysDisabledFocusNode(),
      onTap: () {
        if (widget.onTap != null) widget.onTap!();
        _selectDate(context);
      },
      style: Theme.of(context).textTheme.bodyText1,
      decoration: InputDecoration(
        suffixIcon: Icon(
          Icons.calendar_today,
          color: Theme.of(context).primaryColor,
        ),
        labelText: widget.label,
      ),
    );
  }
}

class _AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
