import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class MainAppbar extends StatelessWidget implements PreferredSizeWidget {
  MainAppbar(
      {required this.preferredSize,
      required this.profileColor,
      required this.wifiColor,
      required this.profileOnPressed,
      required this.wifiOnPressed,
      Key? key})
      : super(key: key);

  final Color profileColor;
  final Color wifiColor;
  final Function()? profileOnPressed;
  final Function()? wifiOnPressed;

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 36, 16, 8),
        height: preferredSize.height,
        color: HexColor("#F8F8F8"),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 32,
              child: Image.asset("lib/assets/images/logo_bpkh.png",
                  key: Key("logo_bpkh"), fit: BoxFit.fitHeight),
            ),
            Row(
              children: [
                IconButton(
                    padding: EdgeInsets.zero,
                    iconSize: 32,
                    icon: Icon(Icons.cloud_upload),
                    color: wifiColor,
                    onPressed: wifiOnPressed),
                SizedBox(width: 8),
                IconButton(
                    padding: EdgeInsets.zero,
                    iconSize: 32,
                    icon: Icon(Icons.account_circle),
                    color: profileColor,
                    onPressed: profileOnPressed),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
