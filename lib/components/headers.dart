import 'package:flutter/material.dart';

Widget subheader(BuildContext context, String str) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 8),
    alignment: Alignment.topLeft,
    child: Text(
      str,
      style: Theme.of(context).textTheme.headline3,
    ),
  );
}
