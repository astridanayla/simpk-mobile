import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:hexcolor/hexcolor.dart';

class PieGraph extends StatelessWidget {
  const PieGraph({required this.dataMap, Key? key}) : super(key: key);

  final Map<String, double> dataMap;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: PieChart(
        dataMap: dataMap,
        colorList: [
          // HexColor("#EB5757"),
          HexColor("F2994A"),
          HexColor("#27AE60")
        ],
        chartLegendSpacing: 24,
        chartRadius: 280,
        legendOptions: LegendOptions(showLegends: false),
        chartValuesOptions: ChartValuesOptions(
          showChartValueBackground: false,
          showChartValues: false,
          showChartValuesInPercentage: false,
          showChartValuesOutside: false,
          decimalPlaces: 1,
        ),
      ),
    );
  }
}
