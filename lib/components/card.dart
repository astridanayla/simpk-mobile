import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';

class CustomCard extends StatefulWidget {
  CustomCard({
    required this.title,
    required this.lastEdited,
    required this.onClick,
    this.isOfflineReport = false,
    this.onOfflineReportDelete,
    this.userIcon = Icons.account_circle,
    this.userName = "",
    Key? key,
  }) : super(key: key);

  final String title;
  final DateTime lastEdited;
  final IconData? userIcon;
  final String? userName;
  final VoidCallback onClick;
  final bool isOfflineReport;
  final VoidCallback? onOfflineReportDelete;
  final state = _CustomCardState();

  @override
  _CustomCardState createState() => state;
}

class _CustomCardState extends State<CustomCard> {
  String _getFormatedLastEdited() {
    String formattedTime = DateFormat('dd/MM/yyyy').format(widget.lastEdited);
    return "Terakhir diedit: " + formattedTime;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
          onTap: widget.onClick,
          splashColor: Colors.blue.withAlpha(30),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  color: HexColor("#DEECED"),
                  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 32.0),
                  child: Icon(Icons.notes,
                      color: Theme.of(context).primaryColor, size: 48),
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.title,
                          style: Theme.of(context).textTheme.headline4),
                      SizedBox(height: 8),
                      Text(_getFormatedLastEdited(),
                          style: Theme.of(context).textTheme.bodyText2),
                      widget.isOfflineReport
                          ? Container()
                          : Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 16),
                                Icon(widget.userIcon),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 1,
                                  child: Text(
                                    widget.userName!,
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                ),
                              ],
                            )
                    ],
                  ),
                ),
              ),
              widget.isOfflineReport
                  ? Expanded(
                      flex: 1,
                      child: IconButton(
                        icon: Icon(Icons.delete),
                        color: Theme.of(context).errorColor,
                        onPressed: widget.onOfflineReportDelete,
                      ),
                    )
                  : Container()
            ],
          )),
    );
  }
}
