import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/services/network_info.dart';

import 'network_info_test.mocks.dart';

@GenerateMocks([Connectivity])
void main() {
  final mockConnectivity = MockConnectivity();

  test('isConnected when have wifi should return true', () async {
    when(mockConnectivity.checkConnectivity())
        .thenAnswer((_) async => ConnectivityResult.wifi);

    final networkInfo = NetworkInfo(mockConnectivity);
    final result = await networkInfo.isConnected();

    expect(true, result);
  });

  test('isConnected when not connected should return false', () async {
    when(mockConnectivity.checkConnectivity())
        .thenAnswer((_) async => ConnectivityResult.none);
    final networkInfo = NetworkInfo(mockConnectivity);
    final result = await networkInfo.isConnected();

    expect(false, result);
  });
}
