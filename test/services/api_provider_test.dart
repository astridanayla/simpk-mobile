import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:intl/intl.dart';
import 'dart:convert';

import 'package:simpk/services/api_provider.dart';

void main() {
  test("API Provider: test fail to login", () async {
    final apiProvider = APIProvider();
    String dummyEmail = "dummyEmail@email.com";
    String dummyPassword = "dummyPassword123";
    String key = "non_field_errors";
    List value = ["A user with this email and password is not found."];

    apiProvider.client = MockClient((request) async {
      final mapJson = {key: value};
      return Response(json.encode(mapJson), 400);
    });

    final response = await apiProvider.login(dummyEmail, dummyPassword);

    expect(response[key], value);
  });

  test("API Provider: test success to login", () async {
    final apiProvider = APIProvider();
    String dummyEmail = "dummyEmail@email.com";
    String dummyPassword = "dummyPassword123";
    String success = "True";
    int statusCode = 200;
    String message = "User logged in successfully.";
    String token = "sometoken";

    Map api_response = {
      "success": success,
      "status code": statusCode,
      "message": message,
      "token": token
    };

    apiProvider.client = MockClient((request) async {
      final mapJson = api_response;
      return Response(json.encode(mapJson), 400);
    });

    final response = await apiProvider.login(dummyEmail, dummyPassword);

    expect(response["success"], success);
    expect(response["status code"], statusCode);
    expect(response["message"], message);
    expect(response["token"], token);
  });

  test("API Provider: test fail to get user details", () async {
    final apiProvider = APIProvider();
    String jwt_token = "Bearer sometoken";
    String key = "detail";
    String value = "Authentication credentials were not provided.";

    apiProvider.client = MockClient((request) async {
      final mapJson = {key: value};
      return Response(json.encode(mapJson), 200);
    });

    final user = await apiProvider.getUserDetails(jwt_token);

    expect(user.name, "");
    expect(user.email, "");
    expect(user.handphone_number, "");
    expect(user.role, "");
  });

  test("API Provider: test success to get user details", () async {
    final apiProvider = APIProvider();
    String jwt_token = "Bearer sometoken";
    bool success = true;
    int status_code = 200;
    String message = "User fetched successfully";

    String user_email = "email@mail.com";
    String user_name = "Nama lengkap";
    String user_hp_num = "08123456789";
    String user_role = "Admin";
    Map<String, String> userData = {
      "email": user_email,
      "nama_lengkap": user_name,
      "no_telp": user_hp_num,
      "role": user_role,
    };

    apiProvider.client = MockClient((request) async {
      final mapJson = {
        "success": success,
        "status code": status_code,
        "message": message,
        "data": [userData]
      };
      return Response(json.encode(mapJson), 200);
    });

    final user = await apiProvider.getUserDetails(jwt_token);

    expect(user.email, user_email);
    expect(user.name, user_name);
    expect(user.handphone_number, user_hp_num);
    expect(user.role, user_role);
  });

  test("API Provider: test success to update details", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";
    bool success = true;
    int status_code = 200;

    String userEmail = "email@mail.com";
    String userName = "Nama lengkap";
    String userHpNum = "08123456789";
    Map<String, String> userData = {
      "email": userEmail,
      "nama_lengkap": userName,
      "no_telp": userHpNum,
    };

    apiProvider.client = MockClient((request) async {
      final mapJson = {
        "success": success,
        "status code": status_code,
        "data": userData
      };
      return Response(json.encode(mapJson), 200);
    });

    final response = await apiProvider.putUserDetails(
        jwtToken, userEmail, userName, userHpNum);

    expect(response, true);
  });

  test("API Provider: test success to update details", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";
    bool success = false;
    int status_code = 400;

    String userEmail = "email";
    String userName = "Nama lengkap";
    String userHpNum = "081234563243243234789";
    Map<String, String> userData = {
      "email": userEmail,
      "nama_lengkap": userName,
      "no_telp": userHpNum,
    };

    apiProvider.client = MockClient((request) async {
      final mapJson = {
        "success": success,
        "status code": status_code,
        "data": userData
      };
      return Response(json.encode(mapJson), 200);
    });

    final response = await apiProvider.putUserDetails(
        jwtToken, userEmail, userName, userHpNum);

    expect(response, false);
  });

  test("API Provider: test success to get all partners", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";

    String partnerName = 'Dummy Partner Name';
    String partnerEmail = 'dummy@mail.com';
    String address = 'Depok, Jawa Barat';

    apiProvider.client = MockClient((request) async {
      List<dynamic> partnerData = [
        {
          "partnerId": 1,
          "name": partnerName,
          "email": partnerEmail,
          "address": address
        }
      ];
      return Response(json.encode(partnerData), 200);
    });

    var partner = await apiProvider.getAllPartners(jwtToken);
    expect(partner[0].name, partnerName);
    expect(partner[0].email, partnerEmail);
    expect(partner[0].address, address);
  });

  test("API Provider: test success to search partners", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";

    String partnerName = 'Dummy Partner Name';
    String partnerEmail = 'dummy@mail.com';
    String address = 'Depok, Jawa Barat';

    apiProvider.client = MockClient((request) async {
      List<dynamic> partnerData = [
        {
          "partnerId": 1,
          "name": partnerName,
          "email": partnerEmail,
          "address": address
        }
      ];
      return Response(json.encode(partnerData), 200);
    });

    var partner = await apiProvider.searchPartners(jwtToken, partnerName);
    expect(partner[0].name, partnerName);
    expect(partner[0].email, partnerEmail);
    expect(partner[0].address, address);
  });

  test("API Provider: test success to get all projects", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";

    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = 'Dummy Coordinate';
    String description = "Dummy Description";
    double progress = 50;
    int allocation = 100000;

    apiProvider.client = MockClient((request) async {
      List<dynamic> projectData = [
        {
          "project_id": 1,
          "nama_proyek": projectName,
          "status": projectStatus,
          "mitra": partnerName,
          "jenis_kegiatan": activityType,
          "daerah": region,
          "koordinat_lokasi": coordinate,
          "deskripsi": description,
          "progres": progress,
          "alokasi_dana": allocation,
        }
      ];
      return Response(json.encode(projectData), 200);
    });

    var project = await apiProvider.getAllProjects(jwtToken);
    expect(project[0].name, projectName);
    expect(project[0].status, projectStatus);
    expect(project[0].partner, partnerName);
    expect(project[0].activityType, activityType);
    expect(project[0].region, region);
    expect(project[0].coordinate, coordinate);
    expect(project[0].description, description);
    expect(project[0].progress, progress / 100);
    expect(project[0].allocation, allocation);
  });

  test("API Provider: test success to search projects", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";

    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = 'Dummy Coordinate';
    String description = 'Dummy Description';
    double progress = 50;
    int allocation = 100000;

    apiProvider.client = MockClient((request) async {
      List<dynamic> projectData = [
        {
          "project_id": 1,
          "nama_proyek": projectName,
          "status": projectStatus,
          "mitra": partnerName,
          "jenis_kegiatan": activityType,
          "daerah": region,
          "koordinat_lokasi": coordinate,
          "deskripsi": description,
          "progres": progress,
          "alokasi_dana": allocation,
        }
      ];
      return Response(json.encode(projectData), 200);
    });

    var project = await apiProvider.searchProjects(jwtToken, projectName);
    expect(project[0].name, projectName);
    expect(project[0].status, projectStatus);
    expect(project[0].partner, partnerName);
    expect(project[0].activityType, activityType);
    expect(project[0].region, region);
    expect(project[0].coordinate, coordinate);
    expect(project[0].description, description);
    expect(project[0].progress, progress / 100);
    expect(project[0].allocation, allocation);
  });

  test("API Provider: test success to create booking", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";

    String activityName = 'Dummy Activity Name';
    DateTime bookingDate = DateFormat("yyyy-MM-dd").parse('2021-01-01');
    DateTime bookingTime = DateFormat("HH:mm:ss").parse('15:00:00');
    String description = 'Lorem ipsum dolor sit amet';
    int creatorId = 1;
    int projectId = 1;

    Map<String, dynamic> bookingData = {
      "id": 1,
      "nama_kegiatan": activityName,
      "tanggal_booking": bookingDate.toString(),
      "waktu_booking": bookingTime.toString(),
      "deskripsi": description,
      "proyek": projectId,
      "user": creatorId
    };

    apiProvider.client = MockClient((request) async {
      final mapJson = bookingData;
      return Response(json.encode(mapJson), 200);
    });

    final booking = await apiProvider.createBooking(jwtToken, activityName,
        bookingDate, bookingTime, description, creatorId);
    expect(booking.activityName, activityName);
    expect(booking.date, bookingDate.toString());
    expect(booking.time, bookingTime.toString());
    expect(booking.description, description);
  });

  test("API Provider: test failed to create booking", () async {
    final apiProvider = APIProvider();
    String jwtToken = "Bearer sometoken";

    String activityName = 'Dummy Activity Name';
    DateTime bookingDate = DateFormat("yyyy-MM-dd").parse('2021-01-01');
    DateTime bookingTime = DateFormat("HH:mm:ss").parse('15:00:00');
    String description = 'Lorem ipsum dolor sit amet';
    int creatorId = 1;

    Map<String, dynamic> bookingData = {
      "status": "failed to create booking",
    };

    apiProvider.client = MockClient((request) async {
      final mapJson = bookingData;
      return Response(json.encode(mapJson), 200);
    });

    final booking = await apiProvider.createBooking(jwtToken, activityName,
        bookingDate, bookingTime, description, creatorId);
    expect(booking.activityName, "");
    expect(booking.date, "");
    expect(booking.time, "");
    expect(booking.description, "");
  });
}
