import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:simpk/components/main_appbar.dart';

void main() {
  Widget _wrapWithMaterialApp(PreferredSizeWidget mainAppbar) {
    return MaterialApp(
      home: Scaffold(appBar: mainAppbar),
    );
  }

  testWidgets("Appbar contains bpkh logo", (WidgetTester tester) async {
    PreferredSizeWidget mainAppbar = MainAppbar(
      preferredSize: Size.fromHeight(80),
      profileColor: Color(0xFFBDBDBD),
      wifiColor: Color(0xFFBDBDBD),
      wifiOnPressed: () {},
      profileOnPressed: () {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(mainAppbar));

    Finder imageFinder = find.byType(Image);
    Image logoBPKH = tester.firstWidget(imageFinder);

    expect(imageFinder, findsOneWidget);
    expect(logoBPKH.key, Key("logo_bpkh"));
  });

  testWidgets("Appbar push navigator after profile icon click",
      (WidgetTester tester) async {
    PreferredSizeWidget mainAppbar = MainAppbar(
      preferredSize: Size.fromHeight(80),
      profileColor: Color(0xFFBDBDBD),
      wifiColor: Color(0xFFBDBDBD),
      wifiOnPressed: () {},
      profileOnPressed: () {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(mainAppbar));

    Finder userIcon = find.byIcon(Icons.account_circle);
    await tester.tap(userIcon);
    await tester.pump();
    expect(find.text("Nama"), findsNothing);
  });

  testWidgets("Appbar push navigator after cloud icon click",
      (WidgetTester tester) async {
    PreferredSizeWidget mainAppbar = MainAppbar(
      preferredSize: Size.fromHeight(80),
      profileColor: Color(0xFFBDBDBD),
      wifiColor: Color(0xFFBDBDBD),
      wifiOnPressed: () {},
      profileOnPressed: () {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(mainAppbar));

    Finder cloudIcon = find.byIcon(Icons.cloud_upload);
    await tester.tap(cloudIcon);
    await tester.pump();
    expect(find.text("Daftar Laporan Offline"), findsNothing);
  });

  testWidgets("Appbar contains profile icon button",
      (WidgetTester tester) async {
    PreferredSizeWidget mainAppbar = MainAppbar(
      preferredSize: Size.fromHeight(80),
      profileColor: Color(0xFFBDBDBD),
      wifiColor: Color(0xFFBDBDBD),
      wifiOnPressed: () {},
      profileOnPressed: () {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(mainAppbar));
    await tester.tap(find.byType(IconButton).at(1));

    Finder iconButtonFinder = find.byType(IconButton).at(1);
    Finder profileIconFinder = find.byIcon(Icons.account_circle);
    Finder iconButtonWithProfileIconFinder =
        find.descendant(of: iconButtonFinder, matching: profileIconFinder);

    expect(iconButtonFinder, findsOneWidget);
    expect(profileIconFinder, findsOneWidget);
    expect(iconButtonWithProfileIconFinder, findsOneWidget);
  });

  testWidgets("Appbar contains cloud icon button", (WidgetTester tester) async {
    PreferredSizeWidget mainAppbar = MainAppbar(
      preferredSize: Size.fromHeight(80),
      profileColor: Color(0xFFBDBDBD),
      wifiColor: Color(0xFFBDBDBD),
      wifiOnPressed: () {},
      profileOnPressed: () {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(mainAppbar));
    await tester.tap(find.byType(IconButton).at(0));

    Finder iconButtonFinder = find.byType(IconButton).at(0);
    Finder cloudIconFinder = find.byIcon(Icons.cloud_upload);
    Finder iconButtonWithProfileIconFinder =
        find.descendant(of: iconButtonFinder, matching: cloudIconFinder);

    expect(iconButtonFinder, findsOneWidget);
    expect(cloudIconFinder, findsOneWidget);
    expect(iconButtonWithProfileIconFinder, findsOneWidget);
  });
}
