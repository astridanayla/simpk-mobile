import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:simpk/components/labeled_checkbox.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget labeledCheckbox) {
    return MaterialApp(home: labeledCheckbox);
  }

  testWidgets("Labeled checkbox contains label", (WidgetTester tester) async {
    bool _isSelected = false;
    Widget labeledCheckbox =
        LabeledCheckbox(label: "Label", value: _isSelected, onChanged: () {});
    await tester.pumpWidget(_wrapWithMaterialApp(labeledCheckbox));

    expect(find.text("Label"), findsOneWidget);
  });

  testWidgets("Labeled checkbox contains checkbox",
      (WidgetTester tester) async {
    bool _isSelected = false;
    Widget labeledCheckbox =
        LabeledCheckbox(label: "Label", value: _isSelected, onChanged: () {});
    await tester.pumpWidget(_wrapWithMaterialApp(labeledCheckbox));

    expect(find.byType(Checkbox), findsOneWidget);
  });
}
