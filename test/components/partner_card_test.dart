import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/partner_card.dart';
import 'package:simpk/modules/partner/models/Partner.dart';
import 'package:simpk/modules/partner/screens/partner_dashboard_page.dart';
import 'package:simpk/services/api_provider.dart';

import '../modules/project_list/screens/project_list_test.mocks.dart';

void main() {
  Partner dummyPartner = Partner(
      id: 1, name: "Rumah Zakat", email: "zakat@gmail.com", address: "Depok");

  Widget _wrapWithMaterialApp(Widget partnerCard) {
    return MaterialApp(
      home: partnerCard,
    );
  }

  GetIt.I.registerSingleton<APIProvider>(MockAPIProvider());

  testWidgets("Partner card: contains partner name",
      (WidgetTester tester) async {
    Widget partnerCard = PartnerCard(
      partnerName: "Rumah Zakat",
      targetPage: PartnerDashboardPage(partner: dummyPartner),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(partnerCard));

    expect(find.text("Rumah Zakat"), findsOneWidget);
  });

  testWidgets("Partner card: contains account icon",
      (WidgetTester tester) async {
    Widget partnerCard = PartnerCard(
      partnerName: "Rumah Zakat",
      targetPage: PartnerDashboardPage(partner: dummyPartner),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(partnerCard));

    expect(find.byIcon(Icons.account_circle), findsOneWidget);
  });

  testWidgets("Partner card: on tap", (WidgetTester tester) async {
    Widget partnerCard = PartnerCard(
      partnerName: "Rumah Zakat",
      targetPage: PartnerDashboardPage(partner: dummyPartner),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(partnerCard));

    await tester.tap(find.byType(Card));
    await tester.pump();

    expect(find.text("MITRA"), findsNothing);
  });
}
