import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:simpk/components/progress_bar.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget progressBar) {
    return MaterialApp(
      home: progressBar,
    );
  }

  testWidgets("Progress bar contains percentage label",
      (WidgetTester tester) async {
    Widget progressBar = ProgressBar(progressValue: 0.5);
    await tester.pumpWidget(_wrapWithMaterialApp(progressBar));

    expect(find.text("50.0%"), findsOneWidget);
  });

  testWidgets("Progress bar contains linear percent indicator",
      (WidgetTester tester) async {
    Widget progressBar = ProgressBar(progressValue: 0.5);
    await tester.pumpWidget(_wrapWithMaterialApp(progressBar));

    expect(find.byType(LinearPercentIndicator), findsOneWidget);
  });
}
