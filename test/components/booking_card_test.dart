import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/booking_card.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/booking/screens/booking_detail.dart';
import 'package:simpk/modules/project/models/Project.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget bookingCard) {
    return MaterialApp(
      home: bookingCard,
    );
  }

  testWidgets("Booking card contains date", (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
        bookingDate: DateTime.utc(2021, 11, 24),
        bookingTitle: "Kunjungan Pemeriksaan",
        bookingTime: DateTime.now(),
        projectName: "Bantuan Korban Banjir",
        assignedUser: "Astrida Nayla",
        onClick: BookingCard(
            bookingDate: DateTime.utc(2021, 11, 20),
            bookingTitle: "Kunjungan Pemeriksaan",
            bookingTime: DateTime.now(),
            projectName: "Bantuan Korban Banjir",
            assignedUser: "Astrida Nayla",
            onClick: BookingDetail(
              project: Project(
                  id: 1,
                  name: "",
                  status: "",
                  partner: "",
                  activityType: "",
                  region: "",
                  coordinate: "",
                  description: "",
                  progress: 0,
                  allocation: 0),
              booking: Booking(
                  id: 18,
                  activityName: "",
                  date: "",
                  time: "",
                  creatorName: ""),
            )));
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("24"), findsOneWidget);
  });

  testWidgets("Booking card contains month", (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 0),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("Nov"), findsOneWidget);
  });

  testWidgets("Booking card contains title", (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("Kunjungan Pemeriksaan"), findsOneWidget);
  });

  testWidgets("Booking card contains time", (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime(2021, 11, 24, 11, 20),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("11:20 AM"), findsOneWidget);
  });

  testWidgets("Booking card contains assigned user name",
      (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("Astrida Nayla"), findsOneWidget);
  });

  testWidgets("Booking card icons are correct", (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.byIcon(Icons.access_time), findsOneWidget);
    expect(find.byIcon(Icons.account_circle), findsOneWidget);
  });

  testWidgets("Booking card month length is no longer than 3",
      (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("November"), findsNothing);
  });

  testWidgets("Booking card month does not include year",
      (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    expect(find.text("2021"), findsNothing);
  });

  testWidgets("Booking card on tap redirect to another page",
      (WidgetTester tester) async {
    Widget bookingCard = BookingCard(
      bookingDate: DateTime.utc(2021, 11, 24),
      bookingTitle: "Kunjungan Pemeriksaan",
      bookingTime: DateTime.now(),
      projectName: "Bantuan Korban Banjir",
      assignedUser: "Astrida Nayla",
      onClick: BookingCard(
          bookingDate: DateTime.utc(2021, 11, 20),
          bookingTitle: "Kunjungan Pemeriksaan",
          bookingTime: DateTime.now(),
          projectName: "Bantuan Korban Banjir",
          assignedUser: "Astrida Nayla",
          onClick: BookingDetail(
            project: Project(
                id: 1,
                name: "",
                status: "",
                partner: "",
                activityType: "",
                region: "",
                coordinate: "",
                description: "",
                progress: 0,
                allocation: 1000000),
            booking: Booking(
                id: 18, activityName: "", date: "", time: "", creatorName: ""),
          )),
    );
    await tester.pumpWidget(_wrapWithMaterialApp(bookingCard));

    await tester.tap(find.byType(BookingCard));
    expect(find.text("Buat Booking Kunjungan"), findsNothing);
  });
}
