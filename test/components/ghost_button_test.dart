import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/ghost_button.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget ghostButton) {
    return MaterialApp(
      home: Scaffold(
        body: ghostButton,
      ),
    );
  }

  testWidgets("Ghost button contains text", (WidgetTester tester) async {
    Widget ghostButton = GhostButton(
        text: "Button Text",
        iconData: Icons.edit,
        targetPage: Scaffold(body: Text("Ringkasan Monitoring & Evaluasi")),
        startOffset: Offset(0.0, 1.0));
    await tester.pumpWidget(_wrapWithMaterialApp(ghostButton));

    expect(find.text("Button Text"), findsOneWidget);
  });

  testWidgets("Ghost button contains icon", (WidgetTester tester) async {
    Widget ghostButton = GhostButton(
        text: "Button Text",
        iconData: Icons.edit,
        targetPage: Scaffold(body: Text("Ringkasan Monitoring & Evaluasi")),
        startOffset: Offset(0.0, 1.0));
    await tester.pumpWidget(_wrapWithMaterialApp(ghostButton));

    expect(find.byIcon(Icons.edit), findsOneWidget);
  });

  testWidgets("Ghost button redirects to target page on tap",
      (WidgetTester tester) async {
    Widget ghostButton = GhostButton(
        text: "Button Text",
        iconData: Icons.edit,
        targetPage: Scaffold(body: Text("Ringkasan Monitoring & Evaluasi")),
        startOffset: Offset(0.0, 1.0));
    await tester.pumpWidget(_wrapWithMaterialApp(ghostButton));

    await tester.tap(find.text("Button Text"));
    await tester.pumpAndSettle();
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsOneWidget);
  });
}
