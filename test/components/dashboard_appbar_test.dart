import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:simpk/components/dashboard_appbar.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget dashboardAppBar) {
    return MaterialApp(
      home: dashboardAppBar,
    );
  }

  testWidgets("Appbar contains label and title", (WidgetTester tester) async {
    Widget dashboardAppBar = DashboardAppBar("PROYEK", "Bantuan Korban Banjir");
    await tester.pumpWidget(_wrapWithMaterialApp(dashboardAppBar));

    expect(find.text("PROYEK"), findsOneWidget);
    expect(find.text("Bantuan Korban Banjir"), findsOneWidget);
  });
}
