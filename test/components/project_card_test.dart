import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:simpk/components/project_card.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/project_dashboard_page.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import '../modules/project/screens/project_detail_tab_test.mocks.dart';

void main() {
  MockProjectAPIProvider mockProvider = MockProjectAPIProvider();
  GetIt.I.registerSingleton<ProjectAPIProvider>(mockProvider);

  Project dummyProject = Project(
    id: 1,
    name: 'Dummy Project Name',
    status: 'Dummy Project Status',
    partner: 'Dummy Partner Name',
    activityType: 'Dummy Activity Type',
    region: 'Dummy Region',
    coordinate: 'Dummy Coordinate',
    description: "Dummy Description",
    progress: 0.5,
    allocation: 100000000,
  );

  Widget _wrapWithMaterialApp(Widget projectCard) {
    return MaterialApp(
      home: projectCard,
    );
  }

  when(mockProvider.getProject(1)).thenAnswer((_) async => dummyProject);

  testWidgets("Project card contains project title",
      (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Proyek BPKH",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));
    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    expect(find.text("Proyek"), findsOneWidget);
  });

  testWidgets("Project card contains location", (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Proyek BPKH",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));
    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    expect(find.text("Jakarta"), findsOneWidget);
  });

  testWidgets("Project card contains project description",
      (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Proyek BPKH",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));
    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    expect(find.text("Proyek BPKH"), findsOneWidget);
  });

  testWidgets("Project card long description gets cropped",
      (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing " +
            "elit, sed do eiusmod tempor incididunt ut labore et dolore " +
            "magna aliqua. Ut enim ad minim veniam.",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));
    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    expect(find.textContaining("..."), findsOneWidget);
  });

  testWidgets("Project card contains progress percentage value",
      (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Proyek BPKH",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));
    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    expect(find.text("50.0%"), findsOneWidget);
  });

  testWidgets("Project card contains progress bar",
      (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Proyek BPKH",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));
    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    expect(find.byType(LinearPercentIndicator), findsOneWidget);
  });

  testWidgets("Project card redirects to project detail page on tap",
      (WidgetTester tester) async {
    Widget projectCard = ProjectCard(
        projectTitle: "Proyek",
        location: "Jakarta",
        description: "Proyek BPKH",
        progressValue: 0.5,
        targetPage: ProjectDashboardPage(project: dummyProject));

    when(mockProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);

    await tester.pumpWidget(_wrapWithMaterialApp(projectCard));

    await tester.tap(find.byType(Card));
    await tester.pumpAndSettle();

    expect(find.text("Biodata Proyek"), findsOneWidget);
  });
}
