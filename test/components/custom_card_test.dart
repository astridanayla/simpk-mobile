import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/custom_card.dart';
import 'package:simpk/config/theme.dart';

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();

  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

void main() {
  testWidgets('Card Component: test strings', (tester) async {
    String title = getRandomString(10);
    String userName = getRandomString(10);
    DateTime lastEdited = DateTime(2021, 6, 28);
    Icon userIcon = Icon(
      Icons.person_rounded,
      color: Colors.white,
    );
    CustomCard testCard = CustomCard(
        title: title,
        userName: userName,
        lastEdited: lastEdited,
        onClick: Scaffold(body: Text("Ringkasan Monitoring & Evaluasi")),
        userIcon: userIcon,
        displayIcon: Icons.note);

    await _pumpTestableWidget(tester, testCard);

    expect(find.text(title), findsOneWidget);
    expect(find.text(userName), findsOneWidget);
    expect(find.text("Terakhir diedit: 28/06/2021"), findsOneWidget);
  });

  testWidgets('Card Component: test on tap', (tester) async {
    String title = getRandomString(10);
    String userName = getRandomString(10);
    DateTime lastEdited = DateTime.now();
    Icon userIcon = Icon(
      Icons.person_rounded,
      color: Colors.white,
    );
    CustomCard testCard = CustomCard(
        title: title,
        userName: userName,
        lastEdited: lastEdited,
        onClick: Scaffold(body: Text("Ringkasan Monitoring & Evaluasi")),
        userIcon: userIcon,
        displayIcon: Icons.note);

    await _pumpTestableWidget(tester, testCard);

    await tester.tap(find.byType(CustomCard));
    await tester.pumpAndSettle();
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsOneWidget);
  });
}
