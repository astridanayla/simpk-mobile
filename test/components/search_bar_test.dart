import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/search_bar.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget searchBar) {
    return MaterialApp(
      home: searchBar,
    );
  }

  testWidgets("Search bar contains label", (WidgetTester tester) async {
    final TextEditingController _searchController = new TextEditingController();

    Widget searchBar = SearchBar(
      label: "Label",
      controller: _searchController,
      onSubmit: (value) => {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(searchBar));

    expect(find.text("Label"), findsOneWidget);
  });

  testWidgets("Search bar contains search icon", (WidgetTester tester) async {
    final TextEditingController _searchController = new TextEditingController();

    Widget searchBar = SearchBar(
      label: "Label",
      controller: _searchController,
      onSubmit: (value) => {},
    );
    await tester.pumpWidget(_wrapWithMaterialApp(searchBar));

    expect(find.byIcon(Icons.search), findsOneWidget);
  });
}
