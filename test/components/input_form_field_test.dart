import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/input_form_field.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget widget) {
    return MaterialApp(
      home: Scaffold(
        body: widget,
      ),
    );
  }

  testWidgets("Textinputform: contains label", (WidgetTester tester) async {
    String dummyLabel = "Dummmy Label";
    Widget inputFormField = InputFormField(
        label: dummyLabel, onChanged: (value) {}, validator: (value) {});
    await tester.pumpWidget(_wrapWithMaterialApp(inputFormField));

    expect(find.text(dummyLabel), findsOneWidget);
  });

  testWidgets("Textinputform: maxLines and initialValue",
      (WidgetTester tester) async {
    String dummyLabel = "Dummmy Label";
    String initialValue = "SOme value";
    Widget inputFormField = InputFormField(
        label: dummyLabel,
        onChanged: (value) {},
        validator: (value) {},
        maxLines: 4,
        initialValue: initialValue);
    await tester.pumpWidget(_wrapWithMaterialApp(inputFormField));

    expect(find.text(initialValue), findsOneWidget);

    final EditableText formfield =
        await tester.widget<EditableText>(find.text(initialValue));

    expect(formfield.maxLines, 4);
  });

  testWidgets("Textinputform: value changes", (WidgetTester tester) async {
    String dummyLabel = "Dummmy Label";
    String initialValue = "Some value";
    Widget inputFormField = InputFormField(
        label: dummyLabel,
        onChanged: (value) {},
        validator: (value) {},
        maxLines: 4,
        initialValue: initialValue);
    await tester.pumpWidget(_wrapWithMaterialApp(inputFormField));

    expect(find.text(initialValue), findsOneWidget);

    String newValue = "New some value";

    await tester.enterText(find.byType(TextFormField).first, newValue);
    await tester.pump();

    expect(find.text(newValue), findsOneWidget);
  });

  testWidgets("Textinputform: auto validate", (WidgetTester tester) async {
    String dummyLabel = "Nama";
    String initialValue = "Dummy name";
    Widget inputFormField = InputFormField(
        label: dummyLabel,
        onChanged: (value) {},
        validator: (value) {
          if (value == null || value.isEmpty || value.length == 0) {
            return dummyLabel + ' tidak boleh kosong.';
          }
          return null;
        },
        autovalidateMode: AutovalidateMode.always,
        initialValue: initialValue);
    await tester.pumpWidget(_wrapWithMaterialApp(inputFormField));

    expect(find.text(initialValue), findsOneWidget);

    String newValue = "";

    await tester.enterText(find.byType(TextFormField).first, newValue);
    await tester.pump(Duration(milliseconds: 400));

    String expectedErrorMessage = dummyLabel + " tidak boleh kosong.";
    expect(find.text(expectedErrorMessage), findsOneWidget);
  });

  testWidgets("Textinputform: enabled form label is visible",
      (WidgetTester tester) async {
    String dummyLabel = "Dummmy Label";
    Widget inputFormField = InputFormField(
      label: dummyLabel,
      onChanged: (value) {},
      validator: (value) {},
      enabled: true,
    );
    await tester.pumpWidget(_wrapWithMaterialApp(inputFormField));

    TextFormField field = tester.widget(find.byType(TextFormField));
    expect(field.enabled, true);
  });

  testWidgets("Textinputform: enabled form label is visible",
      (WidgetTester tester) async {
    String dummyLabel = "Dummmy Label";
    Widget inputFormField = InputFormField(
      label: dummyLabel,
      onChanged: (value) {},
      validator: (value) {},
      enabled: false,
    );
    await tester.pumpWidget(_wrapWithMaterialApp(inputFormField));

    TextFormField field = tester.widget(find.byType(TextFormField));
    expect(field.enabled, false);
  });
}
