import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/bottom_navbar.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/maps/screen/maps.dart';
import 'package:simpk/services/api_provider.dart';

import '../modules/home/screens/home_page_test.mocks.dart';

void main() {
  GetIt.I.registerSingleton<APIProvider>(MockAPIProvider());

  final dummyUniqueRegionsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": [
      "Indonesia",
      "DKI Jakarta",
    ]
  };

  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  late MockHomeAPIProvider mockHomeAPIProvider;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getUniqueRegions())
        .thenAnswer((_) async => dummyUniqueRegionsResponse);
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("Indonesia"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("DKI Jakarta"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
  });

  Widget _wrapWithMaterialApp(Widget bottomNavbar, [String role = 'Internal']) {
    return BlocProvider(
      create: (_) => AuthBloc.withRole(role),
      child: MaterialApp(
        home: Scaffold(
          bottomNavigationBar: bottomNavbar,
        ),
      ),
    );
  }

  testWidgets("Bottom navbar labels are correct", (WidgetTester tester) async {
    Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
    await tester.pumpWidget(_wrapWithMaterialApp(bottomNavbar));

    expect(find.text("Beranda"), findsOneWidget);
    expect(find.text("Lokasi"), findsOneWidget);
    expect(find.text("Mitra"), findsOneWidget);
    expect(find.text("Proyek"), findsOneWidget);
  });

  testWidgets("Bottom navbar icons are correct", (WidgetTester tester) async {
    Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
    await tester.pumpWidget(_wrapWithMaterialApp(bottomNavbar));

    expect(find.byIcon(Icons.home), findsOneWidget);
    expect(find.byIcon(Icons.near_me), findsOneWidget);
    expect(find.byIcon(Icons.people), findsOneWidget);
    expect(find.byIcon(Icons.content_paste), findsOneWidget);
  });

  testWidgets("Bottom navbar items redirect to other page",
      (WidgetTester tester) async {
    Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
    await tester.pumpWidget(_wrapWithMaterialApp(bottomNavbar));

    await tester.tap(find.text("Proyek"));
    await tester.pump();
    expect(find.text("Daftar Proyek"), findsOneWidget);

    await tester.tap(find.text("Lokasi"));
    await tester.pump();
    expect(find.byType(MapsPage), findsOneWidget);

    await tester.tap(find.text("Mitra"));
    await tester.pump();
    expect(find.text("Daftar Mitra"), findsOneWidget);

    await tester.tap(find.text("Beranda"));
    await tester.pump();
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsOneWidget);
  });

  group('role Mitra', () {
    testWidgets("Bottom navbar labels are correct",
        (WidgetTester tester) async {
      Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
      await tester.pumpWidget(_wrapWithMaterialApp(bottomNavbar, 'Mitra'));

      expect(find.text("Beranda"), findsOneWidget);
      expect(find.text("Lokasi"), findsOneWidget);
      expect(find.text("Mitra"), findsNothing);
      expect(find.text("Proyek"), findsOneWidget);
    });

    testWidgets("Bottom navbar icons are correct", (WidgetTester tester) async {
      Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
      await tester.pumpWidget(_wrapWithMaterialApp(bottomNavbar, 'Mitra'));

      expect(find.byIcon(Icons.home), findsOneWidget);
      expect(find.byIcon(Icons.near_me), findsOneWidget);
      expect(find.byIcon(Icons.people), findsNothing);
      expect(find.byIcon(Icons.content_paste), findsOneWidget);
    });
  });

  group('role Professional', () {
    testWidgets("Bottom navbar labels are correct",
        (WidgetTester tester) async {
      Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
      await tester
          .pumpWidget(_wrapWithMaterialApp(bottomNavbar, 'Professional'));

      expect(find.text("Beranda"), findsOneWidget);
      expect(find.text("Lokasi"), findsOneWidget);
      expect(find.text("Mitra"), findsNothing);
      expect(find.text("Proyek"), findsOneWidget);
    });

    testWidgets("Bottom navbar icons are correct", (WidgetTester tester) async {
      Widget bottomNavbar = BottomNavbar(currentPageIndex: 0);
      await tester
          .pumpWidget(_wrapWithMaterialApp(bottomNavbar, 'Professional'));

      expect(find.byIcon(Icons.home), findsOneWidget);
      expect(find.byIcon(Icons.near_me), findsOneWidget);
      expect(find.byIcon(Icons.people), findsNothing);
      expect(find.byIcon(Icons.content_paste), findsOneWidget);
    });
  });
}
