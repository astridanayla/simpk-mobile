import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:pie_chart/pie_chart.dart';

import 'package:simpk/components/pie_graph.dart';

void main() {
  Map<String, double> dataMap = {
    "Belum Mulai": 0.16,
    "Berjalan": 0.51,
    "Selesai": 0.33,
  };

  Widget _wrapWithMaterialApp(Widget pieGraph) {
    return MaterialApp(
      home: pieGraph,
    );
  }

  testWidgets("Pie graph contains pie chart", (WidgetTester tester) async {
    Widget pieGraph = PieGraph(dataMap: dataMap);
    await tester.pumpWidget(_wrapWithMaterialApp(pieGraph));

    expect(find.byType(PieChart), findsOneWidget);
  });

  testWidgets("Pie graph data corresponds to input data",
      (WidgetTester tester) async {
    Widget pieGraph = PieGraph(dataMap: dataMap);
    await tester.pumpWidget(_wrapWithMaterialApp(pieGraph));

    Finder chartFinder = find.byType(PieChart);
    PieChart chart = tester.firstWidget(chartFinder) as PieChart;
    expect(chart.dataMap, dataMap);
  });
}
