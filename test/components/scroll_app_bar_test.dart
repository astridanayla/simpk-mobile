import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/scroll_app_bar.dart';

Future _pumpTestableWidget(
  WidgetTester tester,
  ScrollController controller, {
  double? elevation,
}) async {
  await tester.pumpWidget(
    MaterialApp(
      home: Scaffold(
        appBar: ScrollAppBar(
          scrollController: controller,
          targetElevation: elevation,
        ),
        body: SingleChildScrollView(
          controller: controller,
          child: Container(
            height: 10000,
          ),
        ),
      ),
    ),
  );
}

void main() {
  late ScrollController scrollController;

  setUp(() {
    scrollController = ScrollController();
  });

  tearDown(() {
    scrollController.dispose();
  });

  testWidgets('Elevate to default value after scrolled down', (tester) async {
    await _pumpTestableWidget(tester, scrollController);

    scrollController.animateTo(20,
        duration: Duration(milliseconds: 10), curve: Curves.ease);
    await tester.pumpAndSettle();

    expect(find.byType(AppBar), findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is AppBar && widget.elevation == null),
        findsOneWidget);
  });

  testWidgets('Elevate to default value after scrolled down', (tester) async {
    await _pumpTestableWidget(tester, scrollController, elevation: 10);

    scrollController.animateTo(20,
        duration: Duration(milliseconds: 10), curve: Curves.ease);
    await tester.pumpAndSettle();

    expect(find.byType(AppBar), findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is AppBar && widget.elevation == 10),
        findsOneWidget);
  });
}
