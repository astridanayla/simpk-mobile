import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/components/secondary_button.dart';
import 'package:simpk/modules/project/screens/add_attachment.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import '../modules/project/screens/project_detail_tab_test.mocks.dart';

void main() {
  GetIt.I.registerSingleton<ProjectAPIProvider>(MockProjectAPIProvider());

  Widget _wrapWithMaterialApp(Widget secondaryButton) {
    return MaterialApp(
      home: Scaffold(body: secondaryButton),
    );
  }

  testWidgets("Secondary button contains text", (WidgetTester tester) async {
    Widget secondaryButton = SecondaryButton(
        text: "Tambah Lampiran",
        iconData: Icons.add_circle_outline,
        onPressed: () => {AddAttachment(projectId: 1)});
    await tester.pumpWidget(_wrapWithMaterialApp(secondaryButton));

    expect(find.text("Tambah Lampiran"), findsOneWidget);
  });

  testWidgets("Secondary button contains icon", (WidgetTester tester) async {
    Widget secondaryButton = SecondaryButton(
        text: "Tambah Lampiran",
        iconData: Icons.add_circle_outline,
        onPressed: () => {AddAttachment(projectId: 1)});
    await tester.pumpWidget(_wrapWithMaterialApp(secondaryButton));

    expect(find.byIcon(Icons.add_circle_outline), findsOneWidget);
  });

  testWidgets("Secondary button redirects to target page",
      (WidgetTester tester) async {
    Widget secondaryButton = SecondaryButton(
        text: "Tambah Lampiran",
        iconData: Icons.add_circle_outline,
        onPressed: () => {AddAttachment(projectId: 1)});
    await tester.pumpWidget(_wrapWithMaterialApp(secondaryButton));
    await tester.tap(find.text("Tambah Lampiran"));
    await tester.pump();

    expect(find.text("Tambah Lampiran"), findsOneWidget);
  });
}
