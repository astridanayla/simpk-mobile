import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hexcolor/hexcolor.dart';

import 'package:simpk/components/data_card.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget dataCard) {
    return MaterialApp(
      home: dataCard,
    );
  }

  testWidgets("Data card contains data", (WidgetTester tester) async {
    Widget dataCard =
        DataCard(data: "120", label: "Total Proyek", colorHex: "#01788E");
    await tester.pumpWidget(_wrapWithMaterialApp(dataCard));

    expect(find.text("120"), findsOneWidget);
  });

  testWidgets("Data card contains label", (WidgetTester tester) async {
    Widget dataCard =
        DataCard(data: "120", label: "Total Proyek", colorHex: "#01788E");
    await tester.pumpWidget(_wrapWithMaterialApp(dataCard));

    expect(find.text("Total Proyek"), findsOneWidget);
  });

  testWidgets("Data card text color corresponds to hexcolor input",
      (WidgetTester tester) async {
    Widget dataCard =
        DataCard(data: "120", label: "Total Proyek", colorHex: "#01788E");
    await tester.pumpWidget(_wrapWithMaterialApp(dataCard));

    Text cardText = tester.firstWidget(find.byType(Text)) as Text;
    TextStyle? cardTextStyle = cardText.style;

    expect(cardTextStyle!.color, HexColor("01788E"));
  });

  testWidgets("Data card background color corresponds to hexcolor input",
      (WidgetTester tester) async {
    Widget dataCard =
        DataCard(data: "120", label: "Total Proyek", colorHex: "#01788E");
    await tester.pumpWidget(_wrapWithMaterialApp(dataCard));

    Card card = tester.firstWidget(find.byType(Card)) as Card;

    expect(card.color, HexColor("01788E").withOpacity(0.16));
  });
}
