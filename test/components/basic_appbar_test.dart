import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:simpk/components/basic_appbar.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget basicAppbar) {
    return MaterialApp(
      home: basicAppbar,
    );
  }

  testWidgets("Appbar contains back icon", (WidgetTester tester) async {
    Widget basicAppbar = BasicAppbar("Filter");
    await tester.pumpWidget(_wrapWithMaterialApp(basicAppbar));

    expect(find.byIcon(Icons.arrow_back), findsOneWidget);
  });

  testWidgets("Appbar contains title", (WidgetTester tester) async {
    Widget basicAppbar = BasicAppbar("Filter");
    await tester.pumpWidget(_wrapWithMaterialApp(basicAppbar));

    expect(find.text("Filter"), findsOneWidget);
  });

  testWidgets("Appbar back icon when clicked redirect to previous page",
      (WidgetTester tester) async {
    Widget basicAppbar = BasicAppbar("Filter");
    await tester.pumpWidget(_wrapWithMaterialApp(basicAppbar));

    await tester.tap(find.byIcon(Icons.arrow_back));
    await tester.pumpAndSettle();

    expect(find.text("Filter"), findsNothing);
  });
}
