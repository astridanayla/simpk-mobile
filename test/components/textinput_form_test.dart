import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/texinput_form.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget textInputForm) {
    return MaterialApp(
      home: textInputForm,
    );
  }

  testWidgets("Textinput form contains label", (WidgetTester tester) async {
    Widget textInput = TextInput("Label");
    await tester.pumpWidget(_wrapWithMaterialApp(textInput));

    expect(find.text("Label"), findsOneWidget);
  });
}
