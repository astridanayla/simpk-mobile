import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/custom_dropdown.dart';
import 'package:simpk/config/theme.dart';

void _expectDropdownButton({required String label}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is DropdownButtonFormField &&
          widget.decoration.labelText == label),
      findsOneWidget);
}

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();

  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

void main() {
  testWidgets("Dropdown Component: no tap", (tester) async {
    String _label = "Some label";
    List<String> _items = ["1", "2", "3"];
    String? _value;
    CustomDropdown testDropdown = CustomDropdown(
        label: _label,
        items: _items,
        value: _value,
        onChanged: (value) => {_value = value});

    await _pumpTestableWidget(tester, testDropdown);
    _expectDropdownButton(label: _label);
  });

  testWidgets("Dropdown Component: tap on dropdown", (tester) async {
    String _label = "Some label";
    List<String> _items = ["item 1", "item 2", "item 3"];
    String? _value;
    CustomDropdown testDropdown = CustomDropdown(
        label: _label,
        items: _items,
        value: _value,
        onChanged: (value) => {_value = value});

    await _pumpTestableWidget(tester, testDropdown);
    await tester.tap(find.text(_label));
    await tester.pump();
    await tester.pump(const Duration(seconds: 1));

    expect(find.text("item 1"), findsWidgets);
    expect(find.text("item 2"), findsWidgets);
    expect(find.text("item 3"), findsWidgets);
  });
}
