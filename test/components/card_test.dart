import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';
import 'package:simpk/components/card.dart';
import 'package:simpk/config/theme.dart';

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();

  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

void main() {
  testWidgets('Card Component: test strings', (tester) async {
    String title = getRandomString(10);
    String userName = getRandomString(10);
    DateTime lastEdited = DateTime.now();

    CustomCard testCard = CustomCard(
        title: title,
        userName: userName,
        lastEdited: lastEdited,
        onClick: () => {},
        userIcon: Icons.account_circle);

    await _pumpTestableWidget(tester, testCard);

    expect(find.text(title), findsOneWidget);
    expect(find.text(userName), findsOneWidget);
    expect(find.byIcon(Icons.account_circle), findsOneWidget);
    String formattedLastEdit = DateFormat('dd/MM/yyyy').format(lastEdited);
    expect(find.text("Terakhir diedit: " + formattedLastEdit), findsOneWidget);
  });
}
