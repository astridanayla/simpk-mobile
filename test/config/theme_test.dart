import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/config/theme.dart';

void main() {
  test("Check theme colors", () {
    final ThemeData theme = appTheme();

    expect(theme.primaryColor, Color(0xFF0A9EA2));
    expect(theme.primaryColorLight, Color(0xFF01788E));
    expect(theme.indicatorColor, Color(0xFF0A9EA2));
    expect(theme.backgroundColor, Color(0xFFF8F8F8));
    expect(theme.scaffoldBackgroundColor, Color(0xFFF8F8F8));
    expect(theme.errorColor, Color(0xFFEB5757));
    expect(theme.disabledColor, Color(0xFFBDBDBD));
    expect(theme.shadowColor, Colors.grey.withOpacity(0.4));
  });

  group("Check font styles", () {
    final ThemeData theme = appTheme();

    test('Headline 1', () {
      TextStyle mainHeader = theme.textTheme.headline1!;
      expect(mainHeader.fontFamily, "Lato");
      expect(mainHeader.fontWeight, FontWeight.w900);
      expect(mainHeader.fontSize, 24);
      expect(mainHeader.height, 1);
      expect(mainHeader.color, Color(0xFF0A3254));
    });

    test('Headline 2', () {
      TextStyle sectionHeader = theme.textTheme.headline2!;
      expect(sectionHeader.fontFamily, "Lato");
      expect(sectionHeader.fontWeight, FontWeight.w900);
      expect(sectionHeader.fontSize, 18);
      expect(sectionHeader.height, 1);
      expect(sectionHeader.color, Color(0xFF0A3254));
    });

    test('Headline 3', () {
      TextStyle subHeader = theme.textTheme.headline3!;
      expect(subHeader.fontFamily, "Lato");
      expect(subHeader.fontWeight, FontWeight.w700);
      expect(subHeader.fontSize, 16);
      expect(subHeader.height, 1);
      expect(subHeader.color, Color(0xFF0A4B73));
    });

    test('Headline 4', () {
      TextStyle boldText = theme.textTheme.headline4!;
      expect(boldText.fontFamily, "Lato");
      expect(boldText.fontWeight, FontWeight.bold);
      expect(boldText.fontSize, 14);
      expect(boldText.height, 1);
      expect(boldText.color, Color(0xFF333333));
    });

    test('Headline 5', () {
      TextStyle largeBoldText = theme.textTheme.headline5!;
      expect(largeBoldText.fontFamily, "Lato");
      expect(largeBoldText.fontWeight, FontWeight.w900);
      expect(largeBoldText.fontSize, 32);
      expect(largeBoldText.color, Color(0xFF0A4B73));
    });

    test('Subtitle 1', () {
      TextStyle subtitle = theme.textTheme.subtitle1!;
      expect(subtitle.fontFamily, "Lato");
      expect(subtitle.fontWeight, FontWeight.w900);
      expect(subtitle.fontSize, 20);
      expect(subtitle.height, 1);
      expect(subtitle.color, Color(0xFF0A4B73));
    });

    test('Body Text 1', () {
      TextStyle text = theme.textTheme.bodyText1!;
      expect(text.fontFamily, "Lato");
      expect(text.fontWeight, FontWeight.normal);
      expect(text.fontSize, 14);
      expect(text.height, 1.6);
      expect(text.color, Color(0xFF333333));
    });

    test('Body Text 2', () {
      TextStyle smallText = theme.textTheme.bodyText2!;
      expect(smallText.fontFamily, "Lato");
      expect(smallText.fontWeight, FontWeight.normal);
      expect(smallText.fontSize, 12);
      expect(smallText.height, 1.6);
      expect(smallText.color, Color(0xFF4F4F4F));
    });
  });
}
