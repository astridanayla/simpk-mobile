import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/maps/screen/maps.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/services/api_provider.dart';

import '../../home/screens/home_page_test.mocks.dart';
import '../../monitoring/screens/monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: child,
        theme: theme,
      ),
    ),
  );
}

void _expectTextField({required String label}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      findsOneWidget);
}

@GenerateMocks([APIProvider])
void main() {
  late ReportService mockReportRepository;

  final dummyUniqueRegionsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": [
      "Indonesia",
      "DKI Jakarta",
    ]
  };

  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  setUp(() {
    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets("Maps: cloud icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    Widget mapPage = MapsPage();
    await _pumpTestableWidget(tester, mapPage);

    await tester.tap(find.byIcon(Icons.cloud_upload));
    await tester.pumpAndSettle();

    expect(find.text("Daftar Laporan Offline"), findsOneWidget);
  });

  testWidgets("Maps: profile icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    MockHomeAPIProvider mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getUniqueRegions())
        .thenAnswer((_) async => dummyUniqueRegionsResponse);
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    Widget mapPage = MapsPage();
    await _pumpTestableWidget(tester, mapPage);

    await tester.tap(find.byIcon(Icons.account_circle).at(0));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byIcon(Icons.account_circle), findsWidgets);
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsNothing);

    GetIt.I.unregister(instance: mockHomeAPIProvider);
  });

  testWidgets("Maps: cross button on search bar deletes input",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    MapsPage dummyMapsPage = MapsPage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = '123, 123';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    dummyMapsPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllProjects(token)).thenAnswer((_) async => [
          Project(
            id: 1,
            name: projectName,
            status: projectStatus,
            partner: partnerName,
            activityType: activityType,
            region: region,
            coordinate: coordinate,
            description: description,
            progress: progress,
            allocation: 100000000,
          )
        ]);
    await _pumpTestableWidget(tester, dummyMapsPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    String searchValue = "bantuan";
    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => []);

    await tester.enterText(find.byType(TextFormField), searchValue);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text(searchValue), findsOneWidget);

    await tester.tap(find.byIcon(Icons.clear).at(0));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.text("test"), findsNothing);
  });

  testWidgets("Maps: Initial Page", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    MapsPage dummyMapsPage = MapsPage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = '123, 123';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    dummyMapsPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllProjects(token)).thenAnswer((_) async => [
          Project(
            id: 1,
            name: projectName,
            status: projectStatus,
            partner: partnerName,
            activityType: activityType,
            region: region,
            coordinate: coordinate,
            description: description,
            progress: progress,
            allocation: 100000000,
          )
        ]);
    await _pumpTestableWidget(tester, dummyMapsPage);

    expect(find.byType(CircularProgressIndicator), findsOneWidget);
    expect(find.byType(GoogleMap), findsNothing);

    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.byType(CircularProgressIndicator), findsNothing);
    _expectTextField(label: "Cari Proyek");
    expect(find.byType(GoogleMap), findsOneWidget);
  });

  testWidgets("Maps: Search result", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    MapsPage dummyMapsPage = MapsPage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = '123, 123';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    dummyMapsPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllProjects(token)).thenAnswer((_) async => [
          Project(
            id: 1,
            name: projectName,
            status: projectStatus,
            partner: partnerName,
            activityType: activityType,
            region: region,
            coordinate: coordinate,
            description: description,
            progress: progress,
            allocation: 100000000,
          )
        ]);
    await _pumpTestableWidget(tester, dummyMapsPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    Project project1 = Project(
      id: 1,
      name: "project1",
      status: "status project 1",
      partner: "partner 1",
      activityType: "activity type 1",
      region: "region 1",
      coordinate: "123, 123",
      description: "project 1 description",
      progress: .3,
      allocation: 100000000,
    );

    Project project2 = Project(
      id: 1,
      name: "project2",
      status: "status project 2",
      partner: "partner 2",
      activityType: "activity type 2",
      region: "region 2",
      coordinate: "123, 123",
      description: "project 2 description",
      progress: .4,
      allocation: 100000000,
    );

    String searchValue = "bantuan";
    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => [project1, project2]);

    await tester.enterText(find.byType(TextFormField), searchValue);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text(searchValue), findsOneWidget);
    await tester.ensureVisible(find.text("project1"));
    expect(find.text("project1"), findsWidgets);
    await tester.ensureVisible(find.text("project2"));
    expect(find.text("project2"), findsWidgets);
  });

  testWidgets("Maps: Search empty string", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    MapsPage dummyMapsPage = MapsPage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = '123, 123';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    dummyMapsPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllProjects(token)).thenAnswer((_) async => [
          Project(
            id: 1,
            name: projectName,
            status: projectStatus,
            partner: partnerName,
            activityType: activityType,
            region: region,
            coordinate: coordinate,
            description: description,
            progress: progress,
            allocation: 100000000,
          )
        ]);
    await _pumpTestableWidget(tester, dummyMapsPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    Project project1 = Project(
      id: 1,
      name: "project1",
      status: "status project 1",
      partner: "partner 1",
      activityType: "activity type 1",
      region: "region 1",
      coordinate: "123, 123",
      description: "project 1 description",
      progress: .3,
      allocation: 100000000,
    );

    Project project2 = Project(
      id: 1,
      name: "project2",
      status: "status project 2",
      partner: "partner 2",
      activityType: "activity type 2",
      region: "region 2",
      coordinate: "123, 123",
      description: "project 2 description",
      progress: .4,
      allocation: 100000000,
    );

    String searchValue = "bantuan";
    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => [project1, project2]);
    await tester.enterText(find.byType(TextFormField), searchValue);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text(searchValue), findsOneWidget);
    await tester.ensureVisible(find.text("project1"));
    expect(find.text("project1"), findsWidgets);
    await tester.ensureVisible(find.text("project2"));
    expect(find.text("project2"), findsWidgets);

    String newSearchValue = "";
    await tester.enterText(find.byType(TextFormField), newSearchValue);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text("project1"), findsNothing);
    expect(find.text("project2"), findsNothing);
  });

  testWidgets("Maps: No projects found after search",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    MapsPage dummyMapsPage = MapsPage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = '123, 123';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    dummyMapsPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllProjects(token)).thenAnswer((_) async => [
          Project(
            id: 1,
            name: projectName,
            status: projectStatus,
            partner: partnerName,
            activityType: activityType,
            region: region,
            coordinate: coordinate,
            description: description,
            progress: progress,
            allocation: 100000000,
          )
        ]);
    await _pumpTestableWidget(tester, dummyMapsPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    String searchValue = "bantuan";
    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => []);

    await tester.enterText(find.byType(TextFormField), searchValue);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text("Hasil pencarian tidak ditemukan"), findsOneWidget);
  });

  testWidgets("Maps: Search then tap on the project to move project marker",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    MapsPage dummyMapsPage = MapsPage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = '123, 123';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    dummyMapsPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllProjects(token)).thenAnswer((_) async => [
          Project(
            id: 1,
            name: projectName,
            status: projectStatus,
            partner: partnerName,
            activityType: activityType,
            region: region,
            coordinate: coordinate,
            description: description,
            progress: progress,
            allocation: 100000000,
          )
        ]);
    await _pumpTestableWidget(tester, dummyMapsPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    Project project1 = Project(
      id: 1,
      name: "project1",
      status: "status project 1",
      partner: "partner 1",
      activityType: "activity type 1",
      region: "region 1",
      coordinate: "123, 123",
      description: "project 1 description",
      progress: .3,
      allocation: 100000000,
    );

    Project project2 = Project(
      id: 1,
      name: "project2",
      status: "status project 2",
      partner: "partner 2",
      activityType: "activity type 2",
      region: "region 2",
      coordinate: "123, 123",
      description: "project 2 description",
      progress: .4,
      allocation: 100000000,
    );

    String searchValue = "bantuan";
    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => [project1, project2]);

    await tester.enterText(find.byType(TextFormField), searchValue);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text(searchValue), findsOneWidget);

    await tester.ensureVisible(find.text("project2"));
    await tester.tap(find.text("project2"));
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text(searchValue), findsNothing);
    expect(find.text("project2"), findsOneWidget);
  });
}
