import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/screens/login.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/services/api_provider.dart';

import '../../home/screens/home_page_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(home: child, theme: theme),
    ),
  );
}

void _expectTextField({required String label, required Matcher matcher}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      matcher);
}

void main() {
  final dummyUniqueRegionsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": [
      "Indonesia",
      "DKI Jakarta",
    ]
  };

  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  late MockHomeAPIProvider mockHomeAPIProvider;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getUniqueRegions())
        .thenAnswer((_) async => dummyUniqueRegionsResponse);
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("Indonesia"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("DKI Jakarta"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
  });

  testWidgets("Login: initial state", (WidgetTester tester) async {
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));

    expect(find.byType(Image), findsOneWidget);
    _expectTextField(label: "Email", matcher: findsOneWidget);
    _expectTextField(label: "Password", matcher: findsOneWidget);
    expect(
      find.byWidgetPredicate(
          (widget) => widget is ElevatedButton && widget.child is Text),
      findsOneWidget,
    );
    expect(find.text("Masuk"), findsOneWidget);
    expect(find.text("Lupa Password?"), findsOneWidget);
  });

  testWidgets("Login: tap Lupa Password to go to Lupa Password screen",
      (WidgetTester tester) async {
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));

    await tester.ensureVisible(find.text("Lupa Password?"));
    await tester.tap(find.text("Lupa Password?"));
    await tester.pumpAndSettle();

    expect(find.byType(Image), findsOneWidget);
    expect(find.text("Lupa Password?"), findsOneWidget);
    expect(
        find.text("Segera hubungi admin untuk memperbarui " +
            "password anda dengan mengirim email ke zona.ariemenda@bpkh.go.id"),
        findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is ElevatedButton && widget.child is Text),
        findsOneWidget);
    expect(find.text("Kembali ke Halaman Utama"), findsOneWidget);
  });

  testWidgets("Lupa Password: tap Kembali ke halaman login",
      (WidgetTester tester) async {
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));

    await tester.ensureVisible(find.text("Lupa Password?"));
    await tester.tap(find.text("Lupa Password?"));
    await tester.pumpAndSettle();

    _expectTextField(label: "Email", matcher: findsNothing);
    _expectTextField(label: "Password", matcher: findsNothing);
    expect(find.text("Masuk"), findsNothing);
    expect(
        find.text("Segera hubungi admin untuk memperbarui " +
            "password anda dengan mengirim email ke zona.ariemenda@bpkh.go.id"),
        findsOneWidget);
    expect(find.text("Kembali ke Halaman Utama"), findsOneWidget);

    await tester.ensureVisible(find.text("Kembali ke Halaman Utama"));
    await tester.tap(find.text("Kembali ke Halaman Utama"));
    await tester.pumpAndSettle();

    _expectTextField(label: "Email", matcher: findsOneWidget);
    _expectTextField(label: "Password", matcher: findsOneWidget);
    expect(find.text("Masuk"), findsOneWidget);
    expect(
        find.text("Segera hubungi admin untuk memperbarui " +
            "password anda dengan mengirim email ke zona.ariemenda@bpkh.go.id"),
        findsNothing);
    expect(find.text("Kembali ke Halaman Utama"), findsNothing);
  });

  testWidgets("Login: test text field onChange and validators",
      (WidgetTester tester) async {
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));

    await tester.ensureVisible(find.text('Masuk'));
    await tester.tap(find.text("Masuk"));
    await tester.pump();

    expect(find.text("Email tidak boleh kosong."), findsOneWidget);
    expect(find.text("Password tidak boleh kosong."), findsOneWidget);

    String wrongEmailFormat = "email";
    await tester.enterText(find.byType(TextFormField).at(0), wrongEmailFormat);
    await tester.ensureVisible(find.text('Masuk'));
    await tester.tap(find.text("Masuk"));
    await tester.pump();
    expect(find.textContaining("Masukkan email dengan format yang benar."),
        findsOneWidget);

    String correctEmailFormat = "email@email.com";
    await tester.enterText(
        find.byType(TextFormField).at(0), correctEmailFormat);
    await tester.ensureVisible(find.text('Masuk'));
    await tester.tap(find.text("Masuk"));
    await tester.pump();
    expect(find.text("Email tidak boleh kosong."), findsNothing);
    expect(find.text("Masukkan email dengan format yang benar."), findsNothing);

    String dummyPassword = "somePassword";
    await tester.enterText(find.byType(TextFormField).at(1), dummyPassword);

    when(mockAPIProvider.login(correctEmailFormat, dummyPassword))
        .thenAnswer((_) async => {'success': "True"});

    await tester.ensureVisible(find.text('Masuk'));
    await tester.tap(find.text("Masuk"));
    await tester.pump();
    expect(find.text("Password tidak boleh kosong."), findsNothing);
  });

  testWidgets("Login: password field test visibility",
      (WidgetTester tester) async {
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));

    String dummyPassword = "somePassword";
    await tester.enterText(find.byType(TextFormField).at(1), dummyPassword);
    await tester.pump();

    expect(find.byIcon(Icons.visibility_off), findsNothing);
    expect(find.byIcon(Icons.visibility), findsOneWidget);

    await tester.ensureVisible(find.byIcon(Icons.visibility));
    await tester.tap(find.byIcon(Icons.visibility));
    await tester.pump();

    expect(find.text(dummyPassword), findsOneWidget);
    expect(find.byIcon(Icons.visibility_off), findsOneWidget);
    expect(find.byIcon(Icons.visibility), findsNothing);

    await tester.ensureVisible(find.byIcon(Icons.visibility_off));
    await tester.tap(find.byIcon(Icons.visibility_off));
    await tester.pump();

    expect(find.byIcon(Icons.visibility_off), findsNothing);
    expect(find.byIcon(Icons.visibility), findsOneWidget);
  });

  testWidgets("Login: successful login", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));
    GetIt.I.registerSingleton<APIProvider>(mockAPIProvider);

    String dummyEmail = "email@email.com";
    String dummyPassword = "somePassword";
    String success = "True";
    int statusCode = 200;
    String message = "User logged in  successfully";
    String token = "someToken";
    when(mockAPIProvider.login(dummyEmail, dummyPassword))
        .thenAnswer((_) async => {
              "success": success,
              "status code": statusCode,
              "message": message,
              "token": token,
              "role": "Internal",
            });

    await tester.enterText(find.byType(TextFormField).at(0), dummyEmail);
    await tester.enterText(find.byType(TextFormField).at(1), dummyPassword);
    await tester.pumpAndSettle();

    await tester.ensureVisible(find.text('Masuk'));
    await tester.tap(find.text("Masuk"));
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(sharedPreferences.getString("token"), token);
    expect(find.byType(HomePage), findsOneWidget);

    GetIt.I.unregister(instance: mockAPIProvider);
  });

  testWidgets("Login: fail login", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    LoginScreen dummyLoginScreen = LoginScreen();
    dummyLoginScreen.state.api = mockAPIProvider;
    await _pumpTestableWidget(tester, dummyLoginScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 100));

    String dummyEmail = "email@email.com";
    String dummyPassword = "somePassword";
    when(mockAPIProvider.login(dummyEmail, dummyPassword))
        .thenAnswer((_) async => {
              "non_field_errors": [
                "A user with this email and password is not found."
              ]
            });

    await tester.enterText(find.byType(TextFormField).at(0), dummyEmail);
    await tester.enterText(find.byType(TextFormField).at(1), dummyPassword);
    await tester.pumpAndSettle();

    await tester.ensureVisible(find.text('Masuk'));
    await tester.tap(find.text("Masuk"));
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(
        find.text("Log In gagal, alamat email tidak terdaftar atau password " +
            "yang dimasukkan tidak sesuai"),
        findsOneWidget);
    expect(sharedPreferences.getString("token"), null);
  });
}
