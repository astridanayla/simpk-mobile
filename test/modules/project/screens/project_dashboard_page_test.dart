import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_repository.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/project_dashboard_page.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import '../../monitoring/screens/money_return_details_test.mocks.dart';
import 'project_dashboard_page_test.mocks.dart';
import 'project_detail_tab_test.mocks.dart';

@GenerateMocks([ReportRepository])
void main() {
  MockProjectAPIProvider mockProvider = MockProjectAPIProvider();
  GetIt.I.registerSingleton<ProjectAPIProvider>(mockProvider);
  MoneyReturnAPIProvider moneyReturnApi = MockMoneyReturnAPIProvider();
  GetIt.I.registerSingleton<MoneyReturnAPIProvider>(moneyReturnApi);

  Project dummyProject = Project(
    id: 1,
    name: 'Bantuan Korban Banjir',
    status: 'Dummy Project Status',
    partner: 'Dummy Partner Name',
    activityType: 'Dummy Activity Type',
    region: 'Dummy Region',
    coordinate: 'Dummy Coordinate',
    description: "Dummy Description",
    progress: 0.5,
    allocation: 100000000,
  );

  Widget _wrapWithMaterialApp(Widget projectDashboardPage) {
    return BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: projectDashboardPage,
      ),
    );
  }

  when(mockProvider.getProject(1)).thenAnswer((_) async => dummyProject);

  testWidgets("Project dashboard page : contains tab menu",
      (WidgetTester tester) async {
    when(mockProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    Widget projectDashboardPage = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboardPage));

    expect(find.text("RINGKASAN"), findsOneWidget);
    expect(find.text("MONITORING"), findsOneWidget);
    expect(find.text("BOOKING"), findsOneWidget);
  });

  testWidgets("Project dashboard page contains project title",
      (WidgetTester tester) async {
    when(mockProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    Widget projectDashboardPage = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboardPage));

    expect(find.text("PROYEK"), findsOneWidget);
    expect(find.text("Bantuan Korban Banjir"), findsOneWidget);
  });

  testWidgets("Project dashboard page summary tab is clickable",
      (WidgetTester tester) async {
    when(mockProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    Widget projectDashboardPage = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboardPage));

    await tester.tap(find.text("RINGKASAN"));
    await tester.pumpAndSettle();
    expect(find.text("Progress Penyelesaian"), findsOneWidget);
  });

  testWidgets("Project dashboard page monitoring tab is clickable",
      (WidgetTester tester) async {
    when(mockProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    ReportService mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => []);
    GetIt.I.registerSingleton(mockReportRepository);

    Widget projectDashboardPage = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboardPage));

    await tester.tap(find.text("MONITORING"));
    await tester.pumpAndSettle();

    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets("Project dashboard page booking tab is clickable",
      (WidgetTester tester) async {
    when(mockProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    Widget projectDashboardPage = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboardPage));

    await tester.tap(find.text("BOOKING"));
    await tester.pump();
  });
}
