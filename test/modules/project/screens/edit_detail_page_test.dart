import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/edit_detail_page.dart';
import 'package:simpk/modules/project/screens/project_dashboard_page.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import 'project_detail_tab_test.mocks.dart';

void main() {
  Project dummyProject = Project(
    id: 1,
    name: 'Dummy Project Name',
    status: 'Dummy Project Status',
    partner: 'Dummy Partner Name',
    activityType: 'Dummy Activity Type',
    region: 'Dummy Region',
    coordinate: 'Dummy Coordinate',
    description: "Dummy Description",
    progress: 0.5,
    allocation: 100000000,
  );

  Widget _wrapWithMaterialApp(Widget editDetailPage) {
    return MaterialApp(
      home: editDetailPage,
    );
  }

  final apiProvider = MockProjectAPIProvider();
  when(apiProvider.updateProject(any)).thenAnswer((_) async => 202);
  when(apiProvider.getProject(1)).thenAnswer((_) async => dummyProject);
  GetIt.I.registerSingleton<ProjectAPIProvider>(apiProvider);

  testWidgets("Edit detail page contains title", (WidgetTester tester) async {
    Widget editDetailPage =
        EditDetailPage("Edit Progress", "Persentase Progress", "60.69");
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    expect(find.text("Edit Progress"), findsOneWidget);
  });

  testWidgets("Edit detail page contains text input form and label",
      (WidgetTester tester) async {
    Widget editDetailPage =
        EditDetailPage("Edit Progress", "Persentase Progress", "60.69");
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    expect(find.text("Persentase Progress"), findsOneWidget);
    expect(find.byType(TextFormField), findsOneWidget);
  });

  testWidgets("Edit progress page contains note", (WidgetTester tester) async {
    Widget editDetailPage = EditDetailPage(
      "Edit Progress",
      "Persentase Progress",
      "60",
      inputInstruction: "Instruksi",
    );
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    expect(find.text("Catatan:"), findsOneWidget);
  });

  testWidgets("Note does not appear on other page except edit progress",
      (WidgetTester tester) async {
    Widget editDetailPage =
        EditDetailPage("Edit Lokasi", "Koordinat Lokasi", "1,1");
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    expect(find.text("Catatan:"), findsNothing);
  });

  testWidgets("Edit detail page contains save and cancel button",
      (WidgetTester tester) async {
    Widget editDetailPage =
        EditDetailPage("Edit Progress", "Persentase Progress", "60.69");
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    expect(find.text("Simpan"), findsOneWidget);
    expect(find.text("Batal"), findsOneWidget);
  });

  testWidgets("Edit detail page save button redirect to previous page",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    Widget projectDashboard = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboard));

    await tester.tap(find.text("Edit Progress"));
    await tester.pumpAndSettle();
    expect(find.text("MONITORING"), findsNothing);

    await tester.ensureVisible(find.text("Simpan"));
    await tester.tap(find.text("Simpan"));
    await tester.pumpAndSettle();
    expect(find.text("MONITORING"), findsOneWidget);
  });

  testWidgets("Edit detail page cancel button redirect to previous page",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(dummyProject.id))
        .thenAnswer((_) async => []);
    Widget projectDashboard = ProjectDashboardPage(project: dummyProject);
    await tester.pumpWidget(_wrapWithMaterialApp(projectDashboard));

    await tester.tap(find.text("Edit Progress"));
    await tester.pumpAndSettle();
    expect(find.text("MONITORING"), findsNothing);

    await tester.ensureVisible(find.text("Batal"));
    await tester.tap(find.text("Batal"));
    await tester.pumpAndSettle();
    expect(find.text("MONITORING"), findsOneWidget);
  });

  testWidgets("Edit progress form only accept decimal input",
      (WidgetTester tester) async {
    Widget editDetailPage =
        EditDetailPage("Edit Progress", "Persentase Progress", "60.69");
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    await tester.enterText(find.byType(TextFormField).at(0), "a");
    await tester.pumpAndSettle();
    expect(find.text('Input harus desimal 0-100'), findsOneWidget);
  });

  testWidgets("Edit lokasi form only accept coordinate input",
      (WidgetTester tester) async {
    Widget editDetailPage =
        EditDetailPage("Edit Lokasi", "Koordinat Lokasi", "1,1");
    await tester.pumpWidget(_wrapWithMaterialApp(editDetailPage));

    await tester.enterText(find.byType(TextFormField).at(0), "1,a");
    await tester.pumpAndSettle();
    expect(find.text("Input harus koordinat yang dipisahkan oleh ', '"),
        findsOneWidget);
  });
}
