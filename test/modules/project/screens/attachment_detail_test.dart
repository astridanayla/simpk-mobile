import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/project/models/Attachment.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/attachment_file_detail_page.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import 'project_detail_tab_test.mocks.dart';

void main() {
  Attachment dummyFileAttachment = Attachment(
      id: 2,
      attachmentName: "Dummy File Attachment",
      attachmentType: "Berkas",
      attachmentPath: "google.com",
      dateCreated: DateTime.now(),
      projectId: 1,
      userName: "User");
  Project dummyProject = Project(
    id: 1,
    name: 'Dummy Project Name',
    status: 'Dummy Project Status',
    partner: 'Dummy Partner Name',
    activityType: 'Dummy Activity Type',
    region: 'Dummy Region',
    coordinate: 'Dummy Coordinate',
    description: "Dummy Description",
    progress: 0.5,
    allocation: 100000000,
  );

  final apiProvider = MockProjectAPIProvider();
  when(apiProvider.updateProject(any)).thenAnswer((_) async => 202);
  when(apiProvider.getProject(1)).thenAnswer((_) async => dummyProject);
  GetIt.I.registerSingleton<ProjectAPIProvider>(apiProvider);

  testWidgets('File attachment detail: static content test',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: AttachmentFileDetail(
      projectId: 1,
      attachment: dummyFileAttachment,
    ))));
    await tester.pump();

    expect(find.text('Dummy File Attachment'), findsWidgets);
    expect(find.text('User'), findsWidgets);
    expect(find.textContaining('Dibuat pada:'), findsWidgets);
    expect(find.byIcon(Icons.account_circle), findsOneWidget);
    await tester.tap(find.byIcon(Icons.delete));
    expect(find.byIcon(Icons.delete), findsOneWidget);
    expect(find.text("Lihat Berkas"), findsOneWidget);
  });

  testWidgets('File attachment detail: delete attachment successfully',
      (WidgetTester tester) async {
    AttachmentFileDetail fileDetail =
        AttachmentFileDetail(projectId: 1, attachment: dummyFileAttachment);
    when(apiProvider.deleteAttachment(1, 2)).thenAnswer((_) async => true);

    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: fileDetail),
    ));
    await tester.pump();

    await tester.tap(find.byIcon(Icons.delete));
    await tester.pump(Duration(milliseconds: 1000));
    expect(
        find.text('Anda yakin ingin menghapus lampiran ini?'), findsOneWidget);
    await tester.tap(find.text("Hapus"));
    await tester.pump(Duration(milliseconds: 4000));
  });

  testWidgets('File attachment detail: cancel delete booking',
      (WidgetTester tester) async {
    AttachmentFileDetail fileDetail =
        AttachmentFileDetail(projectId: 1, attachment: dummyFileAttachment);
    when(apiProvider.deleteAttachment(1, 2)).thenAnswer((_) async => true);

    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: fileDetail),
    ));
    await tester.pump();

    await tester.tap(find.byIcon(Icons.delete));
    await tester.pump(Duration(milliseconds: 1000));
    expect(
        find.text('Anda yakin ingin menghapus lampiran ini?'), findsOneWidget);
    await tester.tap(find.text("Kembali"));
    await tester.pump(Duration(milliseconds: 4000));
    expect(find.text('Dummy File Attachment'), findsOneWidget);
  });
}
