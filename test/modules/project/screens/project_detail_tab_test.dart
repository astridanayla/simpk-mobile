import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/progress_bar.dart';
import 'package:simpk/modules/project/blocs/lampiran_bloc.dart';
import 'package:simpk/modules/project/blocs/project_bloc.dart';
import 'package:simpk/modules/project/models/Attachment.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/add_attachment.dart';
import 'package:simpk/modules/project/screens/project_detail_tab.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import 'project_detail_tab_test.mocks.dart';

final _dummyProject = Project(
  id: 1,
  name: 'Bantuan Korban Banjir',
  status: 'Dummy Project Status',
  partner: 'Dummy Partner Name',
  activityType: 'Dummy Activity Type',
  region: 'Dummy Region',
  coordinate: '-6.310016735496198, 106.82030167073002',
  description: "Dummy Description",
  progress: 0.5,
  allocation: 123000000,
);

final dummyFileAttachment = Attachment(
  id: 2,
  attachmentName: "Dummy File Attachment",
  attachmentType: "Berkas",
  attachmentPath: "google.com",
  dateCreated: DateTime.now(),
  projectId: 1,
  userName: "User",
);

final dummyImageAttachment = Attachment(
  id: 1,
  attachmentName: "Dummy Image Attachment",
  attachmentType: "Foto",
  attachmentPath: "lib/assets/images/building_placeholder.jpg",
  dateCreated: DateTime.now(),
  projectId: 1,
  userName: "User",
);

@GenerateMocks([ProjectAPIProvider])
void main() {
  late MockProjectAPIProvider apiProvider;

  setUp(() {
    apiProvider = MockProjectAPIProvider();
    GetIt.I.registerSingleton<ProjectAPIProvider>(apiProvider);
    when(apiProvider.getProject(1)).thenAnswer((_) async => _dummyProject);
  });

  tearDown(() {
    GetIt.I.unregister(instance: apiProvider);
  });

  Future _pumpTestableWidget(WidgetTester tester) async {
    Widget projectDetailPage = MultiBlocProvider(
      providers: [
        BlocProvider<ProjectBloc>(
          create: (context) => ProjectBloc(_dummyProject),
        ),
        BlocProvider<AttachmentBloc>(
            create: (context) => AttachmentBloc(_dummyProject.id)),
      ],
      child: ProjectDetailPage(),
    );
    await tester.pumpWidget(
      MaterialApp(home: projectDetailPage),
    );
  }

  testWidgets("Project detail page contains progress section",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);

    await _pumpTestableWidget(tester);

    expect(find.text("Progress Penyelesaian"), findsOneWidget);
    expect(find.byType(ProgressBar), findsOneWidget);
    expect(find.text("Edit Progress"), findsOneWidget);
  });

  testWidgets("Project detail page contains finance detail section",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);

    expect(find.text("Dana yang Disetujui"), findsOneWidget);
    expect(find.text("Total Realisasi"), findsOneWidget);
    expect(find.text("Sisa Dana"), findsOneWidget);
  });

  testWidgets("Project detail page contains project biodata",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);

    await tester.ensureVisible(find.byIcon(Icons.copy));
    await tester.tap(find.byIcon(Icons.copy));
    await tester.pump();
    expect(find.text("Biodata Proyek"), findsOneWidget);
    expect(find.text("Status"), findsOneWidget);
    expect(find.text("Mitra"), findsOneWidget);
    expect(find.text("Jenis Kegiatan"), findsOneWidget);
    expect(find.text("Daerah"), findsOneWidget);
    expect(find.text("Koordinat Lokasi"), findsOneWidget);
    expect(find.byIcon(Icons.copy), findsOneWidget);
    expect(find.text("Edit Lokasi"), findsOneWidget);
  });

  testWidgets("Project detail page contains button to get map route",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);

    await tester.ensureVisible(find.byType(ElevatedButton));
    await tester.tap(find.text("Rute ke Lokasi"));
    await tester.pump();
    expect(find.text("Rute ke Lokasi"), findsOneWidget);
  });

  testWidgets("Project detail page contains project description",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);

    await tester.ensureVisible(find.text("Deskripsi Proyek"));
    expect(find.text("Deskripsi Proyek"), findsOneWidget);

    await tester.ensureVisible(find.text("Edit Deskripsi"));
    expect(find.text("Edit Deskripsi"), findsOneWidget);
  });

  testWidgets("Project detail page contains attachment",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);

    await tester.ensureVisible(find.text("Lampiran Proyek"));
    expect(find.text("Lampiran Proyek"), findsOneWidget);
  });

  testWidgets("Project detail page tap add attachment button",
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);

    expect(find.text("Tambah Lampiran"), findsOneWidget);
    expect(find.byIcon(Icons.add_circle_outline), findsOneWidget);

    await tester.ensureVisible(find.byType(OutlinedButton));
    await tester.tap(find.text("Tambah Lampiran"));
    await tester.pumpAndSettle();

    expect(find.byType(AddAttachment), findsOneWidget);
  });

  testWidgets('Snackbar appears when copy icon is clicked',
      (WidgetTester tester) async {
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);
    const String snackbarMessage = "Teks berhasil disalin!";
    await _pumpTestableWidget(tester);
    expect(find.text(snackbarMessage), findsNothing);

    Finder copy_icon = find.byIcon(Icons.copy);
    await tester.ensureVisible(copy_icon);
    await tester.tap(copy_icon);
    await tester.pump();
    expect(find.text(snackbarMessage), findsNothing);
  });

  testWidgets('Show project detail data', (tester) async {
    final fetchProject = Project(
      id: 1,
      name: 'Bantuan Korban Banjir',
      status: 'Dummy Project Status',
      partner: 'Dummy Partner Name',
      activityType: 'Dummy Activity Type',
      region: 'Dummy Region',
      coordinate: '-6.310016735496198, 106.82030167073002',
      description: "Dummy Description",
      progress: 0.5,
      allocation: 123000000,
      partnerName: 'Rumah Dummy',
      realization: 20000000,
      reportCount: 9,
    );
    when(apiProvider.getProject(1)).thenAnswer((_) async => fetchProject);
    when(apiProvider.getAllAttachment(_dummyProject.id))
        .thenAnswer((_) async => []);

    await _pumpTestableWidget(tester);

    expect(find.text('Total Monitoring : - kali'), findsOneWidget);
    expect(find.text('Rp -'), findsNWidgets(2));

    expect(find.text('Dummy Project Status'), findsOneWidget);
    expect(find.text('Dummy Partner Name'), findsOneWidget);
    expect(find.text('Dummy Activity Type'), findsOneWidget);
    expect(find.text('Dummy Region'), findsOneWidget);
    expect(find.text('-6.310016735496198, 106.82030167073002'), findsOneWidget);
    expect(find.text("Dummy Description"), findsOneWidget);
    expect(find.text('50.0%'), findsOneWidget);
    expect(find.text('Rp 123.000.000'), findsOneWidget);

    await tester.pumpAndSettle();
    verify(apiProvider.getProject(1)).called(1);

    expect(find.text('Total Monitoring : 9 kali'), findsOneWidget);
    expect(find.text('Rp 20.000.000'), findsOneWidget);
    expect(find.text('Rp 103.000.000'), findsOneWidget);

    expect(find.text('Dummy Project Status'), findsOneWidget);
    expect(find.text('Rumah Dummy'), findsOneWidget);
    expect(find.text('Dummy Activity Type'), findsOneWidget);
    expect(find.text('Dummy Region'), findsOneWidget);
    expect(find.text('-6.310016735496198, 106.82030167073002'), findsOneWidget);
    expect(find.text("Dummy Description"), findsOneWidget);
    expect(find.text('50.0%'), findsOneWidget);
    expect(find.text('Rp 123.000.000'), findsOneWidget);
  });

  // testWidgets('Display all attachments of a project', (tester) async {
  //   final fetchProject = Project(
  //     id: 1,
  //     name: 'Bantuan Korban Banjir',
  //     status: 'Dummy Project Status',
  //     partner: 'Dummy Partner Name',
  //     activityType: 'Dummy Activity Type',
  //     region: 'Dummy Region',
  //     coordinate: '-6.310016735496198, 106.82030167073002',
  //     description: "Dummy Description",
  //     progress: 0.5,
  //     allocation: 123000000,
  //     partnerName: 'Rumah Dummy',
  //     realization: 20000000,
  //     reportCount: 9,
  //   );

  //   when(apiProvider.getProject(1)).thenAnswer((_) async => fetchProject);
  //   when(apiProvider.getAllAttachment(fetchProject.id))
  //   .thenAnswer((_) async => [dummyFileAttachment, dummyImageAttachment]);

  //   await _pumpTestableWidget(tester);

  //   await tester.ensureVisible(find.byType(CustomCard));
  //   expect(find.text('Dummy File Attachment'), findsOneWidget);
  //   expect(find.text('Dummy Image Attachment'), findsOneWidget);
  //   expect(find.byIcon(Icons.file_present), findsOneWidget);
  //   expect(find.byIcon(Icons.photo_size_select_actual), findsOneWidget);
  //   expect(find.textContaining('Dibuat pada:'), findsWidgets);
  //   expect(find.text('User'), findsWidgets);
  // });

  group('actions', () {
    testWidgets('Edit progress will update progress and call updateProject',
        (tester) async {
      when(apiProvider.updateProject(any)).thenAnswer((_) async => 202);
      when(apiProvider.getAllAttachment(_dummyProject.id))
          .thenAnswer((_) async => []);
      await _pumpTestableWidget(tester);

      expect(find.text('50.0%'), findsOneWidget);

      await tester.ensureVisible(find.text('Edit Progress'));
      await tester.tap(find.text('Edit Progress'));
      await tester.pumpAndSettle();

      await tester.enterText(find.byType(TextFormField), '60.0');
      await tester.ensureVisible(find.text('Simpan'));
      await tester.tap(find.text('Simpan'));
      await tester.pump();

      verify(apiProvider.updateProject(any)).called(1);

      expect(find.text('60.0%'), findsOneWidget);
    });

    testWidgets('Edit progress error will show snackbar', (tester) async {
      when(apiProvider.updateProject(any)).thenAnswer((_) async => 415);
      when(apiProvider.getAllAttachment(_dummyProject.id))
          .thenAnswer((_) async => []);

      await _pumpTestableWidget(tester);

      expect(find.text('50.0%'), findsOneWidget);

      await tester.ensureVisible(find.text('Edit Progress'));
      await tester.tap(find.text('Edit Progress'));
      await tester.pumpAndSettle();

      await tester.enterText(find.byType(TextFormField), '60.0');
      await tester.ensureVisible(find.text('Simpan'));
      await tester.tap(find.text('Simpan'));
      await tester.pump();

      verify(apiProvider.updateProject(any)).called(1);
      expect(find.text('Error updating progress.'), findsOneWidget);
      expect(find.text('50.0%'), findsOneWidget);
      expect(find.text('60.0%'), findsNothing);
    });

    testWidgets('Edit coordinate will update coordinate and call updateProject',
        (tester) async {
      when(apiProvider.updateProject(any)).thenAnswer((_) async => 202);
      when(apiProvider.getAllAttachment(_dummyProject.id))
          .thenAnswer((_) async => []);
      await _pumpTestableWidget(tester);

      expect(
          find.text('-6.310016735496198, 106.82030167073002'), findsOneWidget);

      await tester.ensureVisible(find.text('Edit Lokasi'));
      await tester.tap(find.text('Edit Lokasi'));
      await tester.pumpAndSettle();

      await tester.enterText(find.byType(TextFormField), '-7, -107');
      await tester.ensureVisible(find.text('Simpan'));
      await tester.tap(find.text('Simpan'));
      await tester.pumpAndSettle();

      verify(apiProvider.updateProject(any)).called(1);

      expect(find.text('-7, -107'), findsOneWidget);
    });

    testWidgets('Edit coordinate error will show snackbar', (tester) async {
      when(apiProvider.updateProject(any)).thenAnswer((_) async => 415);
      when(apiProvider.getAllAttachment(_dummyProject.id))
          .thenAnswer((_) async => []);

      await _pumpTestableWidget(tester);

      expect(
          find.text('-6.310016735496198, 106.82030167073002'), findsOneWidget);

      await tester.ensureVisible(find.text('Edit Lokasi'));
      await tester.tap(find.text('Edit Lokasi'));
      await tester.pumpAndSettle();

      await tester.enterText(find.byType(TextFormField), '-7, -107');
      await tester.ensureVisible(find.text('Simpan'));
      await tester.tap(find.text('Simpan'));
      await tester.pump();

      verify(apiProvider.updateProject(any)).called(1);
      expect(find.text('Error updating coordinate.'), findsOneWidget);

      await tester.pumpAndSettle();
      expect(
          find.text('-6.310016735496198, 106.82030167073002'), findsOneWidget);
      expect(find.text('-7, -107'), findsNothing);
    });

    testWidgets(
        'Edit description will update description and call updateProject',
        (tester) async {
      when(apiProvider.updateProject(any)).thenAnswer((_) async => 202);
      when(apiProvider.getAllAttachment(_dummyProject.id))
          .thenAnswer((_) async => []);
      await _pumpTestableWidget(tester);

      expect(find.text('Dummy Description'), findsOneWidget);
      final newDesc = 'New description';

      await tester.ensureVisible(find.text('Edit Deskripsi'));
      await tester.tap(find.text('Edit Deskripsi'));
      await tester.pumpAndSettle();

      await tester.enterText(find.byType(TextFormField), newDesc);
      await tester.ensureVisible(find.text('Simpan'));
      await tester.tap(find.text('Simpan'));
      await tester.pumpAndSettle();

      verify(apiProvider.updateProject(any)).called(1);

      expect(find.text(newDesc), findsOneWidget);
    });

    testWidgets('Edit description error will show snackbar', (tester) async {
      when(apiProvider.updateProject(any)).thenAnswer((_) async => 415);
      when(apiProvider.getAllAttachment(_dummyProject.id))
          .thenAnswer((_) async => []);

      await _pumpTestableWidget(tester);

      expect(find.text('Dummy Description'), findsOneWidget);
      final newDesc = 'New description';

      await tester.ensureVisible(find.text('Edit Deskripsi'));
      await tester.tap(find.text('Edit Deskripsi'));
      await tester.pumpAndSettle();

      await tester.enterText(find.byType(TextFormField), newDesc);
      await tester.ensureVisible(find.text('Simpan'));
      await tester.tap(find.text('Simpan'));
      await tester.pump();

      verify(apiProvider.updateProject(any)).called(1);
      expect(find.text('Error updating description.'), findsOneWidget);

      await tester.pumpAndSettle();
      expect(find.text('Dummy Description'), findsOneWidget);
      expect(find.text(newDesc), findsNothing);
    });
  });
}
