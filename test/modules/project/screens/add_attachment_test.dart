import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';

import 'package:simpk/modules/project/screens/add_attachment.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import 'project_detail_tab_test.mocks.dart';

void main() {
  GetIt.I.registerSingleton<ProjectAPIProvider>(MockProjectAPIProvider());

  testWidgets('Add Attachment content test', (WidgetTester tester) async {
    //Build the "Add Attachment App"
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
    await tester.pump();

    //Verify page content
    expect(find.text('Tambah Lampiran'), findsOneWidget);
    expect(find.text('Pilih Jenis Lampiran'), findsOneWidget);
    expect(find.text('Simpan'), findsOneWidget);
    expect(find.text('Batal'), findsOneWidget);
    expect(find.text('Nama Lampiran'), findsNothing);
    expect(find.text('Ambil Foto'), findsNothing);
    expect(find.text('Tambah Berkas'), findsNothing);
  });

  testWidgets('Add Attachment Dropdown test', (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
    await tester.pump();

    await tester.tap(find.text("Pilih Jenis Lampiran"));
    await tester.pump();

    //Verify dropdown content
    expect(find.text('Foto'), findsWidgets);
    expect(find.text("Berkas"), findsWidgets);
  });

  testWidgets('Add Attachment: Image selected test',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
    await tester.pump();

    await tester.tap(find.text("Pilih Jenis Lampiran"));
    await tester.pump();

    await tester.tap(find.text("Foto").last);
    await tester.pump();

    //Verify content after choosing image
    expect(find.text('Nama Lampiran'), findsOneWidget);
    expect(find.text('Ambil Foto'), findsOneWidget);
    expect(find.text('Tambah Berkas'), findsNothing);
  });

  testWidgets('Add Attachment: Berkas selected test',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
    await tester.pump();

    await tester.tap(find.text("Pilih Jenis Lampiran"));
    await tester.pump();

    await tester.tap(find.text("Berkas").last);
    await tester.pump();

    //Verify content after choosing berkas
    expect(find.text('Nama Lampiran'), findsOneWidget);
    expect(find.text('Upload Berkas'), findsOneWidget);
    expect(find.text('Ambil Foto'), findsNothing);
  });

  testWidgets('Add Attachment: Tap Batal to go back',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
    await tester.pump();

    await tester.tap(find.text("Batal"));
    await tester.pumpAndSettle();

    expect(find.byType(Scaffold), findsNothing);
  });

  testWidgets('Add Attachment: Simpan Button is initially disabled',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
    await tester.pump();

    ElevatedButton btn = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn.enabled, false);
  });

  group('$ImagePicker', () {
    const MethodChannel channel =
        MethodChannel('plugins.flutter.io/image_picker');

    final List<MethodCall> log = <MethodCall>[];

    setUp(() {
      channel.setMockMethodCallHandler((MethodCall methodCall) async {
        log.add(methodCall);
        return '';
      });

      log.clear();
    });

    testWidgets('Add Attachment: Take image for choosing image works',
        (WidgetTester tester) async {
      await tester.pumpWidget(
          MaterialApp(home: Scaffold(body: AddAttachment(projectId: 1))));
      await tester.pump();

      await tester.tap(find.text("Pilih Jenis Lampiran"));
      await tester.pump();

      await tester.tap(find.text("Foto").last);
      await tester.pump();

      await tester.tap(find.text("Ambil Foto"));
      await tester.pump();

      expect(
        log,
        <Matcher>[],
      );
    });
  });
}
