import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';

import '../../monitoring/services/monitoring_api_provider_test.mocks.dart';

Project generateProject() => Project(
      id: 1,
      name: 'Bantuan Korban Banjir',
      status: 'Dummy Project Status',
      partner: 'Dummy Partner Name',
      activityType: 'Dummy Activity Type',
      region: 'Dummy Region',
      coordinate: 'Dummy Coordinate',
      description: "Dummy Description",
      progress: 0.5,
      allocation: 100000000,
    );

void main() {
  SharedPreferences.setMockInitialValues({'token': ''});

  test('update project', () async {
    final apiProvider = ProjectAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/monitoring/proyek/1') {
          return Response(request.body, 202);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final project = generateProject();
    final statusCode = await apiProvider.updateProject(project);

    expect(statusCode, 202);
  });

  test('get project', () async {
    final project = generateProject();
    final apiProvider = ProjectAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/monitoring/proyek/1') {
          return Response(jsonEncode(project), 200);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final result = await apiProvider.getProject(1);

    expect(result.id, project.id);
    expect(result.name, project.name);
  });

  test('Test success to get all attachment', () async {
    List<dynamic> attachmentData = [
      {
        "id": 1,
        "nama_lampiran": "dummy",
        "jenis_lampiran": "Berkas",
        "lampiran_file":
            "/media/210604_Final_Term_Exam_Term-2_2021_Rev001.xlsx",
        "user": 1,
        "proyek": 1,
        "user_name": "BPKH",
        "created_at": "2021-07-05"
      }
    ];
    final apiProvider = ProjectAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/monitoring/proyek/1/lampiran') {
          return Response(jsonEncode(attachmentData), 200);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final result = await apiProvider.getAllAttachment(1);

    expect(result[0].attachmentName, attachmentData[0]['nama_lampiran']);
    expect(result[0].attachmentType, attachmentData[0]['jenis_lampiran']);
    expect(result[0].projectId, attachmentData[0]['proyek']);
    expect(result[0].userName, attachmentData[0]['user_name']);
    expect(result[0].dateCreated,
        DateTime.parse(attachmentData[0]['created_at'].toString()));
  });

  test('Test success to delete attachment', () async {
    int attachmentId = 1;
    int projectId = 1;

    final apiProvider = ProjectAPIProvider(
      client: MockClient((request) async {
        if (request.url.path ==
            'monitoring/proyek/$projectId/lampiran/$attachmentId') {
          return Response("No Content", 204);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final result = await apiProvider.deleteAttachment(projectId, attachmentId);
    expect(result, true);
  });
}
