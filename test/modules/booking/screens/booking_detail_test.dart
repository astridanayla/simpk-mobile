import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/booking/screens/booking_detail.dart';
import 'package:simpk/modules/booking/services/booking_api_provider.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/services/api_provider.dart';

import 'booking_tab_test.mocks.dart';

Future pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

Project _dummyProject = Project(
  id: 1,
  name: 'Dummy Project Name',
  status: 'Dummy Project Status',
  partner: 'Dummy Partner Name',
  activityType: 'Dummy Activity Type',
  region: 'Dummy Region',
  coordinate: 'Dummy Coordinate',
  description: "Dummy Description",
  progress: 0.5,
  allocation: 100000000,
);

Booking _dummyBooking = Booking(
  id: 1,
  activityName: "Kunjungan Pemeriksaan",
  date: "2020-12-12",
  time: "10:10:00",
  creatorName: "Fulan bin Fulan",
  description: "Lorem ipsum",
);

@GenerateMocks([APIProvider])
void main() {
  late MockBookingAPIProvider apiProvider;

  setUp(() {
    apiProvider = MockBookingAPIProvider();
    GetIt.I.registerSingleton<BookingAPIProvider>(apiProvider);
    when(apiProvider.getAllBooking(1)).thenAnswer((_) async => [_dummyBooking]);
  });

  tearDown(() {
    GetIt.I.unregister(instance: apiProvider);
  });

  testWidgets('Booking detail: static contents are correct',
      (WidgetTester tester) async {
    BookingDetail bookingDetail = BookingDetail(
      project: _dummyProject,
      booking: _dummyBooking,
    );
    await pumpTestableWidget(tester, bookingDetail);

    expect(find.text('Kunjungan Pemeriksaan'), findsOneWidget);
    expect(find.byIcon(Icons.delete), findsOneWidget);
    expect(find.text('Fulan bin Fulan'), findsOneWidget);
    expect(find.text('Data Kunjungan'), findsOneWidget);
    expect(find.byIcon(Icons.date_range), findsOneWidget);
    expect(find.byIcon(Icons.access_time), findsOneWidget);
    expect(find.text('Keterangan'), findsOneWidget);
    expect(find.textContaining('Lorem ipsum'), findsOneWidget);
  });

  testWidgets('Booking detail: delete booking successfully',
      (WidgetTester tester) async {
    BookingDetail bookingDetail = BookingDetail(
      project: _dummyProject,
      booking: _dummyBooking,
    );

    when(apiProvider.deleteBooking(1, 1)).thenAnswer((_) async => true);

    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: bookingDetail),
    ));
    await tester.pump();

    await tester.tap(find.byIcon(Icons.delete));
    await tester.pump(Duration(milliseconds: 1000));
    expect(
        find.text('Anda yakin ingin menghapus booking ini?'), findsOneWidget);
    await tester.tap(find.text("Hapus"));
    await tester.pump(Duration(milliseconds: 4000));
  });

  testWidgets('Booking detail: cancel delete booking',
      (WidgetTester tester) async {
    BookingDetail bookingDetail = BookingDetail(
      project: _dummyProject,
      booking: _dummyBooking,
    );

    when(apiProvider.deleteBooking(1, 1)).thenAnswer((_) async => true);

    await tester.pumpWidget(MaterialApp(
      home: Scaffold(body: bookingDetail),
    ));
    await tester.pump();

    await tester.tap(find.byIcon(Icons.delete));
    await tester.pump(Duration(milliseconds: 1000));
    expect(
        find.text('Anda yakin ingin menghapus booking ini?'), findsOneWidget);
    await tester.tap(find.text("Kembali"));
    await tester.pump(Duration(milliseconds: 4000));
    expect(find.text('Kunjungan Pemeriksaan'), findsOneWidget);
  });
}
