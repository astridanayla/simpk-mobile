import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/booking/models/Booking.dart';

import 'package:simpk/modules/booking/screens/create_booking.dart';
import 'package:simpk/services/api_provider.dart';

import 'create_booking_test.mocks.dart';

Future pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

@GenerateMocks([APIProvider])
void main() {
  testWidgets('Create Booking: Static content test',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: CreateBooking(projectId: 1))));
    await tester.pump();

    expect(find.text('Buat Booking'), findsOneWidget);
    expect(find.text('Nama Kegiatan'), findsOneWidget);
    expect(find.text('Tanggal'), findsOneWidget);
    expect(find.text('Waktu'), findsOneWidget);
    expect(find.text('Keterangan'), findsOneWidget);
    expect(find.text('Batal'), findsOneWidget);
    expect(find.text('Simpan'), findsOneWidget);
    expect(find.byIcon(Icons.calendar_today), findsOneWidget);
    expect(find.byIcon(Icons.access_time), findsOneWidget);
  });

  testWidgets('Create Booking: Tap batal redirects to previous page',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: CreateBooking(projectId: 1))));
    await tester.pump();

    await tester.tap(find.text("Batal"));
    await tester.pumpAndSettle();

    expect(find.text('Nama Kegiatan'), findsNothing);
    expect(find.text('Tanggal'), findsNothing);
    expect(find.text("Waktu"), findsNothing);
  });

  testWidgets('Create Booking: Simpan button is initially disabled',
      (WidgetTester tester) async {
    await tester.pumpWidget(
        MaterialApp(home: Scaffold(body: CreateBooking(projectId: 1))));
    await tester.pump();

    ElevatedButton btn = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn.enabled, false);
  });

  testWidgets(
      'Create Booking: Button is disabled while mandatory fields not filled',
      (WidgetTester tester) async {
    CreateBooking createBooking = CreateBooking(projectId: 1);
    String activityName = 'Dummy Activity Name';
    String description = 'Dummy Description';

    await pumpTestableWidget(tester, createBooking);
    await tester.pump(Duration(milliseconds: 1000));

    ElevatedButton btn1 = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn1.enabled, false);

    await tester.enterText(find.byType(TextFormField).at(0), activityName);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();

    ElevatedButton btn2 = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn2.enabled, false);

    await tester.enterText(find.byType(TextFormField).at(1), '01/01/2021');
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();

    ElevatedButton btn3 = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn3.enabled, false);

    await tester.tap(find.byIcon(Icons.access_time));
    await tester.pump();
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    ElevatedButton btn4 = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn4.enabled, true);

    await tester.enterText(find.byType(TextFormField).at(3), description);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();

    ElevatedButton btn5 = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn5.enabled, true);

    await tester.tap(find.text("Simpan"));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.text("Buat Booking"), findsOneWidget);
  });

  testWidgets("Create Booking: create booking data with description",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    CreateBooking createBooking = CreateBooking(projectId: 1);
    String activityName = 'Dummy Activity Name';
    DateTime bookingDate = DateFormat.yMd().parse('01/01/2021');
    TimeOfDay picked = TimeOfDay(hour: 11, minute: 0);
    DateTime bookingTime = DateTime(2019, 08, 1, picked.hour, picked.minute, 0);
    String description = 'Dummy Description';
    String username = 'username';
    int projectId = 1;
    String token = sharedPreferences.getString("token")!;
    createBooking.state.api = mockAPIProvider;

    when(mockAPIProvider.createBooking(token, activityName, bookingDate,
            bookingTime, description, projectId))
        .thenAnswer((_) async => Booking(
            id: 1,
            activityName: activityName,
            date: bookingDate.toString(),
            time: bookingTime.toString(),
            description: description,
            creatorName: username));

    await pumpTestableWidget(tester, createBooking);
    await tester.pump(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), activityName);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();
    await tester.enterText(find.byType(TextFormField).at(1), '01/01/2021');
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();
    await tester.tap(find.byIcon(Icons.access_time));
    await tester.pump();
    await tester.tap(find.text('OK'));
    await tester.pump();
    await tester.enterText(find.byType(TextFormField).at(3), description);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();

    ElevatedButton btn = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn.enabled, true);
    await tester.tap(find.text("Simpan"));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.text("Buat Booking"), findsOneWidget);
  });

  testWidgets("Create Booking: create booking data no description",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    CreateBooking createBooking = CreateBooking(projectId: 1);
    String activityName = 'Dummy Activity Name';
    DateTime bookingDate = DateFormat.yMd().parse('01/01/2021');
    TimeOfDay picked = TimeOfDay(hour: 11, minute: 0);
    DateTime bookingTime = DateTime(2019, 08, 1, picked.hour, picked.minute, 0);
    String description = 'Tidak ada keterangan';
    String username = 'username';
    int projectId = 1;
    String token = sharedPreferences.getString("token")!;
    createBooking.state.api = mockAPIProvider;

    when(mockAPIProvider.createBooking(token, activityName, bookingDate,
            bookingTime, description, projectId))
        .thenAnswer((_) async => Booking(
            id: 1,
            activityName: activityName,
            date: bookingDate.toString(),
            time: bookingTime.toString(),
            description: description,
            creatorName: username));

    await pumpTestableWidget(tester, createBooking);
    await tester.pump(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), activityName);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();
    await tester.enterText(find.byType(TextFormField).at(1), '01/01/2021');
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();
    await tester.tap(find.byIcon(Icons.access_time));
    await tester.pump();
    await tester.tap(find.text('OK'));
    await tester.pump();

    ElevatedButton btn = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn.enabled, true);
    await tester.tap(find.text("Simpan"));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.text("Buat Booking"), findsOneWidget);
  });

  testWidgets("Create Booking: create booking with date picker",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    CreateBooking createBooking = CreateBooking(projectId: 1);
    String activityName = 'Dummy Activity Name';
    DateTime bookingDate =
        DateTime(DateTime.now().year, DateTime.now().month, 15);
    TimeOfDay picked = TimeOfDay(hour: 11, minute: 0);
    DateTime bookingTime = DateTime(2019, 08, 1, picked.hour, picked.minute, 0);
    String description = 'Tidak ada keterangan';
    String username = 'username';
    int projectId = 1;
    String token = sharedPreferences.getString("token")!;
    createBooking.state.api = mockAPIProvider;

    when(mockAPIProvider.createBooking(token, activityName, bookingDate,
            bookingTime, description, projectId))
        .thenAnswer((_) async => Booking(
            id: 1,
            activityName: activityName,
            date: bookingDate.toString(),
            time: bookingTime.toString(),
            description: description,
            creatorName: username));

    await pumpTestableWidget(tester, createBooking);
    await tester.pump(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), activityName);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.tap(find.byIcon(Icons.calendar_today));
    await tester.pump();
    await tester.tap(find.text('15'));
    await tester.tap(find.text('OK'));
    await tester.pump();
    await tester.tap(find.byIcon(Icons.access_time));
    await tester.pump();
    await tester.tap(find.text('OK'));
    await tester.pump();

    ElevatedButton btn = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn.enabled, true);
    await tester.tap(find.text("Simpan"));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.text("Buat Booking"), findsOneWidget);
  });
}
