import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/booking_card.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/booking/models/Booking.dart';
import 'package:simpk/modules/booking/screens/booking_tab.dart';
import 'package:simpk/modules/booking/services/booking_api_provider.dart';
import 'package:simpk/modules/project/models/Project.dart';

import 'booking_tab_test.mocks.dart';

Future pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

Project _dummyProject = Project(
  id: 1,
  name: 'Dummy Project Name',
  status: 'Dummy Project Status',
  partner: 'Dummy Partner Name',
  activityType: 'Dummy Activity Type',
  region: 'Dummy Region',
  coordinate: 'Dummy Coordinate',
  description: "Dummy Description",
  progress: 0.5,
  allocation: 100000000,
);

Booking _dummyBooking = Booking(
    id: 1,
    activityName: 'Dummy Event Name',
    date: '2020-01-01',
    time: '09:00:00',
    description: 'Dummy Deskripsi',
    creatorName: 'username');

@GenerateMocks([BookingAPIProvider])
void main() {
  late MockBookingAPIProvider apiProvider;

  setUp(() {
    apiProvider = MockBookingAPIProvider();
    GetIt.I.registerSingleton<BookingAPIProvider>(apiProvider);
    when(apiProvider.getAllBooking(1)).thenAnswer((_) async => [_dummyBooking]);
  });

  tearDown(() {
    GetIt.I.unregister(instance: apiProvider);
  });

  testWidgets('Booking tab: content test', (WidgetTester tester) async {
    await pumpTestableWidget(tester, BookingTab(project: _dummyProject));
    await tester.pump();

    expect(find.text('Buat Booking Kunjungan'), findsOneWidget);
    expect(find.textContaining('atau lihat booking kunjungan'), findsOneWidget);
    expect(find.byIcon(Icons.add_circle_outline_rounded), findsOneWidget);
    expect(find.text('Daftar Booking'), findsOneWidget);
  });

  testWidgets('Booking tab: button redirect to buat booking',
      (WidgetTester tester) async {
    await pumpTestableWidget(tester, BookingTab(project: _dummyProject));
    await tester.pump();

    await tester.tap(find.text("Buat Booking"));
    await tester.pumpAndSettle();
    expect(find.text("Nama Kegiatan"), findsOneWidget);
    expect(find.text("Tanggal"), findsOneWidget);
    expect(find.text("Waktu"), findsOneWidget);
    expect(find.text("Keterangan"), findsOneWidget);
  });

  testWidgets("Booking tab: get all booking list", (WidgetTester tester) async {
    BookingTab bookingTab = BookingTab(project: _dummyProject);
    String eventName = 'Dummy Event Name';
    int id = 1;
    String time = '09:00:00';
    String date = '2020-01-01';
    String desc = 'Dummy Deskripsi';
    String creatorName = "username";

    when(apiProvider.getAllBooking(_dummyProject.id)).thenAnswer((_) async => [
          Booking(
              id: id,
              activityName: eventName,
              date: date,
              time: time,
              description: desc,
              creatorName: creatorName)
        ]);

    await pumpTestableWidget(tester, bookingTab);
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byType(Card), findsWidgets);
    expect(find.byType(BookingCard), findsWidgets);
  });

  testWidgets("Booking tab: card on click redirect to booking detail",
      (WidgetTester tester) async {
    BookingTab bookingTab = BookingTab(project: _dummyProject);
    String eventName = 'Dummy Event Name';
    int id = 1;
    String time = '09:00:00';
    String date = '2020-01-01';
    String desc = 'Dummy Deskripsi';
    String creatorName = "username";

    when(apiProvider.getAllBooking(_dummyProject.id)).thenAnswer((_) async => [
          Booking(
              id: id,
              activityName: eventName,
              date: date,
              time: time,
              description: desc,
              creatorName: creatorName)
        ]);

    await pumpTestableWidget(tester, bookingTab);
    await tester.pump(Duration(milliseconds: 1000));

    await tester.tap(find.byType(BookingCard).first);
    await tester.pumpAndSettle();
    expect(find.byIcon(Icons.delete), findsOneWidget);
  });

  testWidgets("Booking tab: card on click redirect to booking detail",
      (WidgetTester tester) async {
    BookingTab bookingTab = BookingTab(project: _dummyProject);

    when(apiProvider.getAllBooking(_dummyProject.id))
        .thenAnswer((_) async => []);

    await pumpTestableWidget(tester, bookingTab);
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byType(Card), findsNothing);
    expect(find.byType(BookingCard), findsNothing);
  });
}
