import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/labeled_checkbox.dart';

import 'package:simpk/modules/booking/screens/filter_booking_page.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget filterBookingPage) {
    return MaterialApp(
      home: filterBookingPage,
    );
  }

  testWidgets('Filter Booking Page content test', (WidgetTester tester) async {
    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: FilterBookingPage())));
    await tester.pump();

    //Verify page content
    expect(find.textContaining('Staff Ditugaskan'), findsOneWidget);
    expect(find.textContaining('Tanggal'), findsOneWidget);
    expect(find.textContaining('Terapkan Filter'), findsOneWidget);
    expect(find.textContaining('Saya'), findsOneWidget);
    expect(find.textContaining('Hari ini'), findsOneWidget);
  });

  testWidgets("Filter booking page contains apply filter button",
      (WidgetTester tester) async {
    Widget filterBookingPage = FilterBookingPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterBookingPage));

    expect(find.byType(ElevatedButton), findsOneWidget);
    expect(find.text("Terapkan Filter"), findsOneWidget);
    await tester.ensureVisible(find.byType(ElevatedButton));
    await tester.tap(find.byType(ElevatedButton));
  });

  testWidgets("Filter booking page contains labeled checkbox",
      (WidgetTester tester) async {
    Widget filterBookingPage = FilterBookingPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterBookingPage));

    expect(find.byType(LabeledCheckbox), findsWidgets);
  });

  testWidgets("Labeled checkbox is clickable", (WidgetTester tester) async {
    Widget filterBookingPage = FilterBookingPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterBookingPage));

    await tester.tap(find.text("Hari ini"));
    await tester.pump();
    expect(find.byType(LabeledCheckbox), findsWidgets);
  });

  testWidgets("Radio button is clickable", (WidgetTester tester) async {
    Widget filterBookingPage = FilterBookingPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterBookingPage));

    await tester.tap(find.text("Saya"));
    await tester.pump();
  });
}
