import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/booking/services/booking_api_provider.dart';

List<dynamic> bookingData = [
  {
    "id": 1,
    "nama_kegiatan": "dummy booking",
    "waktu_booking": "09:00:00",
    "tanggal_booking": "'2020-01-01'",
    "deskripsi": "Dummy Deskripsi",
    "proyek": 1,
    "user": "username"
  }
];

void main() {
  SharedPreferences.setMockInitialValues({'token': ''});

  test('Test success to get all booking', () async {
    final apiProvider = BookingAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/booking-app/by-proyek/1') {
          return Response(jsonEncode(bookingData), 200);
        }
        return Response('', 404);
      }),
    );

    final result = await apiProvider.getAllBooking(1);

    expect(result[0].activityName, bookingData[0]['nama_kegiatan']);
    expect(result[0].time, bookingData[0]['waktu_booking']);
    expect(result[0].date, bookingData[0]['tanggal_booking']);
    expect(result[0].description, bookingData[0]['deskripsi']);
  });

  test('Test success to delete booking', () async {
    int creatorId = 1;
    int projectId = 1;

    final apiProvider = BookingAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == 'booking-app/by-proyek/$projectId/booking/1') {
          return Response("No Content", 204);
        }
        return Response('', 404);
      }),
    );

    final result = await apiProvider.deleteBooking(creatorId, projectId);
    expect(result, true);
  });
}
