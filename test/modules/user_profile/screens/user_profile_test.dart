import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/authentication/screens/login.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/user_profile/models/user.dart';
import 'package:simpk/modules/user_profile/screens/user_profile.dart';
import 'package:simpk/services/api_provider.dart';

import '../../monitoring/screens/monitoring_tab_test.mocks.dart';
import 'user_profile_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: Scaffold(body: child),
        theme: theme,
      ),
    ),
  );
}

void _expectTextField({required String label}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      findsOneWidget);
}

@GenerateMocks([APIProvider, HomeAPIProvider])
void main() {
  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      // "total": 10,
      "selesai": 5,
      "berjalan": 3,
      // "belum_mulai": 2,
    }
  };

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  late MockHomeAPIProvider mockHomeAPIProvider;
  late ReportService mockReportRepository;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets(
      "Profile page : cloud icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    Widget projectPage = UserProfile();
    await _pumpTestableWidget(tester, projectPage);

    await tester.tap(find.byIcon(Icons.cloud_upload));
    await tester.pumpAndSettle();

    expect(find.text("Daftar Laporan Offline"), findsOneWidget);
  });

  testWidgets(
      "Profile page : profile icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    Widget projectPage = UserProfile();
    await _pumpTestableWidget(tester, projectPage);

    await tester.tap(find.byIcon(Icons.account_circle).at(0));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byIcon(Icons.account_circle), findsWidgets);
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsOneWidget);
  });

  testWidgets("User profile: static texts", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);
    expect(find.byType(CircularProgressIndicator), findsNWidgets(2));
    await tester.pumpAndSettle();

    expect(find.text('Nama'), findsOneWidget);
    expect(find.text('Email'), findsOneWidget);
    expect(find.text('No Handphone'), findsOneWidget);
    expect(find.text('Peran'), findsOneWidget);

    expect(find.text('Ringkasan Monitoring & Evaluasi'), findsOneWidget);
    // expect(find.text('Total Proyek'), findsOneWidget);
    expect(find.text('Proyek Selesai'), findsOneWidget);
    expect(find.text('Proyek Berjalan'), findsOneWidget);

    expect(find.text('Edit Profil'), findsOneWidget);
    expect(find.text('Keluar'), findsOneWidget);
  });

  testWidgets("User profile: dynamic texts", (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text(userName), findsOneWidget);
    expect(find.text(userEmail), findsOneWidget);
    expect(find.text(userHandphoneNum.toString()), findsOneWidget);
    expect(find.text(userRole), findsOneWidget);

    // expect(find.text('Total Proyek'), findsOneWidget);
    // expect(find.text('10'), findsOneWidget);
    expect(find.text('Proyek Selesai'), findsOneWidget);
    expect(find.text('5'), findsOneWidget);
    expect(find.text('Proyek Berjalan'), findsOneWidget);
    expect(find.text('3'), findsOneWidget);
    // expect(find.text('Proyek Belum Mulai'), findsOneWidget);
    // expect(find.text('2'), findsOneWidget);
  });

  testWidgets('User profile: tap on edit profil', (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Edit Profil'));
    await tester.tap(find.text('Edit Profil'));
    await tester.pumpAndSettle();

    expect(find.text('Ringkasan Monitoring & Evaluasi'), findsNothing);
    expect(find.text('Total Proyek'), findsNothing);
    expect(find.text('Proyek Selesai'), findsNothing);
    expect(find.text('Proyek Berjalan'), findsNothing);

    expect(find.text('Edit Profil'), findsNothing);
    expect(find.text('Keluar'), findsNothing);
  });

  testWidgets('User profile: tap on keluar', (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    expect(find.text("Keluar"), findsOneWidget);

    await tester.ensureVisible(find.text('Keluar'));
    await tester.tap(find.text('Keluar'));
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsOneWidget);
    expect(find.text("Keluar"), findsWidgets);
    expect(find.text("Anda yakin ingin keluar dari akun ini?"), findsOneWidget);
    expect(find.text("Kembali"), findsOneWidget);
    expect(find.text("Keluar"), findsWidgets);
  });

  testWidgets('User profile: tap on keluar then tap keluar',
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Keluar'));
    await tester.tap(find.text('Keluar'));
    await tester.pump();

    expect(sharedPreferences.getString("token") != null, true);

    await tester.ensureVisible(find.text('Keluar').last);
    await tester.tap(find.text('Keluar').last);
    await tester.pumpAndSettle();

    expect(find.byType(LoginScreen), findsOneWidget);
    expect(sharedPreferences.getString("token"), null);
  });

  testWidgets('User profile: tap on keluar then tap kembali',
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Keluar'));
    await tester.tap(find.text('Keluar'));
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsOneWidget);

    await tester.ensureVisible(find.text('Kembali'));
    await tester.tap(find.text('Kembali'));
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets('Edit profile: static texts', (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Edit Profil'));
    await tester.tap(find.text('Edit Profil'));
    await tester.pumpAndSettle();

    _expectTextField(label: 'Nama');
    expect(find.text(dummyUserProfile.state.user.name), findsOneWidget);

    _expectTextField(label: 'Email');
    expect(find.text(dummyUserProfile.state.user.email), findsOneWidget);

    _expectTextField(label: 'No Handphone');
    expect(find.text(dummyUserProfile.state.user.handphone_number.toString()),
        findsOneWidget);
  });

  testWidgets('Edit Profile: successfully change user details',
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Edit Profil'));
    await tester.tap(find.text('Edit Profil'));
    await tester.pump();

    String newUserName = "New dummy name";
    String newUserHpNum = "0812345678";

    await tester.enterText(find.byType(TextFormField).at(1), newUserName);
    await tester.pump(Duration(milliseconds: 100));

    await tester.enterText(find.byType(TextFormField).at(2), newUserHpNum);
    await tester.pump(Duration(milliseconds: 100));

    when(mockAPIProvider.putUserDetails(
      token,
      userEmail,
      newUserName,
      newUserHpNum,
    )).thenAnswer((_) async => true);

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: newUserName,
          email: userEmail,
          role: userRole,
          handphone_number: newUserHpNum,
        ));

    await tester.ensureVisible(find.text('Simpan'));
    await tester.tap(find.text("Simpan"));
    await tester.pump();

    expect(find.byType(SnackBar), findsOneWidget);
    expect(find.text("Data berhasil tersimpan!"), findsOneWidget);
    await tester.pump();

    expect(find.text(newUserName), findsOneWidget);
    expect(find.text(newUserHpNum), findsOneWidget);
  });

  testWidgets('Edit Profile: api call failure to change user details',
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Edit Profil'));
    await tester.tap(find.text('Edit Profil'));
    await tester.pump();

    String newUserName = "New dummy name";
    String newUserHpNum = "0812345678";

    await tester.enterText(find.byType(TextFormField).at(1), newUserName);
    await tester.pump(Duration(milliseconds: 100));

    await tester.enterText(find.byType(TextFormField).at(2), newUserHpNum);
    await tester.pump(Duration(milliseconds: 100));

    when(mockAPIProvider.putUserDetails(
      token,
      userEmail,
      newUserName,
      newUserHpNum,
    )).thenAnswer((_) async => false);

    await tester.ensureVisible(find.text('Simpan'));
    await tester.tap(find.text("Simpan"));
    await tester.pump();

    expect(find.byType(SnackBar), findsOneWidget);
    expect(find.text("Data gagal tersimpan! Mohon coba lagi!"), findsOneWidget);
  });

  testWidgets('Edit Profile: successfully change password',
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Ubah Password'));
    await tester.tap(find.text('Ubah Password'));
    await tester.pump();

    String oldPass = "oldpass123";
    String newPass = "newpass123";

    await tester.enterText(find.byType(TextFormField).at(0), oldPass);
    await tester.pump(Duration(milliseconds: 100));

    await tester.enterText(find.byType(TextFormField).at(1), newPass);
    await tester.pump(Duration(milliseconds: 100));

    await tester.enterText(find.byType(TextFormField).at(2), newPass);
    await tester.pump(Duration(milliseconds: 100));

    String successMessage = 'Password changed.';
    when(mockAPIProvider.changePassword(
      token,
      oldPass,
      newPass,
      newPass,
    )).thenAnswer(
        (_) async => {'password_changed': true, 'message': successMessage});

    await tester.ensureVisible(find.text('Simpan'));
    await tester.tap(find.text("Simpan"));
    await tester.pump();

    expect(find.byType(SnackBar), findsOneWidget);
    expect(find.text(successMessage), findsOneWidget);
  });

  testWidgets('Edit Profile: tap on batal', (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Edit Profil'));
    await tester.tap(find.text('Edit Profil'));
    await tester.pump();

    await tester.ensureVisible(find.text('Batal'));
    await tester.tap(find.text('Batal'));
    await tester.pump();
  });

  testWidgets(
      'Edit Profile: if form validator returns false then expect error text',
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    UserProfile dummyUserProfile = UserProfile();
    String userName = 'Dummy User Name';
    String userEmail = 'Dummy User Email';
    String userHandphoneNum = "0987654321";
    String userRole = 'Dummy User Role';
    String token = sharedPreferences.getString("token")!;
    dummyUserProfile.state.api = mockAPIProvider;

    when(mockAPIProvider.getUserDetails(token)).thenAnswer((_) async => User(
          name: userName,
          email: userEmail,
          role: userRole,
          handphone_number: userHandphoneNum,
        ));
    await _pumpTestableWidget(tester, dummyUserProfile);

    await tester.ensureVisible(find.text('Edit Profil'));
    await tester.tap(find.text('Edit Profil'));
    await tester.pump();

    String newUserName = "";
    await tester.enterText(find.byType(TextFormField).at(1), newUserName);
    await tester.pump();
    expect(find.text('Nama tidak boleh kosong.'), findsOneWidget);

    String newUserHPNum = "";
    await tester.enterText(find.byType(TextFormField).at(2), newUserHPNum);
    await tester.pump();
    expect(find.text('No Handphone tidak boleh kosong.'), findsOneWidget);

    String newMoreThan14DigitUserHPNum = "1231231232132113221312321312332";
    await tester.enterText(
        find.byType(TextFormField).at(2), newMoreThan14DigitUserHPNum);
    await tester.pump();
    expect(find.text('Maksimal no handphone 14 digit.'), findsOneWidget);

    await tester.ensureVisible(find.text('Simpan'));
    await tester.tap(find.text('Simpan'));
    await tester.pump();

    expect(find.byType(SnackBar), findsOneWidget);
    expect(find.text("Harap mengisi dengan data yang sesuai!"), findsOneWidget);
  });
}
