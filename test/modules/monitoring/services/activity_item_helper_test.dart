import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/services/activity_item_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'activity_item_helper_test.mocks.dart';

final onCreateSQL = '''
    CREATE TABLE activity_item (
    _id INTEGER PRIMARY KEY,
    report_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    approved TEXT NOT NULL,
    realization TEXT NOT NULL,
    difference INTEGER NOT NULL,
    difference_desc TEXT,
    FOREIGN KEY (report_id)
      REFERENCES report (_id));
    '''
    .trim()
    .replaceAll(RegExp(r"\s+"), " ");

@GenerateMocks([DatabaseProvider])
void main() {
  late Database db;
  late DatabaseProvider mockDbProvider;

  sqfliteFfiInit();

  setUp(() async {
    db = await databaseFactoryFfi.openDatabase(inMemoryDatabasePath);
    await db.execute(onCreateSQL);
    mockDbProvider = MockDatabaseProvider();
    when(mockDbProvider.database).thenAnswer((_) async => db);
  });

  tearDown(() async {
    await db.close();
  });

  test('onCreateSQL is correct', () {
    expect(
      ActivityItemHelper.onCreateSQL.trim().replaceAll(RegExp(r"\s+"), " "),
      onCreateSQL,
    );
  });

  test('insert', () async {
    final activityItemHelper = ActivityItemHelper(dbProvider: mockDbProvider);

    final activityItem = ActivityItem(
      name: 'A',
      reportId: 1,
      approved: 'haha',
      realization: 'haha',
      difference: false,
    );
    final resultId = await activityItemHelper.insert(activityItem);

    verify(mockDbProvider.database);
    final query = await db.query('activity_item');
    expect(query.length, 1);
    expect(query[0], {
      '_id': resultId,
      'report_id': 1,
      'name': 'A',
      'approved': 'haha',
      'realization': 'haha',
      'difference': 0,
      'difference_desc': null,
    });
  });

  test('queryAll', () async {
    final activityItem1 = {
      '_id': 1,
      'report_id': 1,
      'name': 'A',
      'approved': 'he',
      'realization': 'he',
      'difference': 1,
      'difference_desc': 'a',
    };
    final activityItem2 = {
      '_id': 2,
      'report_id': 1,
      'name': 'A',
      'approved': 'he',
      'realization': 'he',
      'difference': 0,
      'difference_desc': null,
    };
    db.insert('activity_item', activityItem1);
    db.insert('activity_item', activityItem2);

    final activityItemHelper = ActivityItemHelper(dbProvider: mockDbProvider);

    List<ActivityItem> activityItems = await activityItemHelper.queryAll();

    verify(mockDbProvider.database);
    expect(activityItems.length, 2);
  });

  test('queryByReportId', () async {
    final activityItem1 = {
      '_id': 1,
      'report_id': 1,
      'name': 'A',
      'approved': 'he',
      'realization': 'he',
      'difference': 1,
      'difference_desc': 'a',
    };
    final activityItem2 = {
      '_id': 2,
      'report_id': 2,
      'name': 'A',
      'approved': 'he',
      'realization': 'he',
      'difference': 0,
      'difference_desc': null,
    };
    db.insert('activity_item', activityItem1);
    db.insert('activity_item', activityItem2);

    final activityItemHelper = ActivityItemHelper(dbProvider: mockDbProvider);

    List<ActivityItem> activityItems =
        await activityItemHelper.queryByReportId(1);

    verify(mockDbProvider.database);
    expect(activityItems.length, 1);
  });
}
