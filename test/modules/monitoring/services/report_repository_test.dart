import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/modules/monitoring/services/report_offline_helper.dart';
import 'package:simpk/modules/monitoring/services/report_repository.dart';
import 'package:simpk/services/network_info.dart';

import 'report_repository_test.mocks.dart';

@GenerateMocks([
  ReportHelper,
  MonitoringAPIProvider,
  NetworkInfo,
  ReportOfflineHelper,
])
void main() {
  late ReportRepository reportRepository;
  late MockReportHelper mockReportHelper;
  late MockMonitoringAPIProvider mockApiProvider;
  late MockNetworkInfo mockNetworkInfo;
  late MockReportOfflineHelper mockReportOfflineHelper;
  int uniqueId = 1;

  Report generateReport() {
    return Report()
      ..id = uniqueId++
      ..projectId = 1
      ..activityType = 'Sarana dan Prasarana'
      ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
      ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
      ..monitoringDate = DateTime(2021, 10, 24)
      ..monitoringPeriod = 'Periode 1'
      ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
      ..conclusion =
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya'
      ..modifiedDate = DateTime(2021, 4, 5);
  }

  final _dummyReports = [
    generateReport()..monitoringPeriod = 'Periode 1',
    generateReport()..monitoringPeriod = 'Periode 1',
    generateReport()..monitoringPeriod = 'Periode 2',
  ];

  void _stubQueryAll() {
    when(mockReportHelper.insert(_dummyReports[0]))
        .thenAnswer((_) async => _dummyReports[0]);
    when(mockReportHelper.insert(_dummyReports[1]))
        .thenAnswer((_) async => _dummyReports[1]);
    when(mockReportHelper.insert(_dummyReports[2]))
        .thenAnswer((_) async => _dummyReports[2]);
    when(mockApiProvider.getReportsByProjectId(1))
        .thenAnswer((_) async => _dummyReports);
    when(mockReportHelper.queryByProjectId(1))
        .thenAnswer((_) async => _dummyReports);
    when(mockReportHelper.deleteByNotInListOfId([1, 2, 3]))
        .thenAnswer((_) async => 0);
  }

  setUp(() {
    mockReportHelper = MockReportHelper();
    mockApiProvider = MockMonitoringAPIProvider();
    mockNetworkInfo = MockNetworkInfo();
    mockReportOfflineHelper = MockReportOfflineHelper();
    uniqueId = 1;

    reportRepository = ReportRepository(
      localDataSource: mockReportHelper,
      remoteDataSource: mockApiProvider,
      networkInfo: mockNetworkInfo,
      reportOfflineHelper: mockReportOfflineHelper,
    );
  });

  test('insert when connected', () async {
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    final _dummyReport = Report();
    when(mockApiProvider.insertReport(_dummyReport))
        .thenAnswer((_) async => _dummyReport..id = 1);
    when(mockReportHelper.insert(_dummyReport))
        .thenAnswer((_) async => _dummyReport);

    final result = await reportRepository.insert(_dummyReport);

    verify(mockApiProvider.insertReport(_dummyReport)).called(1);
    verify(mockReportHelper.insert(_dummyReport)).called(1);
    expect(result.id, 1);
  });

  test('insert when not connected', () async {
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => false);
    final _dummyReport = Report();
    when(mockReportOfflineHelper.insert(_dummyReport))
        .thenAnswer((_) async => _dummyReport);

    final result = await reportRepository.insert(_dummyReport);

    verify(mockReportOfflineHelper.insert(_dummyReport)).called(1);
    expect(result, _dummyReport);
  });

  test('queryByProjectId when connected', () async {
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    _stubQueryAll();

    List<Report> result = await reportRepository.queryByProjectId(1);

    verify(mockApiProvider.getReportsByProjectId(1)).called(1);
    verify(mockReportHelper.insert(_dummyReports[0])).called(1);
    verify(mockReportHelper.insert(_dummyReports[1])).called(1);
    verify(mockReportHelper.insert(_dummyReports[2])).called(1);
    verify(mockReportHelper.queryByProjectId(1)).called(1);
    expect(result, _dummyReports);
  });

  test('queryByProjectId when not connected', () async {
    final _dummyReports = [Report()];
    when(mockReportHelper.queryByProjectId(1))
        .thenAnswer((_) async => _dummyReports);
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => false);

    List<Report> result = await reportRepository.queryByProjectId(1);

    verify(mockReportHelper.queryByProjectId(1)).called(1);
    expect(result, _dummyReports);
  });

  test('queryById when connected', () async {
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    final _dummyReport = Report();
    when(mockApiProvider.getReportById(1))
        .thenAnswer((_) async => _dummyReport);
    when(mockReportHelper.insert(_dummyReport))
        .thenAnswer((_) async => _dummyReport);
    when(mockReportHelper.queryById(1)).thenAnswer((_) async => _dummyReport);

    Report report = await reportRepository.queryById(1);

    verifyInOrder([
      mockApiProvider.getReportById(1),
      mockReportHelper.insert(_dummyReport),
      mockReportHelper.queryById(1),
    ]);

    expect(report, _dummyReport);
  });

  test('queryById when not connected', () async {
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => false);
    final _dummyReport = Report();
    when(mockReportHelper.queryById(1)).thenAnswer((_) async => _dummyReport);

    Report report = await reportRepository.queryById(1);

    verify(mockReportHelper.queryById(1)).called(1);
    expect(report, _dummyReport);
  });

  test('queryByProjectIdAndMonitoringPeriod when connected', () async {
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    _stubQueryAll();
    when(mockReportHelper.queryByProjectIdAndMonitoringPeriod(1, "Periode 1"))
        .thenAnswer((_) async => _dummyReports.sublist(0, 2));

    List<Report> result = await reportRepository
        .queryByProjectIdAndMonitoringPeriod(1, 'Periode 1');

    verify(mockApiProvider.getReportsByProjectId(1)).called(1);
    verify(mockReportHelper.queryByProjectIdAndMonitoringPeriod(1, "Periode 1"))
        .called(1);

    expect(result, [_dummyReports[0], _dummyReports[1]]);
  });

  test('queryByProjectIdAndMonitoringPeriod when not connected', () async {
    final _dummyReports = [Report()];
    when(mockReportHelper.queryByProjectIdAndMonitoringPeriod(1, "Periode 1"))
        .thenAnswer((_) async => _dummyReports);
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => false);

    List<Report> result = await reportRepository
        .queryByProjectIdAndMonitoringPeriod(1, "Periode 1");

    verify(mockReportHelper.queryByProjectIdAndMonitoringPeriod(1, "Periode 1"))
        .called(1);
    expect(result, _dummyReports);
  });

  test('queryMonitoringPeriodByProjectId when connected', () async {
    final _dummyPeriods = ["Periode 1", "Periode 2"];

    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    _stubQueryAll();
    when(mockReportHelper.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => _dummyPeriods);

    List<String> periods =
        await reportRepository.queryMonitoringPeriodByProjectId(1);

    verify(mockReportHelper.queryMonitoringPeriodByProjectId(1)).called(1);
    expect(periods, _dummyPeriods);
  });

  test('queryMonitoringPeriodByProjectId when not connected', () async {
    final _dummyPeriods = ["Periode 1", "Periode 2"];

    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => false);
    when(mockReportHelper.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => _dummyPeriods);

    List<String> periods =
        await reportRepository.queryMonitoringPeriodByProjectId(1);

    verify(mockReportHelper.queryMonitoringPeriodByProjectId(1)).called(1);
    expect(periods, _dummyPeriods);
  });
}
