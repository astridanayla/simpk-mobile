import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart' as dio;
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/response_mapper.dart';

import 'monitoring_api_provider_test.mocks.dart';

@GenerateMocks([dio.Dio])
void main() {
  SharedPreferences.setMockInitialValues({'token': ''});

  test("getReports", () async {
    final dummyReport = Report()
      ..projectId = 1
      ..activityType = 'Sarana dan Prasarana'
      ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
      ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
      ..monitoringDate = DateTime(2021, 10, 24)
      ..monitoringPeriod = 'Periode 1'
      ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
      ..conclusion =
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya'
      ..modifiedDate = DateTime(2021, 4, 5)
      ..authorName = 'BPKH';

    final reportMap = ResponseMapper.reportToJson(dummyReport);
    final apiProvider = MonitoringAPIProvider(
      client: MockClient((request) async {
        final mapJson = [reportMap, reportMap];
        return Response(json.encode(mapJson), 400);
      }),
      dio: MockDio(),
    );

    final response = await apiProvider.getReportsByProjectId(1);

    expect(response.length, 2);
  });

  test("insertReport", () async {
    final _dummyActivityItem = ActivityItem(
      name: 'Merk',
      approved: 'Honda',
      realization: 'Yamaha',
      difference: true,
      differenceDescription:
          'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
    );
    final _dummyRealizationItem = RealizationItem(
      name: 'Merk',
      approved: 1000000,
      realization: 1000000,
      difference: false,
    );
    final _dummyImages =
        ReportImage(image: File("test/modules/monitoring/services/hello.jpg"));
    final _dummyActivityAnalysis = ActivityAnalisis()
      ..problem = 'Nama Masalah'
      ..recommendation = 'Penyelesaian Masalah';
    final dummyReport = Report()
      ..activityType = 'Sarana dan Prasarana'
      ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
      ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
      ..monitoringDate = DateTime(2021, 10, 24)
      ..monitoringPeriod = 'Periode 1'
      ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
      ..activityItems = [_dummyActivityItem, _dummyActivityItem]
      ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
      ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
      ..reportImages = [_dummyImages]
      ..conclusion =
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya'
      ..modifiedDate = DateTime(2021, 4, 5)
      ..authorName = 'BPKH';

    ResponseMapper.reportToJson(dummyReport)['id'] = 1;

    final mockDio = MockDio();
    when(mockDio.options).thenReturn(dio.BaseOptions());
    when(mockDio.post(
      any,
      data: anyNamed('data'),
      options: anyNamed('options'),
    )).thenAnswer((_) async => dio.Response<Map<String, dynamic>>(data: {
          'file': '/media/hello.jpg',
        }, requestOptions: dio.RequestOptions(path: '')));

    final apiProvider = MonitoringAPIProvider(
      client: MockClient((request) async {
        switch (request.url.path) {
          case '/monitoring/laporan':
            return Response(
                json.encode(ResponseMapper.reportToJson(dummyReport..id = 1)),
                400);
          case '/monitoring/laporan/1/item':
            return Response(
                json.encode(
                    ResponseMapper.activityItemToJson(_dummyActivityItem)),
                400);
          case '/monitoring/laporan/1/realisasi-item':
            return Response(
                json.encode(ResponseMapper.realizationItemToJson(
                    _dummyRealizationItem)),
                400);
          case '/monitoring/laporan/1/poin-analisa':
            return Response(
                json.encode(ResponseMapper.activityAnalysisToJson(
                    _dummyActivityAnalysis)),
                400);
          default:
            return Response('', 404);
        }
      }),
      dio: mockDio,
    );

    final response = await apiProvider.insertReport(dummyReport);

    expect(response.id, 1);
  });

  test("getReportById", () async {
    final _dummyActivityItem = ActivityItem(
      name: 'Merk',
      approved: 'Honda',
      realization: 'Yamaha',
      difference: true,
      differenceDescription:
          'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
    );
    final _dummyRealizationItem = RealizationItem(
      name: 'Merk',
      approved: 1000000,
      realization: 1000000,
      difference: false,
    );
    final _dummyImages = ReportImage(image: File("hello.jpg"));
    final _dummyActivityAnalysis = ActivityAnalisis()
      ..problem = 'Nama Masalah'
      ..recommendation = 'Penyelesaian Masalah';
    final dummyReport = Report()
      ..activityType = 'Sarana dan Prasarana'
      ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
      ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
      ..monitoringDate = DateTime(2021, 10, 24)
      ..monitoringPeriod = 'Periode 1'
      ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
      ..activityItems = [_dummyActivityItem, _dummyActivityItem]
      ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
      ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
      ..reportImages = [_dummyImages]
      ..conclusion =
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya'
      ..modifiedDate = DateTime(2021, 4, 5)
      ..authorName = 'BPKH';

    final apiProvider = MonitoringAPIProvider(
      client: MockClient((request) async {
        switch (request.url.path) {
          case '/monitoring/laporan/1':
            return Response(
                json.encode([ResponseMapper.reportToJson(dummyReport)]), 200);
          case '/monitoring/laporan/1/item':
            return Response(
                json.encode(
                    [ResponseMapper.activityItemToJson(_dummyActivityItem)]),
                200);
          case '/monitoring/laporan/1/realisasi-item':
            return Response(
                json.encode([
                  ResponseMapper.realizationItemToJson(_dummyRealizationItem)
                ]),
                200);
          case '/monitoring/laporan/1/poin-analisa':
            return Response(
                json.encode([
                  ResponseMapper.activityAnalysisToJson(_dummyActivityAnalysis)
                ]),
                200);
          // TODO bukti laporan test mock
          // case '/monitoring/laporan/1/bukti-laporan':
          //   return Response(
          //       json.encode([
          //       ResponseMapper.activityAnalysisToJson(_dummyActivityAnalysis)
          //       ]),
          //       200);
          default:
            return Response('', 404);
        }
      }),
      dio: MockDio(),
    );

    final response = await apiProvider.getReportById(1);

    expect(response.activityName, 'Pemeriksaan Rutin Pembangunan Masjid');
    expect(response.activityItems?[0].name, 'Merk');
    expect(response.realizationItems?[0].name, 'Merk');
    expect(response.activityAnalysis?[0].problem, 'Nama Masalah');
    // TODO bukti laporan test
    // expect(response.reportImages?[0].image.path.toString(), 'hello.jpg');
  });

  test("getActivityItems", () async {
    final _dummyActivityItem = ActivityItem(
      name: 'Merk',
      approved: 'Honda',
      realization: 'Yamaha',
      difference: true,
      differenceDescription:
          'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
    );

    final mapping = ResponseMapper.activityItemToJson(_dummyActivityItem);
    final apiProvider = MonitoringAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/monitoring/laporan/1/item') {
          final mapList = [mapping, mapping];
          return Response(json.encode(mapList), 400);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final response = await apiProvider.getActivityItems(1);

    expect(response.length, 2);
  });

  test("getRealizationItems", () async {
    final _dummyRealizationItem = RealizationItem(
      name: 'Merk',
      approved: 1000000,
      realization: 1000000,
      difference: false,
    );

    final mapping = ResponseMapper.realizationItemToJson(_dummyRealizationItem);
    final apiProvider = MonitoringAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/monitoring/laporan/1/realisasi-item') {
          final mapList = [mapping, mapping];
          return Response(json.encode(mapList), 400);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final response = await apiProvider.getRealizationItems(1);

    expect(response.length, 2);
  });

  test("getActivityAnalysis", () async {
    final _dummyActivityAnalysis = ActivityAnalisis()
      ..problem = 'Nama Masalah'
      ..recommendation = 'Penyelesaian Masalah';

    final mapping =
        ResponseMapper.activityAnalysisToJson(_dummyActivityAnalysis);
    final apiProvider = MonitoringAPIProvider(
      client: MockClient((request) async {
        if (request.url.path == '/monitoring/laporan/1/poin-analisa') {
          final mapList = [mapping, mapping];
          return Response(json.encode(mapList), 400);
        }
        return Response('', 404);
      }),
      dio: MockDio(),
    );

    final response = await apiProvider.getActivityAnalysis(1);

    expect(response.length, 2);
  });
}
