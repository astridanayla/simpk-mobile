import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/services/activity_analysis_helper.dart';
import 'package:simpk/modules/monitoring/services/activity_item_helper.dart';
import 'package:simpk/modules/monitoring/services/realization_item_helper.dart';
import 'package:simpk/modules/monitoring/services/report_helper.dart';
import 'package:simpk/modules/monitoring/services/report_image_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'report_helper_test.mocks.dart';

final onCreateSQL = '''
    CREATE TABLE report (
    _id INTEGER PRIMARY KEY,
    project_id INTEGER NOT NULL,
    activity_type TEXT NOT NULL,
    activity_name TEXT NOT NULL,
    execution_method TEXT NOT NULL,
    monitoring_date INTEGER NOT NULL,
    monitoring_period TEXT NOT NULL,
    activity_stage TEXT NOT NULL,
    conclusion TEXT NOT NULL,
    modified_date INTEGER NOT NULL, 
    author_name TEXT)
    '''
    .trim()
    .replaceAll(RegExp(r"\s+"), " ");

@GenerateMocks([
  DatabaseProvider,
  ActivityItemHelper,
  RealizationItemHelper,
  ActivityAnalysisHelper,
  ReportImageHelper,
])
void main() {
  late Database db;
  late DatabaseProvider mockDbProvider;
  late ActivityItemHelper mockActivityItemHelper;
  late RealizationItemHelper mockRealizationItemHelper;
  late ActivityAnalysisHelper mockActivityAnalysisHelper;
  late ReportImageHelper mockReportImageHelper;
  late ReportHelper reportHelper;

  sqfliteFfiInit();

  setUp(() async {
    db = await databaseFactoryFfi.openDatabase(inMemoryDatabasePath);
    await db.execute(onCreateSQL);
    mockDbProvider = MockDatabaseProvider();
    mockActivityItemHelper = MockActivityItemHelper();
    mockRealizationItemHelper = MockRealizationItemHelper();
    mockActivityAnalysisHelper = MockActivityAnalysisHelper();
    mockReportImageHelper = MockReportImageHelper();
    when(mockDbProvider.database).thenAnswer((_) async => db);

    reportHelper = ReportHelper(
      dbProvider: mockDbProvider,
      activityItemHelper: mockActivityItemHelper,
      realizationItemHelper: mockRealizationItemHelper,
      activityAnalysisHelper: mockActivityAnalysisHelper,
      reportImageHelper: mockReportImageHelper,
    );
  });

  tearDown(() async {
    await db.close();
  });

  test('onCreateSQL is correct', () {
    expect(
      ReportHelper.onCreateSQL.trim().replaceAll(RegExp(r"\s+"), " "),
      onCreateSQL,
    );
  });

  test('insert', () async {
    final _dummyActivityItem = ActivityItem(
      name: 'Merk',
      approved: 'Honda',
      realization: 'Yamaha',
      difference: true,
      differenceDescription:
          'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
    );
    final _dummyRealizationItem = RealizationItem(
      name: 'Merk',
      approved: 1000000,
      realization: 1000000,
      difference: false,
    );
    final _dummyImages = ReportImage(image: File("hello.jpg"));
    final _dummyActivityAnalysis = ActivityAnalisis()
      ..problem = 'Nama Masalah'
      ..recommendation = 'Penyelesaian Masalah';
    final _dummyReport = Report()
      ..projectId = 1
      ..activityType = 'Sarana dan Prasarana'
      ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
      ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
      ..monitoringDate = DateTime(2021, 10, 24)
      ..monitoringPeriod = 'Periode 1'
      ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
      ..activityItems = [_dummyActivityItem, _dummyActivityItem]
      ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
      ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
      ..reportImages = [_dummyImages]
      ..conclusion =
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya'
      ..modifiedDate = DateTime(2021, 4, 5)
      ..authorName = 'BPKH';

    when(mockActivityItemHelper.insert(_dummyActivityItem))
        .thenAnswer((_) async => 1);
    when(mockRealizationItemHelper.insert(_dummyRealizationItem))
        .thenAnswer((_) async => 1);
    when(mockActivityAnalysisHelper.insert(_dummyActivityAnalysis))
        .thenAnswer((_) async => 1);
    when(mockReportImageHelper.insert(_dummyImages)).thenAnswer((_) async => 1);

    final result = await reportHelper.insert(_dummyReport);

    verify(mockDbProvider.database);
    verify(mockActivityItemHelper.insert(_dummyActivityItem)).called(2);
    verify(mockRealizationItemHelper.insert(_dummyRealizationItem)).called(2);
    verify(mockActivityAnalysisHelper.insert(_dummyActivityAnalysis)).called(2);
    verify(mockReportImageHelper.insert(_dummyImages)).called(1);

    final query = await db.query('report');
    expect(query.length, 1);
    expect(query[0], {
      '_id': result.id,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 1',
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'author_name': 'BPKH',
    });
  });

  test('queryAll', () async {
    final reportMap = {
      '_id': 1,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 1',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    await db.insert('report', reportMap);

    List<Report> reports = await reportHelper.queryByProjectId(1);

    verify(mockDbProvider.database);
    expect(reports.length, 1);
  });

  test('queryById', () async {
    final reportMap = {
      '_id': 1,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 1',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    await db.insert('report', reportMap);

    when(mockActivityItemHelper.queryByReportId(1)).thenAnswer((_) async => []);
    when(mockRealizationItemHelper.queryByReportId(1))
        .thenAnswer((_) async => []);
    when(mockActivityAnalysisHelper.queryByReportId(1))
        .thenAnswer((_) async => []);
    when(mockReportImageHelper.queryByReportId(1)).thenAnswer((_) async => []);

    Report report = await reportHelper.queryById(1);

    verify(mockDbProvider.database);
    expect(report.id, reportMap['_id']);
    expect(report.activityType, reportMap['activity_type']);
    expect(report.activityName, reportMap['activity_name']);
    expect(report.executionMethod, reportMap['execution_method']);
    expect(report.monitoringDate, DateTime(2021, 10, 24));
    expect(report.monitoringPeriod, reportMap['monitoring_period']);
    expect(report.modifiedDate, DateTime(2021, 4, 5));
    expect(report.activityStage, reportMap['activity_stage']);
    expect(report.conclusion, reportMap['conclusion']);
    expect(report.authorName, reportMap['author_name']);
  });

  test('queryByProjectIdAndMonitoringPeriod', () async {
    final reportMap = {
      '_id': 1,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 1',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    final reportMap2 = {
      '_id': 2,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 2',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    final reportMap3 = {
      '_id': 3,
      'project_id': 2,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 1',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    await db.insert('report', reportMap);
    await db.insert('report', reportMap2);
    await db.insert('report', reportMap3);

    List<Report> reports =
        await reportHelper.queryByProjectIdAndMonitoringPeriod(1, 'Periode 1');

    verify(mockDbProvider.database);
    expect(reports.length, 1);
  });

  test('queryMonitoringPeriodByProjectId', () async {
    final reportMap = {
      '_id': 1,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 1',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    final reportMap2 = {
      '_id': 2,
      'project_id': 1,
      'activity_type': 'Sarana dan Prasarana',
      'activity_name': 'Pemeriksaan Rutin Pembangunan Masjid',
      'execution_method': 'Cek Fisik oleh Bidang Kemaslahatan',
      'monitoring_date': DateTime(2021, 10, 24).millisecondsSinceEpoch,
      'monitoring_period': 'Periode 2',
      'modified_date': DateTime(2021, 4, 5).millisecondsSinceEpoch,
      'activity_stage': 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan',
      'conclusion':
          'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
              'dan dapat dilanjutkan ke tahap selanjutnya',
      'author_name': 'BPKH',
    };
    await db.insert('report', reportMap);
    await db.insert('report', reportMap2);

    List<String> periods =
        await reportHelper.queryMonitoringPeriodByProjectId(1);

    verify(mockDbProvider.database);
    expect(periods.length, 2);
    expect(periods[0], 'Periode 1');
    expect(periods[1], 'Periode 2');
  });
}
