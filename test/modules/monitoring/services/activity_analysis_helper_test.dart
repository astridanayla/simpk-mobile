import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/services/activity_analysis_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'activity_item_helper_test.mocks.dart';

final onCreateSQL = '''
    CREATE TABLE activity_analysis (
    _id INTEGER PRIMARY KEY,
    report_id INTEGER NOT NULL,
    problem TEXT NOT NULL,
    recommendation TEXT NOT NULL,
    FOREIGN KEY (report_id)
      REFERENCES report (_id));
    '''
    .trim()
    .replaceAll(RegExp(r"\s+"), " ");

void main() {
  late Database db;
  late DatabaseProvider mockDbProvider;

  sqfliteFfiInit();

  setUp(() async {
    db = await databaseFactoryFfi.openDatabase(inMemoryDatabasePath);
    await db.execute(onCreateSQL);
    mockDbProvider = MockDatabaseProvider();
    when(mockDbProvider.database).thenAnswer((_) async => db);
  });

  tearDown(() async {
    await db.close();
  });

  test('onCreateSQL is correct', () {
    expect(
      ActivityAnalysisHelper.onCreateSQL.trim().replaceAll(RegExp(r"\s+"), " "),
      onCreateSQL,
    );
  });

  test('insert', () async {
    final activityItemHelper =
        ActivityAnalysisHelper(dbProvider: mockDbProvider);

    final activityAnalysis = ActivityAnalisis()
      ..reportId = 1
      ..problem = "Beda Merk"
      ..recommendation = "Penyelesaian Masalah";
    final resultId = await activityItemHelper.insert(activityAnalysis);

    verify(mockDbProvider.database);
    final query = await db.query('activity_analysis');
    expect(query.length, 1);
    expect(query[0], {
      '_id': resultId,
      'report_id': 1,
      'problem': 'Beda Merk',
      'recommendation': 'Penyelesaian Masalah',
    });
  });

  test('queryAll', () async {
    final activityAnalysis = {
      '_id': 1,
      'report_id': 1,
      'problem': 'Beda Merk',
      'recommendation': 'Penyelesaian Masalah',
    };
    db.insert('activity_analysis', activityAnalysis);

    final activityItemHelper =
        ActivityAnalysisHelper(dbProvider: mockDbProvider);

    List<ActivityAnalisis> activityItems = await activityItemHelper.queryAll();

    verify(mockDbProvider.database);
    expect(activityItems.length, 1);
  });

  test('queryByReportId', () async {
    final activityAnalysis = {
      '_id': 1,
      'report_id': 1,
      'problem': 'Beda Merk',
      'recommendation': 'Penyelesaian Masalah',
    };
    final activityAnalysis2 = {
      '_id': 2,
      'report_id': 2,
      'problem': 'Beda Merk',
      'recommendation': 'Penyelesaian Masalah',
    };
    db.insert('activity_analysis', activityAnalysis);
    db.insert('activity_analysis', activityAnalysis2);

    final activityItemHelper =
        ActivityAnalysisHelper(dbProvider: mockDbProvider);

    List<ActivityAnalisis> activityItems =
        await activityItemHelper.queryByReportId(1);

    verify(mockDbProvider.database);
    expect(activityItems.length, 1);
  });
}
