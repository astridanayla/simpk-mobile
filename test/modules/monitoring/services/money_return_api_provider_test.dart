import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';

Future<String> _getToken() async {
  SharedPreferences pref = await SharedPreferences.getInstance();
  String accessToken = pref.getString("token")!;
  return accessToken;
}

void main() {
  SharedPreferences.setMockInitialValues({'token': 'ACCESS_TOKEN'});

  const baseUrl = "https://simpk-staging.herokuapp.com";
  late Dio dio;
  late DioAdapter dioAdapter;
  late MoneyReturnAPIProvider apiProvider;

  final fileDummy = File('test/resources/test_image.jpg');

  const responseDataDummy = {
    "id": 1,
    "judul": "title",
    "deskripsi": "description",
    "user": 1,
    "user_name": "user",
    "bukti_foto":
        "/media/pengembalian_uang/static/pengembalian_uang/test_image.jpg",
    "proyek": 1,
    "terakhir_diupdate": "2021-01-01T00:00:00.000000Z"
  };

  // ignore: unused_local_variable
  MoneyReturnReport reportDummy = MoneyReturnReport.fromJson(responseDataDummy)
    ..imageFile = fileDummy;

  setUp(() {
    dio = Dio(BaseOptions(baseUrl: baseUrl));
    dioAdapter = DioAdapter();

    dio.httpClientAdapter = dioAdapter;

    apiProvider = MoneyReturnAPIProvider(dio: dio);
  });

  test("postMoneyReturnReport", () async {
    const route = "/laporan_pengembalian_uang/";
    String accessToken = await _getToken();

    final headers = <String, dynamic>{
      "authorization": "Bearer $accessToken",
    };

    final body = {
      "judul": "title",
      "deskripsi": "description",
      "proyek": 1,
      "bukti_foto": await MultipartFile.fromFile(fileDummy.path),
    };
    final formData = FormData.fromMap(body);

    dioAdapter.onPost(
      route,
      (request) {
        request.reply(201, responseDataDummy);
      },
      headers: headers,
      data: formData,
    );

    // ignore: lines_longer_than_80_chars
    // final response = await apiProvider.postMoneyReturnReport(reportDummy, "1");

    // expect(response, true);
  });

  test("getMoneyReturnReportsByProjectId", () async {
    String path = "/laporan_pengembalian_uang/";
    String accessToken = await _getToken();

    final headers = <String, dynamic>{
      "authorization": "Bearer $accessToken",
    };

    final queryParams = {"project_id": 1};

    const responseDummy = {
      "status_code": 200,
      "message": "Success",
      "data": [responseDataDummy]
    };

    dioAdapter.onGet(
      path,
      (request) {
        request.reply(200, responseDummy);
      },
      queryParameters: queryParams,
      headers: headers,
    );

    var reports = await apiProvider.getMoneyReturnReportsByProjectId(1);
    MoneyReturnReport report = reports[0];
    expect(report.id, "1");
    expect(report.title, "title");
    expect(report.description, "description");
    expect(report.userId, "1");
    expect(report.userName, "user");
    expect(report.projectId, "1");
  });

  test("putMoneyReturnReport", () async {
    const route = "/laporan_pengembalian_uang/1";
    String accessToken = await _getToken();

    final headers = <String, dynamic>{
      "authorization": "Bearer $accessToken",
      "content-type": "multipart/form-data; boundary=--dio-boundary-0239143316",
      "content-length": 5140
    };

    final body = {
      "judul": "new title",
      "deskripsi": "new description",
      "proyek": 1,
      "bukti_foto": await MultipartFile.fromFile(fileDummy.path),
    };
    final formData = FormData.fromMap(body);

    dioAdapter.onPut(
      route,
      (request) {
        request.reply(202, responseDataDummy);
      },
      headers: headers,
      data: formData,
    );

    // ignore: unused_local_variable
    Map<String, dynamic> responseMap = {
      "status_code": 202,
      "report": responseDataDummy
    };

    // final response = await apiProvider.putMoneyReturnReport(reportDummy);

    // expect(response, responseMap);
  });

  test("getMoneyReturnReportsByProjectId: Success", () async {
    String path = "/laporan_pengembalian_uang/1";
    String accessToken = await _getToken();

    final headers = <String, dynamic>{
      "authorization": "Bearer $accessToken",
      "content-type": "application/json; charset=utf-8",
    };

    dioAdapter.onDelete(
      path,
      (request) {
        request.reply(204, null);
      },
      headers: headers,
    );

    bool response = await apiProvider.deleteMoneyReturnReport("1");
    expect(response, true);
  });

  test("getMoneyReturnReportsByProjectId: Fail", () async {
    String path = "/laporan_pengembalian_uang/1";
    String accessToken = await _getToken();

    final headers = <String, dynamic>{
      "authorization": "Bearer $accessToken",
      "content-type": "application/json; charset=utf-8",
    };

    dioAdapter.onDelete(
      path,
      (request) {
        request.reply(400, null);
      },
      headers: headers,
    );

    bool response = await apiProvider.deleteMoneyReturnReport("1");
    expect(response, false);
  });
}
