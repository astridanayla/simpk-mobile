import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/realization_item_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'activity_item_helper_test.mocks.dart';

final onCreateSQL = '''
    CREATE TABLE realization_item (
    _id INTEGER PRIMARY KEY,
    report_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    approved INTEGER NOT NULL,
    realization INTEGER NOT NULL,
    difference INTEGER NOT NULL,
    difference_desc TEXT,
    FOREIGN KEY (report_id)
      REFERENCES report (_id));
    '''
    .trim()
    .replaceAll(RegExp(r"\s+"), " ");

void main() {
  late Database db;
  late DatabaseProvider mockDbProvider;

  sqfliteFfiInit();

  setUp(() async {
    db = await databaseFactoryFfi.openDatabase(inMemoryDatabasePath);
    await db.execute(onCreateSQL);
    mockDbProvider = MockDatabaseProvider();
    when(mockDbProvider.database).thenAnswer((_) async => db);
  });

  tearDown(() async {
    await db.close();
  });

  test('onCreateSQL is correct', () {
    expect(
      RealizationItemHelper.onCreateSQL.trim().replaceAll(RegExp(r"\s+"), " "),
      onCreateSQL,
    );
  });

  test('insert', () async {
    final activityItemHelper =
        RealizationItemHelper(dbProvider: mockDbProvider);

    final realizationItem = RealizationItem(
      name: 'A',
      reportId: 1,
      approved: 1000000,
      realization: 1000000,
      difference: false,
    );
    final resultId = await activityItemHelper.insert(realizationItem);

    verify(mockDbProvider.database);
    final query = await db.query('realization_item');
    expect(query.length, 1);
    expect(query[0], {
      '_id': resultId,
      'report_id': 1,
      'name': 'A',
      'approved': 1000000,
      'realization': 1000000,
      'difference': 0,
      'difference_desc': null,
    });
  });

  test('queryAll', () async {
    final activityItem1 = {
      '_id': 1,
      'report_id': 1,
      'name': 'A',
      'approved': 1000000,
      'realization': 1200000,
      'difference': 1,
      'difference_desc': 'a',
    };
    final activityItem2 = {
      '_id': 2,
      'report_id': 1,
      'name': 'A',
      'approved': 2000000,
      'realization': 2000000,
      'difference': 0,
      'difference_desc': null,
    };
    db.insert('realization_item', activityItem1);
    db.insert('realization_item', activityItem2);

    final activityItemHelper =
        RealizationItemHelper(dbProvider: mockDbProvider);
    final List<RealizationItem> activityItems =
        await activityItemHelper.queryAll();

    verify(mockDbProvider.database);
    expect(activityItems.length, 2);
  });

  test('queryByReportId', () async {
    final activityItem1 = {
      '_id': 1,
      'report_id': 1,
      'name': 'A',
      'approved': 1000000,
      'realization': 1200000,
      'difference': 1,
      'difference_desc': 'a',
    };
    final activityItem2 = {
      '_id': 2,
      'report_id': 2,
      'name': 'A',
      'approved': 2000000,
      'realization': 2000000,
      'difference': 0,
      'difference_desc': null,
    };
    db.insert('realization_item', activityItem1);
    db.insert('realization_item', activityItem2);

    final activityItemHelper =
        RealizationItemHelper(dbProvider: mockDbProvider);
    final List<RealizationItem> activityItems =
        await activityItemHelper.queryByReportId(1);

    verify(mockDbProvider.database);
    expect(activityItems.length, 1);
  });
}
