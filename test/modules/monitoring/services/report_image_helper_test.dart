import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/services/report_image_helper.dart';
import 'package:simpk/services/database_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'activity_item_helper_test.mocks.dart';

final onCreateSQL = '''
    CREATE TABLE report_image (
    _id INTEGER PRIMARY KEY,
    report_id INTEGER NOT NULL,
    image_path TEXT NOT NULL,
    FOREIGN KEY (report_id)
      REFERENCES report (_id));
    '''
    .trim()
    .replaceAll(RegExp(r"\s+"), " ");

void main() {
  late Database db;
  late DatabaseProvider mockDbProvider;

  sqfliteFfiInit();

  setUp(() async {
    db = await databaseFactoryFfi.openDatabase(inMemoryDatabasePath);
    await db.execute(onCreateSQL);
    mockDbProvider = MockDatabaseProvider();
    when(mockDbProvider.database).thenAnswer((_) async => db);
  });

  tearDown(() async {
    await db.close();
  });

  test('onCreateSQL is correct', () {
    expect(
      ReportImageHelper.onCreateSQL.trim().replaceAll(RegExp(r"\s+"), " "),
      onCreateSQL,
    );
  });

  test('insert', () async {
    final reportImageHelper = ReportImageHelper(dbProvider: mockDbProvider);

    final image_file = File('test_image.jpg');
    final activityAnalysis = ReportImage(image: image_file)..reportId = 1;
    final resultId = await reportImageHelper.insert(activityAnalysis);

    verify(mockDbProvider.database);
    final query = await db.query('report_image');
    expect(query.length, 1);
    expect(query[0], {
      '_id': resultId,
      'report_id': 1,
      'image_path': image_file.path,
    });
  });

  test('queryAll', () async {
    final activityAnalysis = {
      '_id': 1,
      'report_id': 1,
      'image_path': 'test_image.jpg',
    };
    db.insert('report_image', activityAnalysis);

    final reportImageHelper = ReportImageHelper(dbProvider: mockDbProvider);

    List<ReportImage> activityItems = await reportImageHelper.queryAll();

    verify(mockDbProvider.database);
    expect(activityItems.length, 1);
  });

  test('queryByReportId', () async {
    final reportImage = {
      '_id': 1,
      'report_id': 1,
      'image_path': 'test_image.jpg',
    };
    final reportImage2 = {
      '_id': 2,
      'report_id': 2,
      'image_path': 'test_image.jpg',
    };
    db.insert('report_image', reportImage);
    db.insert('report_image', reportImage2);

    final reportImageHelper = ReportImageHelper(dbProvider: mockDbProvider);

    List<ReportImage> activityItems =
        await reportImageHelper.queryByReportId(1);

    verify(mockDbProvider.database);
    expect(activityItems.length, 1);
  });
}
