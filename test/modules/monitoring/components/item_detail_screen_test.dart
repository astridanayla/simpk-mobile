import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/scroll_app_bar.dart';
import 'package:simpk/modules/monitoring/components/item_detail_screen.dart';

class _DummyItemDetailScreen extends ItemDetailScreen {
  _DummyItemDetailScreen() : super(Object());

  @override
  _DummyItemDetailScreenState createState() => _DummyItemDetailScreenState();
}

class _DummyItemDetailScreenState extends ItemDetailScreenState {
  @override
  Widget body(BuildContext context) {
    return Container();
  }
}

void main() {
  testWidgets('ItemDetailScreen contains ScrollAppBar', (tester) async {
    await tester.pumpWidget(MaterialApp(home: _DummyItemDetailScreen()));

    expect(find.byType(ScrollAppBar), findsOneWidget);
  });
}
