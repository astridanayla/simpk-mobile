import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/components/activity_item_form.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';

import '../../../utils.dart';

Future<ActivityItemForm> _pumpTestableActivityItem(WidgetTester tester,
    [ActivityItemForm? form]) async {
  ThemeData theme = appTheme();
  if (form == null) {
    form = ActivityItemForm(
      activityItem: ActivityItem(),
      onDelete: (_) {},
    );
    form.updateState(itemIndex: 0, showDelete: false);
  }

  await tester.pumpWidget(
    MaterialApp(
      home: Scaffold(
        body: SingleChildScrollView(
          child: form,
        ),
      ),
      theme: theme,
    ),
  );

  return form;
}

void _expectTextField(String label, Matcher matcher) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      matcher);
}

void main() {
  group('Input data', () {
    testWidgets('Check form inputs', (tester) async {
      await _pumpTestableActivityItem(tester);

      expect(find.text('Item 1'), findsOneWidget);
      _expectTextField('Nama Item', findsOneWidget);
      _expectTextField('Persetujuan BPKH', findsOneWidget);
      _expectTextField('Realisasi', findsOneWidget);
      expect(find.text('Ada Perbedaan di Persetujuan dan Realisasi?'),
          findsOneWidget);
      expect(find.text('Ya'), findsOneWidget);
      expect(find.text('Tidak'), findsOneWidget);
    });

    testWidgets("If there's a difference, show 'Deskripsi Perbedaan'",
        (tester) async {
      await _pumpTestableActivityItem(tester);

      expect(find.text('Deskripsi Perbedaan'), findsNothing);
      expect(find.byType(TextFormField), findsNWidgets(3));

      await tester.tap(find.byType(typeOf<Radio<bool>>()).first);
      await tester.pump();

      expect(find.text('Deskripsi Perbedaan'), findsOneWidget);
      expect(find.byType(TextFormField), findsNWidgets(4));

      await tester.tap(find.byType(typeOf<Radio<bool>>()).last);
      await tester.pump();

      expect(find.text('Deskripsi Perbedaan'), findsNothing);
      expect(find.byType(TextFormField), findsNWidgets(3));
    });

    testWidgets('Delete button not exist by default', (tester) async {
      await _pumpTestableActivityItem(tester);

      expect(find.byIcon(Icons.delete), findsNothing);
    });

    testWidgets('Delete button exist if showDeleteButton is true',
        (tester) async {
      ActivityItemForm activityItemForm = ActivityItemForm(
        activityItem: ActivityItem(),
        onDelete: (_) {},
      );
      activityItemForm.updateState(itemIndex: 0, showDelete: true);
      await _pumpTestableActivityItem(tester, activityItemForm);

      expect(find.byIcon(Icons.delete), findsOneWidget);
    });

    testWidgets('Persetujuan dan Realisasi input type number', (tester) async {
      ActivityItemForm activityItemForm = ActivityItemForm(
        activityItem: ActivityItem(),
        onDelete: (_) {},
        type: ActivityItemFormType.number,
      );
      activityItemForm.updateState(itemIndex: 0, showDelete: false);

      await _pumpTestableActivityItem(tester, activityItemForm);

      await tester.enterText(find.byType(TextFormField).at(1), 'Honda');
      await tester.enterText(find.byType(TextFormField).at(2), 'Yamaha');
      await tester.pump();

      activityItemForm.isValid();

      expect(find.text('Persetujuan BPKH harus angka'), findsOneWidget);
      expect(find.text('Realisasi harus angka'), findsOneWidget);
    });

    testWidgets('Update itemIndex after mount', (tester) async {
      final form = await _pumpTestableActivityItem(tester);

      expect(find.text('Item 1'), findsOneWidget);

      form.updateState(itemIndex: 1);
      await tester.pump();

      expect(find.text('Item 1'), findsNothing);
      expect(find.text('Item 2'), findsOneWidget);
    });

    testWidgets('Update showDelete after mount', (tester) async {
      final form = await _pumpTestableActivityItem(tester);

      expect(find.byIcon(Icons.delete), findsNothing);

      form.updateState(showDelete: true);
      await tester.pump();

      expect(find.byIcon(Icons.delete), findsOneWidget);
    });

    testWidgets('Tap delete button calls onDelete function', (tester) async {
      bool onDeleteCalled = false;
      final form = ActivityItemForm(
        activityItem: ActivityItem(),
        onDelete: (_) => onDeleteCalled = true,
      );
      form.updateState(itemIndex: 0, showDelete: true);

      await _pumpTestableActivityItem(tester, form);

      expect(onDeleteCalled, false);

      await tester.tap(find.byIcon(Icons.delete));
      await tester.pump();

      expect(onDeleteCalled, true);
    });

    testWidgets('Enter a valid form will save data', (tester) async {
      final form = await _pumpTestableActivityItem(tester);

      await tester.enterText(find.byType(TextFormField).at(0), 'Merk');
      await tester.enterText(find.byType(TextFormField).at(1), 'Honda');
      await tester.enterText(find.byType(TextFormField).at(2), 'Yamaha');
      await tester.tap(find.byType(typeOf<Radio<bool>>()).first);
      await tester.pump();
      await tester.enterText(find.byType(TextFormField).at(3), 'Beda merk');

      form.isValid();
      await tester.pump();

      expect(form.activityItem.name, 'Merk');
      expect(form.activityItem.approved, 'Honda');
      expect(form.activityItem.realization, 'Yamaha');
      expect(form.activityItem.difference, true);
      expect(form.activityItem.differenceDescription, 'Beda merk');
    });
  });

  group('Validator', () {
    testWidgets('Try to submit empty form will show "... tidak boleh kosong',
        (tester) async {
      final activityItemForm = await _pumpTestableActivityItem(tester);

      await tester.tap(find.byType(typeOf<Radio<bool>>()).first);
      await tester.pump();
      bool isValid = activityItemForm.isValid();
      await tester.pump();

      expect(isValid, false);
      expect(find.text('Nama Item tidak boleh kosong'), findsOneWidget);
      expect(find.text('Persetujuan BPKH tidak boleh kosong'), findsOneWidget);
      expect(find.text('Realisasi tidak boleh kosong'), findsOneWidget);
      expect(
          find.text('Deskripsi Perbedaan tidak boleh kosong'), findsOneWidget);
    });
    testWidgets('Persetujuan and Realisasi same but selectedDiff true',
        (tester) async {
      await _pumpTestableActivityItem(tester);

      await tester.enterText(find.byType(TextFormField).at(1), 'Honda');
      await tester.enterText(find.byType(TextFormField).at(2), 'Honda');

      await tester.tap(find.byType(typeOf<Radio<bool>>()).first);
      await tester.pump();

      expect(
          find.text('Persetujuan dan realisasi bernilai sama'), findsOneWidget);
    });

    testWidgets('Persetujuan and Realisasi different but selectedDiff false',
        (tester) async {
      await _pumpTestableActivityItem(tester);

      await tester.enterText(find.byType(TextFormField).at(1), 'Honda');
      await tester.enterText(find.byType(TextFormField).at(2), 'Yamaha');

      await tester.tap(find.byType(typeOf<Radio<bool>>()).last);
      await tester.pump();

      expect(
          find.text('Persetujuan dan realisasi bernilai beda'), findsOneWidget);
    });
  });
}
