import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/components/activity_analysis_form.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(MaterialApp(home: child, theme: theme));
}

void _expectTextField({required String label}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      findsOneWidget);
}

void main() {
  testWidgets('Activity Analysis Form test widgets', (tester) async {
    int index = 0;
    ActivityAnalysisForm mockActivityAnalisisForm = ActivityAnalysisForm(
      key: UniqueKey(),
      activityAnalysis: ActivityAnalisis(),
      onDelete: (int index) {},
    );
    mockActivityAnalisisForm.index = index;

    await _pumpTestableWidget(tester, Scaffold(body: mockActivityAnalisisForm));

    expect(find.text("Poin " + (mockActivityAnalisisForm.index + 1).toString()),
        findsOneWidget);
    expect(find.text("Masalah / Kendala / Hambatan yang Dihadapi"),
        findsOneWidget);
    _expectTextField(label: "Masalah / Kendala / Hambatan yang Dihadapi");
    expect(find.text("Rekomendasi"), findsOneWidget);
    _expectTextField(label: "Rekomendasi");

    await tester.enterText(
      find.byType(TextFormField).first,
      'Pipa Rusak',
    );
    await tester.enterText(
      find.byType(TextFormField).last,
      'Beli pipa baru',
    );
  });
}
