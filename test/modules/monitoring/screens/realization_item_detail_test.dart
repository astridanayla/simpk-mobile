import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/screens/realization_item_detail.dart';

void main() {
  Intl.defaultLocale = 'id_ID';
  initializeDateFormatting();

  testWidgets('Contains texts with difference', (tester) async {
    final dummyRealizationItem = RealizationItem()
      ..name = 'Merk'
      ..approved = 1000000
      ..realization = 1200000
      ..difference = true
      ..differenceDescription =
          'Beda merk, karena kekuatan mesinnya lebih bagus Honda';

    await tester.pumpWidget(
      MaterialApp(
        home: RealizationItemDetailScreen(dummyRealizationItem),
      ),
    );

    expect(find.text('Merk'), findsOneWidget);
    expect(find.text('Persetujuan BPKH'), findsOneWidget);
    expect(find.text('Rp 1.000.000'), findsOneWidget);
    expect(find.text('Realisasi'), findsOneWidget);
    expect(find.text('Rp 1.200.000'), findsOneWidget);
    expect(find.text('Ada Perbedaan?'), findsOneWidget);
    expect(find.text('Ya'), findsOneWidget);
    expect(find.text('Alasan Perbedaan'), findsOneWidget);
    expect(find.text('Beda merk, karena kekuatan mesinnya lebih bagus Honda'),
        findsOneWidget);
  });

  testWidgets('Contains texts without difference', (tester) async {
    final dummyRealizationItem = RealizationItem()
      ..name = 'Merk'
      ..approved = 1000000
      ..realization = 1000000
      ..difference = false;

    await tester.pumpWidget(
      MaterialApp(
        home: RealizationItemDetailScreen(dummyRealizationItem),
      ),
    );

    expect(find.text('Merk'), findsOneWidget);
    expect(find.text('Persetujuan BPKH'), findsOneWidget);
    expect(find.text('Rp 1.000.000'), findsNWidgets(2));
    expect(find.text('Realisasi'), findsOneWidget);
    expect(find.text('Ada Perbedaan?'), findsOneWidget);
    expect(find.text('Tidak'), findsOneWidget);
    expect(find.text('Alasan Perbedaan'), findsNothing);
  });
}
