import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/screens/activity_analysis_detail.dart';

void main() {
  testWidgets('Contains texts', (tester) async {
    final dummyActivityAnalysis = ActivityAnalisis()
      ..problem = 'Masih banyak anggaran yang tidak terpakai'
      ..recommendation =
          'Perencanaan anggaran sebaiknya lebih efektif untuk kedepannya';

    await tester.pumpWidget(
      MaterialApp(
        home: ActivityAnalysisDetailScreen(dummyActivityAnalysis),
      ),
    );

    expect(find.text('Analisa Pelaksanaan Kegiatan'), findsOneWidget);
    expect(
        find.text('Masalah / Kendala / Hambatan yang Dihadapi',
            skipOffstage: false),
        findsOneWidget);
    expect(
        find.text('Masih banyak anggaran yang tidak terpakai',
            skipOffstage: false),
        findsOneWidget);
    expect(find.text('Rekomendasi', skipOffstage: false), findsOneWidget);
    expect(
        find.text(
            'Perencanaan anggaran sebaiknya lebih efektif untuk kedepannya',
            skipOffstage: false),
        findsOneWidget);
  });
}
