import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/date_picker.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/screens/create_report_1.dart';
import 'package:simpk/modules/monitoring/screens/create_report_2.dart';
import 'package:simpk/services/network_info.dart';
import '../services/report_repository_test.mocks.dart';

void _expectDropdownButton({required String label}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is DropdownButtonFormField &&
          widget.decoration.labelText == label),
      findsOneWidget);
}

void _expectTextField({required String label}) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      findsOneWidget);
}

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(MaterialApp(home: child, theme: theme));
}

Future _selectDropdown(WidgetTester tester, String label, String option) async {
  await tester.tap(find.text(label));
  await tester.pump();

  await tester.tap(find.text(option).last);
  await tester.pump();
}

void main() {
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets("Jenis kegiatan not selected", (tester) async {
    await _pumpTestableWidget(tester, CreateReport1Screen(projectId: 1));

    expect(find.text("Buat Laporan (1/5)"), findsOneWidget);
    _expectDropdownButton(label: "Jenis Kegiatan");
    expect(find.text("Nama Kegiatan"), findsNothing);
    expect(find.text("Metode Pelaksanaan"), findsNothing);
    expect(find.text("Tanggal Monitoring"), findsNothing);
    expect(find.text("Periode Monitoring"), findsNothing);

    expect(find.text("Batal"), findsOneWidget);
    expect(find.text("Selanjutnya"), findsOneWidget);
  });

  testWidgets("Jenis Kegiatan tapped", (tester) async {
    await _pumpTestableWidget(tester, CreateReport1Screen(projectId: 1));

    await tester.tap(find.text("Jenis Kegiatan"));
    await tester.pump();

    expect(find.text("Kendaraan"), findsWidgets);
    expect(find.text("Sarana dan Prasarana"), findsWidgets);
    expect(find.text("Kegiatan"), findsWidgets);
    expect(find.text("Lainnya"), findsWidgets);
  });

  testWidgets("Jenis Kegiatan selected", (tester) async {
    await _pumpTestableWidget(tester, CreateReport1Screen(projectId: 1));

    await _selectDropdown(tester, "Jenis Kegiatan", "Sarana dan Prasarana");

    expect(find.text("Sarana dan Prasarana"), findsOneWidget);
    _expectTextField(label: "Nama Kegiatan");
    _expectDropdownButton(label: "Pilih Metode Pelaksanaan");
    expect(find.text("Tanggal Monitoring"), findsOneWidget);
    expect(find.byType(DatePicker), findsOneWidget);
    _expectDropdownButton(label: "Pilih Periode Monitoring");
    expect(find.text("+ Tambah Periode Baru"), findsOneWidget);
  });

  testWidgets("Tap Batal to go back", (tester) async {
    await _pumpTestableWidget(tester, CreateReport1Screen(projectId: 1));

    await tester.tap(find.text("Batal"));
    await tester.pumpAndSettle();

    expect(find.byType(Scaffold), findsNothing);
  });

  testWidgets("Check text styles", (tester) async {
    ThemeData theme = appTheme();
    await _pumpTestableWidget(tester, CreateReport1Screen(projectId: 1));

    expect(find.text("Buat Laporan (1/5)"), findsOneWidget);
    Text header = tester.firstWidget(find.text("Buat Laporan (1/5)")) as Text;
    expect(header.style!.color, theme.textTheme.headline1!.color);
    expect(header.style!.fontWeight, theme.textTheme.headline1!.fontWeight);
    expect(header.style!.fontSize, theme.textTheme.headline1!.fontSize);
    expect(header.style!.fontFamily, theme.textTheme.headline1!.fontFamily);
  });

  testWidgets("Can only create 1 new monitoring period", (tester) async {
    await _pumpTestableWidget(tester, CreateReport1Screen(projectId: 1));

    await _selectDropdown(tester, "Jenis Kegiatan", "Sarana dan Prasarana");

    // show dropdown
    await tester.ensureVisible(find.text("Pilih Periode Monitoring"));
    await tester.tap(find.text("Pilih Periode Monitoring"));
    await tester.pump();
    expect(find.text("Periode 1"), findsNWidgets(2));
    expect(find.text("Periode 2"), findsNothing);

    // hide dropdown
    await tester.tap(find.text("Buat Laporan (1/5)"));
    await tester.pump();

    // Add new Monitoring Period
    await tester.tap(find.text("+ Tambah Periode Baru"));
    await tester.pump();
    expect(find.text("Periode 2"), findsOneWidget);

    // button is disabled
    TextButton btn =
        await tester.firstWidget(find.byKey(Key('btn_add_period')));
    expect(btn.enabled, false);

    // show dropdown again
    await tester.tap(find.text("Pilih Periode Monitoring"));
    await tester.pumpAndSettle();
    expect(find.text("Periode 1"), findsNWidgets(2));
    expect(find.text("Periode 2"), findsNWidgets(2));
  });

  testWidgets("Monitoring period number continue from the highest number",
      (tester) async {
    await _pumpTestableWidget(
        tester, CreateReport1Screen(projectId: 1, periodNumber: 4));

    await _selectDropdown(tester, "Jenis Kegiatan", "Sarana dan Prasarana");

    // show dropdown
    await tester.ensureVisible(find.text("Pilih Periode Monitoring"));
    await tester.tap(find.text("Pilih Periode Monitoring"));
    await tester.pump();
    expect(find.text("Periode 1"), findsNWidgets(2));
    expect(find.text("Periode 2"), findsNWidgets(2));
    expect(find.text("Periode 3"), findsNWidgets(2));
    expect(find.text("Periode 4"), findsNWidgets(2));
  });

  testWidgets("Navigate to CreateReport2Screen and pass Report data",
      (tester) async {
    ThemeData theme = appTheme();
    await tester.pumpWidget(
        MaterialApp(home: CreateReport1Screen(projectId: 1), theme: theme));

    const dummyActivityType = "Sarana dan Prasarana";
    const dummyActivityName = "Pemeriksaan Rutin";
    var now = DateTime.now();
    var dummyMonitoringDate = DateTime(now.year, now.month, now.day);
    const dummyExecutionMethod = "Cek Fisik oleh Bidang Kemaslahatan";
    const dummyMonitoringPeriod = "Periode 1";

    // set up
    await _selectDropdown(tester, "Jenis Kegiatan", dummyActivityType);
    await tester.enterText(find.byType(TextFormField).first, dummyActivityName);
    await _selectDropdown(
        tester, "Pilih Metode Pelaksanaan", dummyExecutionMethod);
    await tester.tap(find.text("Tanggal Monitoring"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("OK"));
    await tester.pump();
    await _selectDropdown(
        tester, "Pilih Periode Monitoring", dummyMonitoringPeriod);

    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    expect(find.byType(CreateReport2Screen), findsOneWidget);

    var screen = tester.firstWidget(find.byType(CreateReport2Screen))
        as CreateReport2Screen;
    var report = screen.report;

    expect(report.activityType, dummyActivityType);
    expect(report.activityName, dummyActivityName);
    expect(report.executionMethod, dummyExecutionMethod);
    expect(report.monitoringDate, dummyMonitoringDate);
    expect(report.monitoringPeriod, dummyMonitoringPeriod);
  });
}
