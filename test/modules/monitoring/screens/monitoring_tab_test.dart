import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/screens/create_money_return.dart';
import 'package:simpk/modules/monitoring/screens/create_report_1.dart';
import 'package:simpk/modules/monitoring/screens/create_report_2.dart';
import 'package:simpk/modules/monitoring/screens/create_report_3.dart';
import 'package:simpk/modules/monitoring/screens/create_report_4.dart';
import 'package:simpk/modules/monitoring/screens/create_report_5.dart';
import 'package:simpk/modules/monitoring/screens/money_return_details.dart';
import 'package:simpk/modules/monitoring/screens/monitoring_tab.dart';
import 'package:simpk/modules/monitoring/screens/report_detail.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_repository.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project/blocs/project_bloc.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/screens/edit_detail_page.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';
import 'package:simpk/services/network_info.dart';

import '../../../utils.dart';
import '../../project/screens/project_detail_tab_test.mocks.dart';
import '../services/report_repository_test.mocks.dart';
import 'monitoring_tab_test.mocks.dart';

final _dummyProject = Project(
  id: 1,
  name: 'Bantuan Korban Banjir',
  status: 'Dummy Project Status',
  partner: 'Dummy Partner Name',
  activityType: 'Dummy Activity Type',
  region: 'Dummy Region',
  coordinate: '-6.310016735496198, 106.82030167073002',
  description: "Dummy Description",
  progress: 0.5,
  allocation: 123000000,
);

Future _selectDropdown(WidgetTester tester, String label, String option) async {
  await tester.ensureVisible(find.text(label).first);
  await tester.tap(find.text(label).first);
  await tester.pump();

  await tester.ensureVisible(find.text(option).last);
  await tester.tap(find.text(option).last);
  await tester.pump();
}

Report createReport(String reportName) {
  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );
  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';
  return Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = reportName
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5)
    ..authorName = 'BPKH'
    ..reportImages = [
      ReportImage(image: File('test/resources/test_image.jpg'))
    ];
}

MoneyReturnReport createMoneyReturn(String id, String title) {
  return MoneyReturnReport(
    id: id,
    title: title,
    description: title + " description",
    userId: '1',
    userName: 'user',
    image: CachedNetworkImage(
      imageUrl: '',
    ),
    lastUpdated: DateTime.parse("2021-01-01T00:00:00.000000Z"),
  );
}

Future _pumpTestableWidget(WidgetTester tester) async {
  Widget projectDetailPage = BlocProvider<ProjectBloc>(
    create: (context) => ProjectBloc(_dummyProject, skip: true),
    child: MonitoringTab(projectId: 1),
  );
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(home: projectDetailPage),
    ),
  );
}

@GenerateMocks([ReportRepository, MoneyReturnAPIProvider])
void main() {
  late MockProjectAPIProvider apiProvider;
  late MockMonitoringAPIProvider monitoringApi;
  late MockReportRepository mockReportRepository;
  late MockMoneyReturnAPIProvider moneyReturnApi;
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => ['Periode 1', 'Periode 2']);
    when(mockReportRepository.queryByProjectIdAndMonitoringPeriod(
            1, 'Periode 1'))
        .thenAnswer((_) async => [
              createReport('Report 1.1'),
              createReport('Report 1.2'),
              createReport('Report 1.3'),
            ]);
    when(mockReportRepository.queryById(1))
        .thenAnswer((_) async => createReport('Report 1.1'));
    GetIt.I.registerSingleton<ReportService>(mockReportRepository);

    apiProvider = MockProjectAPIProvider();
    GetIt.I.registerSingleton<ProjectAPIProvider>(apiProvider);

    moneyReturnApi = MockMoneyReturnAPIProvider();
    when(moneyReturnApi.getMoneyReturnReportsByProjectId(1))
        .thenAnswer((_) async => [
              createMoneyReturn("1", "Money Return 1"),
              createMoneyReturn("2", "Money Return 2"),
              createMoneyReturn("3", "Money Return 3"),
            ]);
    GetIt.I.registerSingleton<MoneyReturnAPIProvider>(moneyReturnApi);

    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockReportRepository);
    GetIt.I.unregister(instance: apiProvider);
    GetIt.I.unregister(instance: moneyReturnApi);
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets(
    'Screen initial state',
    (WidgetTester tester) async {
      await _pumpTestableWidget(tester);

      verify(mockReportRepository.queryMonitoringPeriodByProjectId(1));
      expect(find.text('Buat Laporan'), findsWidgets);
      expect(find.textContaining('Buat laporan baru'), findsOneWidget);
      expect(find.text('Lihat Laporan'), findsOneWidget);
      expect(find.text('Pilih Periode'), findsOneWidget);
      expect(find.text('Laporan Pengembalian Uang'), findsOneWidget);
      expect(find.text('Tambah Laporan Pengembalian Uang'), findsOneWidget);
      expect(find.text('Tambah kesimpulan setelah monitoring selesai.'),
          findsOneWidget);
    },
  );

  testWidgets('Show "Belum Ada Laporan" when report is empty',
      (WidgetTester tester) async {
    when(mockReportRepository.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => []);

    await _pumpTestableWidget(tester);
    await tester.pumpAndSettle();

    verify(mockReportRepository.queryMonitoringPeriodByProjectId(1));
    expect(find.text('Belum ada laporan'), findsOneWidget);
  });

  testWidgets(
    'Buat laporan button clickable',
    (WidgetTester tester) async {
      await _pumpTestableWidget(tester);
      expect(find.byKey(Key('buat_laporan_button')), findsOneWidget);
      await tester.tap(find.byKey(Key('buat_laporan_button')));
      await tester.pumpAndSettle();
      expect(find.byType(CreateReport1Screen), findsOneWidget);

      CreateReport1Screen screen =
          await tester.firstWidget(find.byType(CreateReport1Screen));
      expect(screen.periodNumber, 2);
    },
  );

  testWidgets(
    'Periode dropdown button clickable',
    (WidgetTester tester) async {
      await _pumpTestableWidget(tester);

      expect(find.byKey(Key('period_dropdown')), findsOneWidget);

      await tester.tap(find.byKey(Key('period_dropdown')));
      await tester.pump();
      await tester.pump(const Duration(seconds: 1));
    },
  );

  testWidgets('Monitoring Dashboard: Click on monitoring period',
      (tester) async {
    await _pumpTestableWidget(tester);
    await tester.pump();
    await _selectDropdown(tester, "Pilih Periode", "Periode 1");

    verify(mockReportRepository.queryByProjectIdAndMonitoringPeriod(
        1, 'Periode 1'));
    expect(find.text("Report 1.1"), findsOneWidget);
    expect(find.text("Report 1.2"), findsOneWidget);
    expect(find.text("Report 1.3"), findsOneWidget);
  });

  testWidgets('Monitoring Dashboard: Tap on tambah laporan pengembalian',
      (tester) async {
    await _pumpTestableWidget(tester);
    expect(find.text('Tambah Laporan Pengembalian Uang'), findsOneWidget);
    await tester.ensureVisible(find.text('Tambah Laporan Pengembalian Uang'));
    await tester.tap(find.text('Tambah Laporan Pengembalian Uang'));
    await tester.pumpAndSettle();

    expect(find.byType(CreateMoneyReturn), findsOneWidget);
  });

  testWidgets('Monitoring dashboard: Tap on laporan pengembalian',
      (tester) async {
    await _pumpTestableWidget(tester);
    await tester.pumpAndSettle();
    expect(find.text('Money Return 1'), findsOneWidget);
    expect(find.text('Money Return 2'), findsOneWidget);
    expect(find.text('Money Return 3'), findsOneWidget);

    await tester.ensureVisible(find.text('Money Return 1'));
    await tester.tap(find.text('Money Return 1'));
    await tester.pumpAndSettle();

    expect(find.byType(MoneyReturnDetails), findsOneWidget);
  });

  testWidgets('Monitoring dashboard: No Money return reports after api call',
      (tester) async {
    when(moneyReturnApi.getMoneyReturnReportsByProjectId(1))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester);
    await tester.pumpAndSettle();
    expect(find.text('Money Return 1'), findsNothing);
    expect(find.text('Money Return 2'), findsNothing);
    expect(find.text('Money Return 3'), findsNothing);
    expect(find.text("Belum ada laporan pengembalian uang"), findsOneWidget);
  });

  testWidgets('Monitoring Dashboard: Click on monitoring report',
      (tester) async {
    monitoringApi = MockMonitoringAPIProvider();
    when(monitoringApi.getReportImageUrlsByReportId(1))
        .thenAnswer((_) async => [""]);
    GetIt.I.registerSingleton<MonitoringAPIProvider>(monitoringApi);

    await _pumpTestableWidget(tester);
    await tester.pumpAndSettle();
    await _selectDropdown(tester, "Pilih Periode", "Periode 1");

    await tester.tap(find.text("Report 1.1"));
    await tester.pumpAndSettle();

    expect(find.byType(ReportDetailScreen), findsOneWidget);
  });

  testWidgets(
      'Tap on tambah kesimpulan will update progress and call updateProject',
      (tester) async {
    when(apiProvider.updateProject(any)).thenAnswer((_) async => 202);
    await _pumpTestableWidget(tester);

    await tester.ensureVisible(find.text('Tambah Kesimpulan'));
    await tester.tap(find.text('Tambah Kesimpulan'));
    await tester.pumpAndSettle();

    expect(find.byType(EditDetailPage), findsOneWidget);
    final editDetailPage =
        await tester.firstWidget(find.byType(EditDetailPage)) as EditDetailPage;

    expect(editDetailPage.title, "Tambah Kesimpulan");
    expect(editDetailPage.label, "Kesimpulan Monitoring");

    await tester.enterText(find.byType(TextFormField), 'Dummy Kesimpulan Baru');
    await tester.ensureVisible(find.text('Simpan'));
    await tester.tap(find.text('Simpan'));
    await tester.pumpAndSettle();

    verify(apiProvider.updateProject(any)).called(1);
    expect(find.text('Dummy Kesimpulan Baru'), findsOneWidget);
  });

  testWidgets('After "Tambah Laporan" will return back to MonitoringTab',
      (tester) async {
    await _pumpTestableWidget(tester);
    await tester.pumpAndSettle();

    await tester.tap(find.byKey(Key('buat_laporan_button')));
    await tester.pumpAndSettle();

    const dummyActivityName = "Pemeriksaan Rutin";
    const dummyPeriod = "Periode 1";

    // CreateReport1Screen
    expect(find.byType(CreateReport1Screen), findsOneWidget);

    await _selectDropdown(tester, "Jenis Kegiatan", "Sarana dan Prasarana");
    await tester.enterText(find.byType(TextFormField).first, dummyActivityName);
    await _selectDropdown(tester, "Pilih Metode Pelaksanaan",
        "Cek Fisik oleh Bidang Kemaslahatan");
    await tester.tap(find.text("Tanggal Monitoring"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("OK"));
    await tester.pump();
    await _selectDropdown(tester, "Pilih Periode Monitoring", dummyPeriod);
    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    // CreateReport2Screen
    expect(find.byType(CreateReport2Screen), findsOneWidget);
    await tester.ensureVisible(find.byType(typeOf<Radio<String>>()).first);
    await tester.tap(find.byType(typeOf<Radio<String>>()).first);
    await tester.enterText(find.byType(TextFormField).at(0), 'Merk');
    await tester.enterText(find.byType(TextFormField).at(1), 'Yamaha');
    await tester.enterText(find.byType(TextFormField).at(2), 'Honda');

    await tester.ensureVisible(find.byType(typeOf<Radio<bool>>()).first);
    await tester.tap(find.byType(typeOf<Radio<bool>>()).first); // ya
    await tester.pump();

    expect(find.text('Deskripsi Perbedaan'), findsOneWidget);
    await tester.ensureVisible(find.byType(TextFormField).at(3));
    await tester.enterText(find.byType(TextFormField).at(3),
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda');
    await tester.pumpAndSettle();

    await tester.ensureVisible(find.text("Selanjutnya"));
    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    // CreateReport3Screen
    expect(find.byType(CreateReport3Screen), findsOneWidget);
    await tester.enterText(find.byType(TextFormField).at(0), 'Merk');
    await tester.enterText(find.byType(TextFormField).at(1), '2000000');
    await tester.enterText(find.byType(TextFormField).at(2), '3000000');

    await tester.ensureVisible(find.byType(typeOf<Radio<bool>>()).first);
    await tester.tap(find.byType(typeOf<Radio<bool>>()).first); // ya
    await tester.pump();
    await tester.enterText(find.byType(TextFormField).at(3),
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda');
    await tester.pump();
    await tester.ensureVisible(find.text("Selanjutnya"));
    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    // CreateReport4Screen
    expect(find.byType(CreateReport4Screen), findsOneWidget);
    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    // CreateReport5Screen
    expect(find.byType(CreateReport5Screen), findsOneWidget);
    CreateReport5Screen screen5 =
        await tester.firstWidget(find.byType(CreateReport5Screen));
    screen5.state.addImage(File('test/resources/test_image.jpg'));
    await tester
        .ensureVisible(find.byType(typeOf<RadioListTile<String>>()).first);
    await tester.tap(find.byType(typeOf<RadioListTile<String>>()).first);
    await tester.pump();

    when(mockReportRepository.insert(screen5.report))
        .thenAnswer((_) async => screen5.report);
    when(mockReportRepository.queryByProjectIdAndMonitoringPeriod(
            1, dummyPeriod))
        .thenAnswer((_) async => [
              createReport('Report 1.1'),
              createReport('Report 1.2'),
              createReport('Report 1.3'),
              createReport(dummyActivityName),
            ]);

    // await tester.tap(find.byType(ElevatedButton).last);
    // await tester.pumpAndSettle();
    // verify(mockReportRepository.insert(screen5.report));

    // expect(find.byType(CreateReport5Screen), findsNothing);
    // verify(mockReportRepository.queryMonitoringPeriodByProjectId(1));
    // verify(mockReportRepository.queryByProjectIdAndMonitoringPeriod(
    //     1, dummyPeriod));
    // expect(find.text(dummyActivityName), findsOneWidget);
  });

  testWidgets(
      "role Professional can't Buat Laporan & Tambah Laporan Pengembalian Uang",
      (tester) async {
    await (WidgetTester tester) async {
      Widget projectDetailPage = BlocProvider<ProjectBloc>(
        create: (context) => ProjectBloc(_dummyProject, skip: true),
        child: MonitoringTab(projectId: 1),
      );
      await tester.pumpWidget(
        BlocProvider(
          create: (_) => AuthBloc.withRole('Professional'),
          child: MaterialApp(home: projectDetailPage),
        ),
      );
    }(tester);

    expect(find.text('Buat Laporan'), findsNothing);
    expect(find.textContaining('Buat laporan baru'), findsNothing);
    expect(find.text('Lihat Laporan'), findsOneWidget);
    expect(find.text('Pilih Periode'), findsOneWidget);
    expect(find.text('Laporan Pengembalian Uang'), findsOneWidget);
    expect(find.text('Tambah Laporan Pengembalian Uang'), findsNothing);
    expect(find.text('Tambah kesimpulan setelah monitoring selesai.'),
        findsOneWidget);
  });
}
