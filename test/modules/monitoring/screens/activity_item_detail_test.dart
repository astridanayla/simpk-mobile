import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/screens/activity_item_detail.dart';

void main() {
  testWidgets('Contains texts with difference', (tester) async {
    final dummyActivityItem = ActivityItem(
        name: 'Merk',
        approved: 'Honda',
        realization: 'Yamaha',
        difference: true,
        differenceDescription:
            'Beda merk, karena kekuatan mesinnya lebih bagus Honda');

    await tester.pumpWidget(
      MaterialApp(
        home: ActivityItemDetailScreen(dummyActivityItem),
      ),
    );

    expect(find.text('Merk'), findsOneWidget);
    expect(find.text('Persetujuan BPKH'), findsOneWidget);
    expect(find.text('Honda'), findsOneWidget);
    expect(find.text('Realisasi'), findsOneWidget);
    expect(find.text('Yamaha'), findsOneWidget);
    expect(find.text('Ada Perbedaan?'), findsOneWidget);
    expect(find.text('Ya'), findsOneWidget);
    expect(find.text('Alasan Perbedaan'), findsOneWidget);
    expect(find.text('Beda merk, karena kekuatan mesinnya lebih bagus Honda'),
        findsOneWidget);
  });

  testWidgets('Contains texts without difference', (tester) async {
    final dummyActivityItem = ActivityItem(
        name: 'Merk',
        approved: 'Honda',
        realization: 'Honda',
        difference: false);

    await tester.pumpWidget(
      MaterialApp(
        home: ActivityItemDetailScreen(dummyActivityItem),
      ),
    );

    expect(find.text('Merk'), findsOneWidget);
    expect(find.text('Persetujuan BPKH'), findsOneWidget);
    expect(find.text('Honda'), findsNWidgets(2));
    expect(find.text('Realisasi'), findsOneWidget);
    expect(find.text('Ada Perbedaan?'), findsOneWidget);
    expect(find.text('Tidak'), findsOneWidget);
    expect(find.text('Alasan Perbedaan'), findsNothing);
  });
}
