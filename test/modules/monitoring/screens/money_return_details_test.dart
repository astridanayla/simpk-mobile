import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/models/MoneyReturnReport.dart';
import 'package:simpk/modules/monitoring/screens/money_return_details.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';

import 'monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(MaterialApp(home: child, theme: theme));
}

MoneyReturnReport mockMoneyReturnReport = MoneyReturnReport(
  title: 'Pengembalian Uang',
  description: 'Deskripsi pengembalian uang',
  image: CachedNetworkImage(
    imageUrl: '',
  ),
  id: '1',
  userId: '1',
  userName: 'User Name',
  lastUpdated: DateTime.parse("2021-01-01T00:00:00.000000Z"),
);

@GenerateMocks([MoneyReturnAPIProvider])
void main() {
  late MockMoneyReturnAPIProvider moneyReturnApi;

  setUp(() {
    moneyReturnApi = MockMoneyReturnAPIProvider();
    GetIt.I.registerSingleton<MoneyReturnAPIProvider>(moneyReturnApi);
  });

  tearDown(() {
    GetIt.I.unregister(instance: moneyReturnApi);
  });

  testWidgets('Money return report details: initial state', (tester) async {
    await _pumpTestableWidget(
      tester,
      Scaffold(
        body: MoneyReturnDetails(
          initMoneyReturnReport: mockMoneyReturnReport,
        ),
      ),
    );

    expect(find.text('Pengembalian Uang'), findsOneWidget);
    expect(find.text('User Name'), findsOneWidget);
    expect(find.byIcon(Icons.delete), findsOneWidget);
    expect(find.byIcon(Icons.edit), findsOneWidget);
    expect(find.byIcon(Icons.account_circle), findsOneWidget);
    expect(find.textContaining('Terakhir diedit: 01/01/2021'), findsOneWidget);
    expect(find.text('Deskripsi'), findsOneWidget);
    expect(find.text('Deskripsi pengembalian uang'), findsOneWidget);
    expect(find.byType(Image), findsOneWidget);
  });

  testWidgets('Money return report details: Change Screen', (tester) async {
    await _pumpTestableWidget(
      tester,
      Scaffold(
        body: MoneyReturnDetails(
          initMoneyReturnReport: mockMoneyReturnReport,
        ),
      ),
    );

    await tester.tap(find.byIcon(Icons.edit));
    await tester.pump();

    expect(find.text('Edit Pengembalian Uang'), findsOneWidget);

    expect(find.byType(TextFormField), findsNWidgets(2));
    expect(find.text('Judul'), findsOneWidget);
    expect(find.text('Pengembalian Uang'), findsOneWidget);
    expect(find.text('Deskripsi'), findsOneWidget);
    expect(find.text('Deskripsi pengembalian uang'), findsOneWidget);

    expect(find.byType(Image), findsOneWidget);

    expect(find.byType(ElevatedButton), findsOneWidget);
    expect(find.text('Ganti foto bukti'), findsOneWidget);
    expect(find.byIcon(Icons.edit), findsOneWidget);

    expect(find.byType(OutlinedButton), findsNWidgets(2));
    expect(find.text('Batal'), findsOneWidget);
    expect(find.text('Simpan'), findsOneWidget);

    await tester.tap(find.text('Batal'));
    await tester.pump();

    expect(find.text('Edit Pengembalian Uang'), findsNothing);
  });

  testWidgets('Money return report details: Delete report success',
      (tester) async {
    await _pumpTestableWidget(
      tester,
      Scaffold(
        body: MoneyReturnDetails(
          initMoneyReturnReport: mockMoneyReturnReport,
        ),
      ),
    );

    await tester.ensureVisible(find.byIcon(Icons.delete));
    await tester.tap(find.byIcon(Icons.delete));
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsOneWidget);
    expect(find.text('Hapus Laporan Pengembalian Uang'), findsOneWidget);
    expect(find.textContaining('Apakah anda yakin ingin menghapus'),
        findsOneWidget);
    expect(find.text('Kembali'), findsOneWidget);
    expect(find.text('Hapus'), findsOneWidget);

    when(moneyReturnApi.deleteMoneyReturnReport("1"))
        .thenAnswer((_) async => true);

    await tester.tap(find.text('Hapus'));
    await tester.pump();
    expect(find.byType(SnackBar), findsOneWidget);
    expect(find.text('Data berhasil terhapus!'), findsOneWidget);

    await tester.pumpAndSettle();
    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets('Money return report details: Delete report fail',
      (tester) async {
    await _pumpTestableWidget(
      tester,
      Scaffold(
        body: MoneyReturnDetails(
          initMoneyReturnReport: mockMoneyReturnReport,
        ),
      ),
    );

    await tester.ensureVisible(find.byIcon(Icons.delete));
    await tester.tap(find.byIcon(Icons.delete));
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsOneWidget);
    expect(find.text('Hapus Laporan Pengembalian Uang'), findsOneWidget);
    expect(find.textContaining('Apakah anda yakin ingin menghapus'),
        findsOneWidget);
    expect(find.text('Kembali'), findsOneWidget);
    expect(find.text('Hapus'), findsOneWidget);

    when(moneyReturnApi.deleteMoneyReturnReport("1"))
        .thenAnswer((_) async => false);

    await tester.tap(find.text('Hapus'));
    await tester.pump();
    expect(find.text('Data gagal dihapus! Mohon coba lagi!'), findsOneWidget);

    await tester.pumpAndSettle();
    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets(
      'Money return report details: Tap delete report but press Kembali',
      (tester) async {
    await _pumpTestableWidget(
      tester,
      Scaffold(
        body: MoneyReturnDetails(
          initMoneyReturnReport: mockMoneyReturnReport,
        ),
      ),
    );

    await tester.ensureVisible(find.byIcon(Icons.delete));
    await tester.tap(find.byIcon(Icons.delete));
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsOneWidget);
    expect(find.text('Hapus Laporan Pengembalian Uang'), findsOneWidget);
    expect(find.textContaining('Apakah anda yakin ingin menghapus'),
        findsOneWidget);
    expect(find.text('Kembali'), findsOneWidget);
    expect(find.text('Hapus'), findsOneWidget);

    await tester.tap(find.text('Kembali'));
    await tester.pumpAndSettle();
    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets('Edit Money return report: Test validator', (tester) async {
    await _pumpTestableWidget(
      tester,
      Scaffold(
        body: MoneyReturnDetails(
          initMoneyReturnReport: mockMoneyReturnReport,
        ),
      ),
    );

    await tester.tap(find.byIcon(Icons.edit));
    await tester.pump();

    await tester.enterText(find.byType(TextFormField).first, '');
    await tester.pump();

    expect(find.text('Judul tidak boleh kosong.'), findsOneWidget);

    await tester.enterText(find.byType(TextFormField).last, '');
    await tester.pump();

    expect(find.text('Deskripsi tidak boleh kosong.'), findsOneWidget);

    await tester.tap(find.text("Simpan"));
    await tester.pump();

    expect(find.text('Isi data dengan penuh!'), findsOneWidget);
  });

  testWidgets('Edit Money return report: Send PUT request to server success',
      (tester) async {
    MoneyReturnReport modifiedMockMoneyReturnReport =
        MoneyReturnReport.copy(mockMoneyReturnReport)
          ..title = "Judul Baru"
          ..description = "Deskripsi Baru";

    Map<String, dynamic> responseMap = {
      "status_code": 202,
      "report": modifiedMockMoneyReturnReport,
    };

    MoneyReturnDetails mockWidget = MoneyReturnDetails(
      initMoneyReturnReport: mockMoneyReturnReport,
    );

    await _pumpTestableWidget(tester, Scaffold(body: mockWidget));

    when(moneyReturnApi
            .putMoneyReturnReport(mockWidget.state.moneyReturnReport))
        .thenAnswer((_) async => responseMap);

    await tester.tap(find.byIcon(Icons.edit));
    await tester.pump();

    await tester.enterText(find.byType(TextFormField).first, 'Judul Baru');
    await tester.pump();

    await tester.enterText(find.byType(TextFormField).last, 'Deskripsi Baru');
    await tester.pump();

    await tester.tap(find.text("Simpan"));
    await tester.pump(Duration(seconds: 2));
    expect(find.text('Data berhasil tersimpan!'), findsOneWidget);

    await tester.pumpAndSettle();
    expect(find.text('Judul Baru'), findsOneWidget);
    expect(find.text('Deskripsi Baru'), findsOneWidget);
  });

  testWidgets('Edit Money return report: Send PUT request to server fail',
      (tester) async {
    MoneyReturnReport modifiedMockMoneyReturnReport =
        MoneyReturnReport.copy(mockMoneyReturnReport)
          ..title = "Judul Baru"
          ..description = "Deskripsi Baru";

    Map<String, dynamic> responseMap = {
      "status_code": 400,
      "report": modifiedMockMoneyReturnReport,
    };

    MoneyReturnDetails mockWidget = MoneyReturnDetails(
      initMoneyReturnReport: mockMoneyReturnReport,
    );

    await _pumpTestableWidget(tester, Scaffold(body: mockWidget));

    when(moneyReturnApi
            .putMoneyReturnReport(mockWidget.state.moneyReturnReport))
        .thenAnswer((_) async => responseMap);

    await tester.tap(find.byIcon(Icons.edit));
    await tester.pump();

    await tester.enterText(find.byType(TextFormField).first, 'Judul Baru');
    await tester.pump();

    await tester.enterText(find.byType(TextFormField).last, 'Deskripsi Baru');
    await tester.pump();

    await tester.tap(find.text("Simpan"));
    await tester.pump(Duration(seconds: 2));
    expect(find.text('Data gagal tersimpan! Mohon coba lagi!'), findsOneWidget);
  });

  // group('$ImagePicker', () {
  //   const MethodChannel channel =
  //       MethodChannel('plugins.flutter.io/image_picker');

  //   final List<MethodCall> log = <MethodCall>[];

  //   setUp(() {
  //     channel.setMockMethodCallHandler((MethodCall methodCall) async {
  //       log.add(methodCall);
  //       return '';
  //     });

  //     log.clear();
  //   });

  //   testWidgets('Money Return Report: Take image for choosing image works',
  //       (WidgetTester tester) async {
  //     await _pumpTestableWidget(
  //       tester,
  //       Scaffold(
  //         body: MoneyReturnDetails(
  //           initMoneyReturnReport: mockMoneyReturnReport,
  //         ),
  //       ),
  //     );

  //     await tester.tap(find.byIcon(Icons.edit));
  //     await tester.pump();

  //     await tester.tap(find.text('Ganti foto bukti'));
  //     await tester.pump();

  //     expect(
  //       log,
  //       <Matcher>[
  //         isMethodCall('pickImage', arguments: {
  //           'source': 0,
  //           'maxWidth': null,
  //           'maxHeight': null,
  //           'imageQuality': null,
  //           'cameraDevice': 0
  //         }),
  //       ],
  //     );
  //   });
  // });
}
