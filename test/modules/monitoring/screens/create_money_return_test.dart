import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/screens/create_money_return.dart';
import 'package:simpk/modules/monitoring/services/money_return_api_provider.dart';
import 'package:simpk/modules/project/models/Project.dart';

import 'monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(MaterialApp(home: child, theme: theme));
}

void main() {
  late MockMoneyReturnAPIProvider moneyReturnApi;

  setUp(() {
    moneyReturnApi = MockMoneyReturnAPIProvider();
    GetIt.I.registerSingleton<MoneyReturnAPIProvider>(moneyReturnApi);
  });

  tearDown(() {
    GetIt.I.unregister(instance: moneyReturnApi);
  });

  final Project mockProject = Project(
    id: 1,
    name: "",
    status: "",
    partner: "",
    activityType: "",
    region: "",
    coordinate: "",
    description: "",
    progress: 0.5,
    allocation: 100000000,
  );

  testWidgets('Create money return report: initial state', (tester) async {
    await _pumpTestableWidget(
        tester, Scaffold(body: CreateMoneyReturn(mockProject)));

    expect(find.text('Buat Pengembalian Uang'), findsOneWidget);
    expect(find.text('Judul'), findsOneWidget);
    expect(find.text('Judul tidak boleh kosong.'), findsOneWidget);
    expect(find.text('Deskripsi'), findsOneWidget);
    expect(find.text('Deskripsi tidak boleh kosong.'), findsOneWidget);
    expect(find.textContaining('Upload Foto'), findsOneWidget);
    expect(find.text('Batal'), findsOneWidget);
    expect(find.text('Simpan'), findsOneWidget);
  });

  testWidgets('Create money return report: Tap on trashbin to remove image',
      (tester) async {
    CreateMoneyReturn mockWidget = CreateMoneyReturn(mockProject);

    await _pumpTestableWidget(tester, mockWidget);

    expect(find.byIcon(Icons.photo_camera), findsOneWidget);
    expect(find.byIcon(Icons.delete), findsNothing);

    mockWidget.state.setImage(File('test/resources/test_image.jpg'));
    await _pumpTestableWidget(tester, mockWidget);
    expect(find.byIcon(Icons.photo_camera), findsNothing);
    expect(find.byIcon(Icons.delete), findsOneWidget);

    // await tester.tap(find.byIcon(Icons.delete));
    // await tester.pump();

    // expect(find.byIcon(Icons.photo_camera), findsOneWidget);
    // expect(find.byIcon(Icons.delete), findsNothing);
  });

  testWidgets('Create money return report: Fill in form data', (tester) async {
    CreateMoneyReturn mockWidget = CreateMoneyReturn(mockProject);
    await _pumpTestableWidget(tester, mockWidget);
    mockWidget.state.setImage(File('test/resources/test_image.jpg'));
    await tester.pump();

    expect(find.text('Judul tidak boleh kosong.'), findsOneWidget);
    expect(find.text('Deskripsi tidak boleh kosong.'), findsOneWidget);
  });

  // testWidgets(
  //ignore: lines_longer_than_80_chars
  //     "Create money return report: Check validity of data after click Simpan",
  //     (tester) async {
  //   CreateMoneyReturn mockWidget = CreateMoneyReturn(mockProject);

  //   await _pumpTestableWidget(tester, mockWidget);

  //   expect(find.text('Judul tidak boleh kosong.'), findsOneWidget);
  //   expect(find.text('Deskripsi tidak boleh kosong.'), findsOneWidget);

  //   await tester.tap(find.text("Simpan"));
  //   await tester.pump();
  //   expect(find.text("Mohon isi data dengan lengkap!"), findsOneWidget);

  //   await tester.enterText(
  //       find.byType(TextFormField).first, "Judul pengembalian uang");
  //   await tester.pump();
  //   expect(find.text('Judul tidak boleh kosong.'), findsNothing);

  //   await tester.tap(find.text("Simpan"));
  //   await tester.pump();
  //   expect(find.text("Mohon isi data dengan lengkap!"), findsOneWidget);

  //   await tester.enterText(
  //       find.byType(TextFormField).last, "Deskripsi pengembalian uang");
  //   await tester.pump();
  //   expect(find.text('Deskripsi tidak boleh kosong.'), findsNothing);

  //   await tester.tap(find.text("Simpan"));
  //   await tester.pump();
  //   expect(find.text("Mohon isi data dengan lengkap!"), findsNothing);
  //   expect(find.text("Mohon upload foto bukti!"), findsOneWidget);

  //   mockWidget.state.setImage(File('test/resources/test_image.jpg'));

  //   await _pumpTestableWidget(tester, mockWidget);
  //   await tester.pump();

  //   await tester.enterText(
  //       find.byType(TextFormField).first, "Suatu gambar pengembalian uang");
  //   await tester.pump();

  //   await tester.enterText(
  //       find.byType(TextFormField).last, "Suatu gambar pengembalian uang");
  //   await tester.pump();

  //   when(moneyReturnApi.postMoneyReturnReport(
  //           mockWidget.state.moneyReturnReport, "1"))
  //       .thenAnswer((_) async => true);

  //   await tester.tap(find.text('Simpan'));
  //   await tester.pump();

  //   expect(find.text("Data berhasil tersimpan!"), findsOneWidget);
  // });

  // testWidgets('Create money return report: Tap Batal to go back',
  //     (WidgetTester tester) async {
  //   await _pumpTestableWidget(
  //       tester, Scaffold(body: CreateMoneyReturn(mockProject)));
  //   await tester.pump();

  //   await tester.tap(find.text("Batal"));
  //   await tester.pumpAndSettle();

  //   expect(find.byType(Scaffold), findsNothing);
  // });
}
