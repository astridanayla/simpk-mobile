import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/components/activity_item_form.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/screens/create_report_3.dart';
import 'package:simpk/modules/monitoring/screens/create_report_4.dart';
import 'package:simpk/services/network_info.dart';
import '../../../utils.dart';
import '../services/report_repository_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
      MaterialApp(home: CreateReport3Screen(report: Report()), theme: theme));
}

void _expectTextField(String label, Matcher matcher) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      matcher);
}

void main() {
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets('Check contents', (tester) async {
    await _pumpTestableWidget(tester);

    expect(find.text('Buat Laporan (3/5)'), findsOneWidget);
    expect(find.byType(typeOf<Radio<bool>>()), findsNWidgets(2));

    expect(find.text('Realisasi Penggunaan Dana'), findsOneWidget);
    expect(find.text('Item 1'), findsOneWidget);
    _expectTextField('Nama Item', findsOneWidget);
    _expectTextField('Persetujuan BPKH', findsOneWidget);
    _expectTextField('Realisasi', findsOneWidget);

    expect(find.text('Ada Perbedaan di Persetujuan dan Realisasi?'),
        findsOneWidget);
    expect(find.text('Ya'), findsOneWidget);
    expect(find.text('Tidak'), findsOneWidget);
    expect(find.byKey(Key('btn_add_item')), findsOneWidget);
    expect(find.text('Tambah Item'), findsOneWidget);

    expect(find.text("Kembali", skipOffstage: false), findsOneWidget);
    expect(find.text("Selanjutnya", skipOffstage: false), findsOneWidget);
  });

  testWidgets('Ensure currency displayed in Persetujuan PBKH and Realisasi',
      (tester) async {
    await _pumpTestableWidget(tester);

    await tester.enterText(find.byType(TextFormField).at(1), '2000000');
    await tester.pump();

    await tester.enterText(find.byType(TextFormField).at(2), '3500000');
    await tester.pump();

    expect(find.text('2.000.000'), findsOneWidget);
    expect(find.text('3.500.000'), findsOneWidget);
  });

  testWidgets('On tap "Tambah Item"', (tester) async {
    await _pumpTestableWidget(tester);

    expect(find.byType(ActivityItemForm), findsOneWidget);
    expect(find.byType(Divider), findsNothing);
    expect(find.text('Item 1'), findsOneWidget);
    expect(find.text('Item 2'), findsNothing);

    await tester.ensureVisible(find.text('Tambah Item'));
    await tester.tap(find.byKey(Key('btn_add_item')));
    await tester.pump();

    // create new item form
    expect(find.byType(ActivityItemForm), findsNWidgets(2));
    // insert divider in between
    expect(find.byType(Divider), findsOneWidget);
    // increment item number
    expect(find.text('Item 1'), findsOneWidget);
    expect(find.text('Item 2'), findsOneWidget);
  });

  testWidgets('On tap delete icon', (tester) async {
    await _pumpTestableWidget(tester);

    await tester.ensureVisible(find.text('Tambah Item'));
    await tester.tap(find.byKey(Key('btn_add_item')));
    await tester.pump();

    // show delete button if more than 1 Item
    expect(find.byType(ActivityItemForm), findsNWidgets(2));
    expect(find.byIcon(Icons.delete), findsNWidgets(2));

    await tester.ensureVisible(find.byIcon(Icons.delete).first);
    await tester.tap(find.byIcon(Icons.delete).first);
    await tester.pump();

    // an ActivityItemForm is reduced after delete
    expect(find.byType(ActivityItemForm), findsOneWidget);
  });

  testWidgets('Correctly delete the first item', (tester) async {
    await _pumpTestableWidget(tester);

    await tester.enterText(find.byType(TextFormField).first, "abc");
    await tester.ensureVisible(find.text('Tambah Item'));
    await tester.tap(find.byKey(Key('btn_add_item')));
    await tester.pump();

    expect(find.text('abc'), findsOneWidget);

    await tester.enterText(find.byType(TextFormField).at(3), "def");
    await tester.ensureVisible(find.byIcon(Icons.delete).first);
    await tester.tap(find.byIcon(Icons.delete).first);
    await tester.pump();

    expect(find.text('abc'), findsNothing);
    expect(find.text('def'), findsOneWidget);
  });

  testWidgets('Navigate and pass Report data to next page', (tester) async {
    await _pumpTestableWidget(tester);

    const dummyName = 'Merk';
    const dummyApproved = '2000000';
    const dummyRealization = '3000000';
    const dummyDifference = true;
    const dummyDifferenceDescription =
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda';

    await tester.enterText(find.byType(TextFormField).at(0), dummyName);
    await tester.enterText(find.byType(TextFormField).at(1), dummyApproved);
    await tester.enterText(find.byType(TextFormField).at(2), dummyRealization);

    await tester.ensureVisible(find.byType(typeOf<Radio<bool>>()).first);
    await tester.tap(find.byType(typeOf<Radio<bool>>()).first); // ya
    await tester.pump();

    expect(find.text('Deskripsi Perbedaan'), findsOneWidget);
    await tester.enterText(
        find.byType(TextFormField).at(3), dummyDifferenceDescription);
    await tester.pump();

    await tester.ensureVisible(find.text("Selanjutnya"));
    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    expect(find.byType(CreateReport4Screen), findsOneWidget);

    var screen = tester.firstWidget(find.byType(CreateReport4Screen))
        as CreateReport4Screen;
    var report = screen.report;

    RealizationItem realizationItem = report.realizationItems![0];
    expect(realizationItem.name, dummyName);
    expect(realizationItem.approved, int.parse(dummyApproved));
    expect(realizationItem.realization, int.parse(dummyRealization));
    expect(realizationItem.difference, dummyDifference);
    expect(realizationItem.differenceDescription, dummyDifferenceDescription);
  });
}
