import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/components/activity_item_form.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/screens/create_report_2.dart';
import 'package:simpk/modules/monitoring/screens/create_report_3.dart';
import 'package:simpk/services/network_info.dart';
import '../../../utils.dart';
import '../services/report_repository_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Report report) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
      MaterialApp(home: CreateReport2Screen(report: report), theme: theme));
}

enum ReportType { kendaraan, sarana, kegiatan, lainnya }

Report _createReport(ReportType reportType) {
  String activityType = "";
  switch (reportType) {
    case ReportType.kendaraan:
      activityType = 'Kendaraan';
      break;
    case ReportType.sarana:
      activityType = 'Sarana dan Prasarana';
      break;
    case ReportType.kegiatan:
      activityType = 'Kegiatan';
      break;
    case ReportType.lainnya:
      activityType = 'Lainnya';
      break;
  }
  return Report()..activityType = activityType;
}

void _expectTextField(String label, Matcher matcher) {
  expect(
      find.byWidgetPredicate((widget) =>
          widget is TextField && widget.decoration!.labelText == label),
      matcher);
}

void main() {
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets('Check contents', (tester) async {
    await _pumpTestableWidget(tester, _createReport(ReportType.kendaraan));

    expect(find.text('Buat Laporan (2/5)'), findsOneWidget);
    expect(find.byType(typeOf<RadioListTile<String>>()), findsNWidgets(2));
    expect(find.byType(typeOf<Radio<bool>>()), findsNWidgets(2));

    expect(find.text('Tahapan Kegiatan yang Akan Dicapai'), findsOneWidget);
    expect(find.text('Pembayaran Pembelian Kendaraan / Mesin / Peralatan'),
        findsOneWidget);
    expect(
        find.text(
            'Penerimaan Kendaraan / Mesin / Peralatan oleh Mitra Kemaslahatan / Penerima manfaat'),
        findsOneWidget);

    expect(find.text('Rincian Kegiatan yang Dilakukan'), findsOneWidget);
    expect(find.text('Item 1'), findsOneWidget);
    _expectTextField('Nama Item', findsOneWidget);
    _expectTextField('Persetujuan BPKH', findsOneWidget);
    _expectTextField('Realisasi', findsOneWidget);

    expect(find.text('Ada Perbedaan di Persetujuan dan Realisasi?'),
        findsOneWidget);
    expect(find.text('Ya'), findsOneWidget);
    expect(find.text('Tidak'), findsOneWidget);
    expect(find.byKey(Key('btn_add_item')), findsOneWidget);
    expect(find.text('Tambah Item'), findsOneWidget);

    expect(find.text("Kembali", skipOffstage: false), findsOneWidget);
    expect(find.text("Selanjutnya", skipOffstage: false), findsOneWidget);
  });

  testWidgets('On tap "Tambah Item"', (tester) async {
    await _pumpTestableWidget(tester, _createReport(ReportType.kendaraan));

    expect(find.byType(ActivityItemForm), findsOneWidget);
    expect(find.byType(Divider), findsNothing);
    expect(find.text('Item 1'), findsOneWidget);
    expect(find.text('Item 2'), findsNothing);

    await tester.ensureVisible(find.text('Tambah Item'));
    await tester.tap(find.byKey(Key('btn_add_item')));
    await tester.pump();

    // create new item form
    expect(find.byType(ActivityItemForm), findsNWidgets(2));
    // insert divider in between
    expect(find.byType(Divider), findsOneWidget);
    // increment item number
    expect(find.text('Item 1'), findsOneWidget);
    expect(find.text('Item 2'), findsOneWidget);
  });

  testWidgets('On tap delete icon', (tester) async {
    await _pumpTestableWidget(tester, _createReport(ReportType.kendaraan));

    await tester.ensureVisible(find.text('Tambah Item'));
    await tester.tap(find.byKey(Key('btn_add_item')));
    await tester.pump();

    // show delete button if more than 1 Item
    expect(find.byType(ActivityItemForm), findsNWidgets(2));
    expect(find.byIcon(Icons.delete), findsNWidgets(2));

    await tester.ensureVisible(find.byIcon(Icons.delete).first);
    await tester.tap(find.byIcon(Icons.delete).first);
    await tester.pump();

    // an ActivityItemForm is reduced after delete
    expect(find.byType(ActivityItemForm), findsOneWidget);
  });

  testWidgets('Correctly delete the first item', (tester) async {
    await _pumpTestableWidget(tester, _createReport(ReportType.kendaraan));

    await tester.enterText(find.byType(TextFormField).first, "abc");
    await tester.ensureVisible(find.text('Tambah Item'));
    await tester.tap(find.byKey(Key('btn_add_item')));
    await tester.pump();

    expect(find.text('abc'), findsOneWidget);

    await tester.enterText(find.byType(TextFormField).at(3), "def");
    await tester.ensureVisible(find.byIcon(Icons.delete).first);
    await tester.tap(find.byIcon(Icons.delete).first);
    await tester.pump();

    expect(find.text('abc'), findsNothing);
    expect(find.text('def'), findsOneWidget);
  });

  testWidgets('Navigate and pass Report data to next page', (tester) async {
    await _pumpTestableWidget(tester, _createReport(ReportType.kendaraan));

    await tester.tap(find.byType(typeOf<Radio<String>>()).first);
    await tester.pump();

    const dummyName = 'Merk';
    const dummyApproved = 'Yamaha';
    const dummyRealization = 'Honda';
    const dummyDifference = true;
    const dummyDifferenceDescription =
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda';

    await tester.enterText(find.byType(TextFormField).at(0), dummyName);
    await tester.enterText(find.byType(TextFormField).at(1), dummyApproved);
    await tester.enterText(find.byType(TextFormField).at(2), dummyRealization);

    await tester.ensureVisible(find.byType(typeOf<Radio<bool>>()).first);
    await tester.tap(find.byType(typeOf<Radio<bool>>()).first); // ya
    await tester.pump();

    expect(find.text('Deskripsi Perbedaan'), findsOneWidget);
    await tester.enterText(
        find.byType(TextFormField).at(3), dummyDifferenceDescription);
    await tester.pump();

    await tester.ensureVisible(find.text("Selanjutnya"));
    await tester.tap(find.text("Selanjutnya"));
    await tester.pumpAndSettle();

    expect(find.byType(CreateReport3Screen), findsOneWidget);

    var screen = tester.firstWidget(find.byType(CreateReport3Screen))
        as CreateReport3Screen;
    var report = screen.report;

    expect(report.activityStage,
        'Pembayaran Pembelian Kendaraan / Mesin / Peralatan');

    ActivityItem activityItem = report.activityItems![0];
    expect(activityItem.name, dummyName);
    expect(activityItem.approved, dummyApproved);
    expect(activityItem.realization, dummyRealization);
    expect(activityItem.difference, dummyDifference);
    expect(activityItem.differenceDescription, dummyDifferenceDescription);
  });
}
