import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_5.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/services/network_info.dart';
import '../services/report_repository_test.mocks.dart';
import 'monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(MaterialApp(home: child, theme: theme));
}

void main() {
  late Report mockReport;
  late ReportService mockReportRepository;
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockReport = Report();
    mockReportRepository = MockReportRepository();
    GetIt.I.registerSingleton(mockReportRepository);

    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockReportRepository);
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets('Create Report 5: initial state of Widget', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport5Screen(report: mockReport));

    expect(find.text("Buat Laporan (5/5)"), findsOneWidget);
    expect(find.text("Bukti Foto Monitoring"), findsOneWidget);

    expect(find.byIcon(Icons.photo_camera), findsOneWidget);
    expect(find.text("Ambil Foto"), findsOneWidget);

    expect(find.text("Kesimpulan Hasil Monitoring"), findsOneWidget);
    expect(
      find.text(
          // ignore: lines_longer_than_80_chars
          "Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH dan dapat dilanjutkan ke tahap selanjutnya"),
      findsOneWidget,
    );
    expect(
      find.text(
          "Progres Kegiatan Kemaslahatan belum sesuai dengan persetujuan dari BPKH dan masih dapat dilanjutkan ke tahap selanjutnya dengan melakukan perbaikan sesuai rekomendasi yang di jelaskan pada Tabel Analisa Pelaksanaan Kegiatan di atas untuk ditindak lanjuti oleh Mitra Kemaslahatan/Penerima Manfaat"),
      findsOneWidget,
    );
    expect(find.text(
            // ignore: lines_longer_than_80_chars
            "Progres Kegiatan Kemaslahatan belum sesuai dengan persetujuan dari BPKH dan tidak dapat dilanjutkan ke tahap selanjutnya"),
        findsOneWidget);
  });

  testWidgets('Create Report 5: Tap on Ambil Foto button', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport5Screen(report: mockReport));

    await tester.tap(find.text("Ambil Foto"));
    await tester.pump();
  });

  testWidgets('Create Report 5: Delete an image after uploading',
      (tester) async {
    Report mockReport = Report();
    CreateReport5Screen mockWidget = CreateReport5Screen(report: mockReport);
    await _pumpTestableWidget(tester, mockWidget);

    expect(find.byIcon(Icons.photo_size_select_actual), findsNothing);
    expect(find.byIcon(Icons.delete), findsNothing);

    mockWidget.state.addImage(File('test/resources/test_image.jpg'));
    await _pumpTestableWidget(tester, mockWidget);
    await tester.pumpAndSettle();

    expect(find.byIcon(Icons.open_in_new), findsOneWidget);
    expect(find.byIcon(Icons.photo_size_select_actual), findsOneWidget);
    expect(find.byIcon(Icons.delete), findsOneWidget);

    // await tester.tap(find.byIcon(Icons.delete));
    // await tester.pump();

    // expect(find.byIcon(Icons.photo_size_select_actual), findsNothing);
    // expect(find.byIcon(Icons.delete), findsNothing);
  });

  testWidgets('Create Report 4: Tap Batal to go back', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport5Screen(report: mockReport));

    await tester.tap(find.text('Kembali'));
    await tester.pumpAndSettle();

    expect(find.byType(Scaffold), findsNothing);
  });

  // group('$ImagePicker', () {
  //   const MethodChannel channel =
  //       MethodChannel('plugins.flutter.io/image_picker');

  //   final List<MethodCall> log = <MethodCall>[];

  //   setUp(() {
  //     channel.setMockMethodCallHandler((MethodCall methodCall) async {
  //       log.add(methodCall);
  //       return '';
  //     });

  //     log.clear();
  //   });

  //   testWidgets('Money Return Report: Take image for choosing image works',
  //       (WidgetTester tester) async {
  //     Report mockReport = Report();
  //     await _pumpTestableWidget(
  //         tester, CreateReport5Screen(report: mockReport));
  //     await tester.pump();

  //     await tester.tap(find.text('Ambil Foto'));
  //     await tester.pump();

  //     expect(
  //       log,
  //       <Matcher>[
  //         isMethodCall('pickImage', arguments: {
  //           'source': 0,
  //           'maxWidth': null,
  //           'maxHeight': null,
  //           'imageQuality': null,
  //           'cameraDevice': 0
  //         }),
  //       ],
  //     );
  //   });
  // });

  testWidgets('Create Report 5: Button enabled after all fields filled',
      (tester) async {
    CreateReport5Screen mockWidget = CreateReport5Screen(report: mockReport);
    await _pumpTestableWidget(tester, mockWidget);

    when(mockReportRepository.insert(mockWidget.report))
        .thenAnswer((_) async => mockWidget.report);

    // ElevatedButton btn = tester.firstWidget(
    //   find.byType(ElevatedButton).last,
    // );
    // expect(btn.enabled, false);

    mockWidget.state.addImage(File('test/resources/test_image.jpg'));

    await _pumpTestableWidget(tester, mockWidget);
    await tester.pump();

    // btn = tester.firstWidget(find.byType(ElevatedButton).last);
    // expect(btn.enabled, false);

    await tester
        .tap(find.textContaining('Progres Kegiatan Kemaslahatan sesuai'));
    await tester.pump();

    // btn = tester.firstWidget(find.byType(ElevatedButton).last);
    // expect(btn.enabled, true);

    // await tester.tap(find.byWidget(btn));
    // await tester.pump();
    // verify(mockReportRepository.insert(mockWidget.report));

    // TODO check datanya kesimpen
  });
}
