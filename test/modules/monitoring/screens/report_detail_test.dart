import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/models/report_image.dart';
import 'package:simpk/modules/monitoring/screens/activity_analysis_detail.dart';
import 'package:simpk/modules/monitoring/screens/activity_item_detail.dart';
import 'package:simpk/modules/monitoring/screens/realization_item_detail.dart';
import 'package:simpk/modules/monitoring/screens/report_detail.dart';
import 'package:simpk/modules/monitoring/services/monitoring_api_provider.dart';
import 'package:simpk/modules/monitoring/services/report_repository.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';

import '../services/report_repository_test.mocks.dart';
import 'monitoring_tab_test.mocks.dart';

final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda');
final _dummyRealizationItem = RealizationItem()
  ..name = 'Merk'
  ..approved = 1000000
  ..realization = 1000000
  ..difference = false;
final _dummyActivityAnalysis = ActivityAnalisis()
  ..problem = 'Nama Masalah'
  ..recommendation = 'Penyelesaian Masalah';
final _dummyReport = Report()
  ..id = 1
  ..activityType = 'Sarana dan Prasarana'
  ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
  ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
  ..monitoringDate = DateTime(2021, 10, 24)
  ..monitoringPeriod = 'Periode 1'
  ..modifiedDate = DateTime(2021, 4, 5)
  ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
  ..activityItems = [_dummyActivityItem, _dummyActivityItem]
  ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
  ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
  ..conclusion =
      'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
          'dan dapat dilanjutkan ke tahap selanjutnya'
  ..authorName = 'BPKH'
  ..reportImages = [ReportImage(image: File('test/resources/test_image.jpg'))];

Future _pumpTestableWidget(WidgetTester tester, [Report? report]) async {
  await tester.pumpWidget(MaterialApp(
    home: ReportDetailScreen(report ?? _dummyReport),
    theme: appTheme(),
  ));
}

void main() {
  late ReportRepository mockReportRepository;
  MockMonitoringAPIProvider monitoringApi = MockMonitoringAPIProvider();
  when(monitoringApi.getReportImageUrlsByReportId(1))
      .thenAnswer((_) async => [""]);
  GetIt.I.registerSingleton<MonitoringAPIProvider>(monitoringApi);

  setUp(() {
    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryById(1))
        .thenAnswer((_) async => _dummyReport);
    GetIt.I.registerSingleton<ReportService>(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockReportRepository);
  });

  group('View report information', () {
    Intl.defaultLocale = 'id_ID';
    initializeDateFormatting();

    testWidgets('Get user on load', (tester) async {
      await _pumpTestableWidget(tester);

      verify(mockReportRepository.queryById(1));
    });

    testWidgets('Report info exists', (tester) async {
      await _pumpTestableWidget(tester);

      expect(find.text('Laporan Monev'), findsOneWidget);
      expect(find.text('Dibuat pada: 05/04/2021'), findsOneWidget);
      expect(find.text('Jenis Kegiatan'), findsOneWidget);
      expect(find.text('Sarana dan Prasarana'), findsOneWidget);
      expect(find.text('Nama Kegiatan'), findsOneWidget);
      expect(find.text('Pemeriksaan Rutin Pembangunan Masjid'), findsOneWidget);
      expect(find.text('Metode Pelaksanaan'), findsOneWidget);
      expect(find.text('Cek Fisik oleh Bidang Kemaslahatan'), findsOneWidget);
      expect(find.text('Tanggal Monitoring'), findsOneWidget);
      expect(find.text('24/10/2021'), findsOneWidget);
      expect(find.text('Periode Monitoring'), findsOneWidget);
      expect(find.text('Periode 1'), findsOneWidget);
      expect(find.text('BPKH'), findsOneWidget);

      expect(find.byIcon(Icons.delete), findsOneWidget);
      expect(find.byKey(Key('btn_delete')), findsOneWidget);
    });

    testWidgets('Activity executions info exists', (tester) async {
      await _pumpTestableWidget(tester);

      expect(find.text('Pelaksanaan Kegiatan'), findsOneWidget);
      expect(find.text('Tahapan Kegiatan yang Akan Dicapai'), findsOneWidget);
      expect(find.text('Pembayaran Pembelian Kendaraan / Mesin / Peralatan'),
          findsOneWidget);

      expect(find.text('Merk'), findsNWidgets(4));
      expect(find.text('Persetujuan BPKH'), findsNWidgets(4));
      expect(find.text('Realisasi'), findsNWidgets(4));
      expect(find.text('Rincian Kegiatan yang Dilakukan'), findsOneWidget);

      expect(
        find.byWidgetPredicate((widget) =>
            widget is RichText &&
            widget.text.toPlainText() == 'Tingkat Kesesuaian : Tidak Sesuai'),
        findsOneWidget,
      );
      expect(find.text('Tahapan Kegiatan yang Akan Dicapai'), findsOneWidget);
      expect(find.text('Honda'), findsNWidgets(2));
      expect(find.text('Yamaha'), findsNWidgets(2));

      expect(
        find.byWidgetPredicate((widget) =>
            widget is RichText &&
            widget.text.toPlainText() == 'Tingkat Kesesuaian : Sesuai'),
        findsOneWidget,
      );
      expect(find.text('Realisasi Penggunaan Dana'), findsOneWidget);
      expect(find.text('Rp 1.000.000'), findsNWidgets(4));
    });

    testWidgets('Activity analysis info exists', (tester) async {
      await _pumpTestableWidget(tester);

      expect(find.text('Analisa Pelaksanaan Kegiatan'), findsOneWidget);
      expect(find.text('Masalah / Kendala / Hambatan yang Dihadapi'),
          findsNWidgets(2));
      expect(find.text('Nama Masalah'), findsNWidgets(2));
    });

    testWidgets('Conclusion exists', (tester) async {
      await _pumpTestableWidget(tester);

      expect(find.text('Kesimpulan Hasil Monitoring'), findsOneWidget);
      expect(
          find.text(
              'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari ' +
                  'BPKH dan dapat dilanjutkan ke tahap selanjutnya'),
          findsOneWidget);
    });

    testWidgets('Show report with null items', (tester) async {
      final report = Report()
        ..id = 1
        ..activityType = 'Sarana dan Prasarana'
        ..activityName = 'Pemeriksaan Rutin Pembangunan Masjid'
        ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
        ..monitoringDate = DateTime(2021, 10, 24)
        ..monitoringPeriod = 'Periode 1'
        ..modifiedDate = DateTime(2021, 4, 5)
        ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
        ..conclusion =
            'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari ' +
                'BPKH dan dapat dilanjutkan ke tahap selanjutnya'
        ..authorName = 'BPKH';
      await _pumpTestableWidget(tester, report);

      final screen = await tester
          .firstWidget<ReportDetailScreen>(find.byType(ReportDetailScreen));

      expect(screen.report.activityItems, []);
      expect(screen.report.realizationItems, []);
      expect(screen.report.activityAnalysis, []);
    });
  });

  group('Tap actions', () {
    testWidgets('Tap activity item navigates to ActivityItemDetailScreen',
        (tester) async {
      await _pumpTestableWidget(tester);

      await tester.ensureVisible(find.text('Honda').first);
      await tester.tap(find.text('Honda').first);
      await tester.pumpAndSettle();

      expect(find.byType(ActivityItemDetailScreen), findsOneWidget);
    });

    testWidgets('Tap realization item navigates to RealizationItemDetailScreen',
        (tester) async {
      await _pumpTestableWidget(tester);

      await tester.ensureVisible(find.text('Rp 1.000.000').first);
      await tester.tap(find.text('Rp 1.000.000').first);
      await tester.pumpAndSettle();

      expect(find.byType(RealizationItemDetailScreen), findsOneWidget);
    });

    testWidgets(
        'Tap activity analysis navigates to ActivityAnalysisDetailScreen',
        (tester) async {
      await _pumpTestableWidget(tester);

      await tester.ensureVisible(find.text('Nama Masalah').first);
      await tester.tap(find.text('Nama Masalah').first);
      await tester.pumpAndSettle();

      expect(find.byType(ActivityAnalysisDetailScreen), findsOneWidget);
    });
  });
}
