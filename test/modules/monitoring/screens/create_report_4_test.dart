import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/screens/create_report_4.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/services/network_info.dart';
import '../services/report_repository_test.mocks.dart';
import 'monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(MaterialApp(home: child, theme: theme));
}

void main() {
  late MockNetworkInfo mockNetworkInfo;
  late MockReportRepository mockReportRepository;

  setUp(() {
    mockReportRepository = MockReportRepository();
    GetIt.I.registerSingleton<ReportService>(mockReportRepository);

    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockReportRepository);
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets('Create Report 4: initial state of Widget', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport4Screen(report: mockReport));

    expect(find.text("Buat Laporan (4/5)"), findsOneWidget);
    expect(find.text("Analisa Pelaksanaan Kegiatan"), findsOneWidget);
    expect(find.text("Poin 1"), findsNothing);
    expect(
        find.text("Masalah / Kendala / Hambatan yang Dihadapi"), findsNothing);
    expect(find.text("Rekomendasi"), findsNothing);
  });

  testWidgets('Create Report 4: tap on add point', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport4Screen(report: mockReport));

    expect(
        find.text("Masalah / Kendala / Hambatan yang Dihadapi"), findsNothing);
    expect(find.text("Rekomendasi"), findsNothing);

    await tester.tap(find.text("Tambah Poin"));
    await tester.pump();
    expect(find.text("Poin 1"), findsOneWidget);
    expect(find.text("Masalah / Kendala / Hambatan yang Dihadapi"),
        findsOneWidget);
    expect(find.text("Rekomendasi"), findsOneWidget);

    await tester.ensureVisible(find.text("Tambah Poin"));
    await tester.tap(find.text("Tambah Poin"));
    await tester.pump();
    expect(find.text("Poin 2"), findsOneWidget);
    expect(
        find.text("Masalah / Kendala / Hambatan yang Dihadapi"), findsWidgets);
    expect(find.text("Rekomendasi"), findsWidgets);
  });

  testWidgets('Create Report 4: Tap on delete point', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport4Screen(report: mockReport));

    expect(
        find.text("Masalah / Kendala / Hambatan yang Dihadapi"), findsNothing);
    expect(find.text("Rekomendasi"), findsNothing);

    await tester.ensureVisible(find.text("Tambah Poin"));
    await tester.tap(find.text("Tambah Poin"));
    await tester.pump();
    expect(find.text("Poin 1"), findsOneWidget);

    await tester.tap(find.text("Tambah Poin"));
    await tester.pump();
    expect(find.text("Poin 2"), findsNothing);

    await tester.tap(find.byType(IconButton).last);
    await tester.pump();
    expect(find.text('Poin 2'), findsNothing);
  });

  testWidgets('Create Report 4: Tap Batal to go back', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport4Screen(report: mockReport));

    await tester.tap(find.text('Kembali'));
    await tester.pumpAndSettle();

    expect(find.byType(Scaffold), findsNothing);
  });

  testWidgets('Create Report 4: tap on next button', (tester) async {
    Report mockReport = Report();
    await _pumpTestableWidget(tester, CreateReport4Screen(report: mockReport));

    ElevatedButton btn = tester.firstWidget(find.byType(ElevatedButton).last);
    expect(btn.enabled, true);
    await tester.tap(find.byWidget(btn));
    await tester.pump();
  });
}
