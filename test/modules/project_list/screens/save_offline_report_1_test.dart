import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/offline_report/screens/save_offline_report_1.dart';
import 'package:simpk/modules/project_list/models/project_data.dart';
import 'package:simpk/services/api_provider.dart';

import 'project_list_test.mocks.dart';

void main() {
  Map<String, dynamic> mockProject1 = {
    "nama_proyek": "Nama proyek",
    "jenis_kegiatan": "Kesehatan",
    "nama_penerima": "Nama Penerima",
    "daerah": "Jakarta, DKI Jakarta",
    "alokasi_dana": "1.00",
    "mitra": null,
    "status": "Berjalan",
    "project_id": 1,
    "koordinat_lokasi": "",
    "deskripsi": "Deskripsi",
    "progres": 0,
    "kesimpulan_monitoring": "Belum ada",
    "total_realisasi": null,
    "jumlah_laporan": 0
  };

  final mockProjectData1 = ProjectData(
    reports: [mockProject1],
    statusCode: 200,
    total: 1,
    nItems: 1,
    errorMessage: '',
  );

  final mockProjectDataNoItems = ProjectData(
    reports: [],
    statusCode: 200,
    total: 0,
    nItems: 0,
    errorMessage: '',
  );

  late APIProvider mockApiProvider;

  setUp(() {
    mockApiProvider = MockAPIProvider();
    Map<String, dynamic> mockQueryMitra = {"mitra": "1"};
    Map<String, dynamic> mockQueryStatus = {
      "status": ["selesai"]
    };
    for (int i = 1; i <= 5; i++) {
      when(mockApiProvider.getProjects(i, '', null))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, '', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'Nama', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'test', {}))
          .thenAnswer((_) async => mockProjectDataNoItems);
      when(mockApiProvider.getProjects(i, '', mockQueryMitra))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, '', mockQueryStatus))
          .thenAnswer((_) async => mockProjectData1);
    }
    GetIt.I.registerSingleton<APIProvider>(mockApiProvider);
  });

  testWidgets("Save offline report 1: static contents are correct",
      (WidgetTester tester) async {
    Widget saveReportPage = SaveOfflineReport1(
      offlineReportId: 1,
    );
    await tester.pumpWidget(MaterialApp(home: Scaffold(body: saveReportPage)));

    expect(find.text("Simpan Proyek Offline (1/2)"), findsOneWidget);
    expect(find.text("Pilih proyek untuk laporan yang akan disimpan"),
        findsOneWidget);
  });
}
