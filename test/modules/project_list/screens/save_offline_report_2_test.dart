import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/offline_report/screens/save_offline_report_2.dart';
import 'package:simpk/services/api_provider.dart';

import '../../home/screens/home_page_test.mocks.dart';
import '../../monitoring/screens/monitoring_tab_test.mocks.dart';

void main() {
  final dummyUniqueRegionsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": [
      "Indonesia",
      "DKI Jakarta",
    ]
  };

  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  late MockHomeAPIProvider mockHomeAPIProvider;
  late APIProvider mockApiProvider;
  late ReportService mockReportRepository;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getUniqueRegions())
        .thenAnswer((_) async => dummyUniqueRegionsResponse);
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("Indonesia"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("DKI Jakarta"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    mockApiProvider = MockAPIProvider();
    GetIt.I.registerSingleton<APIProvider>(mockApiProvider);

    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    when(mockReportRepository.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => ["Periode 1"]);
    when(mockReportRepository.queryByIdOffline(1))
        .thenAnswer((aync) async => mockReport);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
    GetIt.I.unregister(instance: mockApiProvider);
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets('Save offline report 2: static content test',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: SaveOfflineReport2(
      projectId: 1,
      offlineReportId: 1,
    ))));
    await tester.pump();

    expect(find.text("Simpan Proyek Offline (2/2)"), findsOneWidget);
    expect(find.text('+ Tambah Periode Baru'), findsOneWidget);
    expect(find.byType(ElevatedButton), findsOneWidget);
    expect(find.text('Simpan'), findsOneWidget);
  });

  testWidgets('Save offline report 2: save button redirect to other page',
      (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
        home: Scaffold(
            body: SaveOfflineReport2(
      projectId: 1,
      offlineReportId: 1,
    ))));
    await tester.pump();

    await tester.tap(find.text('Simpan'));
    await tester.pump();
    expect(find.text('Simpan Proyek Offline (2/2)'), findsOneWidget);
  });

  testWidgets('Save offline report 2: increment period',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SaveOfflineReport2(
            projectId: 1,
            offlineReportId: 1,
          ),
        ),
      ),
    );
    await tester.pump();

    await tester.tap(find.text('+ Tambah Periode Baru'));
    await tester.pump();
    expect(find.text('Periode 2'), findsNothing);
  });
}
