import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/bottom_navbar.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project_list/models/project_data.dart';
import 'package:simpk/modules/project_list/screens/project_list.dart';
import 'package:simpk/modules/project_list/screens/project_page.dart';
import 'package:simpk/services/api_provider.dart';

import '../../home/screens/home_page_test.mocks.dart';
import '../../project/screens/project_dashboard_page_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: ProjectPage(),
        theme: theme,
      ),
    ),
  );
}

void main() {
  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      // "total": 10,
      "selesai": 5,
      "berjalan": 3,
      // "belum_mulai": 2,
    }
  };

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  Map<String, dynamic> mockProject1 = {
    "nama_proyek": "Nama proyek",
    "jenis_kegiatan": "Kesehatan",
    "nama_penerima": "Nama Penerima",
    "daerah": "Jakarta, DKI Jakarta",
    "alokasi_dana": "1.00",
    "mitra": null,
    "status": "Berjalan",
    "project_id": 1,
    "koordinat_lokasi": "",
    "deskripsi": "Deskripsi",
    "progres": 0,
    "kesimpulan_monitoring": "Belum ada",
    "total_realisasi": null,
    "jumlah_laporan": 0
  };

  final mockProjectData1 = ProjectData(
    reports: [mockProject1],
    statusCode: 200,
    total: 1,
    nItems: 1,
    errorMessage: '',
  );

  final mockProjectDataNoItems = ProjectData(
    reports: [],
    statusCode: 200,
    total: 0,
    nItems: 0,
    errorMessage: '',
  );

  late APIProvider mockApiProvider;
  late MockHomeAPIProvider mockHomeAPIProvider;
  late ReportService mockReportRepository;

  setUp(() {
    mockApiProvider = MockAPIProvider();
    Map<String, dynamic> mockQueryMitra = {"mitra": "1"};
    Map<String, dynamic> mockQueryStatus = {
      "status": ["selesai"]
    };
    for (int i = 1; i <= 5; i++) {
      when(mockApiProvider.getProjects(i, '', null))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, '', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'Nama', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'test', {}))
          .thenAnswer((_) async => mockProjectDataNoItems);
      when(mockApiProvider.getProjects(i, '', mockQueryMitra))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, '', mockQueryStatus))
          .thenAnswer((_) async => mockProjectData1);
    }
    GetIt.I.registerSingleton<APIProvider>(mockApiProvider);

    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockApiProvider);
    GetIt.I.unregister(instance: mockHomeAPIProvider);
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets('project page content', (tester) async {
    await _pumpTestableWidget(tester);

    expect(find.byType(ProjectList), findsOneWidget);
    expect(find.byType(BottomNavbar), findsOneWidget);
  });

  testWidgets("Project page: cloud icon on appbar redirects to home page",
      (WidgetTester tester) async {
    await _pumpTestableWidget(tester);
    await tester.pump();

    await tester.tap(find.byIcon(Icons.cloud_upload));
    await tester.pump();

    expect(find.text("Daftar Proyek"), findsOneWidget);
  });

  testWidgets(
      "Manage offline report: profile icon on appbar redirects to profile page",
      (WidgetTester tester) async {
    final dummyStatusCountsResponse = {
      "status_code": 200,
      "message": "Success",
      "data": {
        "selesai": 5,
        "berjalan": 3,
      }
    };

    late MockHomeAPIProvider mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);

    await _pumpTestableWidget(tester);
    await tester.pump();

    await tester.tap(find.byIcon(Icons.account_circle).at(0));
    await tester.pump();

    expect(find.text("Daftar Proyek"), findsOneWidget);
  });
}
