import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/project_card.dart';
import 'package:simpk/components/search_bar.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/offline_report/screens/save_offline_report_2.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/modules/project/services/project_api_provider.dart';
import 'package:simpk/modules/project_list/models/project_data.dart';
import 'package:simpk/modules/project_list/screens/project_list.dart';
import 'package:simpk/services/api_provider.dart';

import '../../monitoring/screens/monitoring_tab_test.mocks.dart';
import '../../project/screens/project_detail_tab_test.mocks.dart';
import 'project_list_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

@GenerateMocks([APIProvider])
void main() {
  late APIProvider mockAPIProvider;
  late ProjectAPIProvider mockProjectAPIProvider;
  late ReportService mockReportRepository;

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  Map<String, dynamic> mockProject1 = {
    "nama_proyek": "Nama proyek",
    "jenis_kegiatan": "Kesehatan",
    "nama_penerima": "Nama Penerima",
    "daerah": "Jakarta, DKI Jakarta",
    "alokasi_dana": "1.00",
    "mitra": null,
    "status": "Berjalan",
    "project_id": 1,
    "koordinat_lokasi": "",
    "deskripsi": "Deskripsi",
    "progres": 0,
    "kesimpulan_monitoring": "Belum ada",
    "total_realisasi": null,
    "jumlah_laporan": 0
  };

  final mockProjectData1 = ProjectData(
    reports: [mockProject1],
    statusCode: 200,
    total: 1,
    nItems: 1,
    errorMessage: '',
  );

  final mockProjectDataNoItems = ProjectData(
    reports: [],
    statusCode: 200,
    total: 0,
    nItems: 0,
    errorMessage: '',
  );

  setUp(() {
    mockAPIProvider = MockAPIProvider();
    Map<String, dynamic> mockQueryMitra = {"mitra": "1"};
    Map<String, dynamic> mockQueryStatus = {
      "status": ["selesai"]
    };

    for (int i = 1; i <= 5; i++) {
      when(mockAPIProvider.getProjects(i, '', null))
          .thenAnswer((_) async => mockProjectData1);
      when(mockAPIProvider.getProjects(i, '', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockAPIProvider.getProjects(i, 'Nama', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockAPIProvider.getProjects(i, 'test', {}))
          .thenAnswer((_) async => mockProjectDataNoItems);
      when(mockAPIProvider.getProjects(i, '', mockQueryMitra))
          .thenAnswer((_) async => mockProjectData1);
      when(mockAPIProvider.getProjects(i, '', mockQueryStatus))
          .thenAnswer((_) async => mockProjectData1);
    }

    GetIt.I.registerSingleton<APIProvider>(mockAPIProvider);

    mockProjectAPIProvider = MockProjectAPIProvider();
    when(mockProjectAPIProvider.getAllAttachment(1))
        .thenAnswer((_) async => []);
    GetIt.I.registerSingleton<ProjectAPIProvider>(mockProjectAPIProvider);

    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    when(mockReportRepository.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => []);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockAPIProvider);
    GetIt.I.unregister(instance: mockProjectAPIProvider);
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets("static contents are correct", (WidgetTester tester) async {
    Widget projectListScreen = ProjectList(saveOffline: false);
    await _pumpTestableWidget(tester, projectListScreen);

    expect(find.byType(SearchBar), findsOneWidget);
    expect(find.text("Daftar Proyek"), findsOneWidget);
  });

  testWidgets("on save offline, redirects to next page",
      (WidgetTester tester) async {
    Widget projectListScreenOfflineReport = ProjectList(
      saveOffline: true,
      targetPage: (Project project) {
        return SaveOfflineReport2(
          projectId: project.id,
          offlineReportId: 1,
        );
      },
    );

    await _pumpTestableWidget(tester, projectListScreenOfflineReport);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    await tester.tap(find.byType(ProjectCard).first);
    await tester.pumpAndSettle();
    expect(find.text('Simpan Proyek Offline (2/2)'), findsOneWidget);
  });

  testWidgets("displays project cards", (WidgetTester tester) async {
    Widget projectListScreen = ProjectList(saveOffline: false);

    await _pumpTestableWidget(tester, projectListScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.byType(ProjectCard), findsNWidgets(5));
    expect(find.text("Nama proyek"), findsNWidgets(5));
    expect(find.text("Jakarta, DKI Jakarta"), findsNWidgets(5));
  });

  testWidgets("search displays corresponding project",
      (WidgetTester tester) async {
    Widget projectListScreen = ProjectList(saveOffline: false);

    await _pumpTestableWidget(tester, projectListScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), "Nama");
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text('Hasil Pencarian'), findsOneWidget);
    expect(find.byType(ProjectCard), findsWidgets);
    expect(find.text("Nama proyek"), findsNWidgets(5));
    expect(find.text("Jakarta, DKI Jakarta"), findsNWidgets(5));

    for (int i = 1; i <= 5; i++) {
      when(mockAPIProvider.getProjects(i, "", {}))
          .thenAnswer((_) async => mockProjectDataNoItems);
    }

    await tester.enterText(find.byType(TextFormField).at(0), "");
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.byType(ProjectCard), findsNWidgets(5));
  });

  testWidgets("search displays not found project", (WidgetTester tester) async {
    Widget projectListScreen = ProjectList(saveOffline: false);
    String searchValue = "test";

    await _pumpTestableWidget(tester, projectListScreen);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), searchValue);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text('Hasil Pencarian'), findsOneWidget);
    expect(find.text('Hasil pencarian tidak ditemukan'), findsOneWidget);
  });

  testWidgets('filter location', (WidgetTester tester) async {
    Widget partnerDashboardPage = ProjectList(partnerId: 1, saveOffline: false);
    await _pumpTestableWidget(tester, partnerDashboardPage);
    await tester.pumpAndSettle();

    expect(find.byType(ProjectCard), findsNWidgets(5));
    expect(find.text('Nama proyek'), findsNWidgets(5));
  });
}
