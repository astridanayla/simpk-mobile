import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/main_appbar.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/offline_report/screens/manage_offline_report.dart';
import 'package:simpk/services/api_provider.dart';

import '../../home/screens/home_page_test.mocks.dart';
import '../../monitoring/screens/monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: child,
        theme: theme,
      ),
    ),
  );
}

@GenerateMocks([APIProvider])
void main() {
  GetIt.I.registerSingleton<APIProvider>(MockAPIProvider());
  final dummyUniqueRegionsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": [
      "Indonesia",
      "DKI Jakarta",
    ]
  };

  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  late MockHomeAPIProvider mockHomeAPIProvider;
  late ReportService mockReportRepository;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getUniqueRegions())
        .thenAnswer((_) async => dummyUniqueRegionsResponse);
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("Indonesia"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("DKI Jakarta"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets('Manage offline report: static content test',
      (WidgetTester tester) async {
    await tester
        .pumpWidget(MaterialApp(home: Scaffold(body: ManageOfflineReport())));
    await tester.pump();

    expect(find.byType(MainAppbar), findsOneWidget);
    expect(find.text('Daftar Laporan Offline'), findsOneWidget);
    expect(find.byType(Card), findsWidgets);
  });

  testWidgets(
      "Manage offline report: cloud icon on appbar redirects to home page",
      (WidgetTester tester) async {
    await _pumpTestableWidget(tester, ManageOfflineReport());
    await tester.pump();

    await tester.tap(find.byIcon(Icons.cloud_upload));
    await tester.pumpAndSettle();

    expect(find.text("Daftar Laporan Offline"), findsNothing);
  });

  testWidgets(
      "Manage offline report: profile icon on appbar redirects to profile page",
      (WidgetTester tester) async {
    Widget offlinePage = ManageOfflineReport();
    await _pumpTestableWidget(tester, offlinePage);

    await tester.tap(find.byIcon(Icons.account_circle).first);
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byIcon(Icons.account_circle), findsWidgets);
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsNothing);
  });
}
