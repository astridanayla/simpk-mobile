import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simpk/components/custom_dropdown.dart';
import 'package:simpk/components/labeled_checkbox.dart';
import 'package:simpk/modules/project_list/screens/filter_project_page.dart';

void main() {
  Widget _wrapWithMaterialApp(Widget filterProjectPage) {
    return MaterialApp(
      home: filterProjectPage,
    );
  }

  testWidgets("Filter project page contains status filter",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    expect(find.text("Status Proyek"), findsOneWidget);
  });

  testWidgets("Filter project page contains location filter",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    expect(find.text("Lokasi Proyek"), findsOneWidget);
    expect(find.byType(TextFormField), findsOneWidget);
    expect(find.text('Lokasi'), findsOneWidget);
  });

  testWidgets("Activity type filter choices are correct",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    await tester.tap(find.text("Kendaraan"));
    await tester.pumpAndSettle();
    await tester.tap(find.text("Lainnya").first);
    await tester.pumpAndSettle();

    expect(find.text("Lainnya"), findsOneWidget);
  });

  testWidgets("Filter project page contains activity type filter",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    expect(find.text("Jenis Kegiatan"), findsOneWidget);
  });

  testWidgets("Filter project page contains percentage filter",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    expect(find.text("Persentase Proyek"), findsOneWidget);
  });

  testWidgets("Filter project page contains labeled checkbox",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    expect(find.byType(LabeledCheckbox), findsWidgets);
  });

  testWidgets("Filter project page contains apply filter button",
      (WidgetTester tester) async {
    Widget filterProjectPage = FilterProjectPage();
    await tester.pumpWidget(_wrapWithMaterialApp(filterProjectPage));

    expect(find.byType(ElevatedButton), findsOneWidget);
    expect(find.text("Terapkan Filter"), findsOneWidget);
    await tester.ensureVisible(find.byType(ElevatedButton));
    await tester.tap(find.byType(ElevatedButton));
  });

  testWidgets("Labeled checkbox is clickable", (WidgetTester tester) async {
    Widget filterProyekPage = FilterProjectPage();

    await tester.pumpWidget(_wrapWithMaterialApp(filterProyekPage));

    await tester.tap(find.text("Selesai"));
    await tester.pump();
    expect(find.byType(LabeledCheckbox), findsWidgets);
  });

  testWidgets('Filter status selesai', (tester) async {
    late Map<String, dynamic> result;
    final mockObserver = _TestObserver((popped) {
      result = popped as Map<String, dynamic>;
    });

    await tester.pumpWidget(
      MaterialApp(
        home: FilterProjectPage(),
        navigatorObservers: [mockObserver],
      ),
    );

    await tester.tap(find.byType(LabeledCheckbox).first);
    await tester.pump();

    await tester.tap(find.byType(ElevatedButton));

    expect(result, {
      'status': ['selesai']
    });
  });

  testWidgets('Filter location', (tester) async {
    late Map<String, dynamic> result;
    final mockObserver = _TestObserver((popped) {
      result = popped as Map<String, dynamic>;
    });

    await tester.pumpWidget(
      MaterialApp(
        home: FilterProjectPage(),
        navigatorObservers: [mockObserver],
      ),
    );

    final dummyLocation = 'Bandung';

    await tester.ensureVisible(find.byType(TextFormField));
    await tester.enterText(find.byType(TextFormField), dummyLocation);
    await tester.pump();

    await tester.tap(find.byType(ElevatedButton));

    expect(result, {'daerah': dummyLocation});
  });

  testWidgets('filter jenis kegiatan kendaraan', (tester) async {
    late Map<String, dynamic> result;
    final mockObserver = _TestObserver((popped) {
      result = popped as Map<String, dynamic>;
    });

    await tester.pumpWidget(
      MaterialApp(
        home: FilterProjectPage(),
        navigatorObservers: [mockObserver],
      ),
    );

    await tester.tap(find.byType(CustomDropdown).last);
    await tester.pump();
    await tester.tap(find.text('Kendaraan').last);
    await tester.pump();

    await tester.tap(find.byType(ElevatedButton));

    expect(result, {
      'jenis_kegiatan': 'kendaraan',
    });
  });

  testWidgets('filter persentase', (tester) async {
    late Map<String, dynamic> result;
    final mockObserver = _TestObserver((popped) {
      result = popped as Map<String, dynamic>;
    });

    await tester.pumpWidget(
      MaterialApp(
        home: FilterProjectPage(),
        navigatorObservers: [mockObserver],
      ),
    );

    await tester.ensureVisible(find.byType(RangeSlider));

    final Offset topLeft = tester.getTopLeft(find.byType(RangeSlider));
    final Offset bottomRight = tester.getBottomRight(find.byType(RangeSlider));
    final Offset middle = (topLeft + bottomRight) / 2;

    // Drag the end thumb towards the center.
    await tester.dragFrom(topLeft, middle);
    await tester.pump();

    await tester.tap(find.byType(ElevatedButton));

    expect(result, {
      'persentase': '50-100',
    });
  });

  testWidgets('initial filter param in constructor', (tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: FilterProjectPage(filterParam: {
          'status': ['berjalan'],
          'jenis_kegiatan': 'lainnya',
          'persentase': '20-70',
        }),
      ),
    );

    final secondCheckbox =
        await tester.firstWidget(find.byType(Checkbox).at(1)) as Checkbox;
    expect(secondCheckbox.value, true);

    final activityTypeDropdown =
        await tester.firstWidget(find.byType(CustomDropdown)) as CustomDropdown;
    expect(activityTypeDropdown.value, 'Lainnya');

    final rangeSlider =
        await tester.firstWidget(find.byType(RangeSlider)) as RangeSlider;
    expect(rangeSlider.values.start, 20);
    expect(rangeSlider.values.end, 70);
  });
}

class _TestObserver extends NavigatorObserver {
  _TestObserver(this.onPop);

  final void Function(dynamic result) onPop;

  @override
  void didPop(Route route, Route? previousRoute) async {
    final result = await route.popped;
    if (result is Map) onPop(result);
  }
}
