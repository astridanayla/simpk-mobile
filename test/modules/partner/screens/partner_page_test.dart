import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/partner_card.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/partner/models/Partner.dart';
import 'package:simpk/modules/partner/screens/partner_page.dart';
import 'package:simpk/services/api_provider.dart';

import '../../home/screens/home_page_test.mocks.dart';
import '../../monitoring/screens/monitoring_tab_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: Scaffold(body: child),
        theme: theme,
      ),
    ),
  );
}

@GenerateMocks([APIProvider])
void main() {
  late ReportService mockReportRepository;

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  setUp(() {
    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets("Partner page : static contents are correct",
      (WidgetTester tester) async {
    Widget partnerPage = PartnerPage();
    await _pumpTestableWidget(tester, partnerPage);

    // expect(find.byType(SearchBar), findsOneWidget);
    expect(find.text("Daftar Mitra"), findsOneWidget);
  });

  testWidgets(
      "Partner page : cloud icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    Widget partnerPage = PartnerPage();
    await _pumpTestableWidget(tester, partnerPage);

    await tester.tap(find.byIcon(Icons.cloud_upload));
    await tester.pumpAndSettle();

    expect(find.text("Daftar Laporan Offline"), findsOneWidget);
  });

  testWidgets(
      "Partner page : profile icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    final dummyStatusCountsResponse = {
      "status_code": 200,
      "message": "Success",
      "data": {
        "selesai": 5,
        "berjalan": 3,
      }
    };
    late MockHomeAPIProvider mockHomeAPIProvider = MockHomeAPIProvider();
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerLazySingleton<HomeAPIProvider>(() => mockHomeAPIProvider);

    Widget partnerPage = PartnerPage();
    await _pumpTestableWidget(tester, partnerPage);

    await tester.tap(find.byIcon(Icons.account_circle).at(0));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byIcon(Icons.account_circle), findsWidgets);
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsNothing);

    GetIt.I.unregister(instance: mockHomeAPIProvider);
  });

  testWidgets("Partner page : displays partner cards",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    PartnerPage partnerPage = PartnerPage();
    String partnerName = 'Dummy Partner Name';
    String partnerEmail = 'Dummy Partner Email';
    String address = 'Dummy Address';
    String token = sharedPreferences.getString("token")!;
    partnerPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllPartners(token)).thenAnswer((_) async => [
          Partner(
              id: 1, name: partnerName, email: partnerEmail, address: address)
        ]);
    await _pumpTestableWidget(tester, partnerPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.byType(PartnerCard), findsOneWidget);
    expect(find.text(partnerName), findsOneWidget);
  });

  testWidgets("Partner page : displays not found partner",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    PartnerPage partnerPage = PartnerPage();
    String token = sharedPreferences.getString("token")!;
    partnerPage.state.api = mockAPIProvider;

    when(mockAPIProvider.getAllPartners(token)).thenAnswer((_) async => []);
    await _pumpTestableWidget(tester, partnerPage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text('Daftar Mitra'), findsOneWidget);
    expect(find.text('Hasil pencarian tidak ditemukan'), findsOneWidget);
  });
}
