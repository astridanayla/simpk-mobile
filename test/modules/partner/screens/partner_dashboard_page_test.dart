import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simpk/components/search_bar.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/partner/models/Partner.dart';
import 'package:simpk/modules/partner/screens/partner_dashboard_page.dart';
import 'package:simpk/modules/project_list/models/project_data.dart';
import 'package:simpk/modules/project_list/screens/project_list.dart';
import 'package:simpk/services/api_provider.dart';

import '../../project_list/screens/project_list_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester
      .pumpWidget(MaterialApp(home: Scaffold(body: child), theme: theme));
}

@GenerateMocks([APIProvider])
void main() {
  final Partner dummyPartner = Partner(
      id: 1, name: "Rumah Zakat", email: "zakat@gmail.com", address: "Depok");

  Map<String, dynamic> mockProject1 = {
    "nama_proyek": "Nama proyek",
    "jenis_kegiatan": "Kesehatan",
    "nama_penerima": "Nama Penerima",
    "daerah": "Jakarta, DKI Jakarta",
    "alokasi_dana": "1.00",
    "mitra": null,
    "status": "Berjalan",
    "project_id": 1,
    "koordinat_lokasi": "",
    "deskripsi": "Deskripsi",
    "progres": 0,
    "kesimpulan_monitoring": "Belum ada",
    "total_realisasi": null,
    "jumlah_laporan": 0
  };

  final mockProjectData1 = ProjectData(
    reports: [mockProject1],
    statusCode: 200,
    total: 1,
    nItems: 1,
    errorMessage: '',
  );

  final mockProjectDataNoItems = ProjectData(
    reports: [],
    statusCode: 200,
    total: 0,
    nItems: 0,
    errorMessage: '',
  );

  late APIProvider mockApiProvider;

  setUp(() {
    mockApiProvider = MockAPIProvider();
    Map<String, dynamic> mockQueryMitra = {"mitra": "1"};
    Map<String, dynamic> mockQueryStatus = {
      "status": ["selesai"]
    };
    for (int i = 1; i <= 5; i++) {
      when(mockApiProvider.getProjects(i, '', null))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, '', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'Nama', {}))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'test', {}))
          .thenAnswer((_) async => mockProjectDataNoItems);
      when(mockApiProvider.getProjects(i, '', mockQueryMitra))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, 'search input', mockQueryMitra))
          .thenAnswer((_) async => mockProjectData1);
      when(mockApiProvider.getProjects(i, '', mockQueryStatus))
          .thenAnswer((_) async => mockProjectData1);
    }
    GetIt.I.registerSingleton<APIProvider>(mockApiProvider);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockApiProvider);
  });

  testWidgets("Partner dashboard : contents are correct",
      (WidgetTester tester) async {
    Widget partnerDashboardPage = PartnerDashboardPage(partner: dummyPartner);
    await _pumpTestableWidget(tester, partnerDashboardPage);

    expect(find.text("MITRA"), findsOneWidget);
    expect(find.text("Rumah Zakat"), findsOneWidget);

    expect(find.byType(ProjectList), findsOneWidget);
    expect(find.byType(SearchBar), findsOneWidget);
    expect(find.text("Cari Proyek"), findsOneWidget);
    expect(find.text("Daftar Proyek yang Ditugaskan"), findsOneWidget);
  });

  testWidgets("Partner dashboard : project list contents are correct",
      (WidgetTester tester) async {
    Widget partnerDashboardPage = PartnerDashboardPage(partner: dummyPartner);
    await _pumpTestableWidget(tester, partnerDashboardPage);

    expect(find.text("MITRA"), findsOneWidget);
    expect(find.text("Rumah Zakat"), findsOneWidget);

    // expect(find.byType(GhostButton), findsOneWidget);
    // expect(find.text("Filter"), findsOneWidget);
    // expect(find.byIcon(Icons.filter_list), findsOneWidget);
  });

  testWidgets("Partner dashboard : displays search result",
      (WidgetTester tester) async {
    Widget partnerDashboardPage = PartnerDashboardPage(partner: dummyPartner);
    await _pumpTestableWidget(tester, partnerDashboardPage);

    await tester.enterText(find.byType(TextFormField).at(0), "search input");
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();
    expect(find.text('Hasil Pencarian'), findsOneWidget);

    await tester.enterText(find.byType(TextFormField).at(0), "");
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();
    expect(find.text('Hasil Pencarian'), findsNothing);
  });

  testWidgets('Snackbar appears when copy icon is clicked',
      (WidgetTester tester) async {
    const String snackbarMessage = "Teks berhasil disalin!";
    Widget partnerDashboardPage = PartnerDashboardPage(partner: dummyPartner);
    await _pumpTestableWidget(tester, partnerDashboardPage);

    expect(find.text(snackbarMessage), findsNothing);

    Finder copy_icon = find.byIcon(Icons.copy).at(0);
    await tester.ensureVisible(copy_icon);
    await tester.tap(copy_icon);
    await tester.pump();
    expect(find.text(snackbarMessage), findsOneWidget);
  });
}
