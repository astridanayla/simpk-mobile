import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/components/pie_graph.dart';
import 'package:simpk/components/project_card.dart';
import 'package:simpk/config/theme.dart';
import 'package:simpk/modules/authentication/blocs/auth_bloc.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/modules/monitoring/models/ActivityAnalysis.dart';
import 'package:simpk/modules/monitoring/models/Report.dart';
import 'package:simpk/modules/monitoring/models/activity_item.dart';
import 'package:simpk/modules/monitoring/models/realization_item.dart';
import 'package:simpk/modules/monitoring/services/report_service.dart';
import 'package:simpk/modules/project/models/Project.dart';
import 'package:simpk/services/api_provider.dart';

import '../../monitoring/screens/monitoring_tab_test.mocks.dart';
import 'home_page_test.mocks.dart';

Future _pumpTestableWidget(WidgetTester tester, Widget child) async {
  ThemeData theme = appTheme();
  await tester.pumpWidget(
    BlocProvider(
      create: (_) => AuthBloc.withRole('Internal'),
      child: MaterialApp(
        home: Scaffold(body: child),
        theme: theme,
      ),
    ),
  );
}

@GenerateMocks([APIProvider, HomeAPIProvider])
void main() {
  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "selesai": 5,
      "berjalan": 3,
    }
  };

  final _dummyActivityItem = ActivityItem(
    name: 'Merk',
    approved: 'Honda',
    realization: 'Yamaha',
    difference: true,
    differenceDescription:
        'Beda merk, karena kekuatan mesinnya lebih bagus Honda',
  );

  final _dummyRealizationItem = RealizationItem(
    name: 'Merk',
    approved: 1000000,
    realization: 1000000,
    difference: false,
  );
  final _dummyActivityAnalysis = ActivityAnalisis()
    ..problem = 'Nama Masalah'
    ..recommendation = 'Penyelesaian Masalah';

  final mockReport = Report()
    ..id = 1
    ..activityType = 'Sarana dan Prasarana'
    ..activityName = "Judul"
    ..executionMethod = 'Cek Fisik oleh Bidang Kemaslahatan'
    ..monitoringDate = DateTime(2021, 10, 24)
    ..monitoringPeriod = 'Periode 1'
    ..activityStage = 'Pembayaran Pembelian Kendaraan / Mesin / Peralatan'
    ..activityItems = [_dummyActivityItem, _dummyActivityItem]
    ..realizationItems = [_dummyRealizationItem, _dummyRealizationItem]
    ..activityAnalysis = [_dummyActivityAnalysis, _dummyActivityAnalysis]
    ..conclusion =
        'Progres Kegiatan Kemaslahatan sesuai dengan persetujuan dari BPKH ' +
            'dan dapat dilanjutkan ke tahap selanjutnya'
    ..modifiedDate = DateTime(2021, 4, 5);
  ;

  late MockHomeAPIProvider mockHomeAPIProvider;
  late MockAPIProvider mockAPIProvider;
  late ReportService mockReportRepository;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    mockAPIProvider = MockAPIProvider();
    GetIt.I.registerSingleton<APIProvider>(mockAPIProvider);

    mockReportRepository = MockReportRepository();
    when(mockReportRepository.queryAllOffline())
        .thenAnswer((_) async => [mockReport]);
    when(mockReportRepository.queryMonitoringPeriodByProjectId(1))
        .thenAnswer((_) async => []);
    GetIt.I.registerSingleton(mockReportRepository);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
    GetIt.I.unregister(instance: mockAPIProvider);
    GetIt.I.unregister(instance: mockReportRepository);
  });

  testWidgets("Home page : contents are correct", (WidgetTester tester) async {
    Widget homePage = HomePage();
    await _pumpTestableWidget(tester, homePage);

    expect(find.byType(TextFormField), findsOneWidget);
  });

  testWidgets(
      "Home page : cloud icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    Widget homePage = HomePage();
    await _pumpTestableWidget(tester, homePage);

    await tester.tap(find.byIcon(Icons.cloud_upload));
    await tester.pumpAndSettle();

    expect(find.text("Daftar Laporan Offline"), findsOneWidget);
  });

  testWidgets(
      "Home page : profile icon on appbar redirects to corresponding page",
      (WidgetTester tester) async {
    Widget homePage = HomePage();
    await _pumpTestableWidget(tester, homePage);

    await tester.tap(find.byIcon(Icons.account_circle).at(0));
    await tester.pump(Duration(milliseconds: 1000));

    expect(find.byIcon(Icons.account_circle), findsWidgets);
    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsOneWidget);
  });

  testWidgets("Home page : contains monitoring statistics section",
      (WidgetTester tester) async {
    Widget homePage = HomePage();
    await _pumpTestableWidget(tester, homePage);
    await tester.pumpAndSettle();

    expect(find.text("Ringkasan Monitoring & Evaluasi"), findsOneWidget);
    expect(find.byType(PieGraph), findsOneWidget);
    // expect(find.text("Belum Mulai"), findsOneWidget);
    expect(find.text("Berjalan"), findsOneWidget);
    expect(find.text("Selesai"), findsOneWidget);
  });

  testWidgets("Home page : project search displays corresponding project",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    HomePage homePage = HomePage();
    String projectName = 'Dummy Project Name';
    String projectStatus = 'Dummy Project Status';
    String partnerName = 'Dummy Partner Name';
    String activityType = 'Dummy Activity Type';
    String region = 'Dummy Region';
    String coordinate = 'Dummy Coordinate';
    String description = "Dummy Description";
    double progress = 0.5;
    String token = sharedPreferences.getString("token")!;
    String searchValue = "Name";

    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => [
              Project(
                id: 1,
                name: projectName,
                status: projectStatus,
                partner: partnerName,
                activityType: activityType,
                region: region,
                coordinate: coordinate,
                description: description,
                progress: progress,
                allocation: 100000000,
              ),
            ]);
    await _pumpTestableWidget(tester, homePage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), searchValue);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text('Hasil Pencarian'), findsOneWidget);
    expect(find.byType(ProjectCard), findsWidgets);
    expect(find.text(projectName), findsOneWidget);
    expect(find.text(region), findsOneWidget);

    await tester.enterText(find.byType(TextFormField).at(0), "");
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text('Hasil Pencarian'), findsNothing);
  });

  testWidgets("Home page : project search displays not found",
      (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({"token": "someToken"});
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    HomePage homePage = HomePage();
    String token = sharedPreferences.getString("token")!;
    String searchValue = "Name";

    when(mockAPIProvider.searchProjects(token, searchValue))
        .thenAnswer((_) async => []);
    await _pumpTestableWidget(tester, homePage);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    await tester.enterText(find.byType(TextFormField).at(0), searchValue);
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pumpAndSettle(Duration(milliseconds: 1000));

    expect(find.text('Hasil Pencarian'), findsOneWidget);
    expect(find.text('Hasil pencarian tidak ditemukan'), findsOneWidget);
  });
}
