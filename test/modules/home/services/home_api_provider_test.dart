import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';

void main() {
  SharedPreferences.setMockInitialValues({"token": ""});
  final Map dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  test("getProjectStatusCounts", () async {
    final mockClient = MockClient((request) async {
      return Response(json.encode(dummyStatusCountsResponse), 200);
    });

    final apiProvider = HomeAPIProvider(client: mockClient);

    final response = await apiProvider.getProjectStatusCounts();

    expect(response["status_code"], 200);
    expect(response["message"], "Success");
    expect(response["data"], {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    });
  });

  test("getUniqueRegions", () async {
    final dummyUniqueRegionsResponse = {
      "status_code": 200,
      "message": "Success",
      "data": [
        "Indonesia",
        "Jawa Barat",
        "DKI Jakarta",
      ]
    };

    final mockClient = MockClient((request) async {
      return Response(json.encode(dummyUniqueRegionsResponse), 200);
    });

    final apiProvider = HomeAPIProvider(client: mockClient);

    final response = await apiProvider.getUniqueRegions();

    expect(response["status_code"], 200);
    expect(response["message"], "Success");
    expect(response["data"], [
      "Indonesia",
      "Jawa Barat",
      "DKI Jakarta",
    ]);
  });

  test("getProjectStatusCountsByRegion", () async {
    final mockClient = MockClient((request) async {
      return Response(json.encode(dummyStatusCountsResponse), 200);
    });

    final apiProvider = HomeAPIProvider(client: mockClient);

    final response =
        await apiProvider.getProjectStatusCountsByRegion("Indonesia");

    expect(response["status_code"], 200);
    expect(response["message"], "Success");
    expect(response["data"], {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    });
  });
}
