import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simpk/main.dart';
import 'package:simpk/modules/home/screens/home_page.dart';
import 'package:simpk/modules/home/services/home_api_provider.dart';
import 'package:simpk/services/api_provider.dart';
import 'package:simpk/services/network_info.dart';
import 'modules/home/screens/home_page_test.mocks.dart';
import 'modules/monitoring/services/report_repository_test.mocks.dart';

void main() {
  final dummyUniqueRegionsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": [
      "Indonesia",
      "DKI Jakarta",
    ]
  };

  final dummyStatusCountsResponse = {
    "status_code": 200,
    "message": "Success",
    "data": {
      "total": 10,
      "selesai": 5,
      "berjalan": 3,
      "belum_mulai": 2,
    }
  };

  late MockHomeAPIProvider mockHomeAPIProvider;
  late MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockHomeAPIProvider = new MockHomeAPIProvider();
    when(mockHomeAPIProvider.getUniqueRegions())
        .thenAnswer((_) async => dummyUniqueRegionsResponse);
    when(mockHomeAPIProvider.getProjectStatusCounts())
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("Indonesia"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    when(mockHomeAPIProvider.getProjectStatusCountsByRegion("DKI Jakarta"))
        .thenAnswer((_) async => dummyStatusCountsResponse);
    GetIt.I.registerSingleton<HomeAPIProvider>(mockHomeAPIProvider);

    mockNetworkInfo = MockNetworkInfo();
    when(mockNetworkInfo.isConnected()).thenAnswer((_) async => true);
    GetIt.I.registerSingleton<NetworkInfo>(mockNetworkInfo);
  });

  tearDown(() {
    GetIt.I.unregister(instance: mockHomeAPIProvider);
    GetIt.I.unregister(instance: mockNetworkInfo);
  });

  testWidgets(
      "Wait for async is logged in check shows circular progress indicator",
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });

  SharedPreferences.setMockInitialValues({
    "token": "asd",
    "login_time": DateTime.now().toString(),
  });

  testWidgets("If logged in, then it should show Home Page",
      (WidgetTester tester) async {
    MockAPIProvider mockAPIProvider = MockAPIProvider();
    GetIt.I.registerSingleton<APIProvider>(mockAPIProvider);

    await tester.pumpWidget(MyApp());
    await tester.pumpAndSettle();
    expect(find.byType(HomePage), findsOneWidget);

    GetIt.I.unregister(instance: mockAPIProvider);
  });
}
