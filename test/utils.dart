/// Returns the generic class of this function.
/// Useful for testing on [find.byType] for a class with generic.
///
/// ## Sample Code
///
/// ```dart
/// find.byType(typeOf&lt;TextFormField&lt;String&gt;&gt;(), findsOneWidget);
/// ```
Type typeOf<T>() => T;
