# Sistem Informasi Manajemen Program Kemaslahatan - Mobile App

### Deskripsi Proyek
Sistem Informasi Manajemen Program Kemaslahatan merupakan mobile application yang berperan sebagai wadah bagi seluruh proses bisnis dalam realisasi Program Kemaslahatan BPKH. Program Kemaslahatan merupakan sebuah program filantropi yang bersumber dari nilai manfaat Dana Abadi Umat.

### Pipeline Status
| Branch  | Pipeline                                                                                                                   | Coverage                                                                                                                   |
| ------- | -------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| master  | ![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-mobile/badges/master/pipeline.svg)  | ![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-mobile/badges/master/coverage.svg)  |
| staging | ![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-mobile/badges/staging/pipeline.svg) | ![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-mobile/badges/staging/coverage.svg) |

### Project Structure
```
lib
├── assets/     : images, fonts, or static assets
├── components/ : Reusable components / widgets
├── config/     : configuration like themes or routes
├── main.dart 
└── modules/    : application modules
    └── <module_name>/
        ├── assets/     : Module only assets
        ├── bloc/       : Bussines logic files
        ├── componets/  : Module screens components
        ├── models/     : Data models / classes
        ├── services/   : Service and API calls
        ├── screens/    : Module screens
        └── more...
```

### Instalasi
Untuk instalasi proyek ini, jalankan command berikut:
```
git clone https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-mobile.git
git fetch --all
```
Lalu, build aplikasi dengan menjalankan:
```
flutter build apk
```

### Kontributor
- Abdurrafi Arief
- Astrida Nayla
- Mohamad Rifqy Zulkarnaen
- Muhammad Oktoluqman Fakhrianto
- Salman Ahmad Nurhoiriza

